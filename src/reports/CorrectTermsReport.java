package reports;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import reportDataObjects.CorrectTerms;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import net.sf.jasperreports.engine.JRException;

public class CorrectTermsReport
{

	/**
	 * Creates reports in PDF, CSV and XLS formats then stores them to a
	 * specified path. All the params are passed from the FormController
	 * 
	 * @param filePath
	 *            the path to store the generated reports
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 */
	public static void createReports(String filePath,
			HashMap<String, Object> model, String reportName)
	{
		try
		{
			System.out.println("Creatng the Report...");
			createPDF(filePath, model, reportName);
			createCSV(filePath, model, reportName);
			createXLS(filePath, model, reportName);
		} catch (Exception e)
		{

		}

	}

	/**
	 * Creates XLS report
	 * 
	 * @param filePath
	 *            the path to store a generated XLS report
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 * @throws IOException
	 */
	@SuppressWarnings(
	{ "unchecked", "deprecation" })
	private static void createXLS(String filePath,
			HashMap<String, Object> model, String reportName)
			throws IOException
	{
		// TODO Auto-generated method stub
		if (reportName.equals("CorrectTermsReport"))
		{
			System.out.println("Creatng XLS Report...");
			ArrayList<CorrectTerms> sb = (ArrayList<CorrectTerms>) model
					.get("CorrectTermsList");
			String title = "Corrected Terms Report";
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet(title);
			// default column width
			sheet.setDefaultColumnWidth(30);
			// style for headers
			HSSFCellStyle style = workbook.createCellStyle();
			// text align center
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			// colour
			style.setFillForegroundColor(HSSFColor.YELLOW.index);
			style.setFillBackgroundColor(HSSFColor.WHITE.index);
			style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			// border width
			style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			style.setBorderRight(HSSFCellStyle.BORDER_THIN);
			style.setBorderTop(HSSFCellStyle.BORDER_THIN);
			// font
			HSSFFont font = workbook.createFont();
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			font.setColor(HSSFColor.VIOLET.index);
			font.setFontHeightInPoints((short) 12);
			// set font
			style.setFont(font);

			// body style
			HSSFCellStyle bodyStyle = workbook.createCellStyle();
			bodyStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
			// font
			HSSFFont bodyFont = workbook.createFont();
			bodyFont.setFontHeightInPoints((short) 12);
			// set font
			bodyStyle.setFont(bodyFont);
			int index = 0;
			HSSFRow row = sheet.createRow(index);
			HSSFCell cell = null;
			cell = row.createCell(0);
			cell.setCellStyle(style);
			HSSFRichTextString text = new HSSFRichTextString(
					(String) model.get("reportName"));
			cell.setCellValue(text);
			index++;
			HSSFRow dateRow = sheet.createRow(index);
			cell = dateRow.createCell(0);
			cell.setCellStyle(bodyStyle);
			HSSFRichTextString date = new HSSFRichTextString(
					(String) model.get("date"));
			cell.setCellValue(date);
			index++;
			HSSFRow tableHeaderRow = sheet.createRow(index);
			String[] tableHeaders = new String[]
			{ "searchterm", "correctterm", "request" };
			for (short m = 0; m < tableHeaders.length; m++)
			{
				cell = tableHeaderRow.createCell(m);
				cell.setCellStyle(style);
				HSSFRichTextString tabelHeader = new HSSFRichTextString(
						tableHeaders[m]);
				cell.setCellValue(tabelHeader);
			}
			index++;
			for (int n = 0; n < sb.size(); n++)
			{
				HSSFRow tableRow = sheet.createRow(index);
				cell = tableRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue((String) sb.get(n).getSearchterm());
				cell = tableRow.createCell(1);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue((String) sb.get(n).getCorrectto());
				cell = tableRow.createCell(2);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue((Integer) sb.get(n).getRequest());
				index++;
			}
			HSSFRow totalRow = sheet.createRow(index);
			cell = totalRow.createCell(0);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue("Total Request");
			cell = totalRow.createCell(1);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue((Integer) model.get("totalRequests"));

			FileOutputStream fos = new FileOutputStream(filePath
					+ "pages/reports/" + reportName + ".xls");
			workbook.write(fos);
			fos.close();
		}
	}

	/**
	 * Create the PDF form report.
	 * 
	 * @param filePath
	 *            the path which the pdf report locates.
	 * @param model
	 *            the model which is used for creating the report.
	 * @param reportName
	 *            the name of the report.
	 * @throws JRException
	 * @throws DocumentException
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private static void createPDF(String filePath,
			HashMap<String, Object> model, String reportName)
			throws JRException, DocumentException, MalformedURLException,
			IOException
	{
		if (reportName.equals("CorrectTermsReport"))
		{
			System.out.println("Creatng PDF Report...");
			ArrayList<CorrectTerms> sbh = (ArrayList<CorrectTerms>) model
					.get("CorrectTermsList");
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(filePath
					+ "pages/reports/" + reportName + ".pdf"));
			document.open();
			Paragraph title = new Paragraph("Auto Correct Terms\n\n",
					FontFactory.getFont(FontFactory.TIMES_ROMAN, 20, Font.BOLD));
			title.setAlignment(Element.ALIGN_CENTER);
			document.add(title);
			document.add(new Paragraph((String) model.get("reportName")));
			document.add(new Paragraph((String) model.get("date") + "\n\n"));
			Image image = Image.getInstance(filePath
					+ "pages/temp/barchart.jpg");
			image.scaleAbsolute(410, 210);
			float[] widths =
			{ 0.2f, 0.3f, 0.3f, 0.2f };
			PdfPTable table = new PdfPTable(widths);
			PdfPCell chart = new PdfPCell(image);
			chart.setColspan(4);
			chart.setBorder(Rectangle.NO_BORDER);
			table.addCell(chart);
			PdfPCell id = new PdfPCell(new Paragraph("#"));
			PdfPCell searchTerms = new PdfPCell(new Paragraph(
					(String) model.get("th1")));
			PdfPCell correctTerms = new PdfPCell(new Paragraph(
					(String) model.get("th2")));
			PdfPCell requests = new PdfPCell(new Paragraph(
					(String) model.get("th3")));
			id.setBorder(Rectangle.NO_BORDER);
			searchTerms.setBorder(Rectangle.NO_BORDER);
			correctTerms.setBorder(Rectangle.NO_BORDER);
			requests.setBorder(Rectangle.NO_BORDER);
			table.addCell(id);
			table.addCell(searchTerms);
			table.addCell(correctTerms);
			table.addCell(requests);
			for (int i = 0; i < sbh.size(); i++)
			{
				int j = i + 1;
				table.addCell(String.valueOf(j));
				table.addCell(String.valueOf(sbh.get(i).getSearchterm()));
				table.addCell(String.valueOf(sbh.get(i).getCorrectto()));
				table.addCell(String.valueOf(sbh.get(i).getRequest()));
			}
			table.addCell("Total Requests");
			table.addCell(String.valueOf(model.get("totalRequests")));
			document.add(table);
			document.close();
		}
	}

	/**
	 * Create the CSV form report.
	 * 
	 * @param filePath
	 *            the path of the report file where the it locates.
	 * @param model
	 *            the model which is used for creating the report.
	 * @param reportName
	 *            the name of the report.
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private static void createCSV(String filePath,
			HashMap<String, Object> model, String reportName)
			throws IOException
	{
		// TODO Auto-generated method stub
		if (reportName.equals("CorrectTermsReport"))
		{
			System.out.println("Creatng CSV Report...");
			ArrayList<CorrectTerms> sbh = (ArrayList<CorrectTerms>) model
					.get("CorrectTermsList");
			FileWriter writer = new FileWriter(filePath + "pages/reports/"
					+ reportName + ".csv");
			writer.append((String) model.get("reportName"));
			writer.append("\n");
			writer.append((String) model.get("date"));
			writer.append("\n");
			writer.append((String) model.get("th1"));
			writer.append(",");
			writer.append((String) model.get("th2"));
			writer.append(",");
			writer.append((String) model.get("th3"));
			writer.append("\n");
			for (int m = 0; m < sbh.size(); m++)
			{
				writer.append(String.valueOf(sbh.get(m).getSearchterm()));
				writer.append(",");
				writer.append(String.valueOf(sbh.get(m).getCorrectto()));
				writer.append(",");
				writer.append(String.valueOf(sbh.get(m).getRequest()));
				writer.append("\n");
			}
			writer.append("Total Requests");
			writer.append(",");
			writer.append(String.valueOf(model.get("totalRequests")));
			writer.append("\n");
			writer.flush();
			writer.close();
		}
	}
}
