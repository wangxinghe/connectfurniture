package reports;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import reportDataObjects.RequestByUser_All;
import reportDataObjects.RequestByUser_SearchOrNavigate;
import reportDataObjects.RequestByUser_SearchAndNavigate;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import net.sf.jasperreports.engine.JRException;

/**
 * This class is used to generate PDF, CSV, XLS reports
 * 
 */
public class RequestByUserReport
{
	/**
	 * Creates reports in PDF, CSV and XLS formats then stores them to a
	 * specified path. All the params are passed from the FormController
	 * 
	 * @param filePath
	 *            the path to store the generated reports
	 * @param model
	 *            a hashmap contains all the report data
	 * @throws IOException
	 */
	public static void createReports(String filePath,
			HashMap<String, Object> model)
	{
		try
		{
			createPDF(filePath, model);
			createCSV(filePath, model);
			createXLS(filePath, model);
		} catch (Exception e)
		{
		}

	}

	/**
	 * Creates XLS report
	 * 
	 * @param filePath
	 *            the path to store a generated XLS report
	 * @param model
	 *            a hashmap contains all the report data
	 * @throws IOException
	 */
	@SuppressWarnings(
	{ "unchecked", "deprecation" })
	private static void createXLS(String filePath, HashMap<String, Object> model)
			throws IOException
	{
		if (model.get("savedReportName").equals(
				"RequestByUserReport_SearchOnly")
				|| model.get("savedReportName").equals(
						"RequestByUserReport_NavigateOnly"))
		{
			ArrayList<RequestByUser_SearchOrNavigate> tmpArrayList = (ArrayList<RequestByUser_SearchOrNavigate>) model
					.get("ArrayListResultIndex");
			if (tmpArrayList != null)
			{
				String title = (String) model.get("savedReportName");
				HSSFWorkbook workbook = new HSSFWorkbook();
				HSSFSheet sheet = workbook.createSheet(title);
				// default column width
				sheet.setDefaultColumnWidth(30);
				// style for headers
				HSSFCellStyle style = workbook.createCellStyle();
				// text align center
				style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
				// colour
				style.setFillForegroundColor(HSSFColor.YELLOW.index);
				style.setFillBackgroundColor(HSSFColor.WHITE.index);
				style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				// border width
				style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
				style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
				style.setBorderRight(HSSFCellStyle.BORDER_THIN);
				style.setBorderTop(HSSFCellStyle.BORDER_THIN);
				// font
				HSSFFont font = workbook.createFont();
				font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				font.setColor(HSSFColor.VIOLET.index);
				font.setFontHeightInPoints((short) 12);
				// set font
				style.setFont(font);

				// body style
				HSSFCellStyle bodyStyle = workbook.createCellStyle();
				bodyStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				// font
				HSSFFont bodyFont = workbook.createFont();
				bodyFont.setFontHeightInPoints((short) 12);
				// set font
				bodyStyle.setFont(bodyFont);
				int index = 0;
				HSSFRow row = sheet.createRow(index);
				HSSFCell cell = null;
				cell = row.createCell(0);
				cell.setCellStyle(style);
				HSSFRichTextString text = new HSSFRichTextString(
						(String) model.get("savedReportName"));
				cell.setCellValue(text);
				index++;
				HSSFRow dateRow = sheet.createRow(index);
				cell = dateRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				HSSFRichTextString date = new HSSFRichTextString(
						(String) model.get("date"));
				cell.setCellValue(date);
				index++;
				HSSFRow tableHeaderRow = sheet.createRow(index);
				String[] tableHeaders = new String[]
				{ "Number Of Requests", "Number Of Users" };
				for (short m = 0; m < tableHeaders.length; m++)
				{
					cell = tableHeaderRow.createCell(m);
					cell.setCellStyle(style);
					HSSFRichTextString tabelHeader = new HSSFRichTextString(
							tableHeaders[m]);
					cell.setCellValue(tabelHeader);
				}
				index++;
				for (int n = 0; n < tmpArrayList.size(); n++)
				{
					HSSFRow tableRow = sheet.createRow(index);
					cell = tableRow.createCell(0);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue((Integer) tmpArrayList.get(n)
							.getNumberOfRequests());
					cell = tableRow.createCell(1);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue((Integer) tmpArrayList.get(n)
							.getNumberOfUsers());
					index++;
				}

				FileOutputStream fos = new FileOutputStream(filePath
						+ "pages/reports/" + model.get("savedReportName")
						+ ".xls");
				workbook.write(fos);
				fos.close();
			}
		} else if (model.get("savedReportName").equals(
				"RequestByUserReport_SearchAndNavigate"))
		{
			ArrayList<RequestByUser_SearchAndNavigate> tmpArrayList = (ArrayList<RequestByUser_SearchAndNavigate>) model
					.get("ArrayListSearchAndNavigateResultIndex");
			if (tmpArrayList != null)
			{
				String title = (String) model.get("savedReportName");
				HSSFWorkbook workbook = new HSSFWorkbook();
				HSSFSheet sheet = workbook.createSheet(title);
				// default column width
				sheet.setDefaultColumnWidth(30);
				// style for headers
				HSSFCellStyle style = workbook.createCellStyle();
				// text align center
				style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
				// colour
				style.setFillForegroundColor(HSSFColor.YELLOW.index);
				style.setFillBackgroundColor(HSSFColor.WHITE.index);
				style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				// border width
				style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
				style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
				style.setBorderRight(HSSFCellStyle.BORDER_THIN);
				style.setBorderTop(HSSFCellStyle.BORDER_THIN);
				// font
				HSSFFont font = workbook.createFont();
				font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				font.setColor(HSSFColor.VIOLET.index);
				font.setFontHeightInPoints((short) 12);
				// set font
				style.setFont(font);

				// body style
				HSSFCellStyle bodyStyle = workbook.createCellStyle();
				bodyStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				// font
				HSSFFont bodyFont = workbook.createFont();
				bodyFont.setFontHeightInPoints((short) 12);
				// set font
				bodyStyle.setFont(bodyFont);
				int index = 0;
				HSSFRow row = sheet.createRow(index);
				HSSFCell cell = null;
				cell = row.createCell(0);
				cell.setCellStyle(style);
				HSSFRichTextString text = new HSSFRichTextString(
						(String) model.get("savedReportName"));
				cell.setCellValue(text);
				index++;
				HSSFRow dateRow = sheet.createRow(index);
				cell = dateRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				HSSFRichTextString date = new HSSFRichTextString(
						(String) model.get("date"));
				cell.setCellValue(date);
				index++;
				HSSFRow tableHeaderRow = sheet.createRow(index);
				String[] tableHeaders = new String[]
				{ "Number Of Searches", "Number Of Navigates",
						"Number Of Users" };
				for (short m = 0; m < tableHeaders.length; m++)
				{
					cell = tableHeaderRow.createCell(m);
					cell.setCellStyle(style);
					HSSFRichTextString tabelHeader = new HSSFRichTextString(
							tableHeaders[m]);
					cell.setCellValue(tabelHeader);
				}
				index++;
				for (int i = 0; i < tmpArrayList.size(); i++)
				{
					if (tmpArrayList.get(i).getNumberOfNavigates() != 0
							&& tmpArrayList.get(i).getNumberOfUsers() != 0)
					{
						HSSFRow tableRow = sheet.createRow(index);
						cell = tableRow.createCell(0);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue((Integer) tmpArrayList.get(i)
								.getNumberOfSearches());
						cell = tableRow.createCell(1);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue((Integer) tmpArrayList.get(i)
								.getNumberOfNavigates());
						cell = tableRow.createCell(2);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue((Integer) tmpArrayList.get(i)
								.getNumberOfUsers());
						index++;
					}
				}

				FileOutputStream fos = new FileOutputStream(filePath
						+ "pages/reports/" + model.get("savedReportName")
						+ ".xls");
				workbook.write(fos);
				fos.close();
			}
		} else if (model.get("savedReportName").equals(
				"RequestByUserReport_All"))
		{
			ArrayList<RequestByUser_All> tmpArrayList = (ArrayList<RequestByUser_All>) model
					.get("ArrayListAllRequestIndex");
			if (tmpArrayList != null)
			{
				String title = (String) model.get("savedReportName");
				HSSFWorkbook workbook = new HSSFWorkbook();
				HSSFSheet sheet = workbook.createSheet(title);
				// default column width
				sheet.setDefaultColumnWidth(30);
				// style for headers
				HSSFCellStyle style = workbook.createCellStyle();
				// text align center
				style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
				// colour
				style.setFillForegroundColor(HSSFColor.YELLOW.index);
				style.setFillBackgroundColor(HSSFColor.WHITE.index);
				style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				// border width
				style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
				style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
				style.setBorderRight(HSSFCellStyle.BORDER_THIN);
				style.setBorderTop(HSSFCellStyle.BORDER_THIN);
				// font
				HSSFFont font = workbook.createFont();
				font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				font.setColor(HSSFColor.VIOLET.index);
				font.setFontHeightInPoints((short) 12);
				// set font
				style.setFont(font);

				// body style
				HSSFCellStyle bodyStyle = workbook.createCellStyle();
				bodyStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				// font
				HSSFFont bodyFont = workbook.createFont();
				bodyFont.setFontHeightInPoints((short) 12);
				// set font
				bodyStyle.setFont(bodyFont);
				int index = 0;
				HSSFRow row = sheet.createRow(index);
				HSSFCell cell = null;
				cell = row.createCell(0);
				cell.setCellStyle(style);
				HSSFRichTextString text = new HSSFRichTextString(
						(String) model.get("savedReportName"));
				cell.setCellValue(text);
				index++;
				HSSFRow dateRow = sheet.createRow(index);
				cell = dateRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				HSSFRichTextString date = new HSSFRichTextString(
						(String) model.get("date"));
				cell.setCellValue(date);
				index++;
				HSSFRow tableHeaderRow = sheet.createRow(index);
				String[] tableHeaders = new String[]
				{ "Number Of Searches", "Number Of Navigates",
						"Number Of Users" };
				for (short m = 0; m < tableHeaders.length; m++)
				{
					cell = tableHeaderRow.createCell(m);
					cell.setCellStyle(style);
					HSSFRichTextString tabelHeader = new HSSFRichTextString(
							tableHeaders[m]);
					cell.setCellValue(tabelHeader);
				}
				index++;
				for (int i = 0; i < tmpArrayList.size(); i++)
				{
					if (tmpArrayList.get(i).getNumberOfNavigates() == 0
							&& tmpArrayList.get(i).getNumberOfSearches() == 0
							&& tmpArrayList.get(i).getNumberOfUsers() == 0)
						continue;
					HSSFRow tableRow = sheet.createRow(index);
					cell = tableRow.createCell(0);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue((Integer) tmpArrayList.get(i)
							.getNumberOfSearches());
					cell = tableRow.createCell(1);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue((Integer) tmpArrayList.get(i)
							.getNumberOfNavigates());
					cell = tableRow.createCell(2);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue((Integer) tmpArrayList.get(i)
							.getNumberOfUsers());
					index++;
				}

				FileOutputStream fos = new FileOutputStream(filePath
						+ "pages/reports/" + model.get("savedReportName")
						+ ".xls");
				workbook.write(fos);
				fos.close();
			}
		}
	}

	/**
	 * Creates CSV report
	 * 
	 * @param filePath
	 *            the path to store a generated CSV report
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private static void createCSV(String filePath, HashMap<String, Object> model)
			throws IOException
	{
		if (model.get("savedReportName").equals(
				"RequestByUserReport_SearchOnly")
				|| model.get("savedReportName").equals(
						"RequestByUserReport_NavigateOnly"))
		{
			ArrayList<RequestByUser_SearchOrNavigate> tmpArrayList = (ArrayList<RequestByUser_SearchOrNavigate>) model
					.get("ArrayListResultIndex");
			if (tmpArrayList != null)
			{
				FileWriter writer = new FileWriter(filePath + "pages/reports/"
						+ model.get("savedReportName") + ".csv");
				writer.append((String) model.get("savedReportName"));
				writer.append("\n");
				writer.append((String) model.get("date"));
				writer.append("\n");
				writer.append("Number Of Requests");
				writer.append(",");
				writer.append("Number Of Users");
				writer.append("\n");
				int size = tmpArrayList.size();
				for (int i = 0; i < size; i++)
				{
					writer.append(String.valueOf(tmpArrayList.get(i)
							.getNumberOfRequests()));
					writer.append(",");
					writer.append(String.valueOf(tmpArrayList.get(i)
							.getNumberOfUsers()));
					writer.append(",");
					writer.append("\n");
				}
				writer.flush();
				writer.close();
			}
		} else if (model.get("savedReportName").equals(
				"RequestByUserReport_SearchAndNavigate"))
		{
			ArrayList<RequestByUser_SearchAndNavigate> tmpArrayList = (ArrayList<RequestByUser_SearchAndNavigate>) model
					.get("ArrayListSearchAndNavigateResultIndex");
			if (tmpArrayList != null)
			{
				FileWriter writer = new FileWriter(filePath + "pages/reports/"
						+ model.get("savedReportName") + ".csv");
				writer.append((String) model.get("savedReportName"));
				writer.append("\n");
				writer.append((String) model.get("date"));
				writer.append("\n");
				writer.append("Number Of Searches");
				writer.append(",");
				writer.append("Number Of Navigates");
				writer.append(",");
				writer.append("Number Of Users");
				writer.append("\n");
				int size = tmpArrayList.size();
				for (int i = 0; i < size; i++)
				{
					if (tmpArrayList.get(i).getNumberOfNavigates() != 0
							&& tmpArrayList.get(i).getNumberOfUsers() != 0)
					{
						writer.append(String.valueOf(tmpArrayList.get(i)
								.getNumberOfSearches()));
						writer.append(",");
						writer.append(String.valueOf(tmpArrayList.get(i)
								.getNumberOfNavigates()));
						writer.append(",");
						writer.append(String.valueOf(tmpArrayList.get(i)
								.getNumberOfUsers()));
						writer.append(",");
						writer.append("\n");
					}
				}
				writer.flush();
				writer.close();
			}
		} else if (model.get("savedReportName").equals(
				"RequestByUserReport_All"))
		{
			ArrayList<RequestByUser_All> tmpArrayList = (ArrayList<RequestByUser_All>) model
					.get("ArrayListAllRequestIndex");
			if (tmpArrayList != null)
			{
				FileWriter writer = new FileWriter(filePath + "pages/reports/"
						+ model.get("savedReportName") + ".csv");
				writer.append((String) model.get("savedReportName"));
				writer.append("\n");
				writer.append((String) model.get("date"));
				writer.append("\n");
				writer.append("Number Of Searches");
				writer.append(",");
				writer.append("Number Of Navigates");
				writer.append(",");
				writer.append("Number Of Users");
				writer.append("\n");
				int size = tmpArrayList.size();
				for (int i = 0; i < size; i++)
				{
					if (tmpArrayList.get(i).getNumberOfNavigates() == 0
							&& tmpArrayList.get(i).getNumberOfSearches() == 0
							&& tmpArrayList.get(i).getNumberOfUsers() == 0)
						continue;
					writer.append(String.valueOf(tmpArrayList.get(i)
							.getNumberOfSearches()));
					writer.append(",");
					writer.append(String.valueOf(tmpArrayList.get(i)
							.getNumberOfNavigates()));
					writer.append(",");
					writer.append(String.valueOf(tmpArrayList.get(i)
							.getNumberOfUsers()));
					writer.append(",");
					writer.append("\n");
				}
				writer.flush();
				writer.close();
			}
		}
	}

	/**
	 * Creates PDF report
	 * 
	 * @param filePath
	 *            the path to store a generated PDF report
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 * @throws JRException
	 * @throws DocumentException
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private static void createPDF(String filePath, HashMap<String, Object> model)
			throws JRException, DocumentException, MalformedURLException,
			IOException
	{
		if (model.get("savedReportName").equals(
				"RequestByUserReport_SearchOnly")
				|| model.get("savedReportName").equals(
						"RequestByUserReport_NavigateOnly"))
		{
			ArrayList<RequestByUser_SearchOrNavigate> tmpArrayList = (ArrayList<RequestByUser_SearchOrNavigate>) model
					.get("ArrayListResultIndex");
			if (tmpArrayList != null)
			{
				Document document = new Document();
				PdfWriter.getInstance(document, new FileOutputStream(filePath
						+ "pages/reports/" + model.get("savedReportName")
						+ ".pdf"));
				document.open();
				Paragraph title = new Paragraph(
						(String) model.get("savedReportName") + "\n\n",
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 20,
								Font.BOLD));
				title.setAlignment(Element.ALIGN_CENTER);
				document.add(title);
				document.add(new Paragraph((String) model.get("date") + "\n\n"));
				document.add(new Paragraph((String) model.get("date") + "\n\n"));
				float[] widths =
				{ 0.2f, 0.2f };
				PdfPTable table = new PdfPTable(widths);

				PdfPCell numberofRequests = new PdfPCell(new Paragraph(
						"Number Of Requests"));
				PdfPCell numberofUsers = new PdfPCell(new Paragraph(
						"Number Of Users"));

				numberofRequests.setBorder(Rectangle.NO_BORDER);
				numberofUsers.setBorder(Rectangle.NO_BORDER);

				table.addCell(numberofRequests);
				table.addCell(numberofUsers);
				for (int i = 0; i < tmpArrayList.size(); i++)
				{
					table.addCell(String.valueOf(tmpArrayList.get(i)
							.getNumberOfRequests()));
					table.addCell(String.valueOf(tmpArrayList.get(i)
							.getNumberOfUsers()));
				}
				document.add(table);
				document.close();
			}
		} else if (model.get("savedReportName").equals(
				"RequestByUserReport_SearchAndNavigate"))
		{
			ArrayList<RequestByUser_SearchAndNavigate> tmpArrayList = (ArrayList<RequestByUser_SearchAndNavigate>) model
					.get("ArrayListSearchAndNavigateResultIndex");
			if (tmpArrayList != null)
			{
				Document document = new Document();
				PdfWriter.getInstance(document, new FileOutputStream(filePath
						+ "pages/reports/" + model.get("savedReportName")
						+ ".pdf"));
				document.open();
				Paragraph title = new Paragraph(
						(String) model.get("savedReportName") + "\n\n",
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 20,
								Font.BOLD));
				title.setAlignment(Element.ALIGN_CENTER);
				document.add(title);
				document.add(new Paragraph((String) model.get("date") + "\n\n"));
				document.add(new Paragraph((String) model.get("date") + "\n\n"));
				float[] widths =
				{ 0.2f, 0.2f, 0.2f };
				PdfPTable table = new PdfPTable(widths);

				PdfPCell numberOfSearches = new PdfPCell(new Paragraph(
						"Number Of Searches"));
				PdfPCell numberOfNavigates = new PdfPCell(new Paragraph(
						"Number Of Navigates"));
				PdfPCell numberOfUsers = new PdfPCell(new Paragraph(
						"Number Of Users"));

				numberOfSearches.setBorder(Rectangle.NO_BORDER);
				numberOfNavigates.setBorder(Rectangle.NO_BORDER);
				numberOfUsers.setBorder(Rectangle.NO_BORDER);

				table.addCell(numberOfSearches);
				table.addCell(numberOfNavigates);
				table.addCell(numberOfUsers);
				for (int i = 0; i < tmpArrayList.size(); i++)
				{
					if (tmpArrayList.get(i).getNumberOfNavigates() != 0
							&& tmpArrayList.get(i).getNumberOfUsers() != 0)
					{
						table.addCell(String.valueOf(tmpArrayList.get(i)
								.getNumberOfSearches()));
						table.addCell(String.valueOf(tmpArrayList.get(i)
								.getNumberOfNavigates()));
						table.addCell(String.valueOf(tmpArrayList.get(i)
								.getNumberOfUsers()));
					}
				}
				document.add(table);
				document.close();
			}
		} else if (model.get("savedReportName").equals(
				"RequestByUserReport_All"))
		{
			ArrayList<RequestByUser_All> tmpArrayList = (ArrayList<RequestByUser_All>) model
					.get("ArrayListAllRequestIndex");
			if (tmpArrayList != null)
			{
				Document document = new Document();
				PdfWriter.getInstance(document, new FileOutputStream(filePath
						+ "pages/reports/" + model.get("savedReportName")
						+ ".pdf"));
				document.open();
				Paragraph title = new Paragraph(
						(String) model.get("savedReportName") + "\n\n",
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 20,
								Font.BOLD));
				title.setAlignment(Element.ALIGN_CENTER);
				document.add(title);
				document.add(new Paragraph((String) model
						.get("savedReportName")));
				document.add(new Paragraph((String) model.get("date") + "\n\n"));
				float[] widths =
				{ 0.2f, 0.2f, 0.2f };
				PdfPTable table = new PdfPTable(widths);

				PdfPCell numberOfSearches = new PdfPCell(new Paragraph(
						"Number Of Searches"));
				PdfPCell numberOfNavigates = new PdfPCell(new Paragraph(
						"Number Of Navigates"));
				PdfPCell numberOfUsers = new PdfPCell(new Paragraph(
						"Number Of Users"));

				numberOfSearches.setBorder(Rectangle.NO_BORDER);
				numberOfNavigates.setBorder(Rectangle.NO_BORDER);
				numberOfUsers.setBorder(Rectangle.NO_BORDER);

				table.addCell(numberOfSearches);
				table.addCell(numberOfNavigates);
				table.addCell(numberOfUsers);

				for (int i = 0; i < tmpArrayList.size(); i++)
				{
					if (tmpArrayList.get(i).getNumberOfNavigates() == 0
							&& tmpArrayList.get(i).getNumberOfSearches() == 0
							&& tmpArrayList.get(i).getNumberOfUsers() == 0)
						continue;
					table.addCell(String.valueOf(tmpArrayList.get(i)
							.getNumberOfSearches().toString()));
					table.addCell(String.valueOf(tmpArrayList.get(i)
							.getNumberOfNavigates().toString()));
					table.addCell(String.valueOf(tmpArrayList.get(i)
							.getNumberOfUsers().toString()));
				}

				document.add(table);
				document.close();
			}
		}
	}
}
