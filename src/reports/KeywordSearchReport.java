package reports;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.sf.jasperreports.engine.JRException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import others.Unit;
import reportDataObjects.KeywordSearch;
import reportDataObjects.KeywordSearchSummary;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class KeywordSearchReport
{
	/**
	 * Creates reports in PDF, CSV and XLS formats then stores them to a
	 * specified path. All the params are passed from the FormController
	 * 
	 * @param filePath
	 *            the path to store the generated reports
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 */
	public static void createReportsSummary(String filePath,
			HashMap<String, Object> model, String reportName)
	{
		try
		{
			createPDFSummary(filePath, model, reportName);
			createCSVSummary(filePath, model, reportName);
			createXLSSummary(filePath, model, reportName);
		} catch (Exception e)
		{

		}

	}

	/**
	 * Creates reports in PDF, CSV and XLS formats then stores them to a
	 * specified path. All the params are passed from the FormController
	 * 
	 * @param filePath
	 *            the path to store the generated reports
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 */
	public static void createReports(String filePath,
			HashMap<String, Object> model, String reportName)
	{
		try
		{
			// createPDF(filePath, model,reportName);
			createCSV(filePath, model, reportName);
			createXLS(filePath, model, reportName);
		} catch (Exception e)
		{

		}

	}

	/**
	 * Creates XLS report
	 * 
	 * @param filePath
	 *            the path to store a generated XLS report
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 * @throws IOException
	 */
	@SuppressWarnings(
	{ "unchecked", "deprecation" })
	private static void createXLSSummary(String filePath,
			HashMap<String, Object> model, String reportName)
			throws IOException
	{
		List<KeywordSearchSummary> keywordSearchArrayList = (List<KeywordSearchSummary>) model
				.get("keywordSearchArrayListIndex");
		List<KeywordSearch> keywordSearchList = (List<KeywordSearch>) model
				.get("keywordSearchList");
		String title = "Keyword Search Summary Report";
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet(title);
		// default column width
		sheet.setDefaultColumnWidth(30);
		// style for headers
		HSSFCellStyle style = workbook.createCellStyle();
		// text align center
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		// colour
		style.setFillForegroundColor(HSSFColor.YELLOW.index);
		style.setFillBackgroundColor(HSSFColor.WHITE.index);
		style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		// border width
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		style.setBorderRight(HSSFCellStyle.BORDER_THIN);
		style.setBorderTop(HSSFCellStyle.BORDER_THIN);
		// font
		HSSFFont font = workbook.createFont();
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		font.setColor(HSSFColor.VIOLET.index);
		font.setFontHeightInPoints((short) 12);
		// set font
		style.setFont(font);

		// body style
		HSSFCellStyle bodyStyle = workbook.createCellStyle();
		bodyStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		// font
		HSSFFont bodyFont = workbook.createFont();
		bodyFont.setFontHeightInPoints((short) 12);
		// set font
		bodyStyle.setFont(bodyFont);
		int index = 0;
		HSSFRow row = sheet.createRow(index);
		HSSFCell cell = null;
		cell = row.createCell(0);
		cell.setCellStyle(style);
		HSSFRichTextString text = new HSSFRichTextString(
				(String) model.get("reportName"));
		cell.setCellValue(text);
		index++;
		HSSFRow dateRow = sheet.createRow(index);
		cell = dateRow.createCell(0);
		cell.setCellStyle(bodyStyle);
		HSSFRichTextString date = new HSSFRichTextString(
				(String) model.get("date"));
		cell.setCellValue(date);
		index++;
		HSSFRow tableHeaderRow = sheet.createRow(index);
		String[] tableHeaders = new String[]
		{ "#", "No of Records Returned", "Number of Keywords" };
		for (short m = 0; m < tableHeaders.length; m++)
		{
			cell = tableHeaderRow.createCell(m);
			cell.setCellStyle(style);
			HSSFRichTextString tabelHeader = new HSSFRichTextString(
					tableHeaders[m]);
			cell.setCellValue(tabelHeader);
		}
		index++;
		for (int n = 0; n < keywordSearchArrayList.size(); n++)
		{
			HSSFRow tableRow = sheet.createRow(index);
			cell = tableRow.createCell(0);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue(n + 1);
			cell = tableRow.createCell(1);
			cell.setCellStyle(bodyStyle);
			if (keywordSearchArrayList.get(n).getLlimit() == 0)
				cell.setCellValue("["
						+ keywordSearchArrayList.get(n).getLlimit() + "]");
			else if (keywordSearchArrayList.get(n).getLlimit() == keywordSearchArrayList
					.get(n).getHlimit())
				cell.setCellValue("["
						+ keywordSearchArrayList.get(n).getLlimit() + "+]");
			else
				cell.setCellValue("["
						+ keywordSearchArrayList.get(n).getLlimit() + " - "
						+ keywordSearchArrayList.get(n).getHlimit() + "]");
			cell = tableRow.createCell(2);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue((Integer) keywordSearchArrayList.get(n)
					.getRecords());
			index++;
		}

		cell.setCellValue("");
		cell.setCellValue("");

		HSSFRow tableHeaderRow2 = sheet.createRow(index);
		String[] tableHeaders2 = new String[]
		{ "Search Term", "Number of Results", "Number of Requests" };
		for (short m = 0; m < tableHeaders2.length; m++)
		{
			cell = tableHeaderRow2.createCell(m);
			cell.setCellStyle(style);
			HSSFRichTextString tabelHeader2 = new HSSFRichTextString(
					tableHeaders2[m]);
			cell.setCellValue(tabelHeader2);
		}
		index++;
		for (int n = 0; n < keywordSearchList.size(); n++)
		{
			HSSFRow tableRow = sheet.createRow(index);
			cell = tableRow.createCell(0);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue(keywordSearchList.get(n).getSearchterms());
			cell = tableRow.createCell(1);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue((Integer) keywordSearchList.get(n).getRecords());
			cell = tableRow.createCell(2);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue((Integer) keywordSearchList.get(n).getRequests());
			index++;
		}

		FileOutputStream fos = new FileOutputStream(filePath + "pages/reports/"
				+ reportName + ".xls");
		workbook.write(fos);
		fos.close();
	}

	/**
	 * Creates CSV report
	 * 
	 * @param filePath
	 *            the path to store a generated CSV report
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private static void createCSVSummary(String filePath,
			HashMap<String, Object> model, String reportName)
			throws IOException
	{
		List<KeywordSearchSummary> keywordSearchArrayList = (List<KeywordSearchSummary>) model
				.get("keywordSearchArrayListIndex");
		List<KeywordSearch> keywordSearchList = (List<KeywordSearch>) model
				.get("keywordSearchList");
		FileWriter writer = new FileWriter(filePath + "pages/reports/"
				+ reportName + ".csv");
		writer.append((String) model.get("reportName"));
		writer.append("\n");
		writer.append((String) model.get("date"));
		writer.append("\n");
		writer.append("#");
		writer.append(",");
		writer.append("No of Records Returned");
		writer.append(",");
		writer.append("Number of Keywords");
		writer.append("\n");
		for (int m = 0; m < keywordSearchArrayList.size(); m++)
		{
			writer.append(String.valueOf(m + 1));
			writer.append(",");
			if (keywordSearchArrayList.get(m).getLlimit() == 0)
				writer.append("[" + keywordSearchArrayList.get(m).getLlimit()
						+ "]");
			else if (keywordSearchArrayList.get(m).getLlimit() == keywordSearchArrayList
					.get(m).getHlimit())
				writer.append("[" + keywordSearchArrayList.get(m).getLlimit()
						+ "+]");
			else
				writer.append("[" + keywordSearchArrayList.get(m).getLlimit()
						+ " - " + keywordSearchArrayList.get(m).getHlimit()
						+ "]");
			// writer.append("[" + keywordSearchArrayList.get(m).getLlimit() +
			// " - " + keywordSearchArrayList.get(m).getHlimit() + "]");
			writer.append(",");
			writer.append(String.valueOf(keywordSearchArrayList.get(m)
					.getRecords()));
			writer.append("\n");
		}
		writer.append("\n");
		writer.append("\n");
		writer.append("Search Terms");
		writer.append(",");
		writer.append("Number of Results");
		writer.append(",");
		writer.append("Number of Requests");
		writer.append("\n");
		for (int m = 0; m < keywordSearchList.size(); m++)
		{
			writer.append(keywordSearchList.get(m).getSearchterms());
			writer.append(",");
			writer.append(String.valueOf(keywordSearchList.get(m).getRecords()));
			writer.append(",");
			writer.append(String
					.valueOf(keywordSearchList.get(m).getRequests()));
			writer.append("\n");
		}
		writer.flush();
		writer.close();

	}

	/**
	 * Creates PDF report
	 * 
	 * @param filePath
	 *            the path to store a generated PDF report
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 * @throws JRException
	 * @throws DocumentException
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private static void createPDFSummary(String filePath,
			HashMap<String, Object> model, String reportName)
			throws JRException, DocumentException, MalformedURLException,
			IOException
	{
		List<KeywordSearchSummary> keywordSearchArrayList = (List<KeywordSearchSummary>) model
				.get("keywordSearchArrayListIndex");
		List<KeywordSearch> keywordSearchList = (List<KeywordSearch>) model
				.get("keywordSearchList");
		Document document = new Document();
		PdfWriter.getInstance(document, new FileOutputStream(filePath
				+ "pages/reports/" + reportName + ".pdf"));
		document.open();
		Paragraph title = new Paragraph("Keyword Search Report\n\n",
				FontFactory.getFont(FontFactory.TIMES_ROMAN, 20, Font.BOLD));
		title.setAlignment(Element.ALIGN_CENTER);
		document.add(title);
		document.add(new Paragraph((String) model.get("reportName")));
		document.add(new Paragraph((String) model.get("date") + "\n\n"));
		PdfPCell number = new PdfPCell(new Paragraph("#"));
		PdfPCell RecordsUnit = new PdfPCell(new Paragraph(
				"No of Records Returned"));
		PdfPCell TotalCounts = new PdfPCell(new Paragraph("Number of Keywords"));
		number.setBorder(Rectangle.NO_BORDER);
		RecordsUnit.setBorder(Rectangle.NO_BORDER);
		TotalCounts.setBorder(Rectangle.NO_BORDER);
		float[] widths =
		{ 0.1f, 0.6f, 0.3f };
		PdfPTable table = new PdfPTable(widths);
		table.addCell(number);
		table.addCell(RecordsUnit);
		table.addCell(TotalCounts);
		for (int i = 0; i < keywordSearchArrayList.size(); i++)
		{
			table.addCell(String.valueOf(i + 1));
			if (keywordSearchArrayList.get(i).getLlimit() == 0)
				table.addCell("[" + keywordSearchArrayList.get(i).getLlimit()
						+ "]");
			else if (keywordSearchArrayList.get(i).getLlimit() == keywordSearchArrayList
					.get(i).getHlimit())
				table.addCell("[" + keywordSearchArrayList.get(i).getLlimit()
						+ "+]");
			else
				table.addCell("[" + keywordSearchArrayList.get(i).getLlimit()
						+ " - " + keywordSearchArrayList.get(i).getHlimit()
						+ "]");
			// table.addCell("[" + keywordSearchArrayList.get(i).getLlimit() +
			// " - " + keywordSearchArrayList.get(i).getHlimit() + "]");
			table.addCell(String.valueOf(keywordSearchArrayList.get(i)
					.getRecords()));
		}

		document.add(table);
		/*
		 * document.add(new Paragraph((String) model.get("Gabo")));
		 * 
		 * float[] widths2 = {0.50f,0.25f,0.25f};
		 * 
		 * PdfPCell SearchTerm = new PdfPCell(new Paragraph("Search Term"));
		 * PdfPCell Requests = new PdfPCell(new Paragraph("Number of Results"));
		 * PdfPCell Records = new PdfPCell(new Paragraph("Number of Requests"));
		 * number.setBorder(Rectangle.NO_BORDER);
		 * RecordsUnit.setBorder(Rectangle.NO_BORDER);
		 * TotalCounts.setBorder(Rectangle.NO_BORDER);
		 * 
		 * table = new PdfPTable(widths2); table.addCell(SearchTerm);
		 * table.addCell(Requests); table.addCell(Records); for(int i=0;
		 * i<keywordSearchList.size();i++){
		 * table.addCell(keywordSearchList.get(i).getSearchterms());
		 * table.addCell
		 * (String.valueOf(keywordSearchArrayList.get(i).getRecords()));
		 * table.addCell
		 * (String.valueOf(keywordSearchList.get(i).getRequests())); }
		 * document.add(table);
		 */
		document.close();
	}

	/**
	 * Creates XLS report
	 * 
	 * @param filePath
	 *            the path to store a generated XLS report
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 * @throws IOException
	 */
	@SuppressWarnings(
	{ "unchecked", "deprecation" })
	private static void createXLS(String filePath,
			HashMap<String, Object> model, String reportName)
			throws IOException
	{
		List<KeywordSearch> keywordSearchList = (List<KeywordSearch>) model
				.get("keywordSearchList");
		String title = "Keyword Search Report";
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet(title);
		// default column width
		sheet.setDefaultColumnWidth(30);
		// style for headers
		HSSFCellStyle style = workbook.createCellStyle();
		// text align center
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		// colour
		style.setFillForegroundColor(HSSFColor.YELLOW.index);
		style.setFillBackgroundColor(HSSFColor.WHITE.index);
		style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		// border width
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		style.setBorderRight(HSSFCellStyle.BORDER_THIN);
		style.setBorderTop(HSSFCellStyle.BORDER_THIN);
		// font
		HSSFFont font = workbook.createFont();
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		font.setColor(HSSFColor.VIOLET.index);
		font.setFontHeightInPoints((short) 12);
		// set font
		style.setFont(font);

		// body style
		HSSFCellStyle bodyStyle = workbook.createCellStyle();
		bodyStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		// font
		HSSFFont bodyFont = workbook.createFont();
		bodyFont.setFontHeightInPoints((short) 12);
		// set font
		bodyStyle.setFont(bodyFont);
		int index = 0;
		HSSFRow row = sheet.createRow(index);
		HSSFCell cell = null;
		cell = row.createCell(0);
		cell.setCellStyle(style);
		HSSFRichTextString text = new HSSFRichTextString(
				(String) model.get("reportName"));
		cell.setCellValue(text);
		index++;
		HSSFRow dateRow = sheet.createRow(index);
		cell = dateRow.createCell(0);
		cell.setCellStyle(bodyStyle);
		HSSFRichTextString date = new HSSFRichTextString(
				(String) model.get("date"));
		cell.setCellValue(date);
		index++;
		HSSFRow tableHeaderRow = sheet.createRow(index);
		String[] tableHeaders2 = new String[]
		{ "Search Term", "Number of Results", "Number of Requests" };
		for (short m = 0; m < tableHeaders2.length; m++)
		{
			cell = tableHeaderRow.createCell(m);
			cell.setCellStyle(style);
			HSSFRichTextString tabelHeader = new HSSFRichTextString(
					tableHeaders2[m]);
			cell.setCellValue(tabelHeader);
		}
		index++;
		for (int n = 0; n < keywordSearchList.size(); n++)
		{
			HSSFRow tableRow = sheet.createRow(index);
			cell = tableRow.createCell(0);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue(keywordSearchList.get(n).getSearchterms());
			cell = tableRow.createCell(1);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue((Integer) keywordSearchList.get(n).getRecords());
			cell = tableRow.createCell(2);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue((Integer) keywordSearchList.get(n).getRequests());
			index++;
		}

		FileOutputStream fos = new FileOutputStream(filePath + "pages/reports/"
				+ reportName + ".xls");
		workbook.write(fos);
		fos.close();
	}

	/**
	 * Creates CSV report
	 * 
	 * @param filePath
	 *            the path to store a generated CSV report
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private static void createCSV(String filePath,
			HashMap<String, Object> model, String reportName)
			throws IOException
	{
		List<KeywordSearch> keywordSearchList = (List<KeywordSearch>) model
				.get("keywordSearchList");
		FileWriter writer = new FileWriter(filePath + "pages/reports/"
				+ reportName + ".csv");
		writer.append((String) model.get("reportName"));
		writer.append("\n");
		writer.append((String) model.get("date"));
		writer.append("\n");
		writer.append("Search Terms");
		writer.append(",");
		writer.append("Number of Results");
		writer.append(",");
		writer.append("Number of Requests");
		writer.append("\n");
		for (int m = 0; m < keywordSearchList.size(); m++)
		{
			writer.append(keywordSearchList.get(m).getSearchterms());
			writer.append(",");
			writer.append(String.valueOf(keywordSearchList.get(m).getRecords()));
			writer.append(",");
			writer.append(String
					.valueOf(keywordSearchList.get(m).getRequests()));
			writer.append("\n");
		}
		writer.flush();
		writer.close();

	}

	/**
	 * Creates PDF report
	 * 
	 * @param filePath
	 *            the path to store a generated PDF report
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 * @throws JRException
	 * @throws DocumentException
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private static void createPDF(String filePath,
			HashMap<String, Object> model, String reportName)
			throws JRException, DocumentException, MalformedURLException,
			IOException
	{
		ArrayList<Unit> keywordSearchArrayList = (ArrayList<Unit>) model
				.get("keywordSearchArrayListIndex");
		Document document = new Document();
		PdfWriter.getInstance(document, new FileOutputStream(filePath
				+ "pages/reports/" + reportName + ".pdf"));
		document.open();
		Paragraph title = new Paragraph("Keyword Search Report\n\n",
				FontFactory.getFont(FontFactory.TIMES_ROMAN, 20, Font.BOLD));
		title.setAlignment(Element.ALIGN_CENTER);
		document.add(title);
		document.add(new Paragraph((String) model.get("reportName")));
		document.add(new Paragraph((String) model.get("date") + "\n\n"));
		PdfPCell number = new PdfPCell(new Paragraph("#"));
		PdfPCell RecordsUnit = new PdfPCell(new Paragraph(
				"Records Unit[Min - Max]"));
		PdfPCell TotalCounts = new PdfPCell(new Paragraph(
				"Total Counts Of Searchterms"));
		number.setBorder(Rectangle.NO_BORDER);
		RecordsUnit.setBorder(Rectangle.NO_BORDER);
		TotalCounts.setBorder(Rectangle.NO_BORDER);
		float[] widths =
		{ 0.1f, 0.6f, 0.3f };
		PdfPTable table = new PdfPTable(widths);
		table.addCell(number);
		table.addCell(RecordsUnit);
		table.addCell(TotalCounts);
		for (int i = 0; i < keywordSearchArrayList.size(); i++)
		{
			table.addCell(String.valueOf(i + 1));
			table.addCell("[" + keywordSearchArrayList.get(i).getMin() + " - "
					+ keywordSearchArrayList.get(i).getMax() + "]");
			table.addCell(String.valueOf(keywordSearchArrayList.get(i)
					.getValue()));
		}
		document.add(table);
		document.close();
	}
}
