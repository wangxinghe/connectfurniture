package reports;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import others.Unit;

import reportDataObjects.RequestByHour;
import reportDataObjects.SessionByHour;
import reportDataObjects.TopDimensionValues;
import reportDataObjects.TopKeywordsSearch;
import reportDataObjects.TopSearch;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import net.sf.jasperreports.engine.JRException;

/**
 * This class is used to generate PDF, CSV, XLS reports
 * 
 */
public class Report
{

	/**
	 * Creates reports in PDF, CSV and XLS formats then stores them to a
	 * specified path. All the params are passed from the FormController
	 * 
	 * @param filePath
	 *            the path to store the generated reports
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 */
	public static void createReports(String filePath,
			HashMap<String, Object> model, String reportName)
	{
		try
		{
			createPDF(filePath, model, reportName);
			createCSV(filePath, model, reportName);
			createXLS(filePath, model, reportName);
		} catch (Exception e)
		{

		}

	}

	/**
	 * Creates XLS report
	 * 
	 * @param filePath
	 *            the path to store a generated XLS report
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 * @throws IOException
	 */
	@SuppressWarnings(
	{ "unchecked", "deprecation" })
	private static void createXLS(String filePath,
			HashMap<String, Object> model, String reportName)
			throws IOException
	{
		// TODO Auto-generated method stub
		if (reportName.equals("TopDimSearchReport"))
		{
			ArrayList<String> dates = (ArrayList<String>) model.get("dates");
			HashMap<String, Object> topDimsSearchLists = (HashMap<String, Object>) model
					.get("topDimsSearchLists");
			String title = "Top Dimension Searches Report";
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet(title);
			// default column width
			sheet.setDefaultColumnWidth(50);
			// style for headers
			HSSFCellStyle style = workbook.createCellStyle();
			// text align center
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			// colour
			style.setFillForegroundColor(HSSFColor.YELLOW.index);
			style.setFillBackgroundColor(HSSFColor.WHITE.index);
			style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			// border width
			style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			style.setBorderRight(HSSFCellStyle.BORDER_THIN);
			style.setBorderTop(HSSFCellStyle.BORDER_THIN);
			// font
			HSSFFont font = workbook.createFont();
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			font.setColor(HSSFColor.VIOLET.index);
			font.setFontHeightInPoints((short) 12);
			// set font
			style.setFont(font);

			// body style
			HSSFCellStyle bodyStyle = workbook.createCellStyle();
			bodyStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
			// font
			HSSFFont bodyFont = workbook.createFont();
			bodyFont.setFontHeightInPoints((short) 12);
			// set font
			bodyStyle.setFont(bodyFont);
			// header
			String[] headers = new String[]
			{ (String) model.get("reportName"),
					(String) model.get("reportName1") };
			int index = 0;
			HSSFRow row = sheet.createRow(index);
			HSSFCell cell = null;
			for (short i = 0; i < headers.length; i++)
			{
				cell = row.createCell(i);
				cell.setCellStyle(style);
				HSSFRichTextString text = new HSSFRichTextString(headers[i]);
				cell.setCellValue(text);
			}
			index++;
			int i = 0;
			for (String key : topDimsSearchLists.keySet())
			{
				HSSFRow dateRow = sheet.createRow(index);
				cell = dateRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				HSSFRichTextString text = new HSSFRichTextString(key + " : "
						+ dates.get(i));
				cell.setCellValue(text);
				index++;
				HSSFRow tableHeaderRow = sheet.createRow(index);
				String[] tableHeaders = new String[]
				{ "#", "Dimensions", "Requests" };
				for (short m = 0; m < tableHeaders.length; m++)
				{
					cell = tableHeaderRow.createCell(m);
					cell.setCellStyle(style);
					HSSFRichTextString tabelHeader = new HSSFRichTextString(
							tableHeaders[m]);
					cell.setCellValue(tabelHeader);
				}
				index++;
				List<TopSearch> tsl = (List<TopSearch>) topDimsSearchLists
						.get(key);
				for (int n = 0; n < tsl.size(); n++)
				{
					HSSFRow tableRow = sheet.createRow(index);
					cell = tableRow.createCell(0);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue(n + 1);
					cell = tableRow.createCell(1);
					cell.setCellStyle(bodyStyle);
					HSSFRichTextString column1 = new HSSFRichTextString(tsl
							.get(n).getAllDimensions());
					cell.setCellValue(column1);
					cell = tableRow.createCell(2);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue(tsl.get(n).getCount());
					index++;
				}
				i++;
			}
			FileOutputStream fos = new FileOutputStream(filePath
					+ "pages/reports/" + reportName + ".xls");
			workbook.write(fos);
			fos.close();
		} else if (reportName.equals("RequestByTypeReport"))
		{

			HashMap<String, Object> requestByTypeList = (HashMap<String, Object>) model
					.get("requestByTypeList");
			String title = "Request By Type Report";
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet(title);
			// default column width
			sheet.setDefaultColumnWidth(30);
			// style for headers
			HSSFCellStyle style = workbook.createCellStyle();
			// text align center
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			// colour
			style.setFillForegroundColor(HSSFColor.YELLOW.index);
			style.setFillBackgroundColor(HSSFColor.WHITE.index);
			style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			// border width
			style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			style.setBorderRight(HSSFCellStyle.BORDER_THIN);
			style.setBorderTop(HSSFCellStyle.BORDER_THIN);
			// font
			HSSFFont font = workbook.createFont();
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			font.setColor(HSSFColor.VIOLET.index);
			font.setFontHeightInPoints((short) 12);
			// set font
			style.setFont(font);

			// body style
			HSSFCellStyle bodyStyle = workbook.createCellStyle();
			bodyStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
			// font
			HSSFFont bodyFont = workbook.createFont();
			bodyFont.setFontHeightInPoints((short) 12);
			// set font
			bodyStyle.setFont(bodyFont);
			int index = 0;
			HSSFRow row = sheet.createRow(index);
			HSSFCell cell = null;
			cell = row.createCell(0);
			cell.setCellStyle(style);
			HSSFRichTextString text = new HSSFRichTextString(
					(String) model.get("reportName"));
			cell.setCellValue(text);
			index++;
			HSSFRow dateRow = sheet.createRow(index);
			cell = dateRow.createCell(0);
			cell.setCellStyle(bodyStyle);
			HSSFRichTextString date = new HSSFRichTextString(
					(String) model.get("date"));
			cell.setCellValue(date);
			index++;
			HSSFRow tableHeaderRow = sheet.createRow(index);
			String[] tableHeaders = new String[]
			{ "Type Of Request", "Requests" };
			for (short m = 0; m < tableHeaders.length; m++)
			{
				cell = tableHeaderRow.createCell(m);
				cell.setCellStyle(style);
				HSSFRichTextString tabelHeader = new HSSFRichTextString(
						tableHeaders[m]);
				cell.setCellValue(tabelHeader);
			}
			index++;

			for (String key : requestByTypeList.keySet())
			{
				HSSFRow tableRow = sheet.createRow(index);
				cell = tableRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue(key);
				cell = tableRow.createCell(1);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue((Integer) requestByTypeList.get(key));
				index++;
			}
			FileOutputStream fos = new FileOutputStream(filePath
					+ "pages/reports/" + reportName + ".xls");
			workbook.write(fos);
			fos.close();

		} else if (reportName.equals("RequestByHourReport"))
		{
			ArrayList<RequestByHour> rbh = (ArrayList<RequestByHour>) model
					.get("requestByHourList");
			ArrayList<RequestByHour> rbh1 = (ArrayList<RequestByHour>) model
					.get("requestByHourList1");
			if (rbh1 == null)
			{
				String title = "Request By Hour Report";
				HSSFWorkbook workbook = new HSSFWorkbook();
				HSSFSheet sheet = workbook.createSheet(title);
				// default column width
				sheet.setDefaultColumnWidth(30);
				// style for headers
				HSSFCellStyle style = workbook.createCellStyle();
				// text align center
				style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
				// colour
				style.setFillForegroundColor(HSSFColor.YELLOW.index);
				style.setFillBackgroundColor(HSSFColor.WHITE.index);
				style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				// border width
				style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
				style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
				style.setBorderRight(HSSFCellStyle.BORDER_THIN);
				style.setBorderTop(HSSFCellStyle.BORDER_THIN);
				// font
				HSSFFont font = workbook.createFont();
				font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				font.setColor(HSSFColor.VIOLET.index);
				font.setFontHeightInPoints((short) 12);
				// set font
				style.setFont(font);

				// body style
				HSSFCellStyle bodyStyle = workbook.createCellStyle();
				bodyStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				// font
				HSSFFont bodyFont = workbook.createFont();
				bodyFont.setFontHeightInPoints((short) 12);
				// set font
				bodyStyle.setFont(bodyFont);
				int index = 0;
				HSSFRow row = sheet.createRow(index);
				HSSFCell cell = null;
				cell = row.createCell(0);
				cell.setCellStyle(style);
				HSSFRichTextString text = new HSSFRichTextString(
						(String) model.get("reportName"));
				cell.setCellValue(text);
				index++;
				HSSFRow dateRow = sheet.createRow(index);
				cell = dateRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				HSSFRichTextString date = new HSSFRichTextString(
						(String) model.get("date"));
				cell.setCellValue(date);
				index++;
				HSSFRow tableHeaderRow = sheet.createRow(index);
				String[] tableHeaders = new String[]
				{ "Hours", "Requests" };
				for (short m = 0; m < tableHeaders.length; m++)
				{
					cell = tableHeaderRow.createCell(m);
					cell.setCellStyle(style);
					HSSFRichTextString tabelHeader = new HSSFRichTextString(
							tableHeaders[m]);
					cell.setCellValue(tabelHeader);
				}
				index++;
				for (int n = 0; n < rbh.size(); n++)
				{
					HSSFRow tableRow = sheet.createRow(index);
					cell = tableRow.createCell(0);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue((Integer) rbh.get(n).getHour());
					cell = tableRow.createCell(1);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue((Integer) rbh.get(n).getRequests());
					index++;
				}
				HSSFRow totalRow = sheet.createRow(index);
				cell = totalRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue("Total Requests");
				cell = totalRow.createCell(1);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue((Integer) model.get("totalRequests"));

				FileOutputStream fos = new FileOutputStream(filePath
						+ "pages/reports/" + reportName + ".xls");
				workbook.write(fos);
				fos.close();
			} else
			{
				String title = "Request By Hour Report";
				HSSFWorkbook workbook = new HSSFWorkbook();
				HSSFSheet sheet = workbook.createSheet(title);
				// default column width
				sheet.setDefaultColumnWidth(30);
				// style for headers
				HSSFCellStyle style = workbook.createCellStyle();
				// text align center
				style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
				// colour
				style.setFillForegroundColor(HSSFColor.YELLOW.index);
				style.setFillBackgroundColor(HSSFColor.WHITE.index);
				style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				// border width
				style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
				style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
				style.setBorderRight(HSSFCellStyle.BORDER_THIN);
				style.setBorderTop(HSSFCellStyle.BORDER_THIN);
				// font
				HSSFFont font = workbook.createFont();
				font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				font.setColor(HSSFColor.VIOLET.index);
				font.setFontHeightInPoints((short) 12);
				// set font
				style.setFont(font);

				// body style
				HSSFCellStyle bodyStyle = workbook.createCellStyle();
				bodyStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				// font
				HSSFFont bodyFont = workbook.createFont();
				bodyFont.setFontHeightInPoints((short) 12);
				// set font
				bodyStyle.setFont(bodyFont);
				int index = 0;
				HSSFRow row = sheet.createRow(index);
				HSSFCell cell = null;
				cell = row.createCell(0);
				cell.setCellStyle(style);
				HSSFRichTextString text = new HSSFRichTextString(
						(String) model.get("reportName"));
				cell.setCellValue(text);
				index++;
				HSSFRow dateRow = sheet.createRow(index);
				cell = dateRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				HSSFRichTextString date = new HSSFRichTextString(
						(String) model.get("date"));
				cell.setCellValue(date);
				index++;
				HSSFRow tableHeaderRow = sheet.createRow(index);
				String[] tableHeaders = new String[]
				{ "1st Hours", "1st Requests", "2nd Hours", "2nd Requests" };
				for (short m = 0; m < tableHeaders.length; m++)
				{
					cell = tableHeaderRow.createCell(m);
					cell.setCellStyle(style);
					HSSFRichTextString tabelHeader = new HSSFRichTextString(
							tableHeaders[m]);
					cell.setCellValue(tabelHeader);
				}
				index++;

				int size = 0;
				if (rbh.size() > rbh1.size())
				{
					size = rbh.size();
				} else
				{
					size = rbh1.size();
				}
				for (int i = 0; i < size; i++)
				{
					HSSFRow tableRow = sheet.createRow(index);
					if (rbh.size() > i)
					{
						cell = tableRow.createCell(0);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue((Integer) rbh.get(i).getHour());
						cell = tableRow.createCell(1);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue((Integer) rbh.get(i).getRequests());
					} else
					{
						cell = tableRow.createCell(0);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue(" ");
						cell = tableRow.createCell(1);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue(" ");
					}
					if (rbh1.size() > i)
					{
						cell = tableRow.createCell(2);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue((Integer) rbh1.get(i).getHour());
						cell = tableRow.createCell(3);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue((Integer) rbh1.get(i).getRequests());
					} else
					{
						cell = tableRow.createCell(2);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue(" ");
						cell = tableRow.createCell(3);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue(" ");
					}
					index++;
				}

				HSSFRow totalRow = sheet.createRow(index);
				cell = totalRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue("Total Requests");
				cell = totalRow.createCell(1);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue((Integer) model.get("totalRequests"));
				cell = totalRow.createCell(2);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue("Total Requests");
				cell = totalRow.createCell(3);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue((Integer) model.get("totalRequests1"));

				FileOutputStream fos = new FileOutputStream(filePath
						+ "pages/reports/" + reportName + ".xls");
				workbook.write(fos);
				fos.close();
			}
		} else if (reportName.equals("SessionByHourReport"))
		{
			ArrayList<SessionByHour> sbh = (ArrayList<SessionByHour>) model
					.get("sessionByHourList");
			ArrayList<SessionByHour> sbh1 = (ArrayList<SessionByHour>) model
					.get("sessionByHourList1");
			if (sbh1 == null)
			{
				String title = "Session By Hour Report";
				HSSFWorkbook workbook = new HSSFWorkbook();
				HSSFSheet sheet = workbook.createSheet(title);
				// default column width
				sheet.setDefaultColumnWidth(30);
				// style for headers
				HSSFCellStyle style = workbook.createCellStyle();
				// text align center
				style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
				// colour
				style.setFillForegroundColor(HSSFColor.YELLOW.index);
				style.setFillBackgroundColor(HSSFColor.WHITE.index);
				style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				// border width
				style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
				style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
				style.setBorderRight(HSSFCellStyle.BORDER_THIN);
				style.setBorderTop(HSSFCellStyle.BORDER_THIN);
				// font
				HSSFFont font = workbook.createFont();
				font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				font.setColor(HSSFColor.VIOLET.index);
				font.setFontHeightInPoints((short) 12);
				// set font
				style.setFont(font);

				// body style
				HSSFCellStyle bodyStyle = workbook.createCellStyle();
				bodyStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				// font
				HSSFFont bodyFont = workbook.createFont();
				bodyFont.setFontHeightInPoints((short) 12);
				// set font
				bodyStyle.setFont(bodyFont);
				int index = 0;
				HSSFRow row = sheet.createRow(index);
				HSSFCell cell = null;
				cell = row.createCell(0);
				cell.setCellStyle(style);
				HSSFRichTextString text = new HSSFRichTextString(
						(String) model.get("reportName"));
				cell.setCellValue(text);
				index++;
				HSSFRow dateRow = sheet.createRow(index);
				cell = dateRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				HSSFRichTextString date = new HSSFRichTextString(
						(String) model.get("date"));
				cell.setCellValue(date);
				index++;
				HSSFRow tableHeaderRow = sheet.createRow(index);
				String[] tableHeaders = new String[]
				{ "Hours", "Sessions" };
				for (short m = 0; m < tableHeaders.length; m++)
				{
					cell = tableHeaderRow.createCell(m);
					cell.setCellStyle(style);
					HSSFRichTextString tabelHeader = new HSSFRichTextString(
							tableHeaders[m]);
					cell.setCellValue(tabelHeader);
				}
				index++;
				for (int n = 0; n < sbh.size(); n++)
				{
					HSSFRow tableRow = sheet.createRow(index);
					cell = tableRow.createCell(0);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue((Integer) sbh.get(n).getHour());
					cell = tableRow.createCell(1);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue((Integer) sbh.get(n).getSessions());
					index++;
				}
				HSSFRow totalRow = sheet.createRow(index);
				cell = totalRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue("Total Sessions");
				cell = totalRow.createCell(1);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue((Integer) model.get("totalSessions"));

				FileOutputStream fos = new FileOutputStream(filePath
						+ "pages/reports/" + reportName + ".xls");
				workbook.write(fos);
				fos.close();
			} else
			{
				String title = "Session By Hour Report";
				HSSFWorkbook workbook = new HSSFWorkbook();
				HSSFSheet sheet = workbook.createSheet(title);
				// default column width
				sheet.setDefaultColumnWidth(30);
				// style for headers
				HSSFCellStyle style = workbook.createCellStyle();
				// text align center
				style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
				// colour
				style.setFillForegroundColor(HSSFColor.YELLOW.index);
				style.setFillBackgroundColor(HSSFColor.WHITE.index);
				style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				// border width
				style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
				style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
				style.setBorderRight(HSSFCellStyle.BORDER_THIN);
				style.setBorderTop(HSSFCellStyle.BORDER_THIN);
				// font
				HSSFFont font = workbook.createFont();
				font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				font.setColor(HSSFColor.VIOLET.index);
				font.setFontHeightInPoints((short) 12);
				// set font
				style.setFont(font);

				// body style
				HSSFCellStyle bodyStyle = workbook.createCellStyle();
				bodyStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				// font
				HSSFFont bodyFont = workbook.createFont();
				bodyFont.setFontHeightInPoints((short) 12);
				// set font
				bodyStyle.setFont(bodyFont);
				int index = 0;
				HSSFRow row = sheet.createRow(index);
				HSSFCell cell = null;
				cell = row.createCell(0);
				cell.setCellStyle(style);
				HSSFRichTextString text = new HSSFRichTextString(
						(String) model.get("reportName"));
				cell.setCellValue(text);
				index++;
				HSSFRow dateRow = sheet.createRow(index);
				cell = dateRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				HSSFRichTextString date = new HSSFRichTextString(
						(String) model.get("date"));
				cell.setCellValue(date);
				index++;
				HSSFRow tableHeaderRow = sheet.createRow(index);
				String[] tableHeaders = new String[]
				{ "1st Hours", "1st Sessions", "2nd Hours", "2nd Sessions" };
				for (short m = 0; m < tableHeaders.length; m++)
				{
					cell = tableHeaderRow.createCell(m);
					cell.setCellStyle(style);
					HSSFRichTextString tabelHeader = new HSSFRichTextString(
							tableHeaders[m]);
					cell.setCellValue(tabelHeader);
				}
				index++;

				int size = 0;
				if (sbh.size() > sbh1.size())
				{
					size = sbh.size();
				} else
				{
					size = sbh1.size();
				}
				for (int i = 0; i < size; i++)
				{
					HSSFRow tableRow = sheet.createRow(index);
					if (sbh.size() > i)
					{
						cell = tableRow.createCell(0);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue((Integer) sbh.get(i).getHour());
						cell = tableRow.createCell(1);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue((Integer) sbh.get(i).getSessions());
					} else
					{
						cell = tableRow.createCell(0);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue(" ");
						cell = tableRow.createCell(1);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue(" ");
					}
					if (sbh1.size() > i)
					{
						cell = tableRow.createCell(2);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue((Integer) sbh1.get(i).getHour());
						cell = tableRow.createCell(3);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue((Integer) sbh1.get(i).getSessions());
					} else
					{
						cell = tableRow.createCell(2);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue(" ");
						cell = tableRow.createCell(3);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue(" ");
					}
					index++;
				}

				HSSFRow totalRow = sheet.createRow(index);
				cell = totalRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue("Total Sessions");
				cell = totalRow.createCell(1);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue((Integer) model.get("totalSessions"));
				cell = totalRow.createCell(2);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue("Total Sessions");
				cell = totalRow.createCell(3);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue((Integer) model.get("totalSessions1"));

				FileOutputStream fos = new FileOutputStream(filePath
						+ "pages/reports/" + reportName + ".xls");
				workbook.write(fos);
				fos.close();
			}
		} else if (reportName.equals("TopKeywordsSearchReport"))
		{
			List<TopKeywordsSearch> topKeywordsSearchesList = (List<TopKeywordsSearch>) model
					.get("topKeywordsSearchesList");
			if (model.get("autocorrectto").equals("yes"))
			{
				String title = "Top Keywords Searches Report";
				HSSFWorkbook workbook = new HSSFWorkbook();
				HSSFSheet sheet = workbook.createSheet(title);
				// default column width
				sheet.setDefaultColumnWidth(30);
				// style for headers
				HSSFCellStyle style = workbook.createCellStyle();
				// text align center
				style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
				// colour
				style.setFillForegroundColor(HSSFColor.YELLOW.index);
				style.setFillBackgroundColor(HSSFColor.WHITE.index);
				style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				// border width
				style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
				style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
				style.setBorderRight(HSSFCellStyle.BORDER_THIN);
				style.setBorderTop(HSSFCellStyle.BORDER_THIN);
				// font
				HSSFFont font = workbook.createFont();
				font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				font.setColor(HSSFColor.VIOLET.index);
				font.setFontHeightInPoints((short) 12);
				// set font
				style.setFont(font);

				// body style
				HSSFCellStyle bodyStyle = workbook.createCellStyle();
				bodyStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				// font
				HSSFFont bodyFont = workbook.createFont();
				bodyFont.setFontHeightInPoints((short) 12);
				// set font
				bodyStyle.setFont(bodyFont);
				int index = 0;
				HSSFRow row = sheet.createRow(index);
				HSSFCell cell = null;
				cell = row.createCell(0);
				cell.setCellStyle(style);
				HSSFRichTextString text = new HSSFRichTextString(
						(String) model.get("reportName"));
				cell.setCellValue(text);
				index++;
				HSSFRow dateRow = sheet.createRow(index);
				cell = dateRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				HSSFRichTextString date = new HSSFRichTextString(
						(String) model.get("date"));
				cell.setCellValue(date);
				index++;
				HSSFRow tableHeaderRow = sheet.createRow(index);
				String[] tableHeaders = new String[]
				{ "#", "SearchTerms", "AutoCorrectto", "Requests" };
				for (short m = 0; m < tableHeaders.length; m++)
				{
					cell = tableHeaderRow.createCell(m);
					cell.setCellStyle(style);
					HSSFRichTextString tabelHeader = new HSSFRichTextString(
							tableHeaders[m]);
					cell.setCellValue(tabelHeader);
				}
				index++;
				for (int n = 0; n < topKeywordsSearchesList.size(); n++)
				{
					HSSFRow tableRow = sheet.createRow(index);
					cell = tableRow.createCell(0);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue(n + 1);
					cell = tableRow.createCell(1);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue(topKeywordsSearchesList.get(n)
							.getSearchTerms());
					cell = tableRow.createCell(2);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue(topKeywordsSearchesList.get(n)
							.getAutoCorrectTo());
					cell = tableRow.createCell(3);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue((Integer) topKeywordsSearchesList.get(n)
							.getCount());
					index++;
				}

				FileOutputStream fos = new FileOutputStream(filePath
						+ "pages/reports/" + reportName + ".xls");
				workbook.write(fos);
				fos.close();
			} else
			{
				String title = "Top Keywords Searches Report";
				HSSFWorkbook workbook = new HSSFWorkbook();
				HSSFSheet sheet = workbook.createSheet(title);
				// default column width
				sheet.setDefaultColumnWidth(30);
				// style for headers
				HSSFCellStyle style = workbook.createCellStyle();
				// text align center
				style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
				// colour
				style.setFillForegroundColor(HSSFColor.YELLOW.index);
				style.setFillBackgroundColor(HSSFColor.WHITE.index);
				style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				// border width
				style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
				style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
				style.setBorderRight(HSSFCellStyle.BORDER_THIN);
				style.setBorderTop(HSSFCellStyle.BORDER_THIN);
				// font
				HSSFFont font = workbook.createFont();
				font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				font.setColor(HSSFColor.VIOLET.index);
				font.setFontHeightInPoints((short) 12);
				// set font
				style.setFont(font);

				// body style
				HSSFCellStyle bodyStyle = workbook.createCellStyle();
				bodyStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				// font
				HSSFFont bodyFont = workbook.createFont();
				bodyFont.setFontHeightInPoints((short) 12);
				// set font
				bodyStyle.setFont(bodyFont);
				int index = 0;
				HSSFRow row = sheet.createRow(index);
				HSSFCell cell = null;
				cell = row.createCell(0);
				cell.setCellStyle(style);
				HSSFRichTextString text = new HSSFRichTextString(
						(String) model.get("reportName"));
				cell.setCellValue(text);
				index++;
				HSSFRow dateRow = sheet.createRow(index);
				cell = dateRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				HSSFRichTextString date = new HSSFRichTextString(
						(String) model.get("date"));
				cell.setCellValue(date);
				index++;
				HSSFRow tableHeaderRow = sheet.createRow(index);
				String[] tableHeaders = new String[]
				{ "#", "SearchTerms", "Requests" };
				for (short m = 0; m < tableHeaders.length; m++)
				{
					cell = tableHeaderRow.createCell(m);
					cell.setCellStyle(style);
					HSSFRichTextString tabelHeader = new HSSFRichTextString(
							tableHeaders[m]);
					cell.setCellValue(tabelHeader);
				}
				index++;
				for (int n = 0; n < topKeywordsSearchesList.size(); n++)
				{
					HSSFRow tableRow = sheet.createRow(index);
					cell = tableRow.createCell(0);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue(n + 1);
					cell = tableRow.createCell(1);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue(topKeywordsSearchesList.get(n)
							.getSearchTerms());
					cell = tableRow.createCell(2);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue((Integer) topKeywordsSearchesList.get(n)
							.getCount());
					index++;
				}

				FileOutputStream fos = new FileOutputStream(filePath
						+ "pages/reports/" + reportName + ".xls");
				workbook.write(fos);
				fos.close();
			}

		} else if (reportName.equals("TopDimensionValuesReport"))
		{

			ArrayList<String> dates = (ArrayList<String>) model.get("dates");
			HashMap<String, Object> topValsSearchLists = (HashMap<String, Object>) model
					.get("topValsSearchLists");
			String title = "Top Dimension Values Report";
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet(title);
			// default column width
			sheet.setDefaultColumnWidth(50);
			// style for headers
			HSSFCellStyle style = workbook.createCellStyle();
			// text align center
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			// colour
			style.setFillForegroundColor(HSSFColor.YELLOW.index);
			style.setFillBackgroundColor(HSSFColor.WHITE.index);
			style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			// border width
			style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			style.setBorderRight(HSSFCellStyle.BORDER_THIN);
			style.setBorderTop(HSSFCellStyle.BORDER_THIN);
			// font
			HSSFFont font = workbook.createFont();
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			font.setColor(HSSFColor.VIOLET.index);
			font.setFontHeightInPoints((short) 12);
			// set font
			style.setFont(font);

			// body style
			HSSFCellStyle bodyStyle = workbook.createCellStyle();
			bodyStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
			// font
			HSSFFont bodyFont = workbook.createFont();
			bodyFont.setFontHeightInPoints((short) 12);
			// set font
			bodyStyle.setFont(bodyFont);
			// header
			String[] headers = new String[]
			{ (String) model.get("reportName"),
					(String) model.get("reportName1") };
			int index = 0;
			HSSFRow row = sheet.createRow(index);
			HSSFCell cell = null;
			for (short i = 0; i < headers.length; i++)
			{
				cell = row.createCell(i);
				cell.setCellStyle(style);
				HSSFRichTextString text = new HSSFRichTextString(headers[i]);
				cell.setCellValue(text);
			}
			index++;
			int i = 0;
			for (String key : topValsSearchLists.keySet())
			{
				HSSFRow dateRow = sheet.createRow(index);
				cell = dateRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				HSSFRichTextString text = new HSSFRichTextString(key + " : "
						+ dates.get(i));
				cell.setCellValue(text);
				index++;
				HSSFRow tableHeaderRow = sheet.createRow(index);
				String[] tableHeaders = new String[]
				{ "#", "Dimension Values", "Requests", "% of Total" };
				for (short m = 0; m < tableHeaders.length; m++)
				{
					cell = tableHeaderRow.createCell(m);
					cell.setCellStyle(style);
					HSSFRichTextString tabelHeader = new HSSFRichTextString(
							tableHeaders[m]);
					cell.setCellValue(tabelHeader);
				}
				index++;
				List<TopDimensionValues> tsl = (List<TopDimensionValues>) topValsSearchLists
						.get(key);
				for (int n = 0; n < tsl.size(); n++)
				{
					HSSFRow tableRow = sheet.createRow(index);
					cell = tableRow.createCell(0);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue(n + 1);
					cell = tableRow.createCell(1);
					cell.setCellStyle(bodyStyle);
					HSSFRichTextString column1 = new HSSFRichTextString(tsl
							.get(n).getDimensionValue());
					cell.setCellValue(column1);
					cell = tableRow.createCell(2);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue(tsl.get(n).getCount());
					cell = tableRow.createCell(3);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue(tsl.get(n).getPercentage());
					index++;
				}
				i++;
			}
			FileOutputStream fos = new FileOutputStream(filePath
					+ "pages/reports/" + reportName + ".xls");
			workbook.write(fos);
			fos.close();

		} else if (reportName.equals("UserTrendReport"))
		{
			HashMap<String, Object> trend = (HashMap<String, Object>) model
					.get("trend");
			String title = "User Trend Report";
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet(title);
			// default column width
			sheet.setDefaultColumnWidth(30);
			// style for headers
			HSSFCellStyle style = workbook.createCellStyle();
			// text align center
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			// colour
			style.setFillForegroundColor(HSSFColor.YELLOW.index);
			style.setFillBackgroundColor(HSSFColor.WHITE.index);
			style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			// border width
			style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			style.setBorderRight(HSSFCellStyle.BORDER_THIN);
			style.setBorderTop(HSSFCellStyle.BORDER_THIN);
			// font
			HSSFFont font = workbook.createFont();
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			font.setColor(HSSFColor.VIOLET.index);
			font.setFontHeightInPoints((short) 12);
			// set font
			style.setFont(font);
			// body style
			HSSFCellStyle bodyStyle = workbook.createCellStyle();
			bodyStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
			// font
			HSSFFont bodyFont = workbook.createFont();
			bodyFont.setFontHeightInPoints((short) 12);
			// set font
			bodyStyle.setFont(bodyFont);
			int index = 0;
			HSSFRow row = sheet.createRow(index);
			HSSFCell cell = null;
			cell = row.createCell(0);
			cell.setCellStyle(style);
			HSSFRichTextString text = new HSSFRichTextString(
					(String) model.get("reportName"));
			cell.setCellValue(text);
			cell = row.createCell(1);
			cell.setCellStyle(style);
			HSSFRichTextString text1 = new HSSFRichTextString(
					(String) model.get("reportName1"));
			cell.setCellValue(text1);
			index++;
			HSSFRow dateRow = sheet.createRow(index);
			cell = dateRow.createCell(0);
			cell.setCellStyle(bodyStyle);
			HSSFRichTextString date = new HSSFRichTextString(
					(String) model.get("date"));
			cell.setCellValue(date);
			index++;
			HSSFRow tableHeaderRow = sheet.createRow(index);
			String[] tableHeaders = new String[]
			{ "Weeks", "Requests" };
			for (short m = 0; m < tableHeaders.length; m++)
			{
				cell = tableHeaderRow.createCell(m);
				cell.setCellStyle(style);
				HSSFRichTextString tabelHeader = new HSSFRichTextString(
						tableHeaders[m]);
				cell.setCellValue(tabelHeader);
			}
			index++;
			for (String key : trend.keySet())
			{
				HSSFRow tableRow = sheet.createRow(index);
				cell = tableRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue(key);
				cell = tableRow.createCell(1);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue((Integer) trend.get(key));
				index++;
			}

			FileOutputStream fos = new FileOutputStream(filePath
					+ "pages/reports/" + reportName + ".xls");
			workbook.write(fos);
			fos.close();
		} else if (reportName.equals("KeywordSearchReport"))
		{
			ArrayList<Unit> keywordSearchArrayList = (ArrayList<Unit>) model
					.get("keywordSearchArrayListIndex");
			String title = "Keyword Search Report";
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet(title);
			// default column width
			sheet.setDefaultColumnWidth(30);
			// style for headers
			HSSFCellStyle style = workbook.createCellStyle();
			// text align center
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			// colour
			style.setFillForegroundColor(HSSFColor.YELLOW.index);
			style.setFillBackgroundColor(HSSFColor.WHITE.index);
			style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			// border width
			style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			style.setBorderRight(HSSFCellStyle.BORDER_THIN);
			style.setBorderTop(HSSFCellStyle.BORDER_THIN);
			// font
			HSSFFont font = workbook.createFont();
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			font.setColor(HSSFColor.VIOLET.index);
			font.setFontHeightInPoints((short) 12);
			// set font
			style.setFont(font);

			// body style
			HSSFCellStyle bodyStyle = workbook.createCellStyle();
			bodyStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
			// font
			HSSFFont bodyFont = workbook.createFont();
			bodyFont.setFontHeightInPoints((short) 12);
			// set font
			bodyStyle.setFont(bodyFont);
			int index = 0;
			HSSFRow row = sheet.createRow(index);
			HSSFCell cell = null;
			cell = row.createCell(0);
			cell.setCellStyle(style);
			HSSFRichTextString text = new HSSFRichTextString(
					(String) model.get("reportName"));
			cell.setCellValue(text);
			index++;
			HSSFRow dateRow = sheet.createRow(index);
			cell = dateRow.createCell(0);
			cell.setCellStyle(bodyStyle);
			HSSFRichTextString date = new HSSFRichTextString(
					(String) model.get("date"));
			cell.setCellValue(date);
			index++;
			HSSFRow tableHeaderRow = sheet.createRow(index);
			String[] tableHeaders = new String[]
			{ "#", "Records Unit[Min - Max]", "Total Counts Of Searchterms" };
			for (short m = 0; m < tableHeaders.length; m++)
			{
				cell = tableHeaderRow.createCell(m);
				cell.setCellStyle(style);
				HSSFRichTextString tabelHeader = new HSSFRichTextString(
						tableHeaders[m]);
				cell.setCellValue(tabelHeader);
			}
			index++;
			for (int n = 0; n < keywordSearchArrayList.size(); n++)
			{
				HSSFRow tableRow = sheet.createRow(index);
				cell = tableRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue(n + 1);
				cell = tableRow.createCell(1);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue("[" + keywordSearchArrayList.get(n).getMin()
						+ " - " + keywordSearchArrayList.get(n).getMin() + "]");
				cell = tableRow.createCell(2);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue((Integer) keywordSearchArrayList.get(n)
						.getValue());
				index++;
			}

			FileOutputStream fos = new FileOutputStream(filePath
					+ "pages/reports/" + reportName + ".xls");
			workbook.write(fos);
			fos.close();
		}
	}

	/**
	 * Creates CSV report
	 * 
	 * @param filePath
	 *            the path to store a generated CSV report
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private static void createCSV(String filePath,
			HashMap<String, Object> model, String reportName)
			throws IOException
	{
		// TODO Auto-generated method stub
		if (reportName.equals("TopDimSearchReport"))
		{
			ArrayList<String> dates = (ArrayList<String>) model.get("dates");
			HashMap<String, Object> topDimsSearchLists = (HashMap<String, Object>) model
					.get("topDimsSearchLists");
			FileWriter writer = new FileWriter(filePath + "pages/reports/"
					+ reportName + ".csv");
			writer.append((String) model.get("reportName"));
			writer.append(",");
			writer.append("\"" + (String) model.get("reportName1") + "\"");
			writer.append("\n");
			int i = 0;
			for (String key : topDimsSearchLists.keySet())
			{
				writer.append(key + " : " + dates.get(i));
				writer.append("\n");
				writer.append("#");
				writer.append(",");
				writer.append("Dimensions");
				writer.append(",");
				writer.append("Requests");
				writer.append("\n");
				List<TopSearch> tsl = (List<TopSearch>) topDimsSearchLists
						.get(key);
				for (int m = 0; m < tsl.size(); m++)
				{
					writer.append(String.valueOf(m + 1));
					writer.append(",");
					writer.append("\"" + tsl.get(m).getAllDimensions() + "\"");
					writer.append(",");
					writer.append(String.valueOf(tsl.get(m).getCount()));
					writer.append("\n");
				}

				i++;
			}
			writer.flush();
			writer.close();
		} else if (reportName.equals("RequestByTypeReport"))
		{
			HashMap<String, Object> requestByTypeList = (HashMap<String, Object>) model
					.get("requestByTypeList");
			FileWriter writer = new FileWriter(filePath + "pages/reports/"
					+ reportName + ".csv");
			writer.append((String) model.get("reportName"));
			writer.append("\n");
			writer.append((String) model.get("date"));
			writer.append("\n");
			writer.append((String) model.get("th1"));
			writer.append(",");
			writer.append((String) model.get("th2"));
			writer.append("\n");
			for (String key : requestByTypeList.keySet())
			{
				writer.append(key);
				writer.append(",");
				writer.append(String.valueOf(requestByTypeList.get(key)));
				writer.append("\n");
			}
			writer.flush();
			writer.close();
		} else if (reportName.equals("RequestByHourReport"))
		{
			ArrayList<RequestByHour> rbh = (ArrayList<RequestByHour>) model
					.get("requestByHourList");
			ArrayList<RequestByHour> rbh1 = (ArrayList<RequestByHour>) model
					.get("requestByHourList1");
			if (rbh1 == null)
			{
				FileWriter writer = new FileWriter(filePath + "pages/reports/"
						+ reportName + ".csv");
				writer.append((String) model.get("reportName"));
				writer.append("\n");
				writer.append((String) model.get("date"));
				writer.append("\n");
				writer.append((String) model.get("th1"));
				writer.append(",");
				writer.append((String) model.get("th2"));
				writer.append("\n");
				for (int m = 0; m < rbh.size(); m++)
				{
					writer.append(String.valueOf(rbh.get(m).getHour()));
					writer.append(",");
					writer.append(String.valueOf(rbh.get(m).getRequests()));
					writer.append("\n");
				}
				writer.append("Total Requests");
				writer.append(",");
				writer.append(String.valueOf(model.get("totalRequests")));
				writer.append("\n");
				writer.flush();
				writer.close();
			} else
			{
				FileWriter writer = new FileWriter(filePath + "pages/reports/"
						+ reportName + ".csv");
				writer.append((String) model.get("reportName"));
				writer.append("\n");
				writer.append((String) model.get("date"));
				writer.append("\n");
				writer.append((String) model.get("th1"));
				writer.append(",");
				writer.append((String) model.get("th2"));
				writer.append(",");
				writer.append((String) model.get("th3"));
				writer.append(",");
				writer.append((String) model.get("th4"));
				writer.append("\n");
				int size = 0;
				if (rbh.size() > rbh1.size())
				{
					size = rbh.size();
				} else
				{
					size = rbh1.size();
				}
				for (int i = 0; i < size; i++)
				{
					if (rbh.size() > i)
					{
						writer.append(String.valueOf(rbh.get(i).getHour()));
						writer.append(",");
						writer.append(String.valueOf(rbh.get(i).getRequests()));
						writer.append(",");
					} else
					{
						writer.append(" ");
						writer.append(",");
						writer.append(" ");
						writer.append(",");
					}
					if (rbh1.size() > i)
					{
						writer.append(String.valueOf(rbh1.get(i).getHour()));
						writer.append(",");
						writer.append(String.valueOf(rbh1.get(i).getRequests()));
						writer.append("\n");
					} else
					{
						writer.append(" ");
						writer.append(",");
						writer.append(" ");
						writer.append("\n");
					}

				}
				writer.append("Total Requests");
				writer.append(",");
				writer.append(String.valueOf(model.get("totalRequests")));
				writer.append(",");
				writer.append("Total Requests");
				writer.append(",");
				writer.append(String.valueOf(model.get("totalRequests1")));
				writer.append("\n");
				writer.flush();
				writer.close();

			}
		} else if (reportName.equals("SessionByHourReport"))
		{
			ArrayList<SessionByHour> sbh = (ArrayList<SessionByHour>) model
					.get("sessionByHourList");
			ArrayList<SessionByHour> sbh1 = (ArrayList<SessionByHour>) model
					.get("sessionByHourList1");
			if (sbh1 == null)
			{
				FileWriter writer = new FileWriter(filePath + "pages/reports/"
						+ reportName + ".csv");
				writer.append((String) model.get("reportName"));
				writer.append("\n");
				writer.append((String) model.get("date"));
				writer.append("\n");
				writer.append((String) model.get("th1"));
				writer.append(",");
				writer.append((String) model.get("th2"));
				writer.append("\n");
				for (int m = 0; m < sbh.size(); m++)
				{
					writer.append(String.valueOf(sbh.get(m).getHour()));
					writer.append(",");
					writer.append(String.valueOf(sbh.get(m).getSessions()));
					writer.append("\n");
				}
				writer.append("Total Sessions");
				writer.append(",");
				writer.append(String.valueOf(model.get("totalSessions")));
				writer.append("\n");
				writer.flush();
				writer.close();
			} else
			{
				FileWriter writer = new FileWriter(filePath + "pages/reports/"
						+ reportName + ".csv");
				writer.append((String) model.get("reportName"));
				writer.append("\n");
				writer.append((String) model.get("date"));
				writer.append("\n");
				writer.append((String) model.get("th1"));
				writer.append(",");
				writer.append((String) model.get("th2"));
				writer.append(",");
				writer.append((String) model.get("th3"));
				writer.append(",");
				writer.append((String) model.get("th4"));
				writer.append("\n");
				int size = 0;
				if (sbh.size() > sbh1.size())
				{
					size = sbh.size();
				} else
				{
					size = sbh1.size();
				}
				for (int i = 0; i < size; i++)
				{
					if (sbh.size() > i)
					{
						writer.append(String.valueOf(sbh.get(i).getHour()));
						writer.append(",");
						writer.append(String.valueOf(sbh.get(i).getSessions()));
						writer.append(",");
					} else
					{
						writer.append(" ");
						writer.append(",");
						writer.append(" ");
						writer.append(",");
					}
					if (sbh1.size() > i)
					{
						writer.append(String.valueOf(sbh1.get(i).getHour()));
						writer.append(",");
						writer.append(String.valueOf(sbh1.get(i).getSessions()));
						writer.append("\n");
					} else
					{
						writer.append(" ");
						writer.append(",");
						writer.append(" ");
						writer.append("\n");
					}

				}
				writer.append("Total Sessions");
				writer.append(",");
				writer.append(String.valueOf(model.get("totalSessions")));
				writer.append(",");
				writer.append("Total Sessions");
				writer.append(",");
				writer.append(String.valueOf(model.get("totalSessions1")));
				writer.append("\n");
				writer.flush();
				writer.close();
			}
		} else if (reportName.equals("TopDimensionValuesReport"))
		{
			ArrayList<String> dates = (ArrayList<String>) model.get("dates");
			HashMap<String, Object> topValsSearchLists = (HashMap<String, Object>) model
					.get("topValsSearchLists");
			FileWriter writer = new FileWriter(filePath + "pages/reports/"
					+ reportName + ".csv");
			writer.append((String) model.get("reportName"));
			writer.append(",");
			writer.append("\"" + (String) model.get("reportName1") + "\"");
			writer.append("\n");
			int i = 0;
			for (String key : topValsSearchLists.keySet())
			{
				writer.append(key + " : " + dates.get(i));
				writer.append("\n");
				writer.append("#");
				writer.append(",");
				writer.append("Dimension Values");
				writer.append(",");
				writer.append("Requests");
				writer.append(",");
				writer.append("% of Total");
				writer.append("\n");
				List<TopDimensionValues> tsl = (List<TopDimensionValues>) topValsSearchLists
						.get(key);
				for (int m = 0; m < tsl.size(); m++)
				{
					writer.append(String.valueOf(m + 1));
					writer.append(",");
					writer.append("\"" + tsl.get(m).getDimensionValue() + "\"");
					writer.append(",");
					writer.append(String.valueOf(tsl.get(m).getCount()));
					writer.append(",");
					writer.append(String.valueOf(tsl.get(m).getPercentage()));
					writer.append("\n");
				}

				i++;
			}
			writer.flush();
			writer.close();
		} else if (reportName.equals("TopKeywordsSearchReport"))
		{
			List<TopKeywordsSearch> topKeywordsSearchesList = (List<TopKeywordsSearch>) model
					.get("topKeywordsSearchesList");
			if (model.get("autocorrectto").equals("yes"))
			{
				FileWriter writer = new FileWriter(filePath + "pages/reports/"
						+ reportName + ".csv");
				writer.append((String) model.get("reportName"));
				writer.append("\n");
				writer.append((String) model.get("date"));
				writer.append("\n");
				writer.append("#");
				writer.append(",");
				writer.append("SearchTerms");
				writer.append(",");
				writer.append("AutoCorrectto");
				writer.append(",");
				writer.append("Requests");
				writer.append("\n");
				for (int m = 0; m < topKeywordsSearchesList.size(); m++)
				{
					writer.append(String.valueOf(m + 1));
					writer.append(",");
					writer.append(topKeywordsSearchesList.get(m)
							.getSearchTerms());
					writer.append(",");
					writer.append(topKeywordsSearchesList.get(m)
							.getAutoCorrectTo());
					writer.append(",");
					writer.append(String.valueOf(topKeywordsSearchesList.get(m)
							.getCount()));
					writer.append("\n");
				}
				writer.flush();
				writer.close();
			} else
			{
				FileWriter writer = new FileWriter(filePath + "pages/reports/"
						+ reportName + ".csv");
				writer.append((String) model.get("reportName"));
				writer.append("\n");
				writer.append((String) model.get("date"));
				writer.append("\n");
				writer.append("#");
				writer.append(",");
				writer.append("SearchTerms");
				writer.append(",");
				writer.append("Requests");
				writer.append("\n");
				for (int m = 0; m < topKeywordsSearchesList.size(); m++)
				{
					writer.append(String.valueOf(m + 1));
					writer.append(",");
					writer.append(topKeywordsSearchesList.get(m)
							.getSearchTerms());
					writer.append(",");
					writer.append(String.valueOf(topKeywordsSearchesList.get(m)
							.getCount()));
					writer.append("\n");
				}
				writer.flush();
				writer.close();
			}
		} else if (reportName.equals("UserTrendReport"))
		{
			HashMap<String, Object> trend = (HashMap<String, Object>) model
					.get("trend");
			FileWriter writer = new FileWriter(filePath + "pages/reports/"
					+ reportName + ".csv");
			writer.append((String) model.get("reportName"));
			writer.append(",");
			writer.append((String) model.get("reportName1"));
			writer.append("\n");
			writer.append((String) model.get("date"));
			writer.append("\n");
			writer.append("Weeks");
			writer.append(",");
			writer.append("Requests");
			writer.append("\n");
			for (String key : trend.keySet())
			{
				writer.append(key);
				writer.append(",");
				writer.append(String.valueOf(trend.get(key)));
				writer.append("\n");
			}
			writer.flush();
			writer.close();
		} else if (reportName.equals("KeywordSearchReport"))
		{
			ArrayList<Unit> keywordSearchArrayList = (ArrayList<Unit>) model
					.get("keywordSearchArrayListIndex");
			FileWriter writer = new FileWriter(filePath + "pages/reports/"
					+ reportName + ".csv");
			writer.append((String) model.get("reportName"));
			writer.append("\n");
			writer.append((String) model.get("date"));
			writer.append("\n");
			writer.append("#");
			writer.append(",");
			writer.append("Records Unit[Min - Max]");
			writer.append(",");
			writer.append("Total Counts Of Searchterms");
			writer.append("\n");
			for (int m = 0; m < keywordSearchArrayList.size(); m++)
			{
				writer.append(String.valueOf(m + 1));
				writer.append(",");
				writer.append("[" + keywordSearchArrayList.get(m).getMin()
						+ " - " + keywordSearchArrayList.get(m).getMax() + "]");
				writer.append(",");
				writer.append(String.valueOf(keywordSearchArrayList.get(m)
						.getValue()));
				writer.append("\n");
			}
			writer.flush();
			writer.close();
		}

	}

	/**
	 * Creates PDF report
	 * 
	 * @param filePath
	 *            the path to store a generated PDF report
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 * @throws JRException
	 * @throws DocumentException
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private static void createPDF(String filePath,
			HashMap<String, Object> model, String reportName)
			throws JRException, DocumentException, MalformedURLException,
			IOException
	{
		// TODO Auto-generated method stub
		if (reportName.equals("TopDimSearchReport"))
		{
			ArrayList<String> dates = (ArrayList<String>) model.get("dates");
			HashMap<String, Object> topDimsSearchLists = (HashMap<String, Object>) model
					.get("topDimsSearchLists");
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(filePath
					+ "pages/reports/" + reportName + ".pdf"));
			document.open();
			Paragraph title = new Paragraph(
					"Top Dimension Searches Report\n\n", FontFactory.getFont(
							FontFactory.TIMES_ROMAN, 20, Font.BOLD));
			title.setAlignment(Element.ALIGN_CENTER);
			document.add(title);
			document.add(new Paragraph((String) model.get("reportName")));
			document.add(new Paragraph((String) model.get("reportName1")));
			int i = 0;
			for (String key : topDimsSearchLists.keySet())
			{
				document.add(new Paragraph(key + " : " + dates.get(i) + "\n\n"));
				Image image = Image.getInstance(filePath + "pages/temp/" + i
						+ ".jpg");
				image.scaleAbsolute(410, 210);
				float[] widths =
				{ 0.1f, 0.7f, 0.15f };
				PdfPTable table = new PdfPTable(widths);
				PdfPCell chart = new PdfPCell(image);
				chart.setColspan(3);
				PdfPCell id = new PdfPCell(new Paragraph("#"));
				PdfPCell dimensions = new PdfPCell(new Paragraph("Dimensions"));
				PdfPCell requests = new PdfPCell(new Paragraph("Requests"));
				chart.setBorder(Rectangle.NO_BORDER);
				id.setBorder(Rectangle.NO_BORDER);
				dimensions.setBorder(Rectangle.NO_BORDER);
				requests.setBorder(Rectangle.NO_BORDER);
				table.addCell(chart);
				table.addCell(id);
				table.addCell(dimensions);
				table.addCell(requests);
				List<TopSearch> tsl = (List<TopSearch>) topDimsSearchLists
						.get(key);
				for (int m = 0; m < tsl.size(); m++)
				{
					table.addCell(String.valueOf(m + 1));
					table.addCell(tsl.get(m).getAllDimensions());
					table.addCell(String.valueOf(tsl.get(m).getCount()));
				}
				document.add(table);
				i++;
				document.newPage();
			}
			document.close();
		} else if (reportName.equals("RequestByTypeReport"))
		{
			HashMap<String, Object> requestByTypeList = (HashMap<String, Object>) model
					.get("requestByTypeList");
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(filePath
					+ "pages/reports/" + reportName + ".pdf"));
			document.open();
			Paragraph title = new Paragraph("Request By Type Report\n\n",
					FontFactory.getFont(FontFactory.TIMES_ROMAN, 20, Font.BOLD));
			title.setAlignment(Element.ALIGN_CENTER);
			document.add(title);
			document.add(new Paragraph((String) model.get("reportName")));
			document.add(new Paragraph((String) model.get("date") + "\n\n"));
			Image image = Image.getInstance(filePath
					+ "pages/temp/piechart.jpg");
			image.scaleAbsolute(410, 210);
			float[] widths =
			{ 0.8f, 0.2f };
			PdfPTable table = new PdfPTable(widths);
			PdfPCell chart = new PdfPCell(image);
			chart.setColspan(2);
			PdfPCell typeOfRequest = new PdfPCell(new Paragraph(
					(String) model.get("th1")));
			PdfPCell requests = new PdfPCell(new Paragraph(
					(String) model.get("th2")));
			chart.setBorder(Rectangle.NO_BORDER);
			typeOfRequest.setBorder(Rectangle.NO_BORDER);
			requests.setBorder(Rectangle.NO_BORDER);
			table.addCell(chart);
			table.addCell(typeOfRequest);
			table.addCell(requests);
			for (String key : requestByTypeList.keySet())
			{
				table.addCell(key);
				table.addCell(String.valueOf(requestByTypeList.get(key)));
			}
			document.add(table);
			document.close();
		} else if (reportName.equals("RequestByHourReport"))
		{
			ArrayList<RequestByHour> rbh = (ArrayList<RequestByHour>) model
					.get("requestByHourList");
			ArrayList<RequestByHour> rbh1 = (ArrayList<RequestByHour>) model
					.get("requestByHourList1");
			if (rbh1 == null)
			{
				Document document = new Document();
				PdfWriter.getInstance(document, new FileOutputStream(filePath
						+ "pages/reports/" + reportName + ".pdf"));
				document.open();
				Paragraph title = new Paragraph("Request By Hour Report\n\n",
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 20,
								Font.BOLD));
				title.setAlignment(Element.ALIGN_CENTER);
				document.add(title);
				document.add(new Paragraph((String) model.get("reportName")));
				document.add(new Paragraph((String) model.get("date") + "\n\n"));
				Image image = Image.getInstance(filePath
						+ "pages/temp/barchart.jpg");
				image.scaleAbsolute(410, 210);
				float[] widths =
				{ 0.8f, 0.2f };
				PdfPTable table = new PdfPTable(widths);
				PdfPCell chart = new PdfPCell(image);
				chart.setColspan(2);
				chart.setBorder(Rectangle.NO_BORDER);
				table.addCell(chart);

				PdfPCell hours = new PdfPCell(new Paragraph(
						(String) model.get("th1")));
				PdfPCell requests = new PdfPCell(new Paragraph(
						(String) model.get("th2")));

				hours.setBorder(Rectangle.NO_BORDER);
				requests.setBorder(Rectangle.NO_BORDER);

				table.addCell(hours);
				table.addCell(requests);
				for (int i = 0; i < rbh.size(); i++)
				{
					table.addCell(String.valueOf(rbh.get(i).getHour()));
					table.addCell(String.valueOf(rbh.get(i).getRequests()));
				}
				table.addCell("Total Requests");
				table.addCell(String.valueOf(model.get("totalRequests")));
				document.add(table);
				document.close();
			} else
			{
				Document document = new Document();
				PdfWriter.getInstance(document, new FileOutputStream(filePath
						+ "pages/reports/" + reportName + ".pdf"));
				document.open();
				Paragraph title = new Paragraph("Request By Hour Report\n\n",
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 20,
								Font.BOLD));
				title.setAlignment(Element.ALIGN_CENTER);
				document.add(title);
				document.add(new Paragraph((String) model.get("reportName")));
				document.add(new Paragraph((String) model.get("date") + "\n\n"));
				Image image = Image.getInstance(filePath
						+ "pages/temp/barchart.jpg");
				image.scaleAbsolute(410, 210);
				float[] widths =
				{ 0.25f, 0.25f, 0.25f, 0.25f };
				PdfPTable table = new PdfPTable(widths);
				PdfPCell chart = new PdfPCell(image);
				chart.setColspan(4);
				chart.setBorder(Rectangle.NO_BORDER);
				table.addCell(chart);

				PdfPCell hours = new PdfPCell(new Paragraph(
						(String) model.get("th1")));
				PdfPCell requests = new PdfPCell(new Paragraph(
						(String) model.get("th2")));
				PdfPCell hours1 = new PdfPCell(new Paragraph(
						(String) model.get("th3")));
				PdfPCell requests1 = new PdfPCell(new Paragraph(
						(String) model.get("th4")));

				hours.setBorder(Rectangle.NO_BORDER);
				requests.setBorder(Rectangle.NO_BORDER);
				hours1.setBorder(Rectangle.NO_BORDER);
				requests1.setBorder(Rectangle.NO_BORDER);

				table.addCell(hours);
				table.addCell(requests);
				table.addCell(hours1);
				table.addCell(requests1);
				int size = 0;
				if (rbh.size() > rbh1.size())
				{
					size = rbh.size();
				} else
				{
					size = rbh1.size();
				}
				for (int i = 0; i < size; i++)
				{
					if (rbh.size() > i)
					{
						table.addCell(String.valueOf(rbh.get(i).getHour()));
						table.addCell(String.valueOf(rbh.get(i).getRequests()));
					} else
					{
						table.addCell(" ");
						table.addCell(" ");
					}
					if (rbh1.size() > i)
					{
						table.addCell(String.valueOf(rbh1.get(i).getHour()));
						table.addCell(String.valueOf(rbh1.get(i).getRequests()));
					} else
					{
						table.addCell(" ");
						table.addCell(" ");
					}

				}
				table.addCell("Total Requests");
				table.addCell(String.valueOf(model.get("totalRequests")));
				table.addCell("Total Requests");
				table.addCell(String.valueOf(model.get("totalRequests1")));
				document.add(table);
				document.close();
			}
		} else if (reportName.equals("SessionByHourReport"))
		{
			ArrayList<SessionByHour> sbh = (ArrayList<SessionByHour>) model
					.get("sessionByHourList");
			ArrayList<SessionByHour> sbh1 = (ArrayList<SessionByHour>) model
					.get("sessionByHourList1");
			if (sbh1 == null)
			{
				Document document = new Document();
				PdfWriter.getInstance(document, new FileOutputStream(filePath
						+ "pages/reports/" + reportName + ".pdf"));
				document.open();
				Paragraph title = new Paragraph("Session By Hour Report\n\n",
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 20,
								Font.BOLD));
				title.setAlignment(Element.ALIGN_CENTER);
				document.add(title);
				document.add(new Paragraph((String) model.get("reportName")));
				document.add(new Paragraph((String) model.get("date") + "\n\n"));
				Image image = Image.getInstance(filePath
						+ "pages/temp/barchart.jpg");
				image.scaleAbsolute(410, 210);
				float[] widths =
				{ 0.8f, 0.2f };
				PdfPTable table = new PdfPTable(widths);
				PdfPCell chart = new PdfPCell(image);
				chart.setColspan(2);
				chart.setBorder(Rectangle.NO_BORDER);
				table.addCell(chart);

				PdfPCell hours = new PdfPCell(new Paragraph(
						(String) model.get("th1")));
				PdfPCell sessions = new PdfPCell(new Paragraph(
						(String) model.get("th2")));

				hours.setBorder(Rectangle.NO_BORDER);
				sessions.setBorder(Rectangle.NO_BORDER);

				table.addCell(hours);
				table.addCell(sessions);
				for (int i = 0; i < sbh.size(); i++)
				{
					table.addCell(String.valueOf(sbh.get(i).getHour()));
					table.addCell(String.valueOf(sbh.get(i).getSessions()));
				}
				table.addCell("Total Sessions");
				table.addCell(String.valueOf(model.get("totalSessions")));
				document.add(table);
				document.close();
			} else
			{
				Document document = new Document();
				PdfWriter.getInstance(document, new FileOutputStream(filePath
						+ "pages/reports/" + reportName + ".pdf"));
				document.open();
				Paragraph title = new Paragraph("Session By Hour Report\n\n",
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 20,
								Font.BOLD));
				title.setAlignment(Element.ALIGN_CENTER);
				document.add(title);
				document.add(new Paragraph((String) model.get("reportName")));
				document.add(new Paragraph((String) model.get("date") + "\n\n"));
				Image image = Image.getInstance(filePath
						+ "pages/temp/barchart.jpg");
				image.scaleAbsolute(410, 210);
				float[] widths =
				{ 0.25f, 0.25f, 0.25f, 0.25f };
				PdfPTable table = new PdfPTable(widths);
				PdfPCell chart = new PdfPCell(image);
				chart.setColspan(4);
				chart.setBorder(Rectangle.NO_BORDER);
				table.addCell(chart);

				PdfPCell hours = new PdfPCell(new Paragraph(
						(String) model.get("th1")));
				PdfPCell sessions = new PdfPCell(new Paragraph(
						(String) model.get("th2")));
				PdfPCell hours1 = new PdfPCell(new Paragraph(
						(String) model.get("th3")));
				PdfPCell sessions1 = new PdfPCell(new Paragraph(
						(String) model.get("th4")));

				hours.setBorder(Rectangle.NO_BORDER);
				sessions.setBorder(Rectangle.NO_BORDER);
				hours1.setBorder(Rectangle.NO_BORDER);
				sessions1.setBorder(Rectangle.NO_BORDER);

				table.addCell(hours);
				table.addCell(sessions);
				table.addCell(hours1);
				table.addCell(sessions1);
				int size = 0;
				if (sbh.size() > sbh1.size())
				{
					size = sbh.size();
				} else
				{
					size = sbh1.size();
				}
				for (int i = 0; i < size; i++)
				{
					if (sbh.size() > i)
					{
						table.addCell(String.valueOf(sbh.get(i).getHour()));
						table.addCell(String.valueOf(sbh.get(i).getSessions()));
					} else
					{
						table.addCell(" ");
						table.addCell(" ");
					}
					if (sbh1.size() > i)
					{
						table.addCell(String.valueOf(sbh1.get(i).getHour()));
						table.addCell(String.valueOf(sbh1.get(i).getSessions()));
					} else
					{
						table.addCell(" ");
						table.addCell(" ");
					}

				}

				table.addCell("Total Sessions");
				table.addCell(String.valueOf(model.get("totalSessions")));
				table.addCell("Total Sessions");
				table.addCell(String.valueOf(model.get("totalSessions1")));
				document.add(table);
				document.close();
			}
		} else if (reportName.equalsIgnoreCase("TopDimensionValuesReport"))
		{
			ArrayList<String> dates = (ArrayList<String>) model.get("dates");
			HashMap<String, Object> topValsSearchLists = (HashMap<String, Object>) model
					.get("topValsSearchLists");
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(filePath
					+ "pages/reports/" + reportName + ".pdf"));
			document.open();
			Paragraph title = new Paragraph("Top Dimension Values Report\n\n",
					FontFactory.getFont(FontFactory.TIMES_ROMAN, 20, Font.BOLD));
			title.setAlignment(Element.ALIGN_CENTER);
			document.add(title);
			document.add(new Paragraph((String) model.get("reportName")));
			document.add(new Paragraph((String) model.get("reportName1")));
			int i = 0;
			for (String key : topValsSearchLists.keySet())
			{
				document.add(new Paragraph(key + " : " + dates.get(i) + "\n\n"));
				Image image = Image.getInstance(filePath + "pages/temp/" + i
						+ ".jpg");
				image.scaleAbsolute(410, 210);
				float[] widths =
				{ 0.1f, 0.7f, 0.12f, 0.08f };
				PdfPTable table = new PdfPTable(widths);
				PdfPCell chart = new PdfPCell(image);
				chart.setColspan(4);
				PdfPCell id = new PdfPCell(new Paragraph("#",
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10,
								Font.BOLD)));
				PdfPCell dimensions = new PdfPCell(new Paragraph(
						"Dimension Values", FontFactory.getFont(
								FontFactory.TIMES_ROMAN, 10, Font.BOLD)));
				PdfPCell requests = new PdfPCell(new Paragraph("Requests",
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10,
								Font.BOLD)));
				PdfPCell percentages = new PdfPCell(new Paragraph("% of Total",
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10,
								Font.BOLD)));
				chart.setBorder(Rectangle.NO_BORDER);
				id.setBorder(Rectangle.NO_BORDER);
				dimensions.setBorder(Rectangle.NO_BORDER);
				requests.setBorder(Rectangle.NO_BORDER);
				percentages.setBorder(Rectangle.NO_BORDER);
				table.addCell(chart);
				table.addCell(id);
				table.addCell(dimensions);
				table.addCell(requests);
				table.addCell(percentages);
				List<TopDimensionValues> tsl = (List<TopDimensionValues>) topValsSearchLists
						.get(key);
				for (int m = 0; m < tsl.size(); m++)
				{
					table.addCell(new Paragraph(String.valueOf(m + 1),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9)));
					table.addCell(new Paragraph(tsl.get(m).getDimensionValue(),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9)));
					table.addCell(new Paragraph(String.valueOf(tsl.get(m)
							.getCount()), FontFactory.getFont(
							FontFactory.TIMES_ROMAN, 9)));
					table.addCell(new Paragraph(String.valueOf(tsl.get(m)
							.getPercentage()), FontFactory.getFont(
							FontFactory.TIMES_ROMAN, 9)));
				}
				document.add(table);
				i++;
				document.newPage();
			}
			document.close();
		} else if (reportName.equals("TopKeywordsSearchReport"))
		{
			List<TopKeywordsSearch> topKeywordsSearchesList = (List<TopKeywordsSearch>) model
					.get("topKeywordsSearchesList");
			if (model.get("autocorrectto").equals("yes"))
			{
				Document document = new Document();
				PdfWriter.getInstance(document, new FileOutputStream(filePath
						+ "pages/reports/" + reportName + ".pdf"));
				document.open();
				Paragraph title = new Paragraph(
						"Top Keywords Searches Report\n\n",
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 20,
								Font.BOLD));
				title.setAlignment(Element.ALIGN_CENTER);
				document.add(title);
				document.add(new Paragraph((String) model.get("reportName")));
				document.add(new Paragraph((String) model.get("date") + "\n\n"));
				PdfPCell number = new PdfPCell(new Paragraph("#"));
				PdfPCell searchTerms = new PdfPCell(
						new Paragraph("SearchTerms"));
				PdfPCell autoCorrectto = new PdfPCell(new Paragraph(
						"AutoCorrectto"));
				PdfPCell requests = new PdfPCell(new Paragraph("Requests"));
				number.setBorder(Rectangle.NO_BORDER);
				searchTerms.setBorder(Rectangle.NO_BORDER);
				autoCorrectto.setBorder(Rectangle.NO_BORDER);
				requests.setBorder(Rectangle.NO_BORDER);
				float[] widths =
				{ 0.1f, 0.4f, 0.4f, 0.1f };
				PdfPTable table = new PdfPTable(widths);
				table.addCell(number);
				table.addCell(searchTerms);
				table.addCell(autoCorrectto);
				table.addCell(requests);
				for (int i = 0; i < topKeywordsSearchesList.size(); i++)
				{
					table.addCell(String.valueOf(i + 1));
					table.addCell(topKeywordsSearchesList.get(i)
							.getSearchTerms());
					table.addCell(topKeywordsSearchesList.get(i)
							.getAutoCorrectTo());
					table.addCell(String.valueOf(topKeywordsSearchesList.get(i)
							.getCount()));
				}
				document.add(table);
				document.close();
			} else
			{
				Document document = new Document();
				PdfWriter.getInstance(document, new FileOutputStream(filePath
						+ "pages/reports/" + reportName + ".pdf"));
				document.open();
				Paragraph title = new Paragraph(
						"Top Keywords Searches Report\n\n",
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 20,
								Font.BOLD));
				title.setAlignment(Element.ALIGN_CENTER);
				document.add(title);
				document.add(new Paragraph((String) model.get("reportName")));
				document.add(new Paragraph((String) model.get("date") + "\n\n"));
				PdfPCell number = new PdfPCell(new Paragraph("#"));
				PdfPCell searchTerms = new PdfPCell(
						new Paragraph("SearchTerms"));
				PdfPCell requests = new PdfPCell(new Paragraph("Requests"));
				number.setBorder(Rectangle.NO_BORDER);
				searchTerms.setBorder(Rectangle.NO_BORDER);
				requests.setBorder(Rectangle.NO_BORDER);
				float[] widths =
				{ 0.1f, 0.6f, 0.3f };
				PdfPTable table = new PdfPTable(widths);
				table.addCell(number);
				table.addCell(searchTerms);
				table.addCell(requests);
				for (int i = 0; i < topKeywordsSearchesList.size(); i++)
				{
					table.addCell(String.valueOf(i + 1));
					table.addCell(topKeywordsSearchesList.get(i)
							.getSearchTerms());
					table.addCell(String.valueOf(topKeywordsSearchesList.get(i)
							.getCount()));
				}
				document.add(table);
				document.close();
			}
		} else if (reportName.equals("UserTrendReport"))
		{
			HashMap<String, Object> trend = (HashMap<String, Object>) model
					.get("trend");
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(filePath
					+ "pages/reports/" + reportName + ".pdf"));
			document.open();
			Paragraph title = new Paragraph("User Trend Report\n\n",
					FontFactory.getFont(FontFactory.TIMES_ROMAN, 20, Font.BOLD));
			title.setAlignment(Element.ALIGN_CENTER);
			document.add(title);
			document.add(new Paragraph((String) model.get("reportName")));
			document.add(new Paragraph((String) model.get("reportName1")));
			document.add(new Paragraph((String) model.get("date") + "\n\n"));
			Image image = Image.getInstance(filePath + "pages/temp/trend.jpg");
			image.scaleAbsolute(410, 210);
			float[] widths =
			{ 0.8f, 0.2f };
			PdfPTable table = new PdfPTable(widths);
			PdfPCell chart = new PdfPCell(image);
			chart.setColspan(2);
			chart.setBorder(Rectangle.NO_BORDER);
			table.addCell(chart);

			PdfPCell weeks = new PdfPCell(new Paragraph("Weeks"));
			PdfPCell requests = new PdfPCell(new Paragraph("Requests"));

			weeks.setBorder(Rectangle.NO_BORDER);
			requests.setBorder(Rectangle.NO_BORDER);

			table.addCell(weeks);
			table.addCell(requests);
			for (String key : trend.keySet())
			{
				table.addCell(key);
				table.addCell(String.valueOf(trend.get(key)));
			}
			document.add(table);
			document.close();
		} else if (reportName.equals("KeywordSearchReport"))
		{
			ArrayList<Unit> keywordSearchArrayList = (ArrayList<Unit>) model
					.get("keywordSearchArrayListIndex");
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(filePath
					+ "pages/reports/" + reportName + ".pdf"));
			document.open();
			Paragraph title = new Paragraph("Keyword Search Report\n\n",
					FontFactory.getFont(FontFactory.TIMES_ROMAN, 20, Font.BOLD));
			title.setAlignment(Element.ALIGN_CENTER);
			document.add(title);
			document.add(new Paragraph((String) model.get("reportName")));
			document.add(new Paragraph((String) model.get("date") + "\n\n"));
			PdfPCell number = new PdfPCell(new Paragraph("#"));
			PdfPCell RecordsUnit = new PdfPCell(new Paragraph(
					"Records Unit[Min - Max]"));
			PdfPCell TotalCounts = new PdfPCell(new Paragraph(
					"Total Counts Of Searchterms"));
			number.setBorder(Rectangle.NO_BORDER);
			RecordsUnit.setBorder(Rectangle.NO_BORDER);
			TotalCounts.setBorder(Rectangle.NO_BORDER);
			float[] widths =
			{ 0.1f, 0.6f, 0.3f };
			PdfPTable table = new PdfPTable(widths);
			table.addCell(number);
			table.addCell(RecordsUnit);
			table.addCell(TotalCounts);
			for (int i = 0; i < keywordSearchArrayList.size(); i++)
			{
				table.addCell(String.valueOf(i + 1));
				table.addCell("[" + keywordSearchArrayList.get(i).getMin()
						+ " - " + keywordSearchArrayList.get(i).getMax() + "]");
				table.addCell(String.valueOf(keywordSearchArrayList.get(i)
						.getValue()));
			}
			document.add(table);
			document.close();
		}

	}

}
