package reports;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import reportDataObjects.SessionByMinutes;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import net.sf.jasperreports.engine.JRException;

/**
 * This class is used to generate PDF, CSV, XLS reports
 * 
 */
public class SessionByMinutesReport
{
	/**
	 * Creates reports in PDF, CSV and XLS formats then stores them to a
	 * specified path. All the params are passed from the FormController
	 * 
	 * @param filePath
	 *            the path to store the generated reports
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 */
	public static void createReports(String filePath,
			HashMap<String, Object> model, String reportName)
	{
		try
		{
			createPDF(filePath, model, reportName);
			createCSV(filePath, model, reportName);
			createXLS(filePath, model, reportName);
		} catch (Exception e)
		{

		}

	}

	/**
	 * Creates XLS report
	 * 
	 * @param filePath
	 *            the path to store a generated XLS report
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 * @throws IOException
	 */
	@SuppressWarnings(
	{ "unchecked", "deprecation" })
	private static void createXLS(String filePath,
			HashMap<String, Object> model, String reportName)
			throws IOException
	{
		if (reportName.equals("SessionByMinutesReport"))
		{
			ArrayList<SessionByMinutes> rbh = (ArrayList<SessionByMinutes>) model
					.get("ArrayListResultIndex");
			if (rbh != null)
			{
				String title = "Session By Minutes Report";
				HSSFWorkbook workbook = new HSSFWorkbook();
				HSSFSheet sheet = workbook.createSheet(title);
				// default column width
				sheet.setDefaultColumnWidth(30);
				// style for headers
				HSSFCellStyle style = workbook.createCellStyle();
				// text align center
				style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
				// colour
				style.setFillForegroundColor(HSSFColor.YELLOW.index);
				style.setFillBackgroundColor(HSSFColor.WHITE.index);
				style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				// border width
				style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
				style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
				style.setBorderRight(HSSFCellStyle.BORDER_THIN);
				style.setBorderTop(HSSFCellStyle.BORDER_THIN);
				// font
				HSSFFont font = workbook.createFont();
				font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				font.setColor(HSSFColor.VIOLET.index);
				font.setFontHeightInPoints((short) 12);
				// set font
				style.setFont(font);

				// body style
				HSSFCellStyle bodyStyle = workbook.createCellStyle();
				bodyStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				// font
				HSSFFont bodyFont = workbook.createFont();
				bodyFont.setFontHeightInPoints((short) 12);
				// set font
				bodyStyle.setFont(bodyFont);
				int index = 0;
				HSSFRow row = sheet.createRow(index);
				HSSFCell cell = null;
				cell = row.createCell(0);
				cell.setCellStyle(style);
				HSSFRichTextString text = new HSSFRichTextString(
						(String) model.get("reportName"));
				cell.setCellValue(text);
				index++;
				HSSFRow dateRow = sheet.createRow(index);
				cell = dateRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				HSSFRichTextString date = new HSSFRichTextString(
						(String) model.get("date"));
				cell.setCellValue(date);
				index++;
				HSSFRow tableHeaderRow = sheet.createRow(index);
				String[] tableHeaders = new String[]
				{ "Minutes", "Number Of Sessions" };
				for (short m = 0; m < tableHeaders.length; m++)
				{
					cell = tableHeaderRow.createCell(m);
					cell.setCellStyle(style);
					HSSFRichTextString tabelHeader = new HSSFRichTextString(
							tableHeaders[m]);
					cell.setCellValue(tabelHeader);
				}
				index++;
				for (int n = 0; n < rbh.size(); n++)
				{
					HSSFRow tableRow = sheet.createRow(index);
					cell = tableRow.createCell(0);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue((String) rbh.get(n).getMinutes()
							.toString());
					cell = tableRow.createCell(1);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue((String) rbh.get(n).getNumberOfSessions()
							.toString());
					index++;
				}

				FileOutputStream fos = new FileOutputStream(filePath
						+ "pages/reports/" + reportName + ".xls");
				workbook.write(fos);
				fos.close();
			}
		}
	}

	/**
	 * Creates CSV report
	 * 
	 * @param filePath
	 *            the path to store a generated CSV report
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private static void createCSV(String filePath,
			HashMap<String, Object> model, String reportName)
			throws IOException
	{
		if (reportName.equals("SessionByMinutesReport"))
		{
			ArrayList<SessionByMinutes> rbh = (ArrayList<SessionByMinutes>) model
					.get("ArrayListResultIndex");
			if (rbh != null)
			{
				FileWriter writer = new FileWriter(filePath + "pages/reports/"
						+ reportName + ".csv");
				writer.append((String) model.get("reportName"));
				writer.append("\n");
				writer.append((String) model.get("date"));
				writer.append("\n");
				writer.append("Minutes");
				writer.append(",");
				writer.append("Number Of Sessions");
				writer.append("\n");
				int size = rbh.size();
				for (int i = 0; i < size; i++)
				{
					writer.append(String.valueOf(rbh.get(i).getMinutes()
							.toString()));
					writer.append(",");
					writer.append(String.valueOf(rbh.get(i)
							.getNumberOfSessions().toString()));
					writer.append(",");
					writer.append("\n");
				}
				writer.flush();
				writer.close();
			}
		}
	}

	/**
	 * Creates PDF report
	 * 
	 * @param filePath
	 *            the path to store a generated PDF report
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 * @throws JRException
	 * @throws DocumentException
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private static void createPDF(String filePath,
			HashMap<String, Object> model, String reportName)
			throws JRException, DocumentException, MalformedURLException,
			IOException
	{
		if (reportName.equals("SessionByMinutesReport"))
		{
			ArrayList<SessionByMinutes> rbh = (ArrayList<SessionByMinutes>) model
					.get("ArrayListResultIndex");
			if (rbh != null)
			{
				Document document = new Document();
				PdfWriter.getInstance(document, new FileOutputStream(filePath
						+ "pages/reports/" + reportName + ".pdf"));
				document.open();
				Paragraph title = new Paragraph(
						"Session By Minutes Report\n\n", FontFactory.getFont(
								FontFactory.TIMES_ROMAN, 20, Font.BOLD));
				title.setAlignment(Element.ALIGN_CENTER);
				document.add(title);
				document.add(new Paragraph((String) model.get("reportName")));
				document.add(new Paragraph((String) model.get("date") + "\n\n"));
				float[] widths =
				{ 0.2f, 0.2f };
				PdfPTable table = new PdfPTable(widths);

				PdfPCell minutes = new PdfPCell(new Paragraph("Minutes"));
				PdfPCell numberofsessions = new PdfPCell(new Paragraph(
						"Number Of Sessions"));

				minutes.setBorder(Rectangle.NO_BORDER);
				numberofsessions.setBorder(Rectangle.NO_BORDER);

				table.addCell(minutes);
				table.addCell(numberofsessions);
				for (int i = 0; i < rbh.size(); i++)
				{
					table.addCell(String.valueOf(rbh.get(i).getMinutes()
							.toString()));
					table.addCell(String.valueOf(rbh.get(i)
							.getNumberOfSessions().toString()));
				}
				document.add(table);
				document.close();
			}
		}
	}
}
