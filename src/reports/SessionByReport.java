package reports;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import reportDataObjects.SessionBy;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import net.sf.jasperreports.engine.JRException;

public class SessionByReport
{

	/**
	 * Creates reports in PDF, CSV and XLS formats then stores them to a
	 * specified path. All the params are passed from the FormController
	 * 
	 * @param filePath
	 *            the path to store the generated reports
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 */
	public static void createReports(String filePath,
			HashMap<String, Object> model, String reportName)
	{
		try
		{
			System.out.println("Create Reports");
			createPDF(filePath, model, reportName);
			createCSV(filePath, model, reportName);
			createXLS(filePath, model, reportName);
		} catch (Exception e)
		{

		}

	}

	/**
	 * Creates XLS report
	 * 
	 * @param filePath
	 *            the path to store a generated XLS report
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 * @throws IOException
	 */
	@SuppressWarnings(
	{ "unchecked", "deprecation" })
	private static void createXLS(String filePath,
			HashMap<String, Object> model, String reportName)
			throws IOException
	{
		// TODO Auto-generated method stub
		if (reportName.equals("SessionByReport"))
		{
			ArrayList<SessionBy> sb = (ArrayList<SessionBy>) model
					.get("sessionByList");
			ArrayList<SessionBy> sb1 = (ArrayList<SessionBy>) model
					.get("sessionByList1");
			if (sb1 == null)
			{
				String title = "Session By Report";
				HSSFWorkbook workbook = new HSSFWorkbook();
				HSSFSheet sheet = workbook.createSheet(title);
				// default column width
				sheet.setDefaultColumnWidth(30);
				// style for headers
				HSSFCellStyle style = workbook.createCellStyle();
				// text align center
				style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
				// colour
				style.setFillForegroundColor(HSSFColor.YELLOW.index);
				style.setFillBackgroundColor(HSSFColor.WHITE.index);
				style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				// border width
				style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
				style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
				style.setBorderRight(HSSFCellStyle.BORDER_THIN);
				style.setBorderTop(HSSFCellStyle.BORDER_THIN);
				// font
				HSSFFont font = workbook.createFont();
				font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				font.setColor(HSSFColor.VIOLET.index);
				font.setFontHeightInPoints((short) 12);
				// set font
				style.setFont(font);

				// body style
				HSSFCellStyle bodyStyle = workbook.createCellStyle();
				bodyStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				// font
				HSSFFont bodyFont = workbook.createFont();
				bodyFont.setFontHeightInPoints((short) 12);
				// set font
				bodyStyle.setFont(bodyFont);
				int index = 0;
				HSSFRow row = sheet.createRow(index);
				HSSFCell cell = null;
				cell = row.createCell(0);
				cell.setCellStyle(style);
				HSSFRichTextString text = new HSSFRichTextString(
						(String) model.get("reportName"));
				cell.setCellValue(text);
				index++;
				HSSFRow dateRow = sheet.createRow(index);
				cell = dateRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				HSSFRichTextString date = new HSSFRichTextString(
						(String) model.get("date"));
				cell.setCellValue(date);
				index++;
				HSSFRow tableHeaderRow = sheet.createRow(index);
				String[] tableHeaders = new String[]
				{ "Time", "Sessions" };
				for (short m = 0; m < tableHeaders.length; m++)
				{
					cell = tableHeaderRow.createCell(m);
					cell.setCellStyle(style);
					HSSFRichTextString tabelHeader = new HSSFRichTextString(
							tableHeaders[m]);
					cell.setCellValue(tabelHeader);
				}
				index++;
				for (int n = 0; n < sb.size(); n++)
				{
					HSSFRow tableRow = sheet.createRow(index);
					cell = tableRow.createCell(0);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue((String) sb.get(n).getTime());
					cell = tableRow.createCell(1);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue((Integer) sb.get(n).getSessions());
					index++;
				}
				HSSFRow totalRow = sheet.createRow(index);
				cell = totalRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue("Total Sessions");
				cell = totalRow.createCell(1);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue((Integer) model.get("totalSessions"));

				FileOutputStream fos = new FileOutputStream(filePath
						+ "pages/reports/" + reportName + ".xls");
				workbook.write(fos);
				fos.close();
			} else
			{
				String title = "Session By Report";
				HSSFWorkbook workbook = new HSSFWorkbook();
				HSSFSheet sheet = workbook.createSheet(title);
				// default column width
				sheet.setDefaultColumnWidth(30);
				// style for headers
				HSSFCellStyle style = workbook.createCellStyle();
				// text align center
				style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
				// colour
				style.setFillForegroundColor(HSSFColor.YELLOW.index);
				style.setFillBackgroundColor(HSSFColor.WHITE.index);
				style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				// border width
				style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
				style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
				style.setBorderRight(HSSFCellStyle.BORDER_THIN);
				style.setBorderTop(HSSFCellStyle.BORDER_THIN);
				// font
				HSSFFont font = workbook.createFont();
				font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				font.setColor(HSSFColor.VIOLET.index);
				font.setFontHeightInPoints((short) 12);
				// set font
				style.setFont(font);

				// body style
				HSSFCellStyle bodyStyle = workbook.createCellStyle();
				bodyStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				// font
				HSSFFont bodyFont = workbook.createFont();
				bodyFont.setFontHeightInPoints((short) 12);
				// set font
				bodyStyle.setFont(bodyFont);
				int index = 0;
				HSSFRow row = sheet.createRow(index);
				HSSFCell cell = null;
				cell = row.createCell(0);
				cell.setCellStyle(style);
				HSSFRichTextString text = new HSSFRichTextString(
						(String) model.get("reportName"));
				cell.setCellValue(text);
				index++;
				HSSFRow dateRow = sheet.createRow(index);
				cell = dateRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				HSSFRichTextString date = new HSSFRichTextString(
						(String) model.get("date"));
				cell.setCellValue(date);
				index++;
				HSSFRow tableHeaderRow = sheet.createRow(index);
				String[] tableHeaders = new String[]
				{ "1st Times", "1st Sessions", "2nd Times", "2nd Sessions" };
				for (short m = 0; m < tableHeaders.length; m++)
				{
					cell = tableHeaderRow.createCell(m);
					cell.setCellStyle(style);
					HSSFRichTextString tabelHeader = new HSSFRichTextString(
							tableHeaders[m]);
					cell.setCellValue(tabelHeader);
				}
				index++;

				int size = 0;
				if (sb.size() > sb1.size())
				{
					size = sb.size();
				} else
				{
					size = sb1.size();
				}
				for (int i = 0; i < size; i++)
				{
					HSSFRow tableRow = sheet.createRow(index);
					if (sb.size() > i)
					{
						cell = tableRow.createCell(0);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue((String) sb.get(i).getTime());
						cell = tableRow.createCell(1);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue((Integer) sb.get(i).getSessions());
					} else
					{
						cell = tableRow.createCell(0);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue(" ");
						cell = tableRow.createCell(1);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue(" ");
					}
					if (sb1.size() > i)
					{
						cell = tableRow.createCell(2);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue((String) sb1.get(i).getTime());
						cell = tableRow.createCell(3);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue((Integer) sb1.get(i).getSessions());
					} else
					{
						cell = tableRow.createCell(2);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue(" ");
						cell = tableRow.createCell(3);
						cell.setCellStyle(bodyStyle);
						cell.setCellValue(" ");
					}
					index++;
				}

				HSSFRow totalRow = sheet.createRow(index);
				cell = totalRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue("Total Sessions");
				cell = totalRow.createCell(1);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue((Integer) model.get("totalSessions"));
				cell = totalRow.createCell(2);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue("Total Sessions");
				cell = totalRow.createCell(3);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue((Integer) model.get("totalSessions1"));

				FileOutputStream fos = new FileOutputStream(filePath
						+ "pages/reports/" + reportName + ".xls");
				workbook.write(fos);
				fos.close();
			}
		}
	}

	/**
	 * Create the pdf form report.
	 * 
	 * @param filePath
	 *            the path of the report where it locates.
	 * @param model
	 *            the hashmap which stored all the data for creating the report.
	 * @param reportName
	 *            the name of the report.
	 * @throws JRException
	 * @throws DocumentException
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private static void createPDF(String filePath,
			HashMap<String, Object> model, String reportName)
			throws JRException, DocumentException, MalformedURLException,
			IOException
	{
		if (reportName.equals("SessionByReport"))
		{
			System.out.println("Create PDF Reports");
			ArrayList<SessionBy> sbh = (ArrayList<SessionBy>) model
					.get("sessionByList");
			ArrayList<SessionBy> sbh1 = (ArrayList<SessionBy>) model
					.get("sessionByList1");
			if (sbh1 == null)
			{
				Document document = new Document();
				PdfWriter.getInstance(document, new FileOutputStream(filePath
						+ "pages/reports/" + reportName + ".pdf"));
				document.open();
				Paragraph title = new Paragraph("Session By Report\n\n",
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 20,
								Font.BOLD));
				title.setAlignment(Element.ALIGN_CENTER);
				document.add(title);
				document.add(new Paragraph((String) model.get("reportName")));
				document.add(new Paragraph((String) model.get("date") + "\n\n"));
				Image image = Image.getInstance(filePath
						+ "pages/temp/barchart.jpg");
				image.scaleAbsolute(410, 210);
				float[] widths =
				{ 0.8f, 0.2f };
				PdfPTable table = new PdfPTable(widths);
				PdfPCell chart = new PdfPCell(image);
				chart.setColspan(2);
				chart.setBorder(Rectangle.NO_BORDER);
				table.addCell(chart);

				PdfPCell hours = new PdfPCell(new Paragraph(
						(String) model.get("th1")));
				PdfPCell sessions = new PdfPCell(new Paragraph(
						(String) model.get("th2")));

				hours.setBorder(Rectangle.NO_BORDER);
				sessions.setBorder(Rectangle.NO_BORDER);

				table.addCell(hours);
				table.addCell(sessions);
				for (int i = 0; i < sbh.size(); i++)
				{
					table.addCell(String.valueOf(sbh.get(i).getTime()));
					table.addCell(String.valueOf(sbh.get(i).getSessions()));
				}
				table.addCell("Total Sessions");
				table.addCell(String.valueOf(model.get("totalSessions")));
				document.add(table);
				document.close();
			} else
			{
				Document document = new Document();
				PdfWriter.getInstance(document, new FileOutputStream(filePath
						+ "pages/reports/" + reportName + ".pdf"));
				document.open();
				Paragraph title = new Paragraph("Session By Hour Report\n\n",
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 20,
								Font.BOLD));
				title.setAlignment(Element.ALIGN_CENTER);
				document.add(title);
				document.add(new Paragraph((String) model.get("reportName")));
				document.add(new Paragraph((String) model.get("date") + "\n\n"));
				Image image = Image.getInstance(filePath
						+ "pages/temp/barchart.jpg");
				image.scaleAbsolute(410, 210);
				float[] widths =
				{ 0.25f, 0.25f, 0.25f, 0.25f };
				PdfPTable table = new PdfPTable(widths);
				PdfPCell chart = new PdfPCell(image);
				chart.setColspan(4);
				chart.setBorder(Rectangle.NO_BORDER);
				table.addCell(chart);

				PdfPCell hours = new PdfPCell(new Paragraph(
						(String) model.get("th1")));
				PdfPCell sessions = new PdfPCell(new Paragraph(
						(String) model.get("th2")));
				PdfPCell hours1 = new PdfPCell(new Paragraph(
						(String) model.get("th3")));
				PdfPCell sessions1 = new PdfPCell(new Paragraph(
						(String) model.get("th4")));

				hours.setBorder(Rectangle.NO_BORDER);
				sessions.setBorder(Rectangle.NO_BORDER);
				hours1.setBorder(Rectangle.NO_BORDER);
				sessions1.setBorder(Rectangle.NO_BORDER);

				table.addCell(hours);
				table.addCell(sessions);
				table.addCell(hours1);
				table.addCell(sessions1);
				int size = 0;
				if (sbh.size() > sbh1.size())
				{
					size = sbh.size();
				} else
				{
					size = sbh1.size();
				}
				for (int i = 0; i < size; i++)
				{
					if (sbh.size() > i)
					{
						table.addCell(String.valueOf(sbh.get(i).getTime()));
						table.addCell(String.valueOf(sbh.get(i).getSessions()));
					} else
					{
						table.addCell(" ");
						table.addCell(" ");
					}
					if (sbh1.size() > i)
					{
						table.addCell(String.valueOf(sbh1.get(i).getTime()));
						table.addCell(String.valueOf(sbh1.get(i).getSessions()));
					} else
					{
						table.addCell(" ");
						table.addCell(" ");
					}

				}

				table.addCell("Total Sessions");
				table.addCell(String.valueOf(model.get("totalSessions")));
				table.addCell("Total Sessions");
				table.addCell(String.valueOf(model.get("totalSessions1")));
				document.add(table);
				document.close();
			}
		}
	}

	/**
	 * Create the csv report.
	 * 
	 * @param filePath
	 *            the path of the report where it locates after the generation.
	 * @param model
	 *            the hashmap stores all the data that used for creating the
	 *            object.
	 * @param reportName
	 *            the name of the report.
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private static void createCSV(String filePath,
			HashMap<String, Object> model, String reportName)
			throws IOException
	{
		// TODO Auto-generated method stub
		if (reportName.equals("SessionByReport"))
		{
			ArrayList<SessionBy> sbh = (ArrayList<SessionBy>) model
					.get("sessionByList");
			ArrayList<SessionBy> sbh1 = (ArrayList<SessionBy>) model
					.get("sessionByList1");
			if (sbh1 == null)
			{
				FileWriter writer = new FileWriter(filePath + "pages/reports/"
						+ reportName + ".csv");
				writer.append((String) model.get("reportName"));
				writer.append("\n");
				writer.append((String) model.get("date"));
				writer.append("\n");
				writer.append((String) model.get("th1"));
				writer.append(",");
				writer.append((String) model.get("th2"));
				writer.append("\n");
				for (int m = 0; m < sbh.size(); m++)
				{
					writer.append(String.valueOf(sbh.get(m).getTime()));
					writer.append(",");
					writer.append(String.valueOf(sbh.get(m).getSessions()));
					writer.append("\n");
				}
				writer.append("Total Sessions");
				writer.append(",");
				writer.append(String.valueOf(model.get("totalSessions")));
				writer.append("\n");
				writer.flush();
				writer.close();
			} else
			{
				FileWriter writer = new FileWriter(filePath + "pages/reports/"
						+ reportName + ".csv");
				writer.append((String) model.get("reportName"));
				writer.append("\n");
				writer.append((String) model.get("date"));
				writer.append("\n");
				writer.append((String) model.get("th1"));
				writer.append(",");
				writer.append((String) model.get("th2"));
				writer.append(",");
				writer.append((String) model.get("th3"));
				writer.append(",");
				writer.append((String) model.get("th4"));
				writer.append("\n");
				int size = 0;
				if (sbh.size() > sbh1.size())
				{
					size = sbh.size();
				} else
				{
					size = sbh1.size();
				}
				for (int i = 0; i < size; i++)
				{
					if (sbh.size() > i)
					{
						writer.append(String.valueOf(sbh.get(i).getTime()));
						writer.append(",");
						writer.append(String.valueOf(sbh.get(i).getSessions()));
						writer.append(",");
					} else
					{
						writer.append(" ");
						writer.append(",");
						writer.append(" ");
						writer.append(",");
					}
					if (sbh1.size() > i)
					{
						writer.append(String.valueOf(sbh1.get(i).getTime()));
						writer.append(",");
						writer.append(String.valueOf(sbh1.get(i).getSessions()));
						writer.append("\n");
					} else
					{
						writer.append(" ");
						writer.append(",");
						writer.append(" ");
						writer.append("\n");
					}

				}
				writer.append("Total Sessions");
				writer.append(",");
				writer.append(String.valueOf(model.get("totalSessions")));
				writer.append(",");
				writer.append("Total Sessions");
				writer.append(",");
				writer.append(String.valueOf(model.get("totalSessions1")));
				writer.append("\n");
				writer.flush();
				writer.close();
			}
		}
	}
}
