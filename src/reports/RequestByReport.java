package reports;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import reportDataObjects.RequestBy;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import net.sf.jasperreports.engine.JRException;

/**
 * This class is used to generate PDF, CSV, XLS reports
 * 
 */
public class RequestByReport
{
	/**
	 * Creates reports in PDF, CSV and XLS formats then stores them to a
	 * specified path. All the params are passed from the FormController
	 * 
	 * @param filePath
	 *            the path to store the generated reports
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 */
	public static void createReports(String filePath,
			HashMap<String, Object> model, String reportName)
	{
		try
		{
			createPDF(filePath, model, reportName);
			createCSV(filePath, model, reportName);
			createXLS(filePath, model, reportName);
		} catch (Exception e)
		{

		}

	}

	/**
	 * Creates XLS report
	 * 
	 * @param filePath
	 *            the path to store a generated XLS report
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 * @throws IOException
	 */
	@SuppressWarnings(
	{ "unchecked", "deprecation" })
	private static void createXLS(String filePath,
			HashMap<String, Object> model, String reportName)
			throws IOException
	{

		ArrayList<RequestBy> rbh = (ArrayList<RequestBy>) model
				.get("requestByList");
		ArrayList<RequestBy> rbh1 = (ArrayList<RequestBy>) model
				.get("requestByList1");
		if (rbh1 == null)
		{
			String title = reportName;
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet(title);
			// default column width
			sheet.setDefaultColumnWidth(30);
			// style for headers
			HSSFCellStyle style = workbook.createCellStyle();
			// text align center
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			// colour
			style.setFillForegroundColor(HSSFColor.YELLOW.index);
			style.setFillBackgroundColor(HSSFColor.WHITE.index);
			style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			// border width
			style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			style.setBorderRight(HSSFCellStyle.BORDER_THIN);
			style.setBorderTop(HSSFCellStyle.BORDER_THIN);
			// font
			HSSFFont font = workbook.createFont();
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			font.setColor(HSSFColor.VIOLET.index);
			font.setFontHeightInPoints((short) 12);
			// set font
			style.setFont(font);

			// body style
			HSSFCellStyle bodyStyle = workbook.createCellStyle();
			bodyStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
			// font
			HSSFFont bodyFont = workbook.createFont();
			bodyFont.setFontHeightInPoints((short) 12);
			// set font
			bodyStyle.setFont(bodyFont);
			int index = 0;
			HSSFRow row = sheet.createRow(index);
			HSSFCell cell = null;
			cell = row.createCell(0);
			cell.setCellStyle(style);
			HSSFRichTextString text = new HSSFRichTextString(
					(String) model.get("reportName"));
			cell.setCellValue(text);
			index++;
			HSSFRow dateRow = sheet.createRow(index);
			cell = dateRow.createCell(0);
			cell.setCellStyle(bodyStyle);
			HSSFRichTextString date = new HSSFRichTextString(
					(String) model.get("date"));
			cell.setCellValue(date);
			index++;
			HSSFRow tableHeaderRow = sheet.createRow(index);
			String[] tableHeaders = new String[]
			{ "Hours", "Requests" };
			for (short m = 0; m < tableHeaders.length; m++)
			{
				cell = tableHeaderRow.createCell(m);
				cell.setCellStyle(style);
				HSSFRichTextString tabelHeader = new HSSFRichTextString(
						tableHeaders[m]);
				cell.setCellValue(tabelHeader);
			}
			index++;
			for (int n = 0; n < rbh.size(); n++)
			{
				HSSFRow tableRow = sheet.createRow(index);
				cell = tableRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue((String) rbh.get(n).getTime());
				cell = tableRow.createCell(1);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue((Integer) rbh.get(n).getRequests());
				index++;
			}
			HSSFRow totalRow = sheet.createRow(index);
			cell = totalRow.createCell(0);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue("Total Requests");
			cell = totalRow.createCell(1);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue((Integer) model.get("totalRequests"));

			FileOutputStream fos = new FileOutputStream(filePath
					+ "pages/reports/" + reportName + ".xls");
			workbook.write(fos);
			fos.close();
		} else
		{
			String title = reportName;
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet(title);
			// default column width
			sheet.setDefaultColumnWidth(30);
			// style for headers
			HSSFCellStyle style = workbook.createCellStyle();
			// text align center
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			// colour
			style.setFillForegroundColor(HSSFColor.YELLOW.index);
			style.setFillBackgroundColor(HSSFColor.WHITE.index);
			style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			// border width
			style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			style.setBorderRight(HSSFCellStyle.BORDER_THIN);
			style.setBorderTop(HSSFCellStyle.BORDER_THIN);
			// font
			HSSFFont font = workbook.createFont();
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			font.setColor(HSSFColor.VIOLET.index);
			font.setFontHeightInPoints((short) 12);
			// set font
			style.setFont(font);

			// body style
			HSSFCellStyle bodyStyle = workbook.createCellStyle();
			bodyStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
			// font
			HSSFFont bodyFont = workbook.createFont();
			bodyFont.setFontHeightInPoints((short) 12);
			// set font
			bodyStyle.setFont(bodyFont);
			int index = 0;
			HSSFRow row = sheet.createRow(index);
			HSSFCell cell = null;
			cell = row.createCell(0);
			cell.setCellStyle(style);
			HSSFRichTextString text = new HSSFRichTextString(
					(String) model.get("reportName"));
			cell.setCellValue(text);
			index++;
			HSSFRow dateRow = sheet.createRow(index);
			cell = dateRow.createCell(0);
			cell.setCellStyle(bodyStyle);
			HSSFRichTextString date = new HSSFRichTextString(
					(String) model.get("date"));
			cell.setCellValue(date);
			index++;
			HSSFRow tableHeaderRow = sheet.createRow(index);
			String[] tableHeaders = new String[]
			{ "1st Hours", "1st Requests", "2nd Hours", "2nd Requests" };
			for (short m = 0; m < tableHeaders.length; m++)
			{
				cell = tableHeaderRow.createCell(m);
				cell.setCellStyle(style);
				HSSFRichTextString tabelHeader = new HSSFRichTextString(
						tableHeaders[m]);
				cell.setCellValue(tabelHeader);
			}
			index++;

			int size = 0;
			if (rbh.size() > rbh1.size())
			{
				size = rbh.size();
			} else
			{
				size = rbh1.size();
			}
			for (int i = 0; i < size; i++)
			{
				HSSFRow tableRow = sheet.createRow(index);
				if (rbh.size() > i)
				{
					cell = tableRow.createCell(0);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue((String) rbh.get(i).getTime());
					cell = tableRow.createCell(1);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue((Integer) rbh.get(i).getRequests());
				} else
				{
					cell = tableRow.createCell(0);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue(" ");
					cell = tableRow.createCell(1);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue(" ");
				}
				if (rbh1.size() > i)
				{
					cell = tableRow.createCell(2);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue((String) rbh1.get(i).getTime());
					cell = tableRow.createCell(3);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue((Integer) rbh1.get(i).getRequests());
				} else
				{
					cell = tableRow.createCell(2);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue(" ");
					cell = tableRow.createCell(3);
					cell.setCellStyle(bodyStyle);
					cell.setCellValue(" ");
				}
				index++;
			}

			HSSFRow totalRow = sheet.createRow(index);
			cell = totalRow.createCell(0);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue("Total Requests");
			cell = totalRow.createCell(1);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue((Integer) model.get("totalRequests"));
			cell = totalRow.createCell(2);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue("Total Requests");
			cell = totalRow.createCell(3);
			cell.setCellStyle(bodyStyle);
			cell.setCellValue((Integer) model.get("totalRequests1"));

			FileOutputStream fos = new FileOutputStream(filePath
					+ "pages/reports/" + reportName + ".xls");
			workbook.write(fos);
			fos.close();
		}
	}

	/**
	 * Creates CSV report
	 * 
	 * @param filePath
	 *            the path to store a generated CSV report
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private static void createCSV(String filePath,
			HashMap<String, Object> model, String reportName)
			throws IOException
	{
		ArrayList<RequestBy> rbh = (ArrayList<RequestBy>) model
				.get("requestByList");
		ArrayList<RequestBy> rbh1 = (ArrayList<RequestBy>) model
				.get("requestByList1");
		if (rbh1 == null)
		{
			FileWriter writer = new FileWriter(filePath + "pages/reports/"
					+ reportName + ".csv");
			writer.append((String) model.get("reportName"));
			writer.append("\n");
			writer.append((String) model.get("date"));
			writer.append("\n");
			writer.append((String) model.get("th1"));
			writer.append(",");
			writer.append((String) model.get("th2"));
			writer.append("\n");
			for (int m = 0; m < rbh.size(); m++)
			{
				writer.append(String.valueOf(rbh.get(m).getTime()));
				writer.append(",");
				writer.append(String.valueOf(rbh.get(m).getRequests()));
				writer.append("\n");
			}
			writer.append("Total Requests");
			writer.append(",");
			writer.append(String.valueOf(model.get("totalRequests")));
			writer.append("\n");
			writer.flush();
			writer.close();
		} else
		{
			FileWriter writer = new FileWriter(filePath + "pages/reports/"
					+ reportName + ".csv");
			writer.append((String) model.get("reportName"));
			writer.append("\n");
			writer.append((String) model.get("date"));
			writer.append("\n");
			writer.append((String) model.get("th1"));
			writer.append(",");
			writer.append((String) model.get("th2"));
			writer.append(",");
			writer.append((String) model.get("th3"));
			writer.append(",");
			writer.append((String) model.get("th4"));
			writer.append("\n");
			int size = 0;
			if (rbh.size() > rbh1.size())
			{
				size = rbh.size();
			} else
			{
				size = rbh1.size();
			}
			for (int i = 0; i < size; i++)
			{
				if (rbh.size() > i)
				{
					writer.append(String.valueOf(rbh.get(i).getTime()));
					writer.append(",");
					writer.append(String.valueOf(rbh.get(i).getRequests()));
					writer.append(",");
				} else
				{
					writer.append(" ");
					writer.append(",");
					writer.append(" ");
					writer.append(",");
				}
				if (rbh1.size() > i)
				{
					writer.append(String.valueOf(rbh1.get(i).getTime()));
					writer.append(",");
					writer.append(String.valueOf(rbh1.get(i).getRequests()));
					writer.append("\n");
				} else
				{
					writer.append(" ");
					writer.append(",");
					writer.append(" ");
					writer.append("\n");
				}

			}
			writer.append("Total Requests");
			writer.append(",");
			writer.append(String.valueOf(model.get("totalRequests")));
			writer.append(",");
			writer.append("Total Requests");
			writer.append(",");
			writer.append(String.valueOf(model.get("totalRequests1")));
			writer.append("\n");
			writer.flush();
			writer.close();
		}
	}

	/**
	 * Creates PDF report
	 * 
	 * @param filePath
	 *            the path to store a generated PDF report
	 * @param model
	 *            a hashmap contains all the report data
	 * @param reportName
	 *            the name of the report
	 * @throws JRException
	 * @throws DocumentException
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private static void createPDF(String filePath,
			HashMap<String, Object> model, String reportName)
			throws JRException, DocumentException, MalformedURLException,
			IOException
	{
		ArrayList<RequestBy> rbh = (ArrayList<RequestBy>) model
				.get("requestByList");
		ArrayList<RequestBy> rbh1 = (ArrayList<RequestBy>) model
				.get("requestByList1");
		if (rbh1 == null)
		{
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(filePath
					+ "pages/reports/" + reportName + ".pdf"));
			document.open();
			Paragraph title = new Paragraph("Request By Report\n\n",
					FontFactory.getFont(FontFactory.TIMES_ROMAN, 20, Font.BOLD));
			title.setAlignment(Element.ALIGN_CENTER);
			document.add(title);
			document.add(new Paragraph((String) model.get("reportName")));
			document.add(new Paragraph((String) model.get("date") + "\n\n"));
			Image image = Image.getInstance(filePath
					+ "pages/temp/barchart.jpg");
			image.scaleAbsolute(410, 210);
			float[] widths =
			{ 0.8f, 0.2f };
			PdfPTable table = new PdfPTable(widths);
			PdfPCell chart = new PdfPCell(image);
			chart.setColspan(2);
			chart.setBorder(Rectangle.NO_BORDER);
			table.addCell(chart);

			PdfPCell hours = new PdfPCell(new Paragraph(
					(String) model.get("th1")));
			PdfPCell requests = new PdfPCell(new Paragraph(
					(String) model.get("th2")));

			hours.setBorder(Rectangle.NO_BORDER);
			requests.setBorder(Rectangle.NO_BORDER);

			table.addCell(hours);
			table.addCell(requests);
			for (int i = 0; i < rbh.size(); i++)
			{
				table.addCell(String.valueOf(rbh.get(i).getTime()));
				table.addCell(String.valueOf(rbh.get(i).getRequests()));
			}
			table.addCell("Total Requests");
			table.addCell(String.valueOf(model.get("totalRequests")));
			document.add(table);
			document.close();
		} else
		{
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(filePath
					+ "pages/reports/" + reportName + ".pdf"));
			document.open();
			Paragraph title = new Paragraph("Request By Hour Report\n\n",
					FontFactory.getFont(FontFactory.TIMES_ROMAN, 20, Font.BOLD));
			title.setAlignment(Element.ALIGN_CENTER);
			document.add(title);
			document.add(new Paragraph((String) model.get("reportName")));
			document.add(new Paragraph((String) model.get("date") + "\n\n"));
			Image image = Image.getInstance(filePath
					+ "pages/temp/barchart.jpg");
			image.scaleAbsolute(410, 210);
			float[] widths =
			{ 0.25f, 0.25f, 0.25f, 0.25f };
			PdfPTable table = new PdfPTable(widths);
			PdfPCell chart = new PdfPCell(image);
			chart.setColspan(4);
			chart.setBorder(Rectangle.NO_BORDER);
			table.addCell(chart);

			PdfPCell hours = new PdfPCell(new Paragraph(
					(String) model.get("th1")));
			PdfPCell requests = new PdfPCell(new Paragraph(
					(String) model.get("th2")));
			PdfPCell hours1 = new PdfPCell(new Paragraph(
					(String) model.get("th3")));
			PdfPCell requests1 = new PdfPCell(new Paragraph(
					(String) model.get("th4")));

			hours.setBorder(Rectangle.NO_BORDER);
			requests.setBorder(Rectangle.NO_BORDER);
			hours1.setBorder(Rectangle.NO_BORDER);
			requests1.setBorder(Rectangle.NO_BORDER);

			table.addCell(hours);
			table.addCell(requests);
			table.addCell(hours1);
			table.addCell(requests1);
			int size = 0;
			if (rbh.size() > rbh1.size())
			{
				size = rbh.size();
			} else
			{
				size = rbh1.size();
			}
			for (int i = 0; i < size; i++)
			{
				if (rbh.size() > i)
				{
					table.addCell(String.valueOf(rbh.get(i).getTime()));
					table.addCell(String.valueOf(rbh.get(i).getRequests()));
				} else
				{
					table.addCell(" ");
					table.addCell(" ");
				}
				if (rbh1.size() > i)
				{
					table.addCell(String.valueOf(rbh1.get(i).getTime()));
					table.addCell(String.valueOf(rbh1.get(i).getRequests()));
				} else
				{
					table.addCell(" ");
					table.addCell(" ");
				}

			}
			table.addCell("Total Requests");
			table.addCell(String.valueOf(model.get("totalRequests")));
			table.addCell("Total Requests");
			table.addCell(String.valueOf(model.get("totalRequests1")));
			document.add(table);
			document.close();
		}
	}
}
