package debug;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ClassDebug
{
	private static boolean bPrint = true;

	public static void print(String message)
	{
		if (ClassDebug.bPrint)
			System.out.println(message);
	}

	public static String getCurrentDateAndTime()
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}

	public static long getCurrentTime()
	{
		return System.currentTimeMillis();
	}

	public static String timeFormat(long time)
	{
		long milliseconds, seconds, minutes, hours;
		String strMilliseconds, strSeconds, strMinutes, strHours;

		hours = 0;
		minutes = 0;
		seconds = 0;
		milliseconds = 0;

		seconds = time / 1000;
		milliseconds = time - seconds * 1000;
		hours = seconds / 3600;
		seconds = seconds - hours * 3600;
		minutes = seconds / 60;
		seconds = seconds - minutes * 60;

		strHours = String.format("%02d", hours);
		strMinutes = String.format("%02d", minutes);
		strSeconds = String.format("%02d", seconds);
		strMilliseconds = String.format("%03d", milliseconds);

		return strHours + ":" + strMinutes + ":" + strSeconds + "."
				+ strMilliseconds;
	}

	public static int getTimeScope(String fromDate, String toDate)
	{
		DateFormat formatter;
		Date newerDate = null, olderDate = null;
		formatter = new SimpleDateFormat("dd/MM/yy");
		try
		{
			olderDate = (Date) formatter.parse(fromDate);
		} catch (ParseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try
		{
			newerDate = (Date) formatter.parse(toDate);
		} catch (ParseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int diffInDays = (int) ((newerDate.getTime() - olderDate.getTime()) / (1000 * 60 * 60 * 24)) + 2;
		return diffInDays - 1;
	}

}
