package DataMining;

import java.util.*;

public class StandardDeviation
{
	private boolean bias;

	public StandardDeviation(boolean bias)
	{
		this.bias = bias;
	}

	public double calculateStandardDeviation(ArrayList<Double> dataList,
			double mean)
	{
		double stdev;
		double Sum = 0;
		for (double d : dataList)
		{
			Sum += (d - mean) * (d - mean);
		}
		if (bias == true)
		{
			stdev = Math.sqrt(Sum / dataList.size());
		} else
		{
			stdev = Math.sqrt(Sum / (dataList.size() - 1));
		}
		return stdev;
	}
}
