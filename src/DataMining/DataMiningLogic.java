package DataMining;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import debug.ClassDebug;

import reportDataObjects.SimpleUserTrends;

/**
 * A logic implementing class of SimpleUserTrends function in the system.
 * 
 * @author yoursoft
 * 
 */
public class DataMiningLogic
{

	/** The threshold sets the bar of what accumulator will be ingored. */
	private double threshold;

	/**
	 * The depreciationRate depreciates the weight of accumulator by applying
	 * the depreciation formula
	 */
	private double depreciationRate;

	/**
	 * The depreciateInterval represents per how many days should the depreciate
	 * rate be applied to the original number of requests
	 */
	private int depreciateInterval;

	/**
	 * The long format of the very beginning date of the database
	 * 
	 * @see Java Date.parse().
	 */
	private long startLongDate;

	/**
	 * The long format of the last date of the database
	 * 
	 * @see Java Date.parse().
	 */
	private long endLongDate;

	/**
	 * The a is the independent variable of logarithm in scoresWithderivative
	 * methond.
	 */
	private double a;

	/**
	 * A constructor of DataMiningLogic.
	 * 
	 * @param threshold
	 *            The accumulator which with a score smaller than the set
	 *            threshold will be ingnored.
	 */
	public DataMiningLogic(double threshold)
	{
		this.threshold = threshold;
		startLongDate = 0;
		endLongDate = 0;
	}

	/**
	 * A blank constructor of DataMiningLogic.
	 */
	public DataMiningLogic()
	{
		startLongDate = 0;
		endLongDate = 0;
	}

	/**
	 * A setter of depreciationRate.
	 * 
	 * @param depreciationRate
	 */
	public void setDepreciationRate(double depreciationRate)
	{
		this.depreciationRate = depreciationRate;
	}

	/**
	 * A getter of depreciationRate.
	 * 
	 * @return depreciationRate
	 */
	public double getDepreciationRate()
	{
		return depreciationRate;
	}

	/**
	 * A setter of a.
	 * 
	 * @param a
	 *            The independent variable of logorithm.
	 */
	public void setA(double a)
	{
		this.a = a;
	}

	/**
	 * A getter of a.
	 * 
	 * @return a
	 */
	public double getA()
	{
		return a;
	}

	/**
	 * A setter of depreciateInterval
	 * 
	 * @param depreciateInterval
	 */
	public void setDepreciateInterval(int depreciateInterval)
	{
		this.depreciateInterval = depreciateInterval;
	}

	/**
	 * A getter of depreciateInterval
	 * 
	 * @return depreciateInterval
	 */
	public int getDepreciateInterval()
	{
		return depreciateInterval;
	}

	/**
	 * Returns an arraylist of depreciators. A depreciator is defined as follow:
	 * In depreciating method, the former value is depreciate according to the
	 * time. All values from different points of time are converted to the
	 * values at the same point of time-it's usually the present time or the end
	 * point of the end of the whole period. V(present) = V(time) *
	 * (1-depreciationRate)<maths>power</power> (n-1) In order to make the
	 * calculated results be reusable, [(1-depreciationRate)<maths>power</power>
	 * (n-1)] is precalculated and stored in the arraylist in the corresponding
	 * position by the hash function: positionIndex =
	 * (longDate-startLongDate)/depreciateInterval.
	 * 
	 * @param normalizedData
	 *            A HashMap stores original number of request from database and
	 *            the number of request processed by the Standard Score function
	 *            on every single date.
	 * @see JdbcDaoImp getNormalizedDatas(String Dimension).
	 * @return depreciators An arraylist of doubles.
	 */
	private ArrayList<Double> depreciators(
			HashMap<String, List<SimpleUserTrends>> normalizedData)
	{
		for (String s : normalizedData.keySet())
		{
			int lastItemIndex = normalizedData.get(s).size() - 1;
			if (endLongDate < normalizedData.get(s).get(lastItemIndex)
					.getLongdate())
			{
				endLongDate = normalizedData.get(s).get(lastItemIndex)
						.getLongdate();
				System.out.println("end date: "
						+ normalizedData.get(s).get(lastItemIndex).getDate());
			}
			if (startLongDate > normalizedData.get(s).get(0).getLongdate()
					|| startLongDate == 0)
			{
				startLongDate = normalizedData.get(s).get(0).getLongdate();
				System.out.println("start date: "
						+ normalizedData.get(s).get(0).getDate());
			}
		}
		int numOfIntervals;
		// calculate how many intervals in the time range
		if ((((endLongDate - startLongDate) % (depreciateInterval * 60 * 60 * 24)) == 0))
		{
			numOfIntervals = (int) ((endLongDate - startLongDate)
					/ (depreciateInterval * 60 * 60 * 24) / 1000);
		} else
		{
			numOfIntervals = (int) ((endLongDate - startLongDate)
					/ (depreciateInterval * 60 * 60 * 24) / 1000 + 1);
		}
		System.out.println("num of intervals: " + numOfIntervals);
		ArrayList<Double> depreciators = new ArrayList<Double>();
		for (int i = 0; i < numOfIntervals; i++)
		{
			double depreciator = 1 - depreciationRate;
			for (int j = 1; j <= i; j++)
			{
				depreciator = depreciator * depreciator;
			}
			depreciators.add(depreciator);
		}
		return depreciators;
	}

	/**
	 * Return an arraylist of accumulators. Scores in accumulators are the sums
	 * of depreciated numbers of requests per day by applying the depreciation
	 * formula.
	 * 
	 * @param depreciators
	 *            An array of doubles returned by depreciators(HashMap<String,
	 *            List<SimpleUserTrends>> normalizedData).
	 * @param normalizedData
	 *            A HashMap contains normalized number of requests for all
	 *            values in the specified dimension returned by
	 *            Dao.getNormalizedDatas(String Dimension).
	 * @return ArrayList<Accumulator> scores
	 */
	private ArrayList<Accumulator> scores(ArrayList<Double> depreciators,
			HashMap<String, List<SimpleUserTrends>> normalizedData)
	{
		ArrayList<Accumulator> scores = new ArrayList<Accumulator>(
				normalizedData.size());
		// go through the HashMap to retreive data for every value in the
		// specified dimension
		for (String key : normalizedData.keySet())
		{
			double scoreOfSinglValueItem = 0;
			List<SimpleUserTrends> datas = normalizedData.get(key);
			for (int currentItem = 0; currentItem < (datas.size() - 2); currentItem++)
			{
				// Use the hash function: (current data -
				// startLongDate)/depreciateIntervalInMilliSeconds
				// to retreive the corresponding depreciator
				int depreciatorIndex = (int) ((datas.get(currentItem)
						.getLongdate() - startLongDate)
						/ (depreciateInterval * 60 * 60 * 24) / 1000);
				double currRequest = datas.get(currentItem)
						.getNormalizedRequest();
				double nextRequest = datas.get(currentItem + 1)
						.getNormalizedRequest();
				scoreOfSinglValueItem += (nextRequest - currRequest)
						* depreciators.get(depreciatorIndex);
			}
			scores.add(new Accumulator(key, scoreOfSinglValueItem));
		}
		return scores;
	}

	/**
	 * Return an arraylist of accumulators. Scores in accumulators are the sums
	 * of depreciated numbers of requests per day by multiplying the
	 * corresponding derivative value of the natural logarithm of a.
	 * 
	 * @param normalizedData
	 *            A HashMap contains normalized number of requests for all
	 *            values in the specified dimension returned by
	 *            Dao.getNormalizedDatas(String Dimension).
	 * @return ArrayList<Accumulator> scores.
	 */
	private ArrayList<Accumulator> scoresWithDerivative(
			HashMap<String, List<SimpleUserTrends>> normalizedData)
	{
		ArrayList<String> filteredData = new ArrayList<String>();
		// go through the HashMap to retreive data for every value in the
		// specified dimension
		for (String s : normalizedData.keySet())
		{
			List<SimpleUserTrends> datas = normalizedData.get(s);
			double sum = 0;
			double mean;
			for (SimpleUserTrends d : datas)
			{
				sum += d.getRequest();
			}
			mean = sum / datas.size();
			// filter out the values with an average number of requests less
			// than the threshold
			if (mean > threshold && threshold > 0)
			{
				filteredData.add(s);
			}

			// get the last date of the database
			int lastItemIndex = normalizedData.get(s).size() - 1;
			if (endLongDate < normalizedData.get(s).get(lastItemIndex)
					.getLongdate())
			{
				endLongDate = normalizedData.get(s).get(lastItemIndex)
						.getLongdate();
				System.out.println("end date: "
						+ normalizedData.get(s).get(lastItemIndex).getDate());
			}
			if (startLongDate > normalizedData.get(s).get(0).getLongdate()
					|| startLongDate == 0)
			{
				startLongDate = normalizedData.get(s).get(0).getLongdate();
				System.out.println("start date: "
						+ normalizedData.get(s).get(0).getDate());
			}
		}
		ArrayList<Accumulator> scores = new ArrayList<Accumulator>(
				normalizedData.size());
		// System.out.println("size of filtered data set: "+filteredData.size());
		if (filteredData.size() > 0)
		{
			// go through the filtered data set
			for (String key : filteredData)
			{
				double scoreOfSinglValueItem = 0;
				List<SimpleUserTrends> datas = normalizedData.get(key);
				for (int currentItem = 0; currentItem < (datas.size() - 2); currentItem++)
				{
					double currRequest = datas.get(currentItem)
							.getNormalizedRequest();
					double nextRequest = datas.get(currentItem + 1)
							.getNormalizedRequest();
					// calculates the corresponding derviertive of the current
					// date
					double derivative = 1 / (((endLongDate - datas.get(
							currentItem).getLongdate())
							/ (1000 * 3600 * 24) + (1 / Math.log(a))));
					scoreOfSinglValueItem += (nextRequest - currRequest)
							* derivative;
				}
				scores.add(new Accumulator(key, scoreOfSinglValueItem));
			}
		} else
		{
			for (String key : normalizedData.keySet())
			{
				double scoreOfSinglValueItem = 0;
				List<SimpleUserTrends> datas = normalizedData.get(key);
				for (int currentItem = 0; currentItem < (datas.size() - 2); currentItem++)
				{
					double currRequest = datas.get(currentItem)
							.getNormalizedRequest();
					double nextRequest = datas.get(currentItem + 1)
							.getNormalizedRequest();
					double derivative = 1 / (((endLongDate - datas.get(
							currentItem).getLongdate())
							/ (1000 * 3600 * 24) + (1 / Math.log(a))));
					scoreOfSinglValueItem += (nextRequest - currRequest)
							* derivative;
				}
				scores.add(new Accumulator(key, scoreOfSinglValueItem));
			}
		}
		return scores;
	}

	/**
	 * Return a MinHeap which ranks the top n accumulators
	 * 
	 * @param scores
	 *            An arraylist of accumulators contains calculated scores.
	 * @return MinHeap minHeap
	 */
	private MinHeap getMinHeap(ArrayList<Accumulator> scores)
	{
		MinHeap minHeap = new MinHeap(10);
		for (Accumulator a : scores)
		{
			// insert a new accumulator into the minHeap
			minHeap.insert(a);
		}
		return minHeap;
	}

	/**
	 * A method ranks and prints the top n accumulators with names and scores in
	 * console.
	 * 
	 * @param normalizedData
	 *            A HashMap contains normalized number of requests for all
	 *            values in the specified dimension returned by
	 *            Dao.getNormalizedDatas(String Dimension).
	 * @param mode
	 *            A int which indicates which function should be applied to
	 *            calculate the scores of accumulators. <code>1</code> for
	 *            depreciation formula, <code>2</code> for derivative of
	 *            logarithm.
	 */
	public void printTrends(
			HashMap<String, List<SimpleUserTrends>> normalizedData, int mode)
	{
		ArrayList<Accumulator> scores = new ArrayList<Accumulator>();
		if (mode == 1)
		{
			ArrayList<Double> depreciators = depreciators(normalizedData);
			scores = scores(depreciators, normalizedData);
		}
		if (mode == 2)
		{
			scores = scoresWithDerivative(normalizedData);
		}
		MinHeap heap = getMinHeap(scores);
		heap.sort();
		heap.printResults();
	}

	/**
	 * Return a ranked and sorted(small to large) arraylist of top n
	 * accumulators.
	 * 
	 * @param normalizedData
	 *            A HashMap contains normalized number of requests for all
	 *            values in the specified dimension returned by
	 *            Dao.getNormalizedDatas(String Dimension).
	 * @param mode
	 *            A int which indicates which function should be applied to
	 *            calculate the scores of accumulators.
	 * @return ArrayList<Accumulator> reversedHeap.
	 */
	public ArrayList<Accumulator> getSortedHeap(
			HashMap<String, List<SimpleUserTrends>> normalizedData, String mode)
	{
		ArrayList<Accumulator> scores = new ArrayList<Accumulator>();
		if (mode.compareTo("Depreciation") == 0)
		{
			ArrayList<Double> depreciators = depreciators(normalizedData);
			scores = scores(depreciators, normalizedData);
		}
		if (mode.compareTo("Derivative") == 0)
		{
			scores = scoresWithDerivative(normalizedData);
		}
		MinHeap heap = getMinHeap(scores);
		heap.sort();
		ArrayList<Accumulator> sortedHeap = heap.getHeap();
		ArrayList<Accumulator> reversedHeap = new ArrayList<Accumulator>();
		for (int i = 1; i < sortedHeap.size(); i++)
		{
			reversedHeap.add(0, sortedHeap.get(i));
		}
		System.out.println("size of reversedHeap is " + reversedHeap.size());
		heap.printResults();
		return reversedHeap;
	}
}
