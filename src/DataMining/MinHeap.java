package DataMining;

import java.util.*;

/**
 * A min heap takes accumulators as its elements.
 * 
 * @see Heap(data structure) on Wikipedia.
 * @author yoursoft
 */
class MinHeap
{

	/** An arraylist which implements the heaping logic. */
	private ArrayList<Accumulator> Heap;
	/**
	 * The maximum size of the arraylist. It's one size larger than the desired
	 * heap size set by the user.
	 */
	private int maxsize;
	/** A int for recording the actual size of the arraylist when heaping. */
	private int size;

	/**
	 * A constructor of MinHeap. The desired heap size must be set.
	 * 
	 * @param max
	 *            A int represents the desired heap size.
	 */
	public MinHeap(int max)
	{
		maxsize = max + 1;
		Heap = new ArrayList<Accumulator>(maxsize); // the useful numbers starts
													// from position 1
		size = 0;
		Heap.add(0, new Accumulator("", Double.NEGATIVE_INFINITY));
	}

	/** A getter of the heap arraylisst */
	public ArrayList<Accumulator> getHeap()
	{
		return Heap;
	}

	/** Return the index of the leftchild of the specified position. */
	private int leftchild(int pos)
	{
		return 2 * pos;
	}

	/** Return the index of the leftchild of the specified position. */
	private int rightchild(int pos)
	{
		return 2 * pos + 1;
	}

	/** Return the index of the parent of the specified position. */
	private int parent(int pos)
	{
		return pos / 2;
	}

	/**
	 * Return a boolean variable to tell if the specified position is a leaf of
	 * the heap.
	 */
	private boolean isleaf(int pos)
	{
		return ((pos > size / 2) && (pos <= size));
	}

	/** Swap the two specified elements. */
	private void swap(int pos1, int pos2)
	{
		Accumulator tmp;

		tmp = Heap.get(pos1);
		Heap.set(pos1, Heap.get(pos2));
		Heap.set(pos2, tmp);
	}

	/**
	 * Insert a new element into the heap. the heap will automatically push the
	 * larger element to the bottom.
	 */
	public void insert(Accumulator elem)
	{
		if (size < maxsize - 1)
		{
			size++;
			Heap.add(elem);
			int current = size;

			while (Heap.get(current).getScore() < Heap.get(parent(current))
					.getScore())
			{
				swap(current, parent(current));
				current = parent(current);
			}
		} else
		{
			if (elem.getScore() > Heap.get(1).getScore())
			{
				Heap.set(1, elem);
				pushdown(1);
			}
		}
		return;
	}

	// only for debugging
	public void printResults()
	{
		int i;
		for (i = 1; i <= size; i++)
		{
			System.out.print(Heap.get(i).getName() + " ");
			System.out.println(Heap.get(i).getScore());
		}
		System.out.println();
	}

	/** Push the specified element to the right place. */
	private void pushdown(int position)
	{
		int smallestchild;
		while (!isleaf(position))
		{
			smallestchild = leftchild(position);
			if ((smallestchild < size)
					&& (Heap.get(smallestchild).getScore() > Heap.get(
							smallestchild + 1).getScore()))
				smallestchild = smallestchild + 1;
			if (Heap.get(position).getScore() <= Heap.get(smallestchild)
					.getScore())
				return;
			swap(position, smallestchild);
			position = smallestchild;
		}
	}

	/** Sort the heap element from small to larger. */
	public void sort()
	{
		for (int cur = 1; cur < (Heap.size() - 1); cur++)
		{
			int smallest = cur;
			for (int index = cur + 1; index < Heap.size(); index++)
			{
				if (Heap.get(index).getScore() < Heap.get(smallest).getScore())
				{
					smallest = index;
				}
			}
			if (smallest != cur)
			{
				swap(smallest, cur);
			}
		}
	}

	/** Reset the heap to the default setting. */
	public void resetHeap()
	{
		Heap.clear();
		size = 0;
		Heap.add(0, new Accumulator("", Double.NEGATIVE_INFINITY));
	}
}
