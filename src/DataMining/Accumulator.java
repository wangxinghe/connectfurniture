package DataMining;

/**
 * The accumulator class represents a pair of name in String and a score in
 * double. Every single object of accumulator class is to pushed into the
 * MinHeap to be ranked according to the score contained in it.
 * 
 * @author yoursoft
 */
public class Accumulator
{

	/** The name of this accumulator. */
	private String name;

	/** The score of this accumulator. */
	private double score;

	/**
	 * A constructor of Accumulater.
	 * 
	 * @param name
	 *            The name of this accumulator
	 * @param score
	 *            The score of this accumulator
	 */
	public Accumulator(String name, double score)
	{
		this.name = name;
		this.score = score;
	}

	/**
	 * A blank constructor of Accumulator.
	 */
	public Accumulator()
	{
		name = "";
		score = 0;
	}

	/**
	 * A getter of name.
	 * 
	 * @return The name of this accumulator.
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * A setter of name.
	 * 
	 * @param name
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * A getter of score.
	 * 
	 * @return The score of this accumulator.
	 */
	public double getScore()
	{
		return score;
	}

	/**
	 * A setter of score.
	 * 
	 * @param score
	 */
	public void setScore(double score)
	{
		this.score = score;
	}
}
