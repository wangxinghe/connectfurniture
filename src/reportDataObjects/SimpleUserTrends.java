package reportDataObjects;

/**
 * This is a class used to store datas for simple user trends.
 * 
 * @author yoursoft
 * 
 */
public class SimpleUserTrends
{
	/** The number of request of one day in double. */
	private double request;
	/** The normailzed number of one day request. */
	private double normalizedRequest;
	/** The single date. */
	private String date;
	/**
	 * The single date converted to long tyep.
	 * 
	 * @see Java Date.parse().
	 */
	private long longdate;

	/**
	 * A getter of request.
	 * 
	 * @return double request.
	 */
	public double getRequest()
	{
		return request;
	}

	/**
	 * A setter of request.
	 * 
	 * @param request
	 */
	public void setRequest(double request)
	{
		this.request = request;
	}

	/**
	 * A getter of date.
	 * 
	 * @return String date.
	 */
	public String getDate()
	{
		return date;
	}

	/**
	 * A setter of date.
	 * 
	 * @param date
	 */
	public void setDate(String date)
	{
		this.date = date;
	}

	/**
	 * A setter of longDate.
	 * 
	 * @param longdate
	 */
	public void setLongdate(long longdate)
	{
		this.longdate = longdate;
	}

	/**
	 * A getter of longDate.
	 * 
	 * @return long longDate.
	 */
	public long getLongdate()
	{
		return longdate;
	}

	/**
	 * A setter of normalizedRequest.
	 * 
	 * @param normalizedRequest
	 */
	public void setNormalizedRequest(double normalizedRequest)
	{
		this.normalizedRequest = normalizedRequest;
	}

	/**
	 * A getter of normalizedRequest.
	 * 
	 * @return double normalizedRequest.
	 */
	public double getNormalizedRequest()
	{
		return normalizedRequest;
	}

}
