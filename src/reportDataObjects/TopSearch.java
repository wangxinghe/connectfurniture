package reportDataObjects;

import java.math.BigDecimal;

/**
 * This class is used to store information retrieved from the database for the
 * Top Dimension Searche report
 * 
 */
public class TopSearch
{
	String allDimensions;
	String dimensions[];
	int count;
	int total;
	String percentage;

	/**
	 * Returns the percentage of a certain dimension
	 * 
	 * @return the percentage of a certain dimension
	 */
	public String getPercentage()
	{
		double d = (count * 100.00) / total;
		BigDecimal b = new BigDecimal(d);

		return b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue() + "%";
	}

	public void setPercentage(String percentage)
	{
		this.percentage = percentage;
	}

	public String[] getDimensions()
	{
		return dimensions;
	}

	public void setDimensions(String[] dimensions)
	{
		this.dimensions = dimensions;
	}

	public int getCount()
	{
		return count;
	}

	public void setCount(int count)
	{
		this.count = count;
	}

	/**
	 * Returns dimensions separated by ", "
	 * 
	 * @return dimensions separated by ", "
	 */
	public String getAllDimensions()
	{
		String tempDimensions = "";
		int depth = dimensions.length;
		for (int i = 0; i < depth; i++)
		{
			tempDimensions += dimensions[i] + ", ";
		}

		return tempDimensions.substring(0, tempDimensions.length() - 2);
	}

	public void setAllDimensions(String allDimensions)
	{
		this.allDimensions = allDimensions;
	}

	public int getTotal()
	{
		return total;
	}

	public void setTotal(int total)
	{
		this.total = total;
	}

}
