package reportDataObjects;

/**
 * This class is used to store information retrieved from the database for the
 * Request By User report. this include search or navigate request made by
 * users.
 * 
 * @author yoursoft
 * 
 */
public class RequestByUser_SearchOrNavigate
{
	Integer numberOfRequests;
	Integer numberOfUsers;

	/**
	 * set the number of requests
	 * 
	 * @param numberOfRequests
	 */
	public void setNumberOfRequests(Integer numberOfRequests)
	{
		this.numberOfRequests = numberOfRequests;
	}

	/**
	 * set the number of users
	 * 
	 * @param numberOfUsers
	 */
	public void setNumberOfUsers(Integer numberOfUsers)
	{
		this.numberOfUsers = numberOfUsers;
	}

	/**
	 * get the number of requests made by distinct users.
	 * 
	 * @return
	 */
	public Integer getNumberOfRequests()
	{
		return this.numberOfRequests;
	}

	/**
	 * get the number of users
	 * 
	 * @return
	 */
	public Integer getNumberOfUsers()
	{
		return this.numberOfUsers;
	}
}
