package reportDataObjects;

/**
 * This class is used to store information retrieved from the database for the
 * Request By Hour report
 * 
 * @author yoursoft
 */
public class RequestByHour
{

	int hour;
	int requests;

	/**
	 * get the hour
	 * 
	 * @return
	 */
	public int getHour()
	{
		return hour;
	}

	/**
	 * set the hour
	 * 
	 * @param hour
	 */
	public void setHour(int hour)
	{
		this.hour = hour;
	}

	/**
	 * get the number of requests per specific hour
	 * 
	 * @return
	 */
	public int getRequests()
	{
		return requests;
	}

	/**
	 * set the number of requests per specific hour
	 * 
	 * @param requests
	 */
	public void setRequests(int requests)
	{
		this.requests = requests;
	}

}
