package reportDataObjects;

/**
 * This class is used to store information retrieved from the database for the
 * Request By User report. this is for user with search and navigation request.
 * 
 * @author yoursoft
 * 
 */
public class RequestByUser_SearchAndNavigate
{
	Integer numberOfUsers;
	Integer numberOfSearches;
	Integer numberOfNavigates;

	/**
	 * set the number of users
	 * 
	 * @param numberOfUsers
	 */
	public void setNumberOfUsers(Integer numberOfUsers)
	{
		this.numberOfUsers = numberOfUsers;
	}

	/**
	 * set the number of searches
	 * 
	 * @param numberOfSearches
	 */
	public void setNumberOfSearches(Integer numberOfSearches)
	{
		this.numberOfSearches = numberOfSearches;
	}

	/**
	 * set the number of navigates
	 * 
	 * @param numberOfNavigates
	 */
	public void setNumberOfNavigates(Integer numberOfNavigates)
	{
		this.numberOfNavigates = numberOfNavigates;
	}

	/**
	 * get the number of users
	 * 
	 * @return
	 */
	public Integer getNumberOfUsers()
	{
		return this.numberOfUsers;
	}

	/**
	 * get the number of searches made by distinct user
	 * 
	 * @return
	 */
	public Integer getNumberOfSearches()
	{
		return this.numberOfSearches;
	}

	/**
	 * get the number of navigates made by distinct user
	 * 
	 * @return
	 */
	public Integer getNumberOfNavigates()
	{
		return this.numberOfNavigates;
	}
}
