package reportDataObjects;

/**
 * This class is used to store information retrieved from the database for the
 * Session By Hour report it is for getting the number of session per different
 * hour
 * 
 * @author yoursoft
 */
public class SessionByHour
{

	int hour;
	int sessions;

	/**
	 * get the hour
	 * 
	 * @return
	 */
	public int getHour()
	{
		return hour;
	}

	/**
	 * set the hour
	 * 
	 * @param hour
	 */
	public void setHour(int hour)
	{
		this.hour = hour;
	}

	/**
	 * get the number of sessions
	 * 
	 * @return
	 */
	public int getSessions()
	{
		return sessions;
	}

	/**
	 * set the number of sessions
	 * 
	 * @param sessions
	 */
	public void setSessions(int sessions)
	{
		this.sessions = sessions;
	}

}
