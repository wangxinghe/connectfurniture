package reportDataObjects;

/**
 * This class is used to store information retrieved from the database for the
 * Request By Hour report.
 * 
 * @author yoursoft
 * 
 */
public class RequestBy
{

	/** The time period of the sql request in standard sql format. */
	String time;
	/** The time period of the sql request in Australian format. */
	String ftime;
	/** The number of request. */
	int requests;

	/**
	 * A getter of time.
	 * 
	 * @return String time.
	 */
	public String getTime()
	{
		return time;
	}

	/**
	 * A setter of time.
	 * 
	 * @param time
	 *            The full date including year, month, date in String.
	 */
	public void setTime(String time)
	{
		this.time = time;
	}

	/**
	 * A setter of time.
	 * 
	 * @param time
	 *            The months, dates or hours in int.
	 */
	public void setTime(int time)
	{
		this.time = String.valueOf(time);
	}

	/**
	 * A setter of ftime.
	 * 
	 * @param ftime
	 */
	public void setFtime(String ftime)
	{
		this.ftime = ftime;
	}

	/**
	 * A setter of ftime.
	 * 
	 * @param ftime
	 */
	public void setFtime(int ftime)
	{
		this.ftime = String.valueOf(ftime);
	}

	/**
	 * A getter of ftime.
	 * 
	 * @return String ftime.
	 */
	public String getFtime()
	{
		return ftime;
	}

	/**
	 * A getter of requests.
	 * 
	 * @return int requests.
	 */
	public int getRequests()
	{
		return requests;
	}

	/**
	 * A setter of requests.
	 * 
	 * @param requests
	 */
	public void setRequests(int requests)
	{
		this.requests = requests;
	}

}
