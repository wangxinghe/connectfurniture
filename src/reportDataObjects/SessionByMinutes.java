package reportDataObjects;

/**
 * This class is used to store information retrieved from the database for the
 * session by minute report. these information is used to get how many distinct
 * sessions in different minute.
 * 
 * @author yoursoft
 * 
 */
public class SessionByMinutes
{
	Integer minutes;
	Integer numberOfSessions;

	/**
	 * set the minutes
	 * 
	 * @param minutes
	 */
	public void setMinutes(Integer minutes)
	{
		this.minutes = minutes;
	}

	/**
	 * set the number of sessions
	 * 
	 * @param numberOfSessions
	 */
	public void setNumberOfSessions(Integer numberOfSessions)
	{
		this.numberOfSessions = numberOfSessions;
	}

	/**
	 * get the minutes
	 * 
	 * @return
	 */
	public Integer getMinutes()
	{
		return this.minutes;
	}

	/**
	 * get the number of sessions
	 * 
	 * @return
	 */
	public Integer getNumberOfSessions()
	{
		return this.numberOfSessions;
	}
}
