package reportDataObjects;

/**
 * the KeywordSearch object for the report
 * 
 * @author yoursoft
 * 
 */
public class KeywordSearch
{
	String searchterms;
	int records;
	int requests;

	/**
	 * set the number of requests
	 * 
	 * @param requests
	 */
	public void setRequests(int requests)
	{
		this.requests = requests;
	}

	/**
	 * get the number of requests
	 * 
	 * @return
	 */
	public int getRequests()
	{
		return this.requests;
	}

	/**
	 * set the terms that the user use to search
	 * 
	 * @param searchterms
	 */
	public void setSearchterms(String searchterms)
	{
		this.searchterms = searchterms;
	}

	/**
	 * set the record number
	 * 
	 * @param records
	 */
	public void setRecords(int records)
	{
		this.records = records;
	}

	/**
	 * get the terms that the user use to search
	 * 
	 * @return
	 */
	public String getSearchterms()
	{
		return this.searchterms;
	}

	/**
	 * get the number of records
	 * 
	 * @return
	 */
	public int getRecords()
	{
		return this.records;
	}
}
