package reportDataObjects;

/**
 * This class is used to store information retrieved from the database for the
 * Session By report
 * 
 * @author yoursoft
 */
public class SessionBy
{
	String ftime;
	String time;
	int sessions;

	/**
	 * Get the time.
	 * 
	 * @return
	 */
	public String getTime()
	{
		return time;
	}

	/**
	 * Get the formated time.
	 * 
	 * @return
	 */
	public String getFtime()
	{
		return ftime;
	}

	/**
	 * Set the format time into report object.
	 * 
	 * @param ftime
	 */
	public void setFtime(String ftime)
	{
		this.ftime = ftime;
	}

	/**
	 * Change the format data into String.
	 * 
	 * @param ftime
	 */
	public void setFtime(int ftime)
	{
		this.ftime = String.valueOf(ftime);
	}

	/**
	 * Set the time data into the report object.
	 * 
	 * @param time
	 */
	public void setTime(String time)
	{
		this.time = time;
	}

	/**
	 * Change the formated time into String then set to the report object.
	 * 
	 * @param time
	 */
	public void setTime(int time)
	{
		this.time = String.valueOf(time);
	}

	/**
	 * Get the number of the sessions.
	 * 
	 * @return
	 */
	public int getSessions()
	{
		return sessions;
	}

	/**
	 * Set the number of session into report object.
	 * 
	 * @param sessions
	 */
	public void setSessions(int sessions)
	{
		this.sessions = sessions;
	}

}
