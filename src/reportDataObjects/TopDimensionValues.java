package reportDataObjects;

import java.math.BigDecimal;

/**
 * This class is used to store information retrieved from the database for the
 * Top Dimension-Value Search report
 * 
 */
public class TopDimensionValues
{

	String[] dimensions;
	String[] values;
	String dimensionValue;
	int count;
	int total;

	String percentage;

	/**
	 * Returns the percentage of a certain dimension-value pair
	 * 
	 * @return the percentage of a certain dimension-value pair
	 */
	public String getPercentage()
	{
		double d = (count * 100.00) / total;
		BigDecimal b = new BigDecimal(d);

		return b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue() + "%";
	}

	public void setPercentage(String percentage)
	{
		this.percentage = percentage;
	}

	// public String getDimensionValue() {
	// return dimensionValue;
	// }
	// public void setDimensionValue(String dimensionValue) {
	// this.dimensionValue = dimensionValue;
	// }
	public int getCount()
	{
		return count;
	}

	public void setCount(int count)
	{
		this.count = count;
	}

	public String[] getDimensions()
	{
		return dimensions;
	}

	public void setDimensions(String[] dimensions)
	{
		this.dimensions = dimensions;
	}

	public String[] getValues()
	{
		return values;
	}

	public void setValues(String[] values)
	{
		this.values = values;
	}

	public int getTotal()
	{
		return total;
	}

	public void setTotal(int total)
	{
		this.total = total;
	}

	/**
	 * Returns a certain dimension-value pair using " : " to separate them
	 * 
	 * @return a certain dimension-value pair using " : " to separate them
	 */
	public String getDimensionValue()
	{
		String tempDimensionValues = "";
		int depth = dimensions.length;
		for (int i = 0; i < depth; i++)
		{
			tempDimensionValues += dimensions[i] + " : " + values[i] + ", ";
		}

		return tempDimensionValues.substring(0,
				tempDimensionValues.length() - 2);
	}

	public void setDimensionValue(String dimensionValue)
	{
		this.dimensionValue = dimensionValue;
	}

}
