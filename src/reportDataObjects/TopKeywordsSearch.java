package reportDataObjects;

/**
 * This class is used to store information retrieved from the database for the
 * Top Keywords Search report To get the Top keyword search and the proper
 * spelling correction of that search term for a period of time.
 */
public class TopKeywordsSearch
{

	String searchTerms;
	String autoCorrectTo;
	int count;

	/**
	 * get the search term
	 * 
	 * @return
	 */
	public String getSearchTerms()
	{
		return searchTerms;
	}

	/**
	 * set the search term
	 * 
	 * @param searchTerms
	 */
	public void setSearchTerms(String searchTerms)
	{
		this.searchTerms = searchTerms;
	}

	/**
	 * get the amount of time that a specific search term occurred
	 * 
	 * @return
	 */
	public int getCount()
	{
		return count;
	}

	/**
	 * set the amount of time that a specific search term occurred
	 * 
	 * @param count
	 */
	public void setCount(int count)
	{
		this.count = count;
	}

	/**
	 * get the spelling correction of a specific search term
	 * 
	 * @return
	 */
	public String getAutoCorrectTo()
	{
		return autoCorrectTo;
	}

	/**
	 * set the spelling correction of a specific search term
	 * 
	 * @param autoCorrectTo
	 */
	public void setAutoCorrectTo(String autoCorrectTo)
	{
		this.autoCorrectTo = autoCorrectTo;
	}

}
