package reportDataObjects;

/**
 * the KeywordSearchSummary for the report
 * 
 * @author yoursoft
 * 
 */
public class KeywordSearchSummary
{
	int records;
	int requests;
	int llimit;
	int hlimit;

	/**
	 * set the low limit
	 * 
	 * @param llimit
	 */
	public void setLlimit(int llimit)
	{
		this.llimit = llimit;
	}

	/**
	 * get the low limit
	 * 
	 * @return
	 */
	public int getLlimit()
	{
		return this.llimit;
	}

	/**
	 * set the high limit
	 * 
	 * @param hlimit
	 */
	public void setHlimit(int hlimit)
	{
		this.hlimit = hlimit;
	}

	/**
	 * get high limit
	 * 
	 * @return
	 */
	public int getHlimit()
	{
		return this.hlimit;
	}

	/**
	 * set the amount of requests
	 * 
	 * @param requests
	 */
	public void setRequests(int requests)
	{
		this.requests = requests;
	}

	/**
	 * get the amount of requests
	 * 
	 * @return
	 */
	public int getRequests()
	{
		return this.requests;
	}

	/**
	 * set the number of records
	 * 
	 * @param records
	 */
	public void setRecords(int records)
	{
		this.records = records;
	}

	/**
	 * get the number of records
	 * 
	 * @return
	 */
	public int getRecords()
	{
		return this.records;
	}
}
