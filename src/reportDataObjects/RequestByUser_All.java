package reportDataObjects;

/**
 * This class is used to store information retrieved from the database for the
 * Request By User including Navigation and Search request report
 * 
 * @author yoursoft
 * 
 */
public class RequestByUser_All
{
	Integer numberOfUsers;
	Integer numberOfSearches;
	Integer numberOfNavigates;

	/**
	 * set the number of users
	 * 
	 * @param numberOfUsers
	 */
	public void setNumberOfUsers(Integer numberOfUsers)
	{
		this.numberOfUsers = numberOfUsers;
	}

	/**
	 * set the number of searches
	 * 
	 * @param numberOfSearches
	 */
	public void setNumberOfSearches(Integer numberOfSearches)
	{
		this.numberOfSearches = numberOfSearches;
	}

	/**
	 * set the number of navigates
	 * 
	 * @param numberOfNavigates
	 */
	public void setNumberOfNavigates(Integer numberOfNavigates)
	{
		this.numberOfNavigates = numberOfNavigates;
	}

	/**
	 * get the number of users
	 * 
	 * @return
	 */
	public Integer getNumberOfUsers()
	{
		return this.numberOfUsers;
	}

	/**
	 * get the number of searches by distinct user
	 * 
	 * @return
	 */
	public Integer getNumberOfSearches()
	{
		return this.numberOfSearches;
	}

	/**
	 * get the number of navigates by distinct user
	 * 
	 * @return
	 */
	public Integer getNumberOfNavigates()
	{
		return this.numberOfNavigates;
	}
}
