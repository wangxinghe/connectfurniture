package reportDataObjects;

public class TimePerformance
{
	double timeScope;
	double timeConsume;

	public double getTimeScope()
	{
		return timeScope;
	}

	public double getTimeConsume()
	{
		return timeConsume;
	}

	public void setTimeScope(double timeScope)
	{
		this.timeScope = timeScope;
	}

	public void setTimeConsume(double timeConsume)
	{
		this.timeConsume = timeConsume;
	}
}
