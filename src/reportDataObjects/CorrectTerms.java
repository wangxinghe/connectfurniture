package reportDataObjects;

/**
 * The correct term object for the report.
 * 
 * @author yoursoft
 * 
 */
public class CorrectTerms
{

	private String searchterm;
	private String correctto;
	private int request;

	/**
	 * The terms which the user use for search.
	 * 
	 * @return
	 */
	public String getSearchterm()
	{
		return searchterm;
	}

	/**
	 * The terms the search terms auto changed to.
	 * 
	 * @return
	 */
	public String getCorrectto()
	{
		return correctto;
	}

	/**
	 * The number of requests.
	 * 
	 * @return
	 */
	public int getRequest()
	{
		return request;
	}

	/**
	 * Set the terms into report object.
	 * 
	 * @param searchterm
	 */
	public void setSearchterm(String searchterm)
	{
		this.searchterm = searchterm;
	}

	/**
	 * Set the corrected terms into report object.
	 * 
	 * @param correctto
	 */
	public void setCorrectto(String correctto)
	{
		this.correctto = correctto;
	}

	/**
	 * Set the number of requests into report object.
	 * 
	 * @param request
	 */
	public void setRequest(int request)
	{
		this.request = request;
	}
}
