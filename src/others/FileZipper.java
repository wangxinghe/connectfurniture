package others;

import java.io.*;
import java.util.zip.*;

/**
 * A class to zip files.
 * 
 * @author yoursoft
 * 
 */
public class FileZipper
{

	/** An input buffer. */
	private byte[] buffer;
	/** The output zip file name. */
	private String zipFileName;
	/**
	 * The file path of the file which needs to be zip. Note that the file path
	 * doesn't include the file name.
	 */
	private String filePath;
	/**
	 * The file path of the output zip file. Note that the file path doesn't
	 * include the file name.
	 */
	private String outputPath;
	/** The buffer size. */
	private final int BUFFER_SIZE = 1024;

	/**
	 * A constructor of FileZipper.
	 * 
	 * @param zipFileName
	 *            The output zip file name.
	 * @param filePath
	 *            The file path of the file which needs to be zip. Note that the
	 *            file path doesn't include the file name.
	 * @param outputPath
	 *            The file path of the output zip file. Note that the file path
	 *            doesn't include the file name.
	 */
	public FileZipper(String zipFileName, String filePath, String outputPath)
	{
		this.zipFileName = zipFileName;
		this.filePath = filePath;
		this.outputPath = outputPath;
		buffer = new byte[BUFFER_SIZE];
	}

	/** A setter of zipFileName. */
	public void setZipFileName(String zipFileName)
	{
		this.zipFileName = zipFileName;
	}

	/**
	 * A getter of zipFileName.
	 * 
	 * @return zipFileName.
	 */
	public String getZipFileName()
	{
		return zipFileName;
	}

	/** A setter of filePath. */
	public void setFilePath(String filePath)
	{
		this.filePath = filePath;
	}

	/**
	 * A getter of filePaht.
	 * 
	 * @return String filePath.
	 */
	public String getFilePath()
	{
		return filePath;
	}

	/** Zip the all the files in the defined directory. */
	public void zipFile()
	{
		System.out.println("begins to zip files...");
		File f = new File(filePath);
		if (!f.exists())
		{
			System.out.println("file not found.");
			return;
		}
		if (!zipFileName.endsWith(".zip"))
			zipFileName = zipFileName + ".zip";
		try
		{
			File inFolder = new File(filePath);
			String[] files = inFolder.list();
			ZipOutputStream zipout = new ZipOutputStream(new FileOutputStream(
					outputPath + "/" + zipFileName));
			zipout.setLevel(Deflater.DEFAULT_COMPRESSION);
			for (int i = 0; i < files.length; i++)
			{
				System.out.println("files in the folder: " + files[i]);
				BufferedInputStream in = new BufferedInputStream(
						new FileInputStream(filePath + "/" + files[i]),
						BUFFER_SIZE);
				zipout.putNextEntry(new ZipEntry(files[i]));
				int length;
				while ((length = in.read(buffer)) != -1)
				{
					zipout.write(buffer, 0, length);
				}
				zipout.closeEntry();
			}
			zipout.flush();
			zipout.close();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
