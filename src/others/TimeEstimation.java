package others;

import java.util.List;
import reportDataObjects.TimePerformance;
import dbAccess.Dao;
import debug.ClassDebug;

/* Notes:
 * Only when the circumstances of hardware and software system are both stable 
 * is this general way of time performance prediction meaningful!
 */

public class TimeEstimation
{
	public enum Type
	{
		TYPE_MIN, TYPE_MAX, TYPE_RECENT
	};

	int timeScope;
	Dao dao;
	String identifier;
	double timeConsumeOldMin;
	double timeConsumeOldMax;
	double timeConsumeOldRecent;
	double timeConsumeEstimatedMin;
	double timeConsumeEstimatedMax;
	double timeConsumeEstimatedRecent;

	double getTimeConsumeOld(Type type)
	{
		return dao.getTimeConsume(identifier, timeScope, type);
	}

	double getTimeConsume(Type type)
	{
		double tmpTimeConsumeNew;
		List<TimePerformance> tmpList = dao.getAllTimeScopeAndTimeConsume(
				identifier, type);

		if (tmpList.size() <= 1)
			return -1;

		double[] x = new double[tmpList.size()];
		double[] y = new double[tmpList.size()];

		for (int i = 0; i < tmpList.size(); i++)
		{
			x[i] = tmpList.get(i).getTimeScope();
			y[i] = tmpList.get(i).getTimeConsume();
		}

		LeastSquareMethod l = new LeastSquareMethod(x, y, 2);
		tmpTimeConsumeNew = l.fit(timeScope);

		return tmpTimeConsumeNew;
	}

	public TimeEstimation(String fromDate, String toDate, Dao dao,
			String Identifier)
	{
		this.timeScope = ClassDebug.getTimeScope(fromDate, toDate);
		this.dao = dao;
		this.identifier = Identifier;
		timeConsumeOldMin = -1;
		timeConsumeOldMax = -1;
		timeConsumeOldRecent = -1;
		timeConsumeEstimatedMin = -1;
		timeConsumeEstimatedMax = -1;
		timeConsumeEstimatedRecent = -1;
		createTimePerformanceTable();
	}

	public boolean createTimePerformanceTable()
	{
		return dao.createTimePerformanceTable();
	}

	public boolean destroyTimePerformanceTable()
	{
		return dao.destroyTimePerformanceTable();
	}

	public double start()
	{
		timeConsumeOldMin = this.getTimeConsumeOld(Type.TYPE_MIN);
		timeConsumeOldMax = this.getTimeConsumeOld(Type.TYPE_MAX);
		timeConsumeOldRecent = this.getTimeConsumeOld(Type.TYPE_RECENT);
		if (timeConsumeOldMin == -1)
			timeConsumeEstimatedMin = this.getTimeConsume(Type.TYPE_MIN);
		else
			timeConsumeEstimatedMin = timeConsumeOldMin;
		if (timeConsumeOldMax == -1)
			timeConsumeEstimatedMax = this.getTimeConsume(Type.TYPE_MAX);
		else
			timeConsumeEstimatedMax = timeConsumeOldMax;
		if (timeConsumeOldRecent == -1)
			timeConsumeEstimatedRecent = this.getTimeConsume(Type.TYPE_RECENT);
		else
			timeConsumeEstimatedRecent = timeConsumeOldRecent;

		if (timeConsumeEstimatedMin != -1 && timeConsumeEstimatedMax != -1
				&& timeConsumeEstimatedRecent != -1)
		{
			// ClassDebug.print("Make an estimation based on the history data...");
			// ClassDebug.print("Result: ");
			// ClassDebug.print(">>Based on best cases,  it should return in: "
			// + ClassDebug.timeFormat((int)(timeConsumeEstimatedMin * 1000)));
			// ClassDebug.print(">>Based on worst cases, it should return in: "
			// + ClassDebug.timeFormat((int)(timeConsumeEstimatedMax * 1000)));
			// ClassDebug.print(">>Based on recent cases,it should return in: "
			// + ClassDebug.timeFormat((int)(timeConsumeEstimatedRecent *
			// 1000)));
			// ClassDebug.print(">>Average               it should return in: "
			// + ClassDebug.timeFormat((int)(timeConsumeEstimatedMin * 1000 +
			// timeConsumeEstimatedMax * 1000 + timeConsumeEstimatedRecent *
			// 1000) / 3));
			// ClassDebug.print(">>Average[improved],    it should return in: "
			// + ClassDebug.timeFormat((int)(((timeConsumeEstimatedMin * 1000 +
			// timeConsumeEstimatedMax * 1000) / 2) * 0.5 +
			// timeConsumeEstimatedRecent * 1000 * 0.5)));
			return (((timeConsumeEstimatedMin + timeConsumeEstimatedMax) / 2) * 0.5 + timeConsumeEstimatedRecent * 0.5) * 1000;
		} else
			return -1;
		// ClassDebug.print("Not enough history data, unable to make an estimation!");
	}

	public void end(double TimeConsumeNew)
	{
		TimeConsumeNew = TimeConsumeNew / 1000;
		if (timeConsumeOldMin == -1 || timeConsumeOldMax == -1
				|| timeConsumeOldRecent == -1)
			dao.UpdateTimeConsumeTable(identifier, timeScope, TimeConsumeNew,
					TimeConsumeNew, TimeConsumeNew);
		else if (TimeConsumeNew < timeConsumeOldMin)
			dao.UpdateTimeConsumeTable(identifier, timeScope, TimeConsumeNew,
					timeConsumeOldMax, TimeConsumeNew);
		else if (TimeConsumeNew > timeConsumeOldMax)
			dao.UpdateTimeConsumeTable(identifier, timeScope,
					timeConsumeOldMin, TimeConsumeNew, TimeConsumeNew);
		else
			dao.UpdateTimeConsumeTable(identifier, timeScope,
					timeConsumeOldMin, timeConsumeOldMax, TimeConsumeNew);
	}
}
