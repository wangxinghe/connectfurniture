package others;

import java.util.*;
import reportDataObjects.KeywordSearch;

public class Unit
{
	int min;
	int max;
	int value;
	List<KeywordSearch> list;

	public List<KeywordSearch> getList()
	{
		return list;
	}

	public void setList(List<KeywordSearch> list)
	{
		this.list = list;
	}

	public int getMin()
	{
		return min;
	}

	public int getMax()
	{
		return max;
	}

	public int getValue()
	{
		return value;
	}

	public void setMin(int min)
	{
		this.min = min;
	}

	public void setMax(int max)
	{
		this.max = max;
	}

	public void setValue(int value)
	{
		this.value = value;
	}
}
