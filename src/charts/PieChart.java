package charts;

import java.awt.Color;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;

/**
 * This class is used to generate a pie chart
 * 
 */
public class PieChart
{

	/**
	 * Creates a DefaultPieDataset for method createPieChart
	 * 
	 * @param tmpHashMap
	 *            A hashmap contains data
	 * @return DefaultPieDataset dataset
	 */
	public static DefaultPieDataset createDataset(
			HashMap<String, Object> tmpHashMap)
	{
		DefaultPieDataset dataset = new DefaultPieDataset();
		for (String key : tmpHashMap.keySet())
		{
			dataset.setValue(key, (Integer) tmpHashMap.get(key));
		}
		return dataset;
	}

	/**
	 * Creates a pie chart
	 * 
	 * @param tmpHashMap
	 *            A hashmap contains data
	 * @return JFreeChart chart
	 */
	public static JFreeChart createPieChart(HashMap<String, Object> tmpHashMap)
	{
		JFreeChart chart = ChartFactory.createPieChart("",
				createDataset(tmpHashMap), true, true, false);
		PiePlot plot = (PiePlot) chart.getPlot();
		plot.setOutlineVisible(false);
		plot.setBackgroundPaint(Color.white);
		plot.setOutlineVisible(false);
		chart.setBackgroundPaint(Color.white);
		String unitSytle = "{0}: {1}({2})";

		plot.setLabelGenerator(new StandardPieSectionLabelGenerator("{0}({2})",
				NumberFormat.getNumberInstance(), new DecimalFormat("0.00%")));
		plot.setLegendLabelGenerator(new StandardPieSectionLabelGenerator(
				unitSytle, NumberFormat.getNumberInstance(), new DecimalFormat(
						"0.00%")));

		return chart;

	}

}
