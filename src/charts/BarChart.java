package charts;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;

import reportDataObjects.RequestByHour;
import reportDataObjects.SessionByHour;
import reportDataObjects.TopDimensionValues;
import reportDataObjects.TopSearch;

/**
 * This class is used to generate bar charts for reports Top Dimension Searches,
 * Top Dimension-Value Searches, Request By Hour, User Trend and Session By Hour
 */
public class BarChart
{

	/**
	 * Creates a DefaultCategoryDataset for method createBarChart to generate a
	 * bar chart for Top Dimension Searches report
	 * 
	 * @param key
	 *            A certain key of HashMap<String, Object> topDimsSearchLists,
	 *            which refers to a certain week & year pair like: Week1-2011
	 * @param topDimsSearchLists
	 *            A hashmap containing the result lists of Top Dimension
	 *            Searches
	 * @return DefaultCategoryDataset of JFreeChart
	 */
	@SuppressWarnings("unchecked")
	public static DefaultCategoryDataset createDataset(String key,
			HashMap<String, Object> topDimsSearchLists)
	{
		DefaultCategoryDataset categoryDataset = new DefaultCategoryDataset();
		List<TopSearch> tsList = (List<TopSearch>) topDimsSearchLists.get(key);
		for (int i = 0; i < tsList.size(); i++)
		{
			String rowKey1 = "Requests";
			String columnKey1 = String.valueOf(i + 1);
			categoryDataset.setValue(tsList.get(i).getCount(), rowKey1,
					columnKey1);
		}
		return categoryDataset;
	}

	/**
	 * Creates a DefaultCategoryDataset for method createBarChartForValues to
	 * generate a bar chart for Top Dimension-Value Searches report
	 * 
	 * @param key
	 *            A certain key of HashMap<String, Object> topValsSearchLists,
	 *            which refers to a certain week & year pair like: Week1-2011
	 * @param topValsSearchLists
	 *            A hashmap containing the result lists of Top Dimension-Value
	 *            Searches
	 * @return DefaultCategoryDataset of JFreeChart
	 */
	@SuppressWarnings("unchecked")
	public static DefaultCategoryDataset createDatasetForValues(String key,
			HashMap<String, Object> topValsSearchLists)
	{
		DefaultCategoryDataset categoryDataset = new DefaultCategoryDataset();
		// List<TopValuesSearch> tsList = (List<TopValuesSearch>)
		// topValsSearchLists.get(key);
		List<TopDimensionValues> tsList = (List<TopDimensionValues>) topValsSearchLists
				.get(key);
		for (int i = 0; i < tsList.size(); i++)
		{
			String rowKey1 = "Requests";
			String columnKey1 = String.valueOf(i + 1);
			categoryDataset.setValue(tsList.get(i).getCount(), rowKey1,
					columnKey1);
		}
		return categoryDataset;
	}

	/**
	 * Creates a list of bar charts using a hashmap which contains results of
	 * Top Dimension Searches passed from FormController
	 * 
	 * @param topDimsSearchLists
	 *            A hashmap containing result lists of Top Dimension Searches
	 *            report
	 * @return An arrayList which contains generated bar charts
	 */
	public static ArrayList<JFreeChart> createBarChart(
			HashMap<String, Object> topDimsSearchLists)
	{
		ArrayList<JFreeChart> chartList = new ArrayList<JFreeChart>();
		for (String key : topDimsSearchLists.keySet())
		{
			JFreeChart chart = ChartFactory.createBarChart("", "Dimensions(#)",
					"", createDataset(key, topDimsSearchLists),
					PlotOrientation.VERTICAL, false, false, false);
			CategoryPlot plot = chart.getCategoryPlot();
			NumberAxis numberaxis = (NumberAxis) plot.getRangeAxis();
			numberaxis
					.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
			ValueAxis rangeAxis = plot.getRangeAxis();
			rangeAxis.setUpperMargin(0.2);
			CategoryAxis domainAxis = plot.getDomainAxis();
			domainAxis.setLowerMargin(0.01);
			domainAxis.setUpperMargin(0);
			domainAxis.setCategoryLabelPositionOffset(1);
			domainAxis.setCategoryMargin(0.1);
			plot.setBackgroundPaint(Color.white);
			plot.setOutlineVisible(false);
			BarRenderer renderer = (BarRenderer) plot.getRenderer();
			renderer.setDrawBarOutline(false);
			renderer.setShadowVisible(false);
			renderer.setSeriesPaint(0, new Color(58, 111, 6));
			plot.setRenderer(renderer);
			chartList.add(chart);
		}
		return chartList;

	}

	/**
	 * Creates a list of bar charts using a hashmap which contains results of
	 * Top Dimension-Value Searches passed from FormController
	 * 
	 * @param topValsSearchLists
	 *            A hashmap containing result lists of Top Dimension-Value
	 *            Searches report
	 * @return An arrayList which contains generated bar charts
	 */
	public static ArrayList<JFreeChart> createBarChartForValues(
			HashMap<String, Object> topValsSearchLists)
	{
		ArrayList<JFreeChart> chartList = new ArrayList<JFreeChart>();
		for (String key : topValsSearchLists.keySet())
		{
			JFreeChart chart = ChartFactory.createBarChart("", "Value(#)", "",
					createDatasetForValues(key, topValsSearchLists),
					PlotOrientation.VERTICAL, false, false, false);
			CategoryPlot plot = chart.getCategoryPlot();
			NumberAxis numberaxis = (NumberAxis) plot.getRangeAxis();
			numberaxis
					.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
			ValueAxis rangeAxis = plot.getRangeAxis();
			rangeAxis.setUpperMargin(0.2);
			CategoryAxis domainAxis = plot.getDomainAxis();
			domainAxis.setLowerMargin(0.01);
			domainAxis.setUpperMargin(0);
			domainAxis.setCategoryLabelPositionOffset(1);
			domainAxis.setCategoryMargin(0.1);
			plot.setBackgroundPaint(Color.white);
			plot.setOutlineVisible(false);
			BarRenderer renderer = (BarRenderer) plot.getRenderer();
			renderer.setDrawBarOutline(false);
			renderer.setShadowVisible(false);
			renderer.setSeriesPaint(0, new Color(58, 111, 6));
			plot.setRenderer(renderer);
			chartList.add(chart);
		}
		return chartList;

	}

	/**
	 * Creates a DefaultCategoryDataset for method
	 * createBarChartForRequestByHour to generate a bar chart for Request By
	 * Hour report
	 * 
	 * @param requestByHour
	 *            A hashmap containing the result lists of Request By Hour
	 *            report
	 * @return DefaultCategoryDataset of JFreeChart
	 */
	@SuppressWarnings("unchecked")
	public static DefaultCategoryDataset createDatasetForRequestByHour(
			HashMap<String, Object> requestByHour)
	{
		DefaultCategoryDataset categoryDataset = new DefaultCategoryDataset();

		if (requestByHour.size() == 2)
		{
			List<RequestByHour> requestByHourList = (List<RequestByHour>) requestByHour
					.get("requestByHourList");
			List<RequestByHour> requestByHourList1 = (List<RequestByHour>) requestByHour
					.get("requestByHourList1");
			for (int i = 0; i < requestByHourList.size(); i++)
			{
				String rowKey = "Date Range(1st)";
				String columnKey = String.valueOf(requestByHourList.get(i)
						.getHour());
				categoryDataset.setValue(
						requestByHourList.get(i).getRequests(), rowKey,
						columnKey);
			}
			for (int i = 0; i < requestByHourList1.size(); i++)
			{
				String rowKey1 = "Date Range(2nd)";
				String columnKey1 = String.valueOf(requestByHourList1.get(i)
						.getHour());
				categoryDataset.setValue(requestByHourList1.get(i)
						.getRequests(), rowKey1, columnKey1);
			}
		} else
		{
			List<RequestByHour> requestByHourList = (List<RequestByHour>) requestByHour
					.get("requestByHourList");
			for (int i = 0; i < requestByHourList.size(); i++)
			{
				String rowKey = "Requests";
				String columnKey = String.valueOf(requestByHourList.get(i)
						.getHour());
				categoryDataset.setValue(
						requestByHourList.get(i).getRequests(), rowKey,
						columnKey);
			}
		}

		return categoryDataset;
	}

	/**
	 * Creates a bar chart for Request By Hour report
	 * 
	 * @param requestByHour
	 *            A hashmap containing the result lists of Request By Hour
	 *            report passed from FormController
	 * @return A chart of type JFreeChart
	 */
	public static JFreeChart createBarChartForRequestByHour(
			HashMap<String, Object> requestByHour)
	{

		if (requestByHour.size() == 2)
		{
			JFreeChart chart = ChartFactory.createBarChart("", "Hours", "",
					createDatasetForRequestByHour(requestByHour),
					PlotOrientation.VERTICAL, true, true, false);
			CategoryPlot plot = chart.getCategoryPlot();
			NumberAxis numberaxis = (NumberAxis) plot.getRangeAxis();
			numberaxis
					.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
			ValueAxis rangeAxis = plot.getRangeAxis();
			rangeAxis.setUpperMargin(0.2);
			CategoryAxis domainAxis = plot.getDomainAxis();
			domainAxis.setLowerMargin(0.01);
			domainAxis.setUpperMargin(0);
			domainAxis.setCategoryLabelPositionOffset(1);
			domainAxis.setCategoryMargin(0.2);
			plot.setBackgroundPaint(Color.white);
			plot.setOutlineVisible(false);
			BarRenderer renderer = (BarRenderer) plot.getRenderer();
			renderer.setDrawBarOutline(false);
			renderer.setShadowVisible(false);
			renderer.setItemMargin(0);
			renderer.setSeriesPaint(0, new Color(58, 111, 6));
			renderer.setSeriesPaint(1, new Color(211, 64, 56));
			plot.setRenderer(renderer);
			return chart;
		} else
		{
			JFreeChart chart = ChartFactory.createBarChart("", "Hours", "",
					createDatasetForRequestByHour(requestByHour),
					PlotOrientation.VERTICAL, false, false, false);
			CategoryPlot plot = chart.getCategoryPlot();
			NumberAxis numberaxis = (NumberAxis) plot.getRangeAxis();
			numberaxis
					.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
			ValueAxis rangeAxis = plot.getRangeAxis();
			rangeAxis.setUpperMargin(0.2);
			CategoryAxis domainAxis = plot.getDomainAxis();
			domainAxis.setLowerMargin(0.01);
			domainAxis.setUpperMargin(0);
			domainAxis.setCategoryLabelPositionOffset(1);
			domainAxis.setCategoryMargin(0.2);
			plot.setBackgroundPaint(Color.white);
			plot.setOutlineVisible(false);
			BarRenderer renderer = (BarRenderer) plot.getRenderer();
			renderer.setDrawBarOutline(false);
			renderer.setShadowVisible(false);
			renderer.setSeriesPaint(0, new Color(58, 111, 6));
			plot.setRenderer(renderer);
			return chart;
		}

	}

	/**
	 * Creates a DefaultCategoryDataset for method
	 * createBarChartForSessionByHour to generate a bar chart for Session By
	 * Hour report
	 * 
	 * @param sessionByHour
	 *            A hashmap containing the result lists of Session By Hour
	 *            report
	 * @return DefaultCategoryDataset of JFreeChart
	 */
	@SuppressWarnings("unchecked")
	public static DefaultCategoryDataset createDatasetForSessionByHour(
			HashMap<String, Object> sessionByHour)
	{
		DefaultCategoryDataset categoryDataset = new DefaultCategoryDataset();

		if (sessionByHour.size() == 2)
		{
			List<SessionByHour> sessionByHourList = (List<SessionByHour>) sessionByHour
					.get("sessionByHourList");
			List<SessionByHour> sessionByHourList1 = (List<SessionByHour>) sessionByHour
					.get("sessionByHourList1");
			for (int i = 0; i < sessionByHourList.size(); i++)
			{
				String rowKey = "Date Range(1st)";
				String columnKey = String.valueOf(sessionByHourList.get(i)
						.getHour());
				categoryDataset.setValue(
						sessionByHourList.get(i).getSessions(), rowKey,
						columnKey);
			}
			for (int i = 0; i < sessionByHourList1.size(); i++)
			{
				String rowKey1 = "Date Range(2nd)";
				String columnKey1 = String.valueOf(sessionByHourList1.get(i)
						.getHour());
				categoryDataset.setValue(sessionByHourList1.get(i)
						.getSessions(), rowKey1, columnKey1);
			}
		} else
		{
			List<SessionByHour> sessionByHourList = (List<SessionByHour>) sessionByHour
					.get("sessionByHourList");
			for (int i = 0; i < sessionByHourList.size(); i++)
			{
				String rowKey = "Sessions";
				String columnKey = String.valueOf(sessionByHourList.get(i)
						.getHour());
				categoryDataset.setValue(
						sessionByHourList.get(i).getSessions(), rowKey,
						columnKey);
			}
		}

		return categoryDataset;
	}

	/**
	 * Creates a bar chart for Session By Hour report
	 * 
	 * @param sessionByHour
	 *            A hashmap containing the result lists of Session By Hour
	 *            report passed from the FormController
	 * @return A chart of type JFreeChart
	 */
	public static JFreeChart createBarChartForSessionByHour(
			HashMap<String, Object> sessionByHour)
	{

		if (sessionByHour.size() == 2)
		{
			JFreeChart chart = ChartFactory.createBarChart("", "Hours", "",
					createDatasetForSessionByHour(sessionByHour),
					PlotOrientation.VERTICAL, true, true, false);
			CategoryPlot plot = chart.getCategoryPlot();
			NumberAxis numberaxis = (NumberAxis) plot.getRangeAxis();
			numberaxis
					.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
			ValueAxis rangeAxis = plot.getRangeAxis();
			rangeAxis.setUpperMargin(0.2);
			CategoryAxis domainAxis = plot.getDomainAxis();
			domainAxis.setLowerMargin(0.01);
			domainAxis.setUpperMargin(0);
			domainAxis.setCategoryLabelPositionOffset(1);
			domainAxis.setCategoryMargin(0.2);
			plot.setBackgroundPaint(Color.white);
			plot.setOutlineVisible(false);
			BarRenderer renderer = (BarRenderer) plot.getRenderer();
			renderer.setDrawBarOutline(false);
			renderer.setShadowVisible(false);
			renderer.setItemMargin(0);
			renderer.setSeriesPaint(0, new Color(58, 111, 6));
			renderer.setSeriesPaint(1, new Color(211, 64, 56));
			plot.setRenderer(renderer);
			return chart;
		} else
		{
			JFreeChart chart = ChartFactory.createBarChart("", "Hours", "",
					createDatasetForSessionByHour(sessionByHour),
					PlotOrientation.VERTICAL, false, false, false);
			CategoryPlot plot = chart.getCategoryPlot();
			NumberAxis numberaxis = (NumberAxis) plot.getRangeAxis();
			numberaxis
					.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
			ValueAxis rangeAxis = plot.getRangeAxis();
			rangeAxis.setUpperMargin(0.2);
			CategoryAxis domainAxis = plot.getDomainAxis();
			domainAxis.setLowerMargin(0.01);
			domainAxis.setUpperMargin(0);
			domainAxis.setCategoryLabelPositionOffset(1);
			domainAxis.setCategoryMargin(0.2);
			plot.setBackgroundPaint(Color.white);
			plot.setOutlineVisible(false);
			BarRenderer renderer = (BarRenderer) plot.getRenderer();
			renderer.setDrawBarOutline(false);
			renderer.setShadowVisible(false);
			renderer.setSeriesPaint(0, new Color(58, 111, 6));
			plot.setRenderer(renderer);
			return chart;
		}

	}

	/**
	 * Creates a DefaultCategoryDataset for method createBarChartForTrend to
	 * generate a bar chart User Trend report
	 * 
	 * @param trend
	 *            A hashmap containing the result lists of User Trend report
	 * @return DefaultCategoryDataset of JFreeChart
	 */
	public static DefaultCategoryDataset createDatasetForTrend(
			HashMap<String, Object> trend)
	{
		DefaultCategoryDataset categoryDataset = new DefaultCategoryDataset();
		for (String key : trend.keySet())
		{
			String rowKey = "Date Range";
			String columnKey = key;
			categoryDataset.setValue((Integer) trend.get(key), rowKey,
					columnKey);
		}
		return categoryDataset;
	}

	/**
	 * Creates a bar chart for User Trend report
	 * 
	 * @param trend
	 *            A hashmap containing the result lists of User Trend report
	 *            passed from the FormController
	 * @return A chart of JFreeChart
	 */
	public static JFreeChart createBarChartForTrend(
			HashMap<String, Object> trend)
	{
		JFreeChart chart = ChartFactory.createBarChart("", "Date Range", "",
				createDatasetForTrend(trend), PlotOrientation.VERTICAL, false,
				false, false);
		CategoryPlot plot = chart.getCategoryPlot();
		NumberAxis numberaxis = (NumberAxis) plot.getRangeAxis();
		numberaxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		ValueAxis rangeAxis = plot.getRangeAxis();
		rangeAxis.setUpperMargin(0.2);
		CategoryAxis domainAxis = plot.getDomainAxis();
		domainAxis.setLowerMargin(0.01);
		domainAxis.setUpperMargin(0);
		domainAxis.setCategoryLabelPositionOffset(1);
		domainAxis.setCategoryMargin(0.2);
		plot.setBackgroundPaint(Color.white);
		plot.setOutlineVisible(false);
		BarRenderer renderer = (BarRenderer) plot.getRenderer();
		renderer.setDrawBarOutline(false);
		renderer.setShadowVisible(false);
		renderer.setSeriesPaint(0, new Color(58, 111, 6));
		plot.setRenderer(renderer);
		return chart;
	}
}
