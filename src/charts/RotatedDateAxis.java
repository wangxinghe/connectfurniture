package charts;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTick;
import org.jfree.chart.axis.Tick;
import org.jfree.ui.RectangleEdge;

public class RotatedDateAxis extends DateAxis
{
	private final double tickAngle;

	private static final long serialVersionUID = 1L;

	/**
	 * @see org.jfree.chart.axis.DateAxis#refreshTicksHorizontal(java.awt.Graphics2D,
	 *      java.awt.geom.Rectangle2D, org.jfree.ui.RectangleEdge) Posted by
	 *      travis_cooper {@link}
	 *      http://www.jfree.org/phpBB2/viewtopic.php?p=71317
	 */

	/*
	 * Creates a roteated date axis with no label
	 * 
	 * @param tickAngle - the axis tick angle.
	 */
	public RotatedDateAxis(double angle)
	{
		super();
		this.tickAngle = angle;
	}

	/*
	 * Creates a date axis with the specified label.
	 * 
	 * @param label - the axis label (null permitted).
	 * 
	 * @param tickAngle - the axis tick angle.
	 */
	public RotatedDateAxis(String label, double angle)
	{
		super(label);
		this.tickAngle = angle;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected List<Tick> refreshTicksHorizontal(Graphics2D g2,
			Rectangle2D dataArea, RectangleEdge edge)
	{
		List<Tick> ticks = super.refreshTicksHorizontal(g2, dataArea, edge);
		List<Tick> ret = new ArrayList<Tick>();
		for (Tick tick : (List<Tick>) ticks)
		{
			if (tick instanceof DateTick)
			{
				DateTick dateTick = (DateTick) tick;
				ret.add(new DateTick(dateTick.getDate(), dateTick.getText(),
						dateTick.getTextAnchor(), dateTick.getRotationAnchor(),
						tickAngle));
			} else
			{
				ret.add(tick);
			}
		}
		return ret;
	}
}
