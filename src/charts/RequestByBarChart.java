package charts;

import java.awt.Color;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.DateTickMarkPosition;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.IntervalXYDataset;

import reportDataObjects.RequestBy;
import charts.SparselyLabeledCategoryAxis;

public class RequestByBarChart
{
	/**
	 * Creates a DefaultCategoryDataset for method createBarChartForRequestBy to
	 * generate a bar chart for Request By Hour of Day/ Day of Month/ Month of
	 * Year/ Date report
	 * 
	 * @param requestBy
	 *            A hashmap containing the result lists of Request By report
	 * @return DefaultCategoryDataset of JFreeChart
	 */
	@SuppressWarnings("unchecked")
	public static DefaultCategoryDataset createDatasetForRequestBy(
			HashMap<String, Object> requestBy)
	{
		DefaultCategoryDataset categoryDataset = new DefaultCategoryDataset();
		if (requestBy.get("requestByList1") != null)
		{
			List<RequestBy> requestByList = (List<RequestBy>) requestBy
					.get("requestByList");
			List<RequestBy> requestByList1 = (List<RequestBy>) requestBy
					.get("requestByList1");
			for (int i = 0; i < requestByList.size(); i++)
			{
				String rowKey = "Date Range(1st)";
				String columnKey = String.valueOf(requestByList.get(i)
						.getTime());
				categoryDataset.setValue(requestByList.get(i).getRequests(),
						rowKey, columnKey);
			}
			for (int i = 0; i < requestByList1.size(); i++)
			{
				String rowKey1 = "Date Range(2nd)";
				String columnKey1 = String.valueOf(requestByList1.get(i)
						.getTime());
				categoryDataset.setValue(requestByList1.get(i).getRequests(),
						rowKey1, columnKey1);
			}
		} else
		{
			List<RequestBy> requestByList = (List<RequestBy>) requestBy
					.get("requestByList");
			for (int i = 0; i < requestByList.size(); i++)
			{
				String rowKey = "Requests";
				String columnKey = String.valueOf(requestByList.get(i)
						.getTime());
				System.out.println("columnKey " + i + " " + columnKey + "/t");
				System.out.println("request " + "*<>* "
						+ requestByList.get(i).getRequests());
				categoryDataset.setValue(requestByList.get(i).getRequests(),
						rowKey, columnKey);
			}
		}

		return categoryDataset;
	}

	/**
	 * Creates a IntervalXYDataset for method createBarChartForRequestBy to
	 * generate a bar chart for Request By Date report The data set is a
	 * TimeSeries which can be used by the chart factory to create XYBarChart.
	 * 
	 * @param requestBy
	 *            A hashmap containing the result lists of Request By report
	 * @return IntervalXYDataset of JFreeChart
	 */
	@SuppressWarnings("unchecked")
	public static IntervalXYDataset createDatasetForRequstByGroupByDate(
			HashMap<String, Object> requestBy)
	{
		TimeSeries series = new TimeSeries("Date", Day.class);
		if (requestBy.get("requestByList1") == null)
		{
			List<RequestBy> requestByList = (List<RequestBy>) requestBy
					.get("requestByList");
			for (int i = 0; i < requestByList.size(); i++)
			{
				// get time(String) from (RequestBy)requestByList.get(index),
				// convert it from string to date, and construct a Day object
				// with the new Day(Date) constructor
				series.add(
						new Day(Date.valueOf(requestByList.get(i).getTime())),
						requestByList.get(i).getRequests());
			}
		}
		return new TimeSeriesCollection(series);
	}

	/**
	 * Creates a DefaultCategoryDataset for method createBarChartForRequestBy to
	 * generate a bar chart for Request By Hour of Day/ Day of Month/ Month of
	 * Year/ Date report
	 * 
	 * @param requestBy
	 *            A hashmap containing the result lists of Request By report
	 * @return DefaultCategoryDataset of JFreeChart
	 */
	@SuppressWarnings("unchecked")
	public static DefaultCategoryDataset createDatasetForRequstByGroupByDateCompared(
			HashMap<String, Object> requestBy)
	{
		DefaultCategoryDataset categoryDataset = new DefaultCategoryDataset();

		if (requestBy.get("requestByList1") != null)
		{
			List<RequestBy> requestByList = (List<RequestBy>) requestBy
					.get("requestByList");
			List<RequestBy> requestByList1 = (List<RequestBy>) requestBy
					.get("requestByList1");
			for (int i = 0; i < requestByList.size(); i++)
			{
				String rowKey = "Date Range(1st)";
				String columnKey = requestByList.get(i).getFtime()
						.substring(0, 5);
				categoryDataset.setValue(requestByList.get(i).getRequests(),
						rowKey, columnKey);
			}
			for (int i = 0; i < requestByList1.size(); i++)
			{
				String rowKey1 = "Date Range(2nd)";
				String columnKey1 = requestByList1.get(i).getFtime()
						.substring(0, 5);
				categoryDataset.setValue(requestByList1.get(i).getRequests(),
						rowKey1, columnKey1);
			}
		}
		return categoryDataset;
	}

	/**
	 * Creates a bar chart for Request By report
	 * 
	 * @param requestBy
	 *            A hashmap containing the result lists of Request By report
	 *            passed from FormController
	 * @return A chart of type JFreeChart
	 */
	public static JFreeChart createBarChartForRequestBy(
			HashMap<String, Object> requestBy, String timeUnit)
	{
		// If the groupby option in RequestBy function is chosen as "Date",
		// for the number of date label in the ValueAxis is larger, number of
		// ValueAxis
		// should be reduced to avoid overlapping.
		if (timeUnit.compareTo("Date") == 0)
		{
			// if size of requestBy is two, requests for two date ranges are
			// going to be compared.
			// time series can't be used in this occasion. The
			// SparselyLabledCatetoryAxis is used
			// to allow customizing number of lables in value axis.
			if (requestBy.get("requestByList1") != null)
			{
				int numberOfTicks = 20;
				// define number of lables when the new object is constructed.
				SparselyLabeledCategoryAxis customAxis = new SparselyLabeledCategoryAxis(
						numberOfTicks);
				JFreeChart chart = ChartFactory.createBarChart("", timeUnit,
						"",
						createDatasetForRequstByGroupByDateCompared(requestBy),
						PlotOrientation.VERTICAL, true, true, false);
				CategoryPlot plot = chart.getCategoryPlot();
				NumberAxis numberaxis = (NumberAxis) plot.getRangeAxis();
				numberaxis.setStandardTickUnits(NumberAxis
						.createIntegerTickUnits());
				ValueAxis rangeAxis = plot.getRangeAxis();
				rangeAxis.setUpperMargin(0.2);

				customAxis.setLowerMargin(0.01);
				customAxis.setUpperMargin(0);
				customAxis.setCategoryLabelPositionOffset(1);
				customAxis.setCategoryMargin(0.2);
				customAxis
						.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
				plot.setDomainAxis(customAxis);

				plot.setBackgroundPaint(Color.white);
				plot.setOutlineVisible(false);
				BarRenderer renderer = (BarRenderer) plot.getRenderer();
				renderer.setDrawBarOutline(false);
				renderer.setShadowVisible(false);
				renderer.setItemMargin(0);
				renderer.setSeriesPaint(0, new Color(58, 111, 6));
				renderer.setSeriesPaint(1, new Color(211, 64, 56));
				plot.setRenderer(renderer);
				return chart;
			}
			// if size of requestBy is one, use the default XYPlot
			else
			{
				// create plot ...
				IntervalXYDataset dataSet = createDatasetForRequstByGroupByDate(requestBy);
				JFreeChart chart = ChartFactory.createXYBarChart(
						(String) requestBy.get("Title"), timeUnit, true, "",
						dataSet, PlotOrientation.VERTICAL, true, true, false);
				XYPlot plot = chart.getXYPlot();
				RotatedDateAxis domainAxis = new RotatedDateAxis(0);
				domainAxis.setTickMarkPosition(DateTickMarkPosition.MIDDLE);
				NumberAxis numberAxis = (NumberAxis) plot.getRangeAxis();
				ValueAxis rangeAxis = plot.getRangeAxis();
				rangeAxis.setUpperMargin(0.2);
				numberAxis.setStandardTickUnits(NumberAxis
						.createIntegerTickUnits());
				domainAxis.setLowerMargin(0.01);
				domainAxis.setUpperMargin(0);
				domainAxis.setAutoRange(true);
				domainAxis.setDateFormatOverride(new SimpleDateFormat(
						"dd/MM/yyyy"));
				plot.setBackgroundPaint(Color.white);
				plot.setOutlineVisible(false);
				plot.setDomainAxis(domainAxis);
				XYBarRenderer renderer = (XYBarRenderer) plot.getRenderer();
				renderer.setSeriesPaint(0, new Color(58, 111, 6));
				renderer.setShadowVisible(false);
				plot.setRenderer(renderer);
				return chart;
			}
		} else
		{
			if (requestBy.size() == 2)
			{
				JFreeChart chart = ChartFactory.createBarChart("", timeUnit,
						"", createDatasetForRequestBy(requestBy),
						PlotOrientation.VERTICAL, true, true, false);
				CategoryPlot plot = chart.getCategoryPlot();
				NumberAxis numberaxis = (NumberAxis) plot.getRangeAxis();
				numberaxis.setStandardTickUnits(NumberAxis
						.createIntegerTickUnits());
				ValueAxis rangeAxis = plot.getRangeAxis();
				rangeAxis.setUpperMargin(0.2);
				CategoryAxis domainAxis = plot.getDomainAxis();
				domainAxis.setLowerMargin(0.01);
				domainAxis.setUpperMargin(0);
				domainAxis.setCategoryLabelPositionOffset(1);
				domainAxis.setCategoryMargin(0.2);
				domainAxis
						.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
				plot.setBackgroundPaint(Color.white);
				plot.setOutlineVisible(false);
				BarRenderer renderer = (BarRenderer) plot.getRenderer();
				renderer.setDrawBarOutline(false);
				renderer.setShadowVisible(false);
				renderer.setItemMargin(0);
				renderer.setSeriesPaint(0, new Color(58, 111, 6));
				renderer.setSeriesPaint(1, new Color(211, 64, 56));
				plot.setRenderer(renderer);
				return chart;
			} else
			{
				JFreeChart chart = ChartFactory.createBarChart("", timeUnit,
						"", createDatasetForRequestBy(requestBy),
						PlotOrientation.VERTICAL, false, false, false);
				CategoryPlot plot = chart.getCategoryPlot();
				NumberAxis numberaxis = (NumberAxis) plot.getRangeAxis();
				numberaxis.setStandardTickUnits(NumberAxis
						.createIntegerTickUnits());
				ValueAxis rangeAxis = plot.getRangeAxis();
				rangeAxis.setUpperMargin(0.2);
				CategoryAxis domainAxis = plot.getDomainAxis();
				domainAxis.setLowerMargin(0.01);
				domainAxis.setUpperMargin(0);
				domainAxis.setCategoryLabelPositionOffset(1);
				domainAxis.setCategoryMargin(0.2);
				domainAxis
						.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
				plot.setBackgroundPaint(Color.white);
				plot.setOutlineVisible(false);
				BarRenderer renderer = (BarRenderer) plot.getRenderer();
				renderer.setDrawBarOutline(false);
				renderer.setShadowVisible(false);
				renderer.setSeriesPaint(0, new Color(58, 111, 6));
				plot.setRenderer(renderer);
				return chart;
			}
		}
	}
}
