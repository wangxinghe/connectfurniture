package charts;

import java.awt.Color;
import java.util.ArrayList;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;

import reportDataObjects.CorrectTerms;

public class CorrectTermsBarChart
{
	/**
	 * Creates a DefaultCategoryDataset for method
	 * createBarChartForSessionByHour to generate a bar chart for Session By
	 * Hour report
	 * 
	 * @param sessionByHour
	 *            A hashmap containing the result lists of Session By Hour
	 *            report
	 * @return DefaultCategoryDataset of JFreeChart
	 */
	public static DefaultCategoryDataset createDatasetForCorrectTerms(
			ArrayList<CorrectTerms> CorrectTerms, int topNum)
	{
		DefaultCategoryDataset categoryDataset = new DefaultCategoryDataset();
		int numOfResults;
		if (CorrectTerms.size() < topNum)
			numOfResults = CorrectTerms.size();
		else
			numOfResults = topNum;
		for (int i = 0; i < numOfResults; i++)
		{
			String rowKey = "Request";
			String columnKey = String.valueOf(CorrectTerms.get(i)
					.getSearchterm());
			categoryDataset.setValue(CorrectTerms.get(i).getRequest(), rowKey,
					columnKey);
			System.out.println(CorrectTerms.get(i).getRequest());
		}

		return categoryDataset;
	}

	public static JFreeChart createBarChartForCorrectTerms(
			ArrayList<CorrectTerms> CorrectTerms, int topNum)
	{
		JFreeChart chart = ChartFactory.createBarChart("", "Searched Terms",
				"", createDatasetForCorrectTerms(CorrectTerms, topNum),
				PlotOrientation.VERTICAL, false, false, false);
		CategoryPlot plot = chart.getCategoryPlot();
		CategoryAxis xAxis = (CategoryAxis) plot.getDomainAxis();
		xAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
		NumberAxis numberaxis = (NumberAxis) plot.getRangeAxis();
		numberaxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		ValueAxis rangeAxis = plot.getRangeAxis();
		rangeAxis.setUpperMargin(0.2);
		CategoryAxis domainAxis = plot.getDomainAxis();
		domainAxis.setLowerMargin(0.01);
		domainAxis.setUpperMargin(0);
		domainAxis.setCategoryLabelPositionOffset(1);
		domainAxis.setCategoryMargin(0.2);
		plot.setBackgroundPaint(Color.white);
		plot.setOutlineVisible(false);
		BarRenderer renderer = (BarRenderer) plot.getRenderer();
		renderer.setDrawBarOutline(false);
		renderer.setShadowVisible(false);
		renderer.setSeriesPaint(0, new Color(58, 111, 6));
		plot.setRenderer(renderer);
		return chart;

	}
}
