package charts;

import java.awt.Color;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.DateTickMarkPosition;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.IntervalXYDataset;

import reportDataObjects.SessionBy;

public class SessionByBarChart
{

	/**
	 * Creates a DefaultCategoryDataset for method
	 * createBarChartForSessionByHour to generate a bar chart for Session By
	 * Hour report
	 * 
	 * @param sessionByHour
	 *            A hashmap containing the result lists of Session By Hour
	 *            report
	 * @return DefaultCategoryDataset of JFreeChart
	 */
	@SuppressWarnings("unchecked")
	public static DefaultCategoryDataset createDatasetForSessionBy(
			HashMap<String, Object> sessionBy)
	{
		DefaultCategoryDataset categoryDataset = new DefaultCategoryDataset();

		if (sessionBy.size() == 2)
		{
			List<SessionBy> sessionByList = (List<SessionBy>) sessionBy
					.get("sessionByList");
			List<SessionBy> sessionByList1 = (List<SessionBy>) sessionBy
					.get("sessionByList1");
			for (int i = 0; i < sessionByList.size(); i++)
			{
				String rowKey = "Date Range(1st)";
				String columnKey = String.valueOf(sessionByList.get(i)
						.getTime());
				categoryDataset.setValue(sessionByList.get(i).getSessions(),
						rowKey, columnKey);
			}
			for (int i = 0; i < sessionByList1.size(); i++)
			{
				String rowKey1 = "Date Range(2nd)";
				String columnKey1 = String.valueOf(sessionByList1.get(i)
						.getTime());
				categoryDataset.setValue(sessionByList1.get(i).getSessions(),
						rowKey1, columnKey1);
			}
		} else
		{
			List<SessionBy> sessionByList = (List<SessionBy>) sessionBy
					.get("sessionByList");
			for (int i = 0; i < sessionByList.size(); i++)
			{
				String rowKey = "Sessions";
				String columnKey = String.valueOf(sessionByList.get(i)
						.getTime());
				categoryDataset.setValue(sessionByList.get(i).getSessions(),
						rowKey, columnKey);
			}
		}

		return categoryDataset;
	}

	/**
	 * Creates a IntervalXYDataset for method createBarChartForsessionBy to
	 * generate a bar chart for Request By Date report
	 * 
	 * @param sessionBy
	 *            A hashmap containing the result lists of Session By report
	 * @return DefaultCategoryDataset of JFreeChart
	 */
	@SuppressWarnings("unchecked")
	public static IntervalXYDataset createDatasetForSessionByGroupByDate(
			HashMap<String, Object> sessionBy)
	{
		TimeSeries series = new TimeSeries("Date", Day.class);
		if (sessionBy.size() == 1)
		{
			List<SessionBy> sessionByList = (List<SessionBy>) sessionBy
					.get("sessionByList");
			for (int i = 0; i < sessionByList.size(); i++)
			{
				// get time(String) from (sessionBy)sessionByList.get(index),
				// convert it from string to date, and construct a Day object
				// with the new Day(Date) constructor
				series.add(
						new Day(Date.valueOf(sessionByList.get(i).getTime())),
						sessionByList.get(i).getSessions());
			}
		}
		return new TimeSeriesCollection(series);
	}

	/**
	 * Creates a DefaultCategoryDataset for method createBarChartForsessionBy to
	 * generate a bar chart for Request By Hour of Day/ Day of Month/ Month of
	 * Year/ Date report
	 * 
	 * @param sessionBy
	 *            A hashmap containing the result lists of Request By report
	 * @return DefaultCategoryDataset of JFreeChart
	 */
	@SuppressWarnings("unchecked")
	public static DefaultCategoryDataset createDatasetForRequstByGroupByDateCompared(
			HashMap<String, Object> sessionBy)
	{
		DefaultCategoryDataset categoryDataset = new DefaultCategoryDataset();

		if (sessionBy.size() == 2)
		{
			List<SessionBy> sessionByList = (List<SessionBy>) sessionBy
					.get("sessionByList");
			List<SessionBy> sessionByList1 = (List<SessionBy>) sessionBy
					.get("sessionByList1");
			for (int i = 0; i < sessionByList.size(); i++)
			{
				String rowKey = "Date Range(1st)";
				String columnKey = sessionByList.get(i).getFtime()
						.substring(0, 5);
				categoryDataset.setValue(sessionByList.get(i).getSessions(),
						rowKey, columnKey);
			}
			for (int i = 0; i < sessionByList1.size(); i++)
			{
				String rowKey1 = "Date Range(2nd)";
				String columnKey1 = sessionByList1.get(i).getFtime()
						.substring(0, 5);
				categoryDataset.setValue(sessionByList1.get(i).getSessions(),
						rowKey1, columnKey1);
			}
		}
		return categoryDataset;
	}

	public static JFreeChart createBarChartForSessionBy(
			HashMap<String, Object> sessionBy, String type)
	{
		if (type.compareTo("Date") == 0)
		{
			if (sessionBy.size() == 2)
			{
				int numberOfTicks = 20;
				SparselyLabeledCategoryAxis customAxis = new SparselyLabeledCategoryAxis(
						numberOfTicks);
				JFreeChart chart = ChartFactory.createBarChart("", type, "",
						createDatasetForRequstByGroupByDateCompared(sessionBy),
						PlotOrientation.VERTICAL, true, true, false);
				CategoryPlot plot = chart.getCategoryPlot();
				NumberAxis numberaxis = (NumberAxis) plot.getRangeAxis();
				numberaxis.setStandardTickUnits(NumberAxis
						.createIntegerTickUnits());
				ValueAxis rangeAxis = plot.getRangeAxis();
				rangeAxis.setUpperMargin(0.2);

				customAxis.setLowerMargin(0.01);
				customAxis.setUpperMargin(0);
				customAxis.setCategoryLabelPositionOffset(1);
				customAxis.setCategoryMargin(0.2);
				customAxis
						.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
				plot.setDomainAxis(customAxis);

				plot.setBackgroundPaint(Color.white);
				plot.setOutlineVisible(false);
				BarRenderer renderer = (BarRenderer) plot.getRenderer();
				renderer.setDrawBarOutline(false);
				renderer.setShadowVisible(false);
				renderer.setItemMargin(0);
				renderer.setSeriesPaint(0, new Color(58, 111, 6));
				renderer.setSeriesPaint(1, new Color(211, 64, 56));
				plot.setRenderer(renderer);
				return chart;
			} else
			{
				// create plot ...
				IntervalXYDataset dataSet = createDatasetForSessionByGroupByDate(sessionBy);
				JFreeChart chart = ChartFactory.createXYBarChart("", type,
						true, "", dataSet, PlotOrientation.VERTICAL, true,
						true, false);
				XYPlot plot = chart.getXYPlot();
				RotatedDateAxis domainAxis = new RotatedDateAxis(0);
				domainAxis.setTickMarkPosition(DateTickMarkPosition.MIDDLE);
				NumberAxis numberAxis = (NumberAxis) plot.getRangeAxis();
				ValueAxis rangeAxis = plot.getRangeAxis();
				rangeAxis.setUpperMargin(0.2);
				numberAxis.setStandardTickUnits(NumberAxis
						.createIntegerTickUnits());
				domainAxis.setLowerMargin(0.01);
				domainAxis.setUpperMargin(0);
				domainAxis.setAutoRange(true);
				domainAxis.setDateFormatOverride(new SimpleDateFormat(
						"dd/MM/yyyy"));
				plot.setBackgroundPaint(Color.white);
				plot.setOutlineVisible(false);
				plot.setDomainAxis(domainAxis);
				XYBarRenderer renderer = (XYBarRenderer) plot.getRenderer();
				renderer.setSeriesPaint(0, new Color(58, 111, 6));
				renderer.setShadowVisible(false);
				plot.setRenderer(renderer);
				return chart;
			}
		} else
		{
			if (sessionBy.size() == 2)
			{
				JFreeChart chart = ChartFactory.createBarChart("", type, "",
						createDatasetForSessionBy(sessionBy),
						PlotOrientation.VERTICAL, true, true, false);
				CategoryPlot plot = chart.getCategoryPlot();
				CategoryAxis xAxis = (CategoryAxis) plot.getDomainAxis();
				xAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
				NumberAxis numberaxis = (NumberAxis) plot.getRangeAxis();
				numberaxis.setStandardTickUnits(NumberAxis
						.createIntegerTickUnits());
				ValueAxis rangeAxis = plot.getRangeAxis();
				rangeAxis.setUpperMargin(0.2);
				CategoryAxis domainAxis = plot.getDomainAxis();
				domainAxis.setLowerMargin(0.01);
				domainAxis.setUpperMargin(0);
				domainAxis.setCategoryLabelPositionOffset(1);
				domainAxis.setCategoryMargin(0.2);
				plot.setBackgroundPaint(Color.white);
				plot.setOutlineVisible(false);
				BarRenderer renderer = (BarRenderer) plot.getRenderer();
				renderer.setDrawBarOutline(false);
				renderer.setShadowVisible(false);
				renderer.setItemMargin(0);
				renderer.setSeriesPaint(0, new Color(58, 111, 6));
				renderer.setSeriesPaint(1, new Color(211, 64, 56));
				plot.setRenderer(renderer);
				return chart;
			} else
			{
				JFreeChart chart = ChartFactory.createBarChart("", type, "",
						createDatasetForSessionBy(sessionBy),
						PlotOrientation.VERTICAL, false, false, false);
				CategoryPlot plot = chart.getCategoryPlot();
				CategoryAxis xAxis = (CategoryAxis) plot.getDomainAxis();
				xAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
				NumberAxis numberaxis = (NumberAxis) plot.getRangeAxis();
				numberaxis.setStandardTickUnits(NumberAxis
						.createIntegerTickUnits());
				ValueAxis rangeAxis = plot.getRangeAxis();
				rangeAxis.setUpperMargin(0.2);
				CategoryAxis domainAxis = plot.getDomainAxis();
				domainAxis.setLowerMargin(0.01);
				domainAxis.setUpperMargin(0);
				domainAxis.setCategoryLabelPositionOffset(1);
				domainAxis.setCategoryMargin(0.2);
				plot.setBackgroundPaint(Color.white);
				plot.setOutlineVisible(false);
				BarRenderer renderer = (BarRenderer) plot.getRenderer();
				renderer.setDrawBarOutline(false);
				renderer.setShadowVisible(false);
				renderer.setSeriesPaint(0, new Color(58, 111, 6));
				plot.setRenderer(renderer);
				return chart;
			}
		}
	}
}
