package validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import springMVC.FormFormater;

import forms.SessionByHourForm;

public class SessionbyhourFormValidator implements Validator
{

	public boolean supports(Class<?> clazz)
	{
		// TODO Auto-generated method stub
		return SessionByHourForm.class.isAssignableFrom(clazz);
	}

	public void validate(Object target, Errors errors)
	{
		Pattern pattern = Pattern
				.compile("^(((0[1-9]|[12]\\d|3[01])\\/(0[13578]|1[02])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|[12]\\d|30)\\/(0[13456789]|1[012])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|1\\d|2[0-8])\\/02\\/((19|[2-9]\\d)\\d{2}))|(29\\/02\\/((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$");
		if (target.getClass().equals(SessionByHourForm.class))
		{
			SessionByHourForm shf = (SessionByHourForm) target;
			if (shf.getToDate().equals("") || shf.getFromDate().equals(""))
			{
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fromDate",
						"field.required", "Please provide the start date");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "toDate",
						"field.required", "Please provide the end date");
			} else
			{
				Matcher matcher1 = pattern.matcher(shf.getFromDate());
				Matcher matcher2 = pattern.matcher(shf.getToDate());
				if (!matcher1.matches() || !matcher2.matches())
				{
					if (!matcher1.matches())
					{
						errors.rejectValue("fromDate", "wrong.format");
					} else
					{
						errors.rejectValue("toDate", "wrong.format");
					}
				} else
				{
					if (!shf.getToDate().equals(shf.getFromDate()))
					{
						if (!FormFormater.compareDates(
								FormFormater.formatDate(shf.getToDate()),
								FormFormater.formatDate(shf.getFromDate())))
						{
							errors.rejectValue("toDate", "wrong.date");
						}
					}
				}

			}
			if (shf.getCompareFromDate().equals("")
					&& (!shf.getCompareToDate().equals(""))
					|| (!shf.getCompareFromDate().equals(""))
					&& shf.getCompareToDate().equals(""))
			{
				if (shf.getCompareFromDate().equals(""))
				{
					errors.rejectValue("compareFromDate",
							"wrong.compareFromDate");
				} else if (shf.getCompareToDate().equals(""))
				{
					errors.rejectValue("compareToDate", "wrong.compareToDate");
				}
			} else if ((!shf.getCompareFromDate().equals(""))
					&& (!shf.getCompareToDate().equals("")))
			{
				Matcher matcher1 = pattern.matcher(shf.getCompareFromDate());
				Matcher matcher2 = pattern.matcher(shf.getCompareToDate());
				if (!matcher1.matches() || !matcher2.matches())
				{
					if (!matcher1.matches())
					{
						errors.rejectValue("compareFromDate", "wrong.format");
					} else
					{
						errors.rejectValue("compareToDate", "wrong.format");
					}
				} else
				{
					if (!shf.getCompareFromDate()
							.equals(shf.getCompareToDate()))
					{
						if (!FormFormater
								.compareDates(FormFormater.formatDate(shf
										.getCompareToDate()), FormFormater
										.formatDate(shf.getCompareFromDate())))
						{
							errors.rejectValue("compareToDate", "wrong.date");
						}
					}
				}
			}
		}
	}
}
