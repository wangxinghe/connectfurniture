package validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import springMVC.FormFormater;
import forms.TopSearchesForm;

public class TopsearchesFormValidator implements Validator
{

	@Override
	public boolean supports(Class<?> clazz)
	{
		// TODO Auto-generated method stub
		return TopSearchesForm.class.isAssignableFrom(clazz);
	}

	public void validate(Object target, Errors errors)
	{
		Pattern pattern = Pattern
				.compile("^(((0[1-9]|[12]\\d|3[01])\\/(0[13578]|1[02])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|[12]\\d|30)\\/(0[13456789]|1[012])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|1\\d|2[0-8])\\/02\\/((19|[2-9]\\d)\\d{2}))|(29\\/02\\/((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$");

		if (target.getClass().equals(TopSearchesForm.class))
		{
			TopSearchesForm tsf = (TopSearchesForm) target;
			if (tsf.getFromDate().equals("") || tsf.getToDate().equals(""))
			{
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fromDate",
						"field.required", "Please provide the start date");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "toDate",
						"field.required", "Please provide the end date");
			} else
			{
				Matcher matcher1 = pattern.matcher(tsf.getFromDate());
				Matcher matcher2 = pattern.matcher(tsf.getToDate());
				if (!matcher1.matches() || !matcher2.matches())
				{
					if (!matcher1.matches())
					{
						errors.rejectValue("fromDate", "wrong.format");
					} else
					{
						errors.rejectValue("toDate", "wrong.format");
					}
				} else
				{
					if (!tsf.getToDate().equals(tsf.getFromDate()))
					{
						if (!FormFormater.compareDates(
								FormFormater.formatDate(tsf.getToDate()),
								FormFormater.formatDate(tsf.getFromDate())))
						{
							errors.rejectValue("toDate", "wrong.date");
						}
					}
				}
			}
		}
	}

}
