package validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import springMVC.FormFormater;

import forms.SessionByForm;

public class SessionByFormValidator implements Validator
{

	/**
	 * A validator for application-specific objects.
	 * 
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	public boolean supports(Class<?> clazz)
	{
		// TODO Auto-generated method stub
		return SessionByForm.class.isAssignableFrom(clazz);
	}

	/**
	 * Check the format of the data in the form.
	 * 
	 * @see org.springframework.validation.Validator#validate(java.lang.Object,
	 *      org.springframework.validation.Errors)
	 */
	public void validate(Object target, Errors errors)
	{
		Pattern pattern = Pattern
				.compile("^(((0[1-9]|[12]\\d|3[01])\\/(0[13578]|1[02])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|[12]\\d|30)\\/(0[13456789]|1[012])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|1\\d|2[0-8])\\/02\\/((19|[2-9]\\d)\\d{2}))|(29\\/02\\/((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$");
		if (target.getClass().equals(SessionByForm.class))
		{
			SessionByForm sf = (SessionByForm) target;
			if (sf.getToDate().equals("") || sf.getFromDate().equals(""))
			{
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fromDate",
						"field.required", "Please provide the start date");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "toDate",
						"field.required", "Please provide the end date");
			} else
			{
				Matcher matcher1 = pattern.matcher(sf.getFromDate());
				Matcher matcher2 = pattern.matcher(sf.getToDate());
				if (!matcher1.matches() || !matcher2.matches())
				{
					if (!matcher1.matches())
					{
						errors.rejectValue("fromDate", "wrong.format");
					} else
					{
						errors.rejectValue("toDate", "wrong.format");
					}
				} else
				{
					if (!sf.getToDate().equals(sf.getFromDate()))
					{
						if (!FormFormater.compareDates(
								FormFormater.formatDate(sf.getToDate()),
								FormFormater.formatDate(sf.getFromDate())))
						{
							errors.rejectValue("toDate", "wrong.date");
						}
					}
				}

			}
			if (sf.getCompareFromDate().equals("")
					&& (!sf.getCompareToDate().equals(""))
					|| (!sf.getCompareFromDate().equals(""))
					&& sf.getCompareToDate().equals(""))
			{
				if (sf.getCompareFromDate().equals(""))
				{
					errors.rejectValue("compareFromDate",
							"wrong.compareFromDate");
				} else if (sf.getCompareToDate().equals(""))
				{
					errors.rejectValue("compareToDate", "wrong.compareToDate");
				}
			} else if ((!sf.getCompareFromDate().equals(""))
					&& (!sf.getCompareToDate().equals("")))
			{
				Matcher matcher1 = pattern.matcher(sf.getCompareFromDate());
				Matcher matcher2 = pattern.matcher(sf.getCompareToDate());
				if (!matcher1.matches() || !matcher2.matches())
				{
					if (!matcher1.matches())
					{
						errors.rejectValue("compareFromDate", "wrong.format");
					} else
					{
						errors.rejectValue("compareToDate", "wrong.format");
					}
				} else
				{
					if (!sf.getCompareFromDate().equals(sf.getCompareToDate()))
					{
						if (!FormFormater
								.compareDates(FormFormater.formatDate(sf
										.getCompareToDate()), FormFormater
										.formatDate(sf.getCompareFromDate())))
						{
							errors.rejectValue("compareToDate", "wrong.date");
						}
					}
				}
			}
		}
	}
}
