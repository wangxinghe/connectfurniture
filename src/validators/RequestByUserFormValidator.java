package validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import springMVC.FormFormater;

import forms.RequestByUserForm;

public class RequestByUserFormValidator implements Validator
{
	Pattern pattern = Pattern
			.compile("^(((0[1-9]|[12]\\d|3[01])\\/(0[13578]|1[02])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|[12]\\d|30)\\/(0[13456789]|1[012])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|1\\d|2[0-8])\\/02\\/((19|[2-9]\\d)\\d{2}))|(29\\/02\\/((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$");

	@Override
	public boolean supports(Class<?> clazz)
	{
		return RequestByUserForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors)
	{

		// To debug this validator is being called and working, please
		// de-comment the following line.
		// System.out.println("requestbyformvalidator");

		if (target.getClass().equals(RequestByUserForm.class))
		{
			RequestByUserForm tmpForm = (RequestByUserForm) target;
			if (tmpForm.getToDate().equals("")
					|| tmpForm.getFromDate().equals(""))
			{
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fromDate",
						"field.required", "Please provide the start date");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "toDate",
						"field.required", "Please provide the end date");
			} else
			{
				Matcher matcher1 = pattern.matcher(tmpForm.getFromDate());
				Matcher matcher2 = pattern.matcher(tmpForm.getToDate());
				if (!matcher1.matches() || !matcher2.matches())
				{
					if (!matcher1.matches())
					{
						errors.rejectValue("fromDate", "wrong.format");
					} else
					{
						errors.rejectValue("toDate", "wrong.format");
					}
				} else
				{
					if (!tmpForm.getToDate().equals(tmpForm.getFromDate()))
					{
						if (!FormFormater.compareDates(
								FormFormater.formatDate(tmpForm.getToDate()),
								FormFormater.formatDate(tmpForm.getFromDate())))
						{
							errors.rejectValue("toDate", "wrong.date");
						}
					}
				}
			}
		}
	}
}
