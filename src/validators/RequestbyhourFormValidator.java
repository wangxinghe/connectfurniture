package validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import springMVC.FormFormater;
import forms.RequestByHourForm;

public class RequestbyhourFormValidator implements Validator
{
	Pattern pattern = Pattern
			.compile("^(((0[1-9]|[12]\\d|3[01])\\/(0[13578]|1[02])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|[12]\\d|30)\\/(0[13456789]|1[012])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|1\\d|2[0-8])\\/02\\/((19|[2-9]\\d)\\d{2}))|(29\\/02\\/((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$");

	@Override
	public boolean supports(Class<?> clazz)
	{
		return RequestByHourForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors)
	{

		// To debug this validator is being called and working, please
		// de-comment the following line.
		// System.out.println("requestbyhourformvalidator");

		if (target.getClass().equals(RequestByHourForm.class))
		{
			RequestByHourForm rhf = (RequestByHourForm) target;
			if (rhf.getToDate().equals("") || rhf.getFromDate().equals(""))
			{
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fromDate",
						"field.required", "Please provide the start date");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "toDate",
						"field.required", "Please provide the end date");
			} else
			{
				Matcher matcher1 = pattern.matcher(rhf.getFromDate());
				Matcher matcher2 = pattern.matcher(rhf.getToDate());
				if (!matcher1.matches() || !matcher2.matches())
				{
					if (!matcher1.matches())
					{
						errors.rejectValue("fromDate", "wrong.format");
					} else
					{
						errors.rejectValue("toDate", "wrong.format");
					}
				} else
				{
					if (!rhf.getToDate().equals(rhf.getFromDate()))
					{
						if (!FormFormater.compareDates(
								FormFormater.formatDate(rhf.getToDate()),
								FormFormater.formatDate(rhf.getFromDate())))
						{
							errors.rejectValue("toDate", "wrong.date");
						}
					}
				}
			}
			if (rhf.getCompareFromDate().equals("")
					&& (!rhf.getCompareToDate().equals(""))
					|| (!rhf.getCompareFromDate().equals(""))
					&& rhf.getCompareToDate().equals(""))
			{
				if (rhf.getCompareFromDate().equals(""))
				{
					errors.rejectValue("compareFromDate",
							"wrong.compareFromDate");
				} else if (rhf.getCompareToDate().equals(""))
				{
					errors.rejectValue("compareToDate", "wrong.compareToDate");
				}
			} else if ((!rhf.getCompareFromDate().equals(""))
					&& (!rhf.getCompareToDate().equals("")))
			{
				Matcher matcher1 = pattern.matcher(rhf.getCompareFromDate());
				Matcher matcher2 = pattern.matcher(rhf.getCompareToDate());
				if (!matcher1.matches() || !matcher2.matches())
				{
					if (!matcher1.matches())
					{
						errors.rejectValue("compareFromDate", "wrong.format");
					} else
					{
						errors.rejectValue("compareToDate", "wrong.format");
					}
				} else
				{
					if (!rhf.getCompareFromDate()
							.equals(rhf.getCompareToDate()))
					{
						if (!FormFormater
								.compareDates(FormFormater.formatDate(rhf
										.getCompareToDate()), FormFormater
										.formatDate(rhf.getCompareFromDate())))
						{
							errors.rejectValue("compareToDate", "wrong.date");
						}
					}
				}
			}
		}
	}
}
