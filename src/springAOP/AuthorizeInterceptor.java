package springAOP;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * This class redirects users to home page if they try to access admin pages
 * without login and provides the admin logout functionality
 * 
 */
public class AuthorizeInterceptor extends HandlerInterceptorAdapter
{

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception
	{
		boolean handlerOk = super.preHandle(request, response, handler);
		if (handlerOk)
		{
			if (request.getRequestURI().endsWith("addlogfile")
					|| request.getRequestURI().endsWith("removelogfile"))
			{
				if (request.getSession().getAttribute("username") == null)
				{
					response.sendRedirect("login");
				}
			} else if (request.getRequestURI().endsWith("logout"))
			{
				request.getSession().removeAttribute("username");
				response.sendRedirect("home");
			}
			return true;
		}
		return false;
	}

}
