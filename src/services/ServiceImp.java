package services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import reportDataObjects.RequestByHour;
import reportDataObjects.SessionByHour;
import reportDataObjects.TopDimensionValues;
import reportDataObjects.TopSearch;
import springMVC.FormFormater;

import dbAccess.Dao;

/**
 * An implementation of interface Service. This class does most of the business
 * logics, offers certain service to the FormController.
 * 
 */
public class ServiceImp implements Service
{

	Dao dao;

	public void setDao(Dao dao)
	{
		this.dao = dao;
	}

	@Override
	public void loadLogFiles(final String filePath)
	{
		System.out.println("Changing engine to InnoDB, please wait...");
		dao.changeEngineToInnoDB();
		System.out.println("Loading logs, please wait...");
		dao.loadLogs(filePath);
		System.out.println("Changing engine to MyISAM, please wait...");
		dao.changeEngineToMyISAM();
		System.out.println("Refreshing tables, please wait...");
		ArrayList<QueryThread> qtList = new ArrayList<QueryThread>();
		for (int i = 0; i <= 9; i++)
		{
			QueryThread qt = new QueryThread(dao, i);
			qt.start();
			qtList.add(qt);
		}
		for (QueryThread qt : qtList)
		{
			try
			{
				qt.join();
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
		System.out
				.println("All materialised tables have been created successfully!");
		System.out.println("Ready for use!");
	}

	@Override
	public void createTables()
	{
		dao.createTableAdmin();
		dao.createTableMainlogs();
		dao.createTableFile();
		dao.createTableDimensions();
		dao.createTableValues();
	}

	@Override
	public void removeLogs(final String start, final String end)
	{
		dao.deleteFromMainlogsTable(FormFormater.formatDate(start),
				FormFormater.formatDate(end));
		dao.deleteFromDimensionsTable();
		System.out
				.println("Removed. Changes will take effect after the next upload.");

	}

	@Override
	public boolean login(final String uname, final String pwd)
	{
		return dao.checkAdmin(uname, pwd);
	}

	@Override
	public List<TopSearch> getTopSearches(String fromDate, String toDate,
			String searchOrder, int numRecords, String depth, String[] incDims)
	{

		List<TopSearch> topDimensions = dao.getTopSearches(
				FormFormater.formatDate(fromDate),
				FormFormater.formatDate(toDate), searchOrder, numRecords,
				FormFormater.formatDepth(depth), incDims);

		return topDimensions;

	}

	@Override
	public HashMap<String, Object> getCommon()
	{
		HashMap<String, Object> common = new HashMap<String, Object>();
		common.put("title", "Admin Links");
		common.put("addPageName", "addlogfile");
		common.put("addValue", "Add Log Files");
		common.put("removePageName", "removelogfile");
		common.put("removeValue", "Remove Log Files");

		return common;
	}

	@Override
	public List<String> getDimensions()
	{

		return dao.getDimensions();
	}

	@Override
	public LinkedHashMap<String, Object> getTopDimsSearches(String fromDate,
			String toDate, String searchOrder, int numRecords, String depth,
			String breakdownBy, String[] incDims)
	{
		String lastDate = dao.getLastDate();
		String firstDate = dao.getFirstDate();
		if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), firstDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
			toDate = FormFormater.formatDateTwo(lastDate);
		} else if ((!FormFormater.compareDates(
				FormFormater.formatDate(fromDate), firstDate))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(FormFormater.formatDate(toDate),
						lastDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
		} else if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(fromDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), lastDate)))
		{
			toDate = FormFormater.formatDateTwo(lastDate);
		}
		LinkedHashMap<String, Object> topDimsSearches = new LinkedHashMap<String, Object>();
		HashMap<Integer, Object> temp = new HashMap<Integer, Object>();
		List<QueryThread> qtList = new ArrayList<QueryThread>();
		ArrayList<String> dates = new ArrayList<String>();
		if (breakdownBy.equals("All"))
		{
			topDimsSearches.put(
					"All",
					getTopSearches(fromDate, toDate, searchOrder, numRecords,
							depth, incDims));
			dates.add(" " + fromDate + " - " + toDate);
			topDimsSearches.put("dates", dates);
			return topDimsSearches;
		} else if (breakdownBy.equals("Weekly"))
		{
			ArrayList<String> weeks = FormFormater.getWeeks(fromDate, toDate);
			int size = weeks.size();
			int index = 0;
			for (int i = 0; i < size; i = i + 3)
			{
				String week = weeks.get(i);
				String startDate = weeks.get(i + 1);
				String endDate = weeks.get(i + 2);
				dates.add(" " + startDate + " - " + endDate);
				QueryThread qt = new QueryThread(index, this, temp, week,
						startDate, endDate, numRecords, depth, searchOrder,
						incDims, 10);
				topDimsSearches.put(week, index);
				qt.start();
				qtList.add(qt);
				index++;

			}
			for (QueryThread qt : qtList)
			{
				try
				{
					qt.join();
				} catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
			for (String key : topDimsSearches.keySet())
			{
				topDimsSearches.put(key,
						temp.get((Integer) topDimsSearches.get(key)));
			}
			topDimsSearches.put("dates", dates);
			return topDimsSearches;
		} else if (breakdownBy.equals("Monthly"))
		{
			ArrayList<String> months = FormFormater.getMonths(fromDate, toDate);
			int size = months.size();
			int index = 0;
			for (int i = 0; i < size; i = i + 3)
			{
				String month = months.get(i);
				String startDate = months.get(i + 1);
				String endDate = months.get(i + 2);
				dates.add(" " + startDate + " - " + endDate);
				QueryThread qt = new QueryThread(index, this, temp, month,
						startDate, endDate, numRecords, depth, searchOrder,
						incDims, 10);
				topDimsSearches.put(month, index);
				qt.start();
				qtList.add(qt);
				index++;
			}
			for (QueryThread qt : qtList)
			{
				try
				{
					qt.join();
				} catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
			for (String key : topDimsSearches.keySet())
			{
				topDimsSearches.put(key,
						temp.get((Integer) topDimsSearches.get(key)));
			}
			topDimsSearches.put("dates", dates);
			return topDimsSearches;
		} else if (breakdownBy.equals("Seasonal"))
		{
			ArrayList<String> seasons = FormFormater.getSeasons(fromDate,
					toDate);
			int size = seasons.size();
			int index = 0;
			for (int i = 0; i < size; i = i + 3)
			{
				String season = seasons.get(i);
				String startDate = seasons.get(i + 1);
				String endDate = seasons.get(i + 2);
				dates.add(" " + startDate + " - " + endDate);
				QueryThread qt = new QueryThread(index, this, temp, season,
						startDate, endDate, numRecords, depth, searchOrder,
						incDims, 10);
				topDimsSearches.put(season, index);
				qt.start();
				qtList.add(qt);
				index++;

			}
			for (QueryThread qt : qtList)
			{
				try
				{
					qt.join();
				} catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
			for (String key : topDimsSearches.keySet())
			{
				topDimsSearches.put(key,
						temp.get((Integer) topDimsSearches.get(key)));
			}
			topDimsSearches.put("dates", dates);
			return topDimsSearches;
		}

		return null;
	}

	@Override
	public HashMap<String, Object> getRequestByType(String fromDate,
			String toDate)
	{
		String lastDate = dao.getLastDate();
		String firstDate = dao.getFirstDate();
		if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), firstDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
			toDate = FormFormater.formatDateTwo(lastDate);
		} else if ((!FormFormater.compareDates(
				FormFormater.formatDate(fromDate), firstDate))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(FormFormater.formatDate(toDate),
						lastDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
		} else if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(fromDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), lastDate)))
		{
			toDate = FormFormater.formatDateTwo(lastDate);
		}
		HashMap<String, Object> requestByType = new HashMap<String, Object>();
		List<QueryThread> qtList = new ArrayList<QueryThread>();
		String date = fromDate + " - " + toDate;
		QueryThread qt0 = new QueryThread(dao, this, requestByType,
				"Search-Only Requests", FormFormater.formatDate(fromDate),
				FormFormater.formatDate(toDate), 12);
		qt0.start();
		qtList.add(qt0);
		QueryThread qt1 = new QueryThread(dao, this, requestByType,
				"Navigate-Only Requests", FormFormater.formatDate(fromDate),
				FormFormater.formatDate(toDate), 13);
		qt1.start();
		qtList.add(qt1);
		QueryThread qt2 = new QueryThread(dao, this, requestByType,
				"Search-And-Navigate Requests",
				FormFormater.formatDate(fromDate),
				FormFormater.formatDate(toDate), 14);
		qt2.start();
		qtList.add(qt2);
		requestByType.put("date", date);
		for (QueryThread qt : qtList)
		{
			try
			{
				qt.join();
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
		return requestByType;
	}

	@Override
	public HashMap<String, Object> getRequestByHour(String fromDate,
			String toDate, String compareFromDate, String compareToDate)
	{
		String lastDate = dao.getLastDate();
		String firstDate = dao.getFirstDate();
		if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), firstDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
			toDate = FormFormater.formatDateTwo(lastDate);
		} else if ((!FormFormater.compareDates(
				FormFormater.formatDate(fromDate), firstDate))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(FormFormater.formatDate(toDate),
						lastDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
		} else if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(fromDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), lastDate)))
		{
			toDate = FormFormater.formatDateTwo(lastDate);
		}
		HashMap<String, Object> requestByHour = new HashMap<String, Object>();
		String date = fromDate + " - " + toDate;
		if ((!compareFromDate.equals("")) && (!compareToDate.equals("")))
		{
			if (FormFormater.compareDates(
					FormFormater.formatDate(compareToDate), dao.getLastDate()))
			{
				compareToDate = FormFormater.formatDateTwo(dao.getLastDate());
			}
			date += "  Compared With  " + compareFromDate + " - "
					+ compareToDate;
			List<RequestByHour> requestByHourList1 = dao.getRequestByHours(
					FormFormater.formatDate(compareFromDate),
					FormFormater.formatDate(compareToDate));
			requestByHour.put("requestByHourList1", requestByHourList1);
		}
		List<RequestByHour> requestByHourList = dao.getRequestByHours(
				FormFormater.formatDate(fromDate),
				FormFormater.formatDate(toDate));
		requestByHour.put("requestByHourList", requestByHourList);
		requestByHour.put("date", date);
		return requestByHour;
	}

	@Override
	public List<String> getDimensionValues(String dim)
	{
		// TODO Auto-generated method stub
		return dao.getDimensionValues(dim);
	}

	@Override
	public HashMap<String, Object> getSessionByHour(String fromDate,
			String toDate, String compareFromDate, String compareToDate)
	{
		String lastDate = dao.getLastDate();
		String firstDate = dao.getFirstDate();
		if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), firstDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
			toDate = FormFormater.formatDateTwo(lastDate);
		} else if ((!FormFormater.compareDates(
				FormFormater.formatDate(fromDate), firstDate))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(FormFormater.formatDate(toDate),
						lastDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
		} else if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(fromDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), lastDate)))
		{
			toDate = FormFormater.formatDateTwo(lastDate);
		}
		HashMap<String, Object> sessionByHour = new HashMap<String, Object>();
		String date = fromDate + " - " + toDate;
		if ((!compareFromDate.equals("")) && (!compareToDate.equals("")))
		{
			if (FormFormater.compareDates(
					FormFormater.formatDate(compareToDate), dao.getLastDate()))
			{
				compareToDate = FormFormater.formatDateTwo(dao.getLastDate());
			}
			date += "  Compared With  " + compareFromDate + " - "
					+ compareToDate;
			List<SessionByHour> sessionByHourList1 = dao.getSessionByHours(
					FormFormater.formatDate(compareFromDate),
					FormFormater.formatDate(compareToDate));
			sessionByHour.put("sessionByHourList1", sessionByHourList1);
		}
		List<SessionByHour> sessionByHourList = dao.getSessionByHours(
				FormFormater.formatDate(fromDate),
				FormFormater.formatDate(toDate));
		sessionByHour.put("sessionByHourList", sessionByHourList);
		sessionByHour.put("date", date);
		return sessionByHour;
	}

	@Override
	public List<TopDimensionValues> getTopDimensionValues(String fromDate,
			String toDate, int numRecords, String depth, String[] incDims,
			String[] incVals, String[] incDims1, String[] incVals1)
	{
		// TODO Auto-generated method stub
		List<TopDimensionValues> topDimensionValues = dao
				.getTopDimensionValues(FormFormater.formatDate(fromDate),
						FormFormater.formatDate(toDate),
						FormFormater.formatDepth(depth), numRecords, incDims,
						incVals, incDims1, incVals1);

		return topDimensionValues;
	}

	@Override
	public LinkedHashMap<String, Object> getTopDimVals(String fromDate,
			String toDate, int numRecords, String depth, String breakdownBy,
			String[] incDims, String[] incVals, String[] incDims1,
			String[] incVals1)
	{
		// TODO Auto-generated method stub
		String lastDate = dao.getLastDate();
		String firstDate = dao.getFirstDate();
		if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), firstDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
			toDate = FormFormater.formatDateTwo(lastDate);
		} else if ((!FormFormater.compareDates(
				FormFormater.formatDate(fromDate), firstDate))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(FormFormater.formatDate(toDate),
						lastDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
		} else if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(fromDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), lastDate)))
		{
			toDate = FormFormater.formatDateTwo(lastDate);
		}
		LinkedHashMap<String, Object> topDimVals = new LinkedHashMap<String, Object>();
		HashMap<Integer, Object> temp = new HashMap<Integer, Object>();
		List<QueryThread> qtList = new ArrayList<QueryThread>();
		ArrayList<String> dates = new ArrayList<String>();
		if (breakdownBy.equals("All"))
		{
			topDimVals.put(
					"All",
					getTopDimensionValues(fromDate, toDate, numRecords, depth,
							incDims, incVals, incDims1, incVals1));
			dates.add(" " + fromDate + " - " + toDate);
			topDimVals.put("dates", dates);
			return topDimVals;
		} else if (breakdownBy.equals("Weekly"))
		{
			ArrayList<String> weeks = FormFormater.getWeeks(fromDate, toDate);
			int size = weeks.size();
			int index = 0;
			for (int i = 0; i < size; i = i + 3)
			{
				String week = weeks.get(i);
				String startDate = weeks.get(i + 1);
				String endDate = weeks.get(i + 2);
				dates.add(" " + startDate + " - " + endDate);
				QueryThread qt = new QueryThread(index, this, temp, week,
						startDate, endDate, numRecords, depth, incDims,
						incVals, incDims1, incVals1, 11);
				topDimVals.put(week, index);
				qt.start();
				qtList.add(qt);
				index++;
			}
			for (QueryThread qt : qtList)
			{
				try
				{
					qt.join();
				} catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
			for (String key : topDimVals.keySet())
			{
				topDimVals.put(key, temp.get((Integer) topDimVals.get(key)));
			}
			topDimVals.put("dates", dates);
			return topDimVals;
		} else if (breakdownBy.equals("Monthly"))
		{
			ArrayList<String> months = FormFormater.getMonths(fromDate, toDate);
			int size = months.size();
			int index = 0;
			for (int i = 0; i < size; i = i + 3)
			{
				String month = months.get(i);
				String startDate = months.get(i + 1);
				String endDate = months.get(i + 2);
				dates.add(" " + startDate + " - " + endDate);
				QueryThread qt = new QueryThread(index, this, temp, month,
						startDate, endDate, numRecords, depth, incDims,
						incVals, incDims1, incVals1, 11);
				topDimVals.put(month, index);
				qt.start();
				qtList.add(qt);
				index++;
			}
			for (QueryThread qt : qtList)
			{
				try
				{
					qt.join();
				} catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
			for (String key : topDimVals.keySet())
			{
				topDimVals.put(key, temp.get((Integer) topDimVals.get(key)));
			}
			topDimVals.put("dates", dates);
			return topDimVals;
		} else if (breakdownBy.equals("Seasonal"))
		{
			ArrayList<String> seasons = FormFormater.getSeasons(fromDate,
					toDate);
			int size = seasons.size();
			int index = 0;
			for (int i = 0; i < size; i = i + 3)
			{
				String season = seasons.get(i);
				String startDate = seasons.get(i + 1);
				String endDate = seasons.get(i + 2);
				dates.add(" " + startDate + " - " + endDate);
				QueryThread qt = new QueryThread(index, this, temp, season,
						startDate, endDate, numRecords, depth, incDims,
						incVals, incDims1, incVals1, 11);
				topDimVals.put(season, index);
				qt.start();
				qtList.add(qt);
				index++;
			}
			for (QueryThread qt : qtList)
			{
				try
				{
					qt.join();
				} catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
			for (String key : topDimVals.keySet())
			{
				topDimVals.put(key, temp.get((Integer) topDimVals.get(key)));
			}
			topDimVals.put("dates", dates);
			return topDimVals;
		}

		return null;
	}

	@Override
	public HashMap<String, Object> getTopKeywordsSearches(String fromDate,
			String toDate, String resultType, int numRecords)
	{
		String lastDate = dao.getLastDate();
		String firstDate = dao.getFirstDate();
		if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), firstDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
			toDate = FormFormater.formatDateTwo(lastDate);
		} else if ((!FormFormater.compareDates(
				FormFormater.formatDate(fromDate), firstDate))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(FormFormater.formatDate(toDate),
						lastDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
		} else if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(fromDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), lastDate)))
		{
			toDate = FormFormater.formatDateTwo(lastDate);
		}
		HashMap<String, Object> topKeywordsSearches = new HashMap<String, Object>();
		String date = fromDate + " - " + toDate;
		topKeywordsSearches.put("date", date);
		topKeywordsSearches
				.put("topKeywordsSearchesList",
						dao.getTopKeywordsSearches(
								FormFormater.formatDate(fromDate),
								FormFormater.formatDate(toDate), resultType,
								numRecords));
		return topKeywordsSearches;
	}

	@Override
	public int getTrendArray(String fromDateOfTrend, String toDateOfTrend,
			String[] incDimsOfTrend, String[] incValsOfTrend,
			String[] incDims1OfTrend, String[] incVals1OfTrend)
	{
		// TODO Auto-generated method stub
		return dao.getWeekTrend(FormFormater.formatDate(fromDateOfTrend),
				FormFormater.formatDate(toDateOfTrend), incDimsOfTrend,
				incValsOfTrend, incDims1OfTrend, incVals1OfTrend);
	}

	@Override
	public LinkedHashMap<String, Object> getTrend(String fromDateOfTrend,
			String toDateOfTrend, String[] incDimsOfTrend,
			String[] incValsOfTrend, String[] incDims1OfTrend,
			String[] incVals1oFTrend)
	{
		// TODO Auto-generated method stub
		String lastDate = dao.getLastDate();
		String firstDate = dao.getFirstDate();
		if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDateOfTrend)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDateOfTrend), firstDate)))
		{
			fromDateOfTrend = FormFormater.formatDateTwo(firstDate);
			toDateOfTrend = FormFormater.formatDateTwo(lastDate);
		} else if ((!FormFormater.compareDates(
				FormFormater.formatDate(fromDateOfTrend), firstDate))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(toDateOfTrend)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(toDateOfTrend), lastDate)))
		{
			fromDateOfTrend = FormFormater.formatDateTwo(firstDate);
		} else if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDateOfTrend)))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(fromDateOfTrend)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDateOfTrend), lastDate)))
		{
			toDateOfTrend = FormFormater.formatDateTwo(lastDate);
		}
		LinkedHashMap<String, Object> trend = new LinkedHashMap<String, Object>();
		ArrayList<String> weeks = FormFormater.getWeeks(fromDateOfTrend,
				toDateOfTrend);
		for (int i = 0; i < weeks.size(); i = i + 3)
		{
			// String week = weeks.get(i);
			String startDate = weeks.get(i + 1);
			String endDate = weeks.get(i + 2);
			trend.put(
					startDate + " - " + endDate,
					getTrendArray(startDate, endDate, incDimsOfTrend,
							incValsOfTrend, incDims1OfTrend, incVals1oFTrend));
		}
		trend.put("date", fromDateOfTrend + " - " + toDateOfTrend);
		return trend;
	}
}
