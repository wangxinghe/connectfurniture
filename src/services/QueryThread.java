package services;

import java.util.HashMap;

import springMVC.FormFormater;

import dbAccess.Dao;

/**
 * Thread for some of the queries to improve the performance
 * 
 */
public class QueryThread extends Thread
{

	Dao dao;
	ServiceImp si;
	int numRecords;
	int index;
	String depth;
	String breakdown;
	int queryNumber;
	String fromDate;
	String toDate;
	String searchOrder;
	String[] incDims;
	String[] incVals;
	String[] incDims1;
	String[] incVals1;
	String requestType;
	HashMap<Integer, Object> temp;
	HashMap<String, Object> requestByType;

	/**
	 * This constructor is for creating all the materialized tables
	 * 
	 * @param dao
	 *            a reference passed from the ServiceImp
	 * @param queryNumber
	 *            to specify which query to run in the thread
	 */
	public QueryThread(Dao dao, int queryNumber)
	{
		this.dao = dao;
		this.queryNumber = queryNumber;
	}

	/**
	 * This constructor is for the Top Dimension Search report
	 * 
	 * @param index
	 *            the position of a certain topDimensionList passed from the
	 *            FormController. Index is used to compose and sort a hashmap
	 *            later
	 * @param si
	 *            a reference of ServiceImp
	 * @param temp
	 *            a temporary hashmap to store Top Dimension Search reports
	 * @param breakdown
	 *            either weekly or monthly or seasonal
	 * @param fromDate
	 *            the from date specified
	 * @param toDate
	 *            the to date specified
	 * @param numRecords
	 *            the number of records to return
	 * @param depth
	 *            either one or two or three
	 * @param searchOrder
	 *            either yes or no
	 * @param incDims
	 *            including dimensions selected
	 * @param queryNumber
	 *            to specify which query to run in the thread
	 */
	public QueryThread(int index, ServiceImp si, HashMap<Integer, Object> temp,
			String breakdown, String fromDate, String toDate, int numRecords,
			String depth, String searchOrder, String[] incDims, int queryNumber)
	{
		this.index = index;
		this.si = si;
		this.temp = temp;
		this.breakdown = breakdown;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.numRecords = numRecords;
		this.depth = depth;
		this.searchOrder = searchOrder;
		this.incDims = incDims;
		this.queryNumber = queryNumber;
	}

	public QueryThread(int index, Dao dao, HashMap<Integer, Object> temp,
			String breakdown, String fromDate, String toDate, int numRecords,
			String depth, String searchOrder, String[] incDims, int queryNumber)
	{
		this.index = index;
		this.dao = dao;
		this.temp = temp;
		this.breakdown = breakdown;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.numRecords = numRecords;
		this.depth = depth;
		this.searchOrder = searchOrder;
		this.incDims = incDims;
		this.queryNumber = queryNumber;
	}

	/**
	 * This constructor is for the Top Dimension-Value Search report
	 * 
	 * @param index
	 *            the position of a certain topDimValList passed from the
	 *            FormController. Index is used to compose and sort a hashmap
	 *            later
	 * @param si
	 *            a reference of ServiceImp
	 * @param temp
	 *            a temporary hashmap to store Top Dimension-Value Search
	 *            reports
	 * @param breakdown
	 *            either weekly or monthly or seasonal
	 * @param fromDate
	 *            the from date specified
	 * @param toDate
	 *            the to date specified
	 * @param numRecords
	 *            the number of records to return
	 * @param depth
	 *            either one or two or three
	 * @param incDims
	 *            including dimensions selected from the first dimension-value
	 *            pair
	 * @param incVals
	 *            including values selected from the first dimension-value pair
	 * @param incDims1
	 *            including dimensions selected from the second dimension-value
	 *            pair
	 * @param incVals1
	 *            including values selected from the second dimension-value pair
	 * @param queryNumber
	 *            to specify which query to run in the thread
	 */
	public QueryThread(int index, ServiceImp si, HashMap<Integer, Object> temp,
			String breakdown, String fromDate, String toDate, int numRecords,
			String depth, String[] incDims, String[] incVals,
			String[] incDims1, String[] incVals1, int queryNumber)
	{
		this.index = index;
		this.si = si;
		this.temp = temp;
		this.breakdown = breakdown;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.numRecords = numRecords;
		this.depth = depth;
		this.incDims = incDims;
		this.incVals = incVals;
		this.incDims1 = incDims1;
		this.incVals1 = incVals1;
		this.queryNumber = queryNumber;
	}

	/**
	 * This constructor is for the Request By Type report
	 * 
	 * @param dao
	 *            a reference passed from the ServiceImp
	 * @param si
	 *            a reference of ServiceImp passed from the ServiceImp
	 * @param requestByType
	 *            specify the report
	 * @param requestType
	 *            either navigate-only or search-only or navigate then search
	 * @param fromDate
	 *            the from date specified
	 * @param toDate
	 *            the to date specified
	 * @param queryNumber
	 *            specify which query to run in the thread
	 */
	public QueryThread(Dao dao, ServiceImp si,
			HashMap<String, Object> requestByType, String requestType,
			String fromDate, String toDate, int queryNumber)
	{
		this.dao = dao;
		this.si = si;
		this.requestByType = requestByType;
		this.requestType = requestType;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.queryNumber = queryNumber;
	}

	public QueryThread(Dao dao, HashMap<String, Object> requestByType,
			String requestType, String fromDate, String toDate, int queryNumber)
	{
		this.dao = dao;
		this.requestByType = requestByType;
		this.requestType = requestType;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.queryNumber = queryNumber;
	}

	public QueryThread(int index, Dao dao, HashMap<Integer, Object> temp,
			String breakdown, String fromDate, String toDate, int numRecords,
			String depth, String[] incDims, String[] incVals,
			String[] incDims1, String[] incVals1, int queryNumber)
	{
		this.index = index;
		this.dao = dao;
		this.temp = temp;
		this.breakdown = breakdown;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.numRecords = numRecords;
		this.depth = depth;
		this.incDims = incDims;
		this.incVals = incVals;
		this.incDims1 = incDims1;
		this.incVals1 = incVals1;
		this.queryNumber = queryNumber;
	}

	@Override
	public void run()
	{
		if (queryNumber == 0)
		{
			dao.createRequestByUserTable();
			System.out
					.println("Materialised table for request by a user per day (search, navigate and all) has been created successfully");
		} else if (queryNumber == 1)
		{
			dao.createDimensionsJoinedOneNoSeq();
			System.out
					.println("Materialised table for single dimensions without sequencing has been created successfully");
		} else if (queryNumber == 2)
		{
			dao.createDimensionsJoinedOneWithSeq();
			System.out
					.println("Materialised table for single dimensions with sequencing has been created successfully");
		} else if (queryNumber == 3)
		{
			dao.createDimensionsJoinedTwoNoSeq();
			System.out
					.println("Materialised table for two dimensions without sequencing has been created successfully");
		} else if (queryNumber == 4)
		{
			dao.createDimensionsJoinedTwoWithSeq();
			System.out
					.println("Materialised table for two dimensions with sequencing has been created successfully");
		} else if (queryNumber == 5)
		{
			dao.createDimensionsJoinedThreeNoSeq();
			System.out
					.println("Materialised table for three dimensions without sequencing has been created successfully");
		} else if (queryNumber == 6)
		{
			dao.createDimensionsJoinedThreeWithSeq();
			System.out
					.println("Materialised table for three dimensions with sequencing has been created successfully");
		} else if (queryNumber == 7)
		{
			dao.createDimensionValuesJoinedOne();
			System.out
					.println("Materialised table for single dimension value pairs has been created successfully");
		} else if (queryNumber == 8)
		{
			dao.createDimensionValuesJoinedTwo();
			System.out
					.println("Materialised table for double dimension value pairs has been created successfully");
		} else if (queryNumber == 9)
		{
			dao.createDimensionValuesJoinedThree();
			System.out
					.println("Materialised table for triple dimension value pairs has been created successfully");
		} else if (queryNumber == 10)
		{
			temp.put(index, dao.getTopSearches(
					FormFormater.formatDate(fromDate),
					FormFormater.formatDate(toDate), searchOrder, numRecords,
					FormFormater.formatDepth(depth), incDims));
		} else if (queryNumber == 11)
		{
			temp.put(index, dao.getTopDimensionValues(
					FormFormater.formatDate(fromDate),
					FormFormater.formatDate(toDate),
					FormFormater.formatDepth(depth), numRecords, incDims,
					incVals, incDims1, incVals1));
		} else if (queryNumber == 12)
		{
			requestByType.put(requestType,
					dao.getSearchRequest(fromDate, toDate));
		} else if (queryNumber == 13)
		{
			requestByType
					.put(requestType, dao.getNaviRequest(fromDate, toDate));
		} else if (queryNumber == 14)
		{
			requestByType
					.put(requestType, dao.getBothRequest(fromDate, toDate));
		}
	}

}
