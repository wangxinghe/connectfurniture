package services;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import reportDataObjects.TopDimensionValues;
import reportDataObjects.TopSearch;

/**
 * This Interface invokes methods from Dao to do business logics then returns
 * results to the FormController
 * 
 */
public interface Service
{

	/**
	 * Loads the log files to the database
	 * 
	 * @param filePath
	 *            the absolute path of a certain folder containing the log files
	 */
	void loadLogFiles(String filePath);

	/**
	 * Create tables: admin, mainlogs, dimensions, valuetable and files
	 * 
	 */
	void createTables();

	/**
	 * Removes rows in the mainlogs table and the dimensions table
	 * 
	 * @param start
	 *            the from date specified
	 * @param end
	 *            the to date specified
	 */
	void removeLogs(String start, String end);

	/**
	 * Returns true if login successful or false if not
	 * 
	 * @param uname
	 *            the username input
	 * @param pwd
	 *            the password input
	 * @return true if login successful or false if not
	 */
	boolean login(String uname, String pwd);

	/**
	 * Returns a list of type TopSearch for Top Dimension Search report
	 * 
	 * @param fromDate
	 *            the from date input
	 * @param toDate
	 *            the to date input
	 * @param searchOrder
	 *            yes for order important, no for order not important
	 * @param numRecords
	 *            the number of records input
	 * @param depth
	 *            1 for one dimension, 2 for two dimensions, 3 for three
	 *            dimensions
	 * @param incDims
	 *            the including dimensions input
	 * @return a list of type TopSearch for Top Dimension Search report
	 */
	List<TopSearch> getTopSearches(String fromDate, String toDate,
			String searchOrder, int numRecords, String depth, String[] incDims);

	/**
	 * Returns a LinkedHashMap containing results of Top Dimension Search report
	 * 
	 * @param fromDate
	 *            the from date input
	 * @param toDate
	 *            the to date input
	 * @param searchOrder
	 *            yes for order important, no for order not important
	 * @param numRecords
	 *            the number of records input
	 * @param depth
	 *            1 for one dimension, 2 for two dimensions, 3 for three
	 *            dimensions
	 * @param breakdownBy
	 *            either weekly or monthly or seasonal
	 * @param incDims
	 *            the including dimensions input
	 * @return a LinkedHashMap containing results of Top Dimension Search report
	 */
	LinkedHashMap<String, Object> getTopDimsSearches(String fromDate,
			String toDate, String searchOrder, int numRecords, String depth,
			String breakdownBy, String[] incDims);

	/**
	 * Returns a hashmap containing the common things need to be displayed on
	 * every page like 'adminlogin', 'logout'
	 * 
	 * @return a hashmap containing the common things need to be displayed on
	 *         every page like 'adminlogin', 'logout'
	 */
	HashMap<String, Object> getCommon();

	/**
	 * Returns a list of distinct dimensions
	 * 
	 * @return a list of distinct dimensions
	 */
	List<String> getDimensions();

	/**
	 * Returns a hashmap containing results for the Request By Type report
	 * 
	 * @param fromDate
	 *            the from date input
	 * @param toDate
	 *            the to date input
	 * @return a hashmap containing results for the Request By Type report
	 */
	HashMap<String, Object> getRequestByType(String fromDate, String toDate);

	/**
	 * Returns a hashmap containing results for the Request By Hour report
	 * 
	 * @param fromDate
	 *            the from date input of the first date range
	 * @param toDate
	 *            the to date input of the first date range
	 * @param compareFromDate
	 *            the from date input of the second date range
	 * @param compareToDate
	 *            the to date input of the second date range
	 * @return a hashmap containing results for the Request By Hour report
	 */
	HashMap<String, Object> getRequestByHour(String fromDate, String toDate,
			String compareFromDate, String compareToDate);

	/**
	 * Returns a list of values for a specified dimension
	 * 
	 * @param dim
	 *            dimension
	 * @return a list of values for a specified dimension
	 */
	List<String> getDimensionValues(String dim);

	/**
	 * Returns a hashmap containing results for the Session By Hour report
	 * 
	 * @param fromDate
	 *            the from date input of the first date range
	 * @param toDate
	 *            the to date input of the first date range
	 * @param compareFromDate
	 *            the from date input of the second date range
	 * @param compareToDate
	 *            the to date input of the second date range
	 * @return a hashmap containing results for the Session By Hour report
	 */
	HashMap<String, Object> getSessionByHour(String fromDate, String toDate,
			String compareFromDate, String compareToDate);

	/**
	 * Returns a list of type TopDimensionValues for Top Dimension-Value Search
	 * report
	 * 
	 * @param fromDate
	 *            the from date specified
	 * @param toDate
	 *            the to date specified
	 * @param numRecords
	 *            the number of records input
	 * @param depth
	 *            1 for one dimension-value pair, 2 for two dimension-value
	 *            pairs, 3 for three dimension-value pairs
	 * @param incDims
	 *            the including dimensions input of the first dimension-value
	 *            pair
	 * @param incVals
	 *            the including values input of the first dimension-value pair
	 * @param incDims1
	 *            the including dimensions input of the second dimension-value
	 *            pair
	 * @param incVals1
	 *            the including values input of the second dimension-value pair
	 * @return a list of type TopDimensionValues for Top Dimension-Value Search
	 *         report
	 */
	List<TopDimensionValues> getTopDimensionValues(String fromDate,
			String toDate, int numRecords, String depth, String[] incDims,
			String[] incVals, String[] incDims1, String[] incVals1);

	/**
	 * Returns a LinkedHashMap containing results of Top Dimension-Value Search
	 * report
	 * 
	 * @param fromDate
	 *            the from date specified
	 * @param toDate
	 *            the to date specified
	 * @param numRecords
	 *            the number of records input
	 * @param depth
	 *            1 for one dimension-value pair, 2 for two dimension-value
	 *            pairs, 3 for three dimension-value pairs
	 * @param breakdownBy
	 *            either weekly or monthly or seasonal
	 * @param incDims
	 *            the including dimensions input of the first dimension-value
	 *            pair
	 * @param incVals
	 *            the including values input of the first dimension-value pair
	 * @param incDims1
	 *            the including dimensions input of the second dimension-value
	 *            pair
	 * @param incVals1
	 *            the including values input of the second dimension-value pair
	 * @return a LinkedHashMap containing results of Top Dimension-Value Search
	 *         report
	 */
	LinkedHashMap<String, Object> getTopDimVals(String fromDate, String toDate,
			int numRecords, String depth, String breakdownBy, String[] incDims,
			String[] incVals, String[] incDims1, String[] incVals1);

	/**
	 * Returns a hashmap containing results of the Top Keywords Search report
	 * 
	 * @param fromDate
	 *            the from date specified
	 * @param toDate
	 *            the to date specified
	 * @param resultType
	 *            either 'All' or 'ReturnZeroRecords' or 'AutoCorrected'
	 * @param numRecords
	 *            the number of records input
	 * @return a hashmap containing results of the Top Keywords Search report
	 */
	HashMap<String, Object> getTopKeywordsSearches(String fromDate,
			String toDate, String resultType, int numRecords);

	/**
	 * Returns number of requests for each week between the from date and the to
	 * date
	 * 
	 * @param fromDateOfTrend
	 *            the from date of trend input
	 * @param toDateOfTrend
	 *            the to date of trend input
	 * @param incDimsOfTrend
	 *            the dimension selected of the first dimension-value pair
	 * @param incValsOfTrend
	 *            the value selected of the first dimension-value pair
	 * @param incDims1OfTrend
	 *            the dimension selected of the second dimension-value pair
	 * @param incVals1OfTrend
	 *            the value selected of the second dimension-value pair
	 * @return number of requests for each week between the from date and the to
	 *         date
	 */
	int getTrendArray(String fromDateOfTrend, String toDateOfTrend,
			String[] incDimsOfTrend, String[] incValsOfTrend,
			String[] incDims1OfTrend, String[] incVals1OfTrend);

	/**
	 * Returns the result of the User Trend report
	 * 
	 * @param fromDateOfTrend
	 *            the from date of trend input
	 * @param toDateOfTrend
	 *            the to date of trend input
	 * @param incDimsOfTrend
	 *            the dimension selected of the first dimension-value pair
	 * @param incValsOfTrend
	 *            the value selected of the first dimension-value pair
	 * @param incDims1OfTrend
	 *            the dimension selected of the second dimension-value pair
	 * @param incVals1OfTrend
	 *            the value selected of the second dimension-value pair
	 * @return the result of the User Trend report
	 */
	LinkedHashMap<String, Object> getTrend(String fromDateOfTrend,
			String toDateOfTrend, String[] incDimsOfTrend,
			String[] incValsOfTrend, String[] incDims1OfTrend,
			String[] incVals1OfTrend);
}
