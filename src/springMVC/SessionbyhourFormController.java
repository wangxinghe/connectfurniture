package springMVC;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import charts.BarChart;

import reportDataObjects.SessionByHour;
import reports.Report;

import dbAccess.Dao;
import debug.ClassDebug;
import forms.SessionByHourForm;

@SuppressWarnings("deprecation")
public class SessionbyhourFormController extends SimpleFormController
{
	protected Dao dao;

	public void setDao(Dao dao)
	{
		this.dao = dao;
	}

	@Override
	protected HashMap<String, Object> referenceData(HttpServletRequest request)
			throws Exception
	{
		ClassDebug.print("SessionbyhourFormController.referencesData()...");
		HashMap<String, Object> model = new HashMap<String, Object>();
		if (request.getSession().getAttribute("username") == null)
		{
			model.put("pageName", "login");
			model.put("value", "AdminLogin");
		} else
		{
			model.put("pageName", "logout");
			model.put("value", "Logout");
			HashMap<String, Object> common = new HashMap<String, Object>();
			common.put("title", "Admin Links");
			common.put("addPageName", "addlogfile");
			common.put("addValue", "Add Log Files");
			common.put("removePageName", "removelogfile");
			common.put("removeValue", "Remove Log Files");
			model.put("common", common);
		}

		return model;
	}

	protected Object formBackingObject(HttpServletRequest request)
			throws Exception
	{
		ClassDebug.print("SessionbyhourFormController.formBackingObject()...");
		if (request.getRequestURI().endsWith("sessionbyhour"))
		{
			setCommandClass(SessionByHourForm.class);
			setCommandName("sessionbyhourForm");
			SessionByHourForm shf = new SessionByHourForm();

			if (request.getParameter("Reset") == null)
			{
				@SuppressWarnings("unchecked")
				HashMap<String, Object> HashMap_sessionbyhourForm = (HashMap<String, Object>) request
						.getSession().getAttribute("sessionbyhourForm");
				if (HashMap_sessionbyhourForm != null)
				{
					shf.setCompareFromDate((String) HashMap_sessionbyhourForm
							.get("compareFromDate"));
					shf.setCompareToDate((String) HashMap_sessionbyhourForm
							.get("compareToDate"));
					shf.setFromDate((String) HashMap_sessionbyhourForm
							.get("fromDate"));
					shf.setToDate((String) HashMap_sessionbyhourForm
							.get("toDate"));
				} else
				{
					shf.setFromDate(FormFormater.getPreviousMonth());
					shf.setToDate(FormFormater.getCurrentDate());
				}
			} else
			{
				@SuppressWarnings("unchecked")
				HashMap<String, Object> HashMap_sessionbyhourForm = (HashMap<String, Object>) request
						.getSession().getAttribute("sessionbyhourForm");
				if (HashMap_sessionbyhourForm != null)
					request.getSession()
							.setAttribute("sessionbyhourForm", null);
				shf.setFromDate(FormFormater.getPreviousMonth());
				shf.setToDate(FormFormater.getCurrentDate());
			}

			this.setFormView("InputView\\sessionbyhourInputView");
			return shf;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception
	{
		ClassDebug.print("SessionbyhourFormController.onSubmit()...");
		if (getCommandName().equals("sessionbyhourForm"))
		{
			SessionByHourForm form = (SessionByHourForm) command;
			getValidator().validate(form, errors);

			long startTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Start the query at "
					+ ClassDebug.getCurrentDateAndTime());

			HashMap<String, Object> HashMap_sessionbyhourForm = new HashMap<String, Object>();
			HashMap_sessionbyhourForm.put("fromDate", form.getFromDate());
			HashMap_sessionbyhourForm.put("toDate", form.getToDate());
			HashMap_sessionbyhourForm.put("compareFromDate",
					form.getCompareFromDate());
			HashMap_sessionbyhourForm.put("compareToDate",
					form.getCompareToDate());
			request.getSession().setAttribute("sessionbyhourForm",
					HashMap_sessionbyhourForm);

			HashMap<String, Object> model = new HashMap<String, Object>();
			HashMap<String, Object> sessionByHour = new HashMap<String, Object>();// =
																					// service.getSessionByHour(form.getFromDate(),
																					// form.getToDate(),
																					// form.getCompareFromDate(),
																					// form.getCompareToDate());
			String lastDate = dao.getLastDate();
			String firstDate = dao.getFirstDate();
			String fromDate = form.getFromDate();
			String toDate = form.getToDate();
			String compareFromDate = form.getCompareFromDate();
			String compareToDate = form.getCompareToDate();
			if ((!FormFormater.compareDates(lastDate,
					FormFormater.formatDate(toDate)))
					&& (!FormFormater.compareDates(
							FormFormater.formatDate(fromDate), firstDate)))
			{
				fromDate = FormFormater.formatDateTwo(firstDate);
				toDate = FormFormater.formatDateTwo(lastDate);
			} else if ((!FormFormater.compareDates(
					FormFormater.formatDate(fromDate), firstDate))
					&& (!FormFormater.compareDates(firstDate,
							FormFormater.formatDate(toDate)))
					&& (!FormFormater.compareDates(
							FormFormater.formatDate(toDate), lastDate)))
			{
				fromDate = FormFormater.formatDateTwo(firstDate);
			} else if ((!FormFormater.compareDates(lastDate,
					FormFormater.formatDate(toDate)))
					&& (!FormFormater.compareDates(firstDate,
							FormFormater.formatDate(fromDate)))
					&& (!FormFormater.compareDates(
							FormFormater.formatDate(fromDate), lastDate)))
			{
				toDate = FormFormater.formatDateTwo(lastDate);
			}
			String date = fromDate + " - " + toDate;
			if ((!compareFromDate.equals("")) && (!compareToDate.equals("")))
			{
				if (FormFormater.compareDates(
						FormFormater.formatDate(compareToDate),
						dao.getLastDate()))
				{
					compareToDate = FormFormater.formatDateTwo(dao
							.getLastDate());
				}
				date += "  Compared With  " + compareFromDate + " - "
						+ compareToDate;
				List<SessionByHour> sessionByHourList1 = dao.getSessionByHours(
						FormFormater.formatDate(compareFromDate),
						FormFormater.formatDate(compareToDate));
				sessionByHour.put("sessionByHourList1", sessionByHourList1);
			}
			List<SessionByHour> sessionByHourList = dao.getSessionByHours(
					FormFormater.formatDate(fromDate),
					FormFormater.formatDate(toDate));
			sessionByHour.put("sessionByHourList", sessionByHourList);
			sessionByHour.put("date", date);

			model.put("date", sessionByHour.get("date"));
			sessionByHour.remove("date");
			//
			JFreeChart chart = BarChart
					.createBarChartForSessionByHour(sessionByHour);
			String filePath = this.getServletContext().getRealPath("/");
			ChartUtilities.saveChartAsJPEG(new File(filePath
					+ "pages/temp/barchart.jpg"), chart, 726, 400);
			//
			if (sessionByHour.size() == 2)
			{
				int totalSessions = 0;
				int totalSessions1 = 0;
				ArrayList<SessionByHour> sbh = (ArrayList<SessionByHour>) sessionByHour
						.get("sessionByHourList");
				ArrayList<SessionByHour> sbh1 = (ArrayList<SessionByHour>) sessionByHour
						.get("sessionByHourList1");
				for (int i = 0; i < sbh.size(); i++)
				{
					totalSessions += sbh.get(i).getSessions();
				}
				for (int i = 0; i < sbh1.size(); i++)
				{
					totalSessions1 += sbh1.get(i).getSessions();
				}
				model.put("sessionByHourList", sbh);
				model.put("sessionByHourList1", sbh1);
				model.put("totalSessions", totalSessions);
				model.put("totalSessions1", totalSessions1);
				model.put("th1", "1st Hours");
				model.put("th2", "1st Sessions");
				model.put("th3", "2nd Hours");
				model.put("th4", "2nd Sessions");
				model.put("reportName", "Session By Hour");
				model.put("img", "pages/temp/barchart.jpg");
			} else
			{
				int totalSessions = 0;
				ArrayList<SessionByHour> sbh = (ArrayList<SessionByHour>) sessionByHour
						.get("sessionByHourList");
				for (int i = 0; i < sbh.size(); i++)
				{
					totalSessions += sbh.get(i).getSessions();
				}
				model.put("sessionByHourList", sbh);
				model.put("totalSessions", totalSessions);
				model.put("th1", "Hours");
				model.put("th2", "Sessions");
				model.put("reportName", "Session By Hour");
				model.put("img", "pages/temp/barchart.jpg");
			}

			if (request.getSession().getAttribute("username") == null)
			{
				model.put("pageName", "login");
				model.put("value", "AdminLogin");
			} else
			{
				model.put("pageName", "logout");
				model.put("value", "Logout");
				HashMap<String, Object> common = new HashMap<String, Object>();
				common.put("title", "Admin Links");
				common.put("addPageName", "addlogfile");
				common.put("addValue", "Add Log Files");
				common.put("removePageName", "removelogfile");
				common.put("removeValue", "Remove Log Files");
				model.put("common", common);
			}
			// start to generate reports (pdf,xls,csv)
			Report.createReports(filePath, model, "SessionByHourReport");
			model.put("savedReportName", "SessionByHourReport");

			long endTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Complete the Query at "
					+ ClassDebug.getCurrentDateAndTime());
			ClassDebug.print("Time Cost: "
					+ ClassDebug.timeFormat(endTime - startTime));

			return new ModelAndView("OutputView\\sessionbyhourOutputView",
					model);
		}
		return null;
	}
}
