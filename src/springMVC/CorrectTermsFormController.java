package springMVC;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.ModelAndView;
import dbAccess.Dao;
import debug.ClassDebug;
import forms.CorrectTermsForm;
import reportDataObjects.CorrectTerms;
import reports.CorrectTermsReport;

import charts.CorrectTermsBarChart;

/**
 * Correct Terms form controller which control the form for the auto corrected
 * terms report.
 * 
 * @author yoursoft
 * 
 */
@SuppressWarnings("deprecation")
public class CorrectTermsFormController extends SimpleFormController
{

	protected Dao dao;

	/**
	 * Set the data access object.
	 * 
	 * @param dao
	 *            data access object.
	 */
	public void setDao(Dao dao)
	{
		this.dao = dao;
	}

	/**
	 * Get the common links and put it into common.
	 * 
	 * @return the hashmap of links.
	 */
	public HashMap<String, Object> getCommon()
	{
		HashMap<String, Object> common = new HashMap<String, Object>();
		common.put("title", "Admin Links");
		common.put("addPageName", "addlogfile");
		common.put("addValue", "Add Log Files");
		common.put("removePageName", "removelogfile");
		common.put("removeValue", "Remove Log Files");
		return common;
	}

	/**
	 * Set the options you need to use in the input view.
	 * 
	 * @see org.springframework.web.servlet.mvc.SimpleFormController#referenceData(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	protected HashMap<String, Object> referenceData(HttpServletRequest request)
			throws Exception
	{
		HashMap<String, Object> model = new HashMap<String, Object>();
		if (request.getSession().getAttribute("username") == null)
		{
			model.put("pageName", "login");
			model.put("value", "AdminLogin");
		} else
		{
			model.put("pageName", "logout");
			model.put("value", "Logout");
			model.put("common", getCommon());
		}
		if (request.getRequestURI().endsWith("correctterms"))
		{
			ArrayList<String> topNum = new ArrayList<String>();
			topNum.add("10");
			topNum.add("20");
			topNum.add("30");
			model.put("topNum", topNum);
		}
		return model;
	}

	/**
	 * Retrieve a backing object for the current form from the given request.
	 * 
	 * @see org.springframework.web.servlet.mvc.AbstractFormController#formBackingObject(javax.servlet.http.HttpServletRequest)
	 */
	protected Object formBackingObject(HttpServletRequest request)
			throws Exception
	{
		if (request.getRequestURI().endsWith("correctterms"))
		{
			setCommandClass(CorrectTermsForm.class);
			setCommandName("CorrectTermsForm");
			CorrectTermsForm ctf = new CorrectTermsForm();

			if (request.getParameter("Reset") == null)
			{
				@SuppressWarnings("unchecked")
				HashMap<String, Object> HashMap_CorrectTermsForm = (HashMap<String, Object>) request
						.getSession().getAttribute("CorrectTermsForm");
				if (HashMap_CorrectTermsForm != null)
				{

					ctf.setTopNum((Integer) HashMap_CorrectTermsForm
							.get("topNum"));
				} else
				{
					ctf.setFromDate(FormFormater.getPreviousMonth());
					ctf.setToDate(FormFormater.getCurrentDate());
				}

				if (request.getSession().getAttribute("fromDate") != null
						&& request.getSession().getAttribute("toDate") != null)
				{
					ctf.setFromDate((String) request.getSession().getAttribute(
							"fromDate"));
					ctf.setToDate((String) request.getSession().getAttribute(
							"toDate"));
				}
			} else
			{
				@SuppressWarnings("unchecked")
				HashMap<String, Object> HashMap_CorrectTermsForm = (HashMap<String, Object>) request
						.getSession().getAttribute("CorrectTermsForm");
				if (HashMap_CorrectTermsForm != null)
					request.getSession().setAttribute("CorrectTermsForm", null);
				request.getSession().removeAttribute("fromDate");
				request.getSession().removeAttribute("toDate");
				ctf.setFromDate(FormFormater.getPreviousMonth());
				ctf.setToDate(FormFormater.getCurrentDate());
			}

			this.setFormView("InputView\\correctTermsInputView");
			return ctf;
		}
		return null;
	}

	/**
	 * Performing a submit action and rendering the specified success view
	 * 
	 * @see org.springframework.web.servlet.mvc.SimpleFormController#onSubmit(javax.servlet.http.HttpServletRequest,
	 *      javax.servlet.http.HttpServletResponse, java.lang.Object,
	 *      org.springframework.validation.BindException)
	 */
	@SuppressWarnings("unchecked")
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception
	{
		if (getCommandName().equals("CorrectTermsForm"))
		{
			CorrectTermsForm form = (CorrectTermsForm) command;
			getValidator().validate(form, errors);
			long startTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Start the query at "
					+ ClassDebug.getCurrentDateAndTime());
			HashMap<String, Object> HashMap_CorrectTermsForm = new HashMap<String, Object>();
			HashMap_CorrectTermsForm.put("fromDate", form.getFromDate());
			HashMap_CorrectTermsForm.put("toDate", form.getToDate());
			HashMap_CorrectTermsForm.put("topNum", form.getTopNum());
			request.getSession().setAttribute("CorrectTermsForm",
					HashMap_CorrectTermsForm);
			request.getSession().setAttribute("fromDate", form.getFromDate());
			request.getSession().setAttribute("toDate", form.getToDate());

			/**
			 * 'model' hashmap stores the data for the report.
			 */
			HashMap<String, Object> model = new HashMap<String, Object>();

			/**
			 * 'modelForView' hashmap stores the data for the output view.
			 */
			HashMap<String, Object> modelForView = new HashMap<String, Object>();
			HashMap<String, Object> CorrectTerms = getAutoCorrectedSearchTerms(
					form.getFromDate(), form.getToDate());
			modelForView.put("date", CorrectTerms.get("date"));
			model.put("date", CorrectTerms.get("date"));
			CorrectTerms.remove("date");
			ArrayList<CorrectTerms> ctsForView = new ArrayList<CorrectTerms>();
			int numOfResults;
			int n;
			n = ((ArrayList<CorrectTerms>) CorrectTerms.get("CorrectTermsList"))
					.size();
			int m;
			m = form.getTopNum();
			if (n > m)
				numOfResults = m;
			else
				numOfResults = n;
			for (int i = 0; i < numOfResults; i++)
			{
				ctsForView.add(((ArrayList<CorrectTerms>) CorrectTerms
						.get("CorrectTermsList")).get(i));
			}

			// Generate a chart for the report.
			JFreeChart chart = CorrectTermsBarChart
					.createBarChartForCorrectTerms(ctsForView, form.getTopNum());
			String filePath = this.getServletContext().getRealPath("/");
			ChartUtilities.saveChartAsJPEG(new File(filePath
					+ "pages/temp/barchart.jpg"), chart, 726, 400);
			//
			int totalRequests = 0;
			int totalRequests1 = 0;
			ArrayList<CorrectTerms> cts = (ArrayList<CorrectTerms>) CorrectTerms
					.get("CorrectTermsList");
			for (int j = 0; j < cts.size(); j++)
			{
				totalRequests += cts.get(j).getRequest();
			}
			for (int j = 0; j < ctsForView.size(); j++)
			{
				totalRequests1 += ctsForView.get(j).getRequest();
			}

			model.put("CorrectTermsList", cts);
			modelForView.put("CorrectTermsList", ctsForView);
			model.put("totalRequests", totalRequests);
			modelForView.put("totalRequests", totalRequests1);
			model.put("th1", "Search Terms");
			modelForView.put("th1", "Search Term");
			model.put("th2", "Auto Correct Terms");
			modelForView.put("th2", "Auto-Corrected Term");
			model.put("th3", "Requests");
			modelForView.put("th3", "Requests");
			model.put("reportName", "AutoCorrected Term Search");
			modelForView.put("reportName", "Report: Top " + numOfResults
					+ " Auto-Corrected Search Terms");
			model.put("img", "pages/temp/barchart.jpg");
			modelForView.put("img", "pages/temp/barchart.jpg");
			if (request.getSession().getAttribute("username") == null)
			{
				model.put("pageName", "login");
				model.put("value", "AdminLogin");
				modelForView.put("pageName", "login");
				modelForView.put("value", "AdminLogin");
			} else
			{
				model.put("pageName", "logout");
				model.put("value", "Logout");
				model.put("common", getCommon());
				modelForView.put("pageName", "logout");
				modelForView.put("value", "Logout");
				modelForView.put("common", getCommon());
			}

			// start to generate reports (pdf,xls,csv)
			CorrectTermsReport.createReports(filePath, model,
					"CorrectTermsReport");
			modelForView.put("savedReportName", "CorrectTermsReport");
			model.put("savedReportName", "CorrectTermsReport");

			// Check if the report was created to avoid problems in the View
			modelForView.put("pdfReport", new File(filePath + "pages/reports/"
					+ modelForView.get("savedReportName") + ".pdf").exists());
			modelForView.put("csvReport", new File(filePath + "pages/reports/"
					+ modelForView.get("savedReportName") + ".csv").exists());
			modelForView.put("xlsReport", new File(filePath + "pages/reports/"
					+ modelForView.get("savedReportName") + ".xls").exists());
			long endTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Complete the Query at "
					+ ClassDebug.getCurrentDateAndTime());
			ClassDebug.print("Time Cost: "
					+ ClassDebug.timeFormat(endTime - startTime));
			return new ModelAndView("OutputView\\correctTermsOutputView",
					modelForView);
		}
		return null;
	}

	/**
	 * Submit callback with all parameters.
	 * 
	 * @param fromDate
	 *            the start of the report time range.
	 * @param toDate
	 *            the end date of the report time range.
	 * @return the prepared model and view, or null.
	 */
	private HashMap<String, Object> getAutoCorrectedSearchTerms(
			String fromDate, String toDate)
	{

		// TODO Auto-generated method stub
		ClassDebug.print("CorrectTermsFormController.getCorrectTerms()...");
		String lastDate = dao.getLastDate();
		String firstDate = dao.getFirstDate();

		if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), firstDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
			toDate = FormFormater.formatDateTwo(lastDate);
		} else if ((!FormFormater.compareDates(
				FormFormater.formatDate(fromDate), firstDate))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(FormFormater.formatDate(toDate),
						lastDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
		} else if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(fromDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), lastDate)))
		{
			toDate = FormFormater.formatDateTwo(lastDate);
		}

		/**
		 * This hashmap stores the data from the database and the date.
		 */
		HashMap<String, Object> CorrectTerms_Map = new HashMap<String, Object>();
		String date = fromDate + " - " + toDate;
		List<CorrectTerms> CorrectTerms = dao.getAutoCorrectedSearchTerms(
				FormFormater.formatDate(fromDate),
				FormFormater.formatDate(toDate));
		CorrectTerms_Map.put("CorrectTermsList", CorrectTerms);
		CorrectTerms_Map.put("date", date);

		return CorrectTerms_Map;
	}

}
