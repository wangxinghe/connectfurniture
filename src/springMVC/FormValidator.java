package springMVC;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import forms.KeywordSearchForm;
import forms.LoginForm;
import forms.RemoveForm;
import forms.RequestByHourForm;
import forms.RequestByTypeForm;
import forms.SessionByHourForm;
import forms.TopDimensionValuesForm;
import forms.TopKeywordsSearchesForm;
import forms.TopSearchesForm;
import forms.UploadForm;

/**
 * This class is used to validate all the forms
 * 
 */
public class FormValidator implements Validator
{

	@Override
	public boolean supports(Class<?> clazz)
	{
		// TODO Auto-generated method stub
		return LoginForm.class.isAssignableFrom(clazz)
				|| RemoveForm.class.isAssignableFrom(clazz)
				|| UploadForm.class.isAssignableFrom(clazz)
				|| RequestByHourForm.class.isAssignableFrom(clazz)
				|| TopSearchesForm.class.isAssignableFrom(clazz)
				|| RequestByTypeForm.class.isAssignableFrom(clazz)
				|| SessionByHourForm.class.isAssignableFrom(clazz)
				|| TopDimensionValuesForm.class.isAssignableFrom(clazz)
				|| TopKeywordsSearchesForm.class.isAssignableFrom(clazz)
				|| KeywordSearchForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		Pattern pattern = Pattern
				.compile("^(((0[1-9]|[12]\\d|3[01])\\/(0[13578]|1[02])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|[12]\\d|30)\\/(0[13456789]|1[012])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|1\\d|2[0-8])\\/02\\/((19|[2-9]\\d)\\d{2}))|(29\\/02\\/((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$");
		if (target.getClass().equals(LoginForm.class))
		{
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username",
					"field.required", "Please provide a username");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password",
					"field.required", "Please provide a password");
		} else if (target.getClass().equals(RemoveForm.class))
		{
			RemoveForm rf = (RemoveForm) target;
			if (rf.getStartDate().equals("") || rf.getEndDate().equals(""))
			{
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "startDate",
						"field.required", "Please provide the start date");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "endDate",
						"field.required", "Please provide the end date");
			} else
			{
				Matcher matcher1 = pattern.matcher(rf.getStartDate());
				Matcher matcher2 = pattern.matcher(rf.getEndDate());
				if (!matcher1.matches() || !matcher2.matches())
				{
					if (!matcher1.matches())
					{
						errors.rejectValue("startDate", "wrong.format");
					} else
					{
						errors.rejectValue("endDate", "wrong.format");
					}
				} else
				{
					if (!rf.getEndDate().equals(rf.getStartDate()))
					{
						if (!FormFormater.compareDates(
								FormFormater.formatDate(rf.getEndDate()),
								FormFormater.formatDate(rf.getStartDate())))
						{
							errors.rejectValue("endDate", "wrong.date");
						}
					}
				}

			}
		} else if (target.getClass().equals(TopSearchesForm.class))
		{
			TopSearchesForm tsf = (TopSearchesForm) target;
			if (tsf.getFromDate().equals("") || tsf.getToDate().equals(""))
			{
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fromDate",
						"field.required", "Please provide the start date");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "toDate",
						"field.required", "Please provide the end date");
			} else
			{
				Matcher matcher1 = pattern.matcher(tsf.getFromDate());
				Matcher matcher2 = pattern.matcher(tsf.getToDate());
				if (!matcher1.matches() || !matcher2.matches())
				{
					if (!matcher1.matches())
					{
						errors.rejectValue("fromDate", "wrong.format");
					} else
					{
						errors.rejectValue("toDate", "wrong.format");
					}
				} else
				{
					if (!tsf.getToDate().equals(tsf.getFromDate()))
					{
						if (!FormFormater.compareDates(
								FormFormater.formatDate(tsf.getToDate()),
								FormFormater.formatDate(tsf.getFromDate())))
						{
							errors.rejectValue("toDate", "wrong.date");
						}
					}
				}

			}

		} else if (target.getClass().equals(RequestByTypeForm.class))
		{
			RequestByTypeForm rtf = (RequestByTypeForm) target;
			if (rtf.getToDate().equals("") || rtf.getFromDate().equals(""))
			{
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fromDate",
						"field.required", "Please provide the start date");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "toDate",
						"field.required", "Please provide the end date");
			} else
			{
				Matcher matcher1 = pattern.matcher(rtf.getFromDate());
				Matcher matcher2 = pattern.matcher(rtf.getToDate());
				if (!matcher1.matches() || !matcher2.matches())
				{
					if (!matcher1.matches())
					{
						errors.rejectValue("fromDate", "wrong.format");
					} else
					{
						errors.rejectValue("toDate", "wrong.format");
					}
				} else
				{
					if (!rtf.getToDate().equals(rtf.getFromDate()))
					{
						if (!FormFormater.compareDates(
								FormFormater.formatDate(rtf.getToDate()),
								FormFormater.formatDate(rtf.getFromDate())))
						{
							errors.rejectValue("toDate", "wrong.date");
						}
					}
				}

			}
		} else if (target.getClass().equals(RequestByHourForm.class))
		{
			RequestByHourForm rhf = (RequestByHourForm) target;
			if (rhf.getToDate().equals("") || rhf.getFromDate().equals(""))
			{
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fromDate",
						"field.required", "Please provide the start date");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "toDate",
						"field.required", "Please provide the end date");
			} else
			{
				Matcher matcher1 = pattern.matcher(rhf.getFromDate());
				Matcher matcher2 = pattern.matcher(rhf.getToDate());
				if (!matcher1.matches() || !matcher2.matches())
				{
					if (!matcher1.matches())
					{
						errors.rejectValue("fromDate", "wrong.format");
					} else
					{
						errors.rejectValue("toDate", "wrong.format");
					}
				} else
				{
					if (!rhf.getToDate().equals(rhf.getFromDate()))
					{
						if (!FormFormater.compareDates(
								FormFormater.formatDate(rhf.getToDate()),
								FormFormater.formatDate(rhf.getFromDate())))
						{
							errors.rejectValue("toDate", "wrong.date");
						}
					}
				}

			}
			if (rhf.getCompareFromDate().equals("")
					&& (!rhf.getCompareToDate().equals(""))
					|| (!rhf.getCompareFromDate().equals(""))
					&& rhf.getCompareToDate().equals(""))
			{
				if (rhf.getCompareFromDate().equals(""))
				{
					errors.rejectValue("compareFromDate",
							"wrong.compareFromDate");
				} else if (rhf.getCompareToDate().equals(""))
				{
					errors.rejectValue("compareToDate", "wrong.compareToDate");
				}
			} else if ((!rhf.getCompareFromDate().equals(""))
					&& (!rhf.getCompareToDate().equals("")))
			{
				Matcher matcher1 = pattern.matcher(rhf.getCompareFromDate());
				Matcher matcher2 = pattern.matcher(rhf.getCompareToDate());
				if (!matcher1.matches() || !matcher2.matches())
				{
					if (!matcher1.matches())
					{
						errors.rejectValue("compareFromDate", "wrong.format");
					} else
					{
						errors.rejectValue("compareToDate", "wrong.format");
					}
				} else
				{
					if (!rhf.getCompareFromDate()
							.equals(rhf.getCompareToDate()))
					{
						if (!FormFormater
								.compareDates(FormFormater.formatDate(rhf
										.getCompareToDate()), FormFormater
										.formatDate(rhf.getCompareFromDate())))
						{
							errors.rejectValue("compareToDate", "wrong.date");
						}
					}
				}

			}
		} else if (target.getClass().equals(SessionByHourForm.class))
		{
			SessionByHourForm shf = (SessionByHourForm) target;
			if (shf.getToDate().equals("") || shf.getFromDate().equals(""))
			{
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fromDate",
						"field.required", "Please provide the start date");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "toDate",
						"field.required", "Please provide the end date");
			} else
			{
				Matcher matcher1 = pattern.matcher(shf.getFromDate());
				Matcher matcher2 = pattern.matcher(shf.getToDate());
				if (!matcher1.matches() || !matcher2.matches())
				{
					if (!matcher1.matches())
					{
						errors.rejectValue("fromDate", "wrong.format");
					} else
					{
						errors.rejectValue("toDate", "wrong.format");
					}
				} else
				{
					if (!shf.getToDate().equals(shf.getFromDate()))
					{
						if (!FormFormater.compareDates(
								FormFormater.formatDate(shf.getToDate()),
								FormFormater.formatDate(shf.getFromDate())))
						{
							errors.rejectValue("toDate", "wrong.date");
						}
					}
				}

			}
			if (shf.getCompareFromDate().equals("")
					&& (!shf.getCompareToDate().equals(""))
					|| (!shf.getCompareFromDate().equals(""))
					&& shf.getCompareToDate().equals(""))
			{
				if (shf.getCompareFromDate().equals(""))
				{
					errors.rejectValue("compareFromDate",
							"wrong.compareFromDate");
				} else if (shf.getCompareToDate().equals(""))
				{
					errors.rejectValue("compareToDate", "wrong.compareToDate");
				}
			} else if ((!shf.getCompareFromDate().equals(""))
					&& (!shf.getCompareToDate().equals("")))
			{
				Matcher matcher1 = pattern.matcher(shf.getCompareFromDate());
				Matcher matcher2 = pattern.matcher(shf.getCompareToDate());
				if (!matcher1.matches() || !matcher2.matches())
				{
					if (!matcher1.matches())
					{
						errors.rejectValue("compareFromDate", "wrong.format");
					} else
					{
						errors.rejectValue("compareToDate", "wrong.format");
					}
				} else
				{
					if (!shf.getCompareFromDate()
							.equals(shf.getCompareToDate()))
					{
						if (!FormFormater
								.compareDates(FormFormater.formatDate(shf
										.getCompareToDate()), FormFormater
										.formatDate(shf.getCompareFromDate())))
						{
							errors.rejectValue("compareToDate", "wrong.date");
						}
					}
				}

			}
		} else if (target.getClass().equals(TopDimensionValuesForm.class))
		{
			TopDimensionValuesForm tdvf = (TopDimensionValuesForm) target;
			if (tdvf.getSubmitButton().equals("View Report"))
			{
				if (tdvf.getFromDate().equals("")
						|| tdvf.getToDate().equals(""))
				{
					ValidationUtils.rejectIfEmptyOrWhitespace(errors,
							"fromDate", "field.required",
							"Please provide the start date");
					ValidationUtils.rejectIfEmptyOrWhitespace(errors, "toDate",
							"field.required", "Please provide the end date");
				} else
				{
					Matcher matcher1 = pattern.matcher(tdvf.getFromDate());
					Matcher matcher2 = pattern.matcher(tdvf.getToDate());
					if (!matcher1.matches() || !matcher2.matches())
					{
						if (!matcher1.matches())
						{
							errors.rejectValue("fromDate", "wrong.format");
						} else
						{
							errors.rejectValue("toDate", "wrong.format");
						}
					} else
					{
						if (!tdvf.getToDate().equals(tdvf.getFromDate()))
						{
							if (!FormFormater
									.compareDates(FormFormater.formatDate(tdvf
											.getToDate()), FormFormater
											.formatDate(tdvf.getFromDate())))
							{
								errors.rejectValue("toDate", "wrong.date");
							}
						}
					}

				}
			} else if (tdvf.getSubmitButton().equals("View Trend"))
			{
				String[] FirstDims = tdvf.getIncDimsOfTrend();
				String FirstDim = FirstDims[0];
				// System.out.println(FirstDim);
				String[] SecondDims = tdvf.getIncDims1OfTrend();
				String SecondDim = SecondDims[0];
				String[] FirstVals = tdvf.getIncValsOfTrend();
				String FirstVal = FirstVals[0];
				String[] SecondVals = tdvf.getIncVals1OfTrend();
				String SecondVal = SecondVals[0];
				if (FirstDim.equals("--Select--")
						&& SecondDim.equals("--Select--"))
				{
					errors.rejectValue("submitButton",
							"wrong.missingAllTrendOptions");
				} else if (FirstVal.equals("--Select--")
						&& (!FirstDim.equals("--Select--"))
						&& SecondVal.equals("--Select--")
						&& SecondDim.equals("--Select--"))
				{
					errors.rejectValue("incValsOfTrend",
							"wrong.missingTrendValue");
				} else if (SecondVal.equals("--Select--")
						&& (!SecondDim.equals("--Select--"))
						&& FirstDim.equals("--Select--")
						&& FirstVal.equals("--Select--"))
				{
					errors.rejectValue("incVals1OfTrend",
							"wrong.missingTrendValue");
				} else if ((!FirstVal.equals("--Select--"))
						&& (!FirstDim.equals("--Select--"))
						&& SecondVal.equals("--Select--")
						&& (!SecondDim.equals("--Select--")))
				{
					errors.rejectValue("incVals1OfTrend",
							"wrong.missingTrendValue");
				} else if (FirstVal.equals("--Select--")
						&& (!FirstDim.equals("--Select--"))
						&& (!SecondVal.equals("--Select--"))
						&& (!SecondDim.equals("--Select--")))
				{
					errors.rejectValue("incValsOfTrend",
							"wrong.missingTrendValue");
				} else if (FirstVal.equals("--Select--")
						&& (!FirstDim.equals("--Select--"))
						&& SecondVal.equals("--Select--")
						&& (!SecondDim.equals("--Select--")))
				{
					errors.rejectValue("incVals1OfTrend",
							"wrong.missingTrendValue");
					errors.rejectValue("incValsOfTrend",
							"wrong.missingTrendValue");
				}
				if (tdvf.getFromDateOfTrend().equals("")
						|| tdvf.getToDateOfTrend().equals(""))
				{
					ValidationUtils.rejectIfEmptyOrWhitespace(errors,
							"fromDateOfTrend", "field.required",
							"Please provide the start date");
					ValidationUtils.rejectIfEmptyOrWhitespace(errors,
							"toDateOfTrend", "field.required",
							"Please provide the end date");
				} else
				{
					Matcher matcher1 = pattern.matcher(tdvf
							.getFromDateOfTrend());
					Matcher matcher2 = pattern.matcher(tdvf.getToDateOfTrend());
					if (!matcher1.matches() || !matcher2.matches())
					{
						if (!matcher1.matches())
						{
							errors.rejectValue("fromDateOfTrend",
									"wrong.format");
						} else
						{
							errors.rejectValue("toDateOfTrend", "wrong.format");
						}
					} else
					{
						if (!tdvf.getToDateOfTrend().equals(
								tdvf.getFromDateOfTrend()))
						{
							if (!FormFormater.compareDates(FormFormater
									.formatDate(tdvf.getToDateOfTrend()),
									FormFormater.formatDate(tdvf
											.getFromDateOfTrend())))
							{
								errors.rejectValue("toDateOfTrend",
										"wrong.date");
							}
						}
					}

				}
			}

		} else if (target.getClass().equals(TopKeywordsSearchesForm.class))
		{
			TopKeywordsSearchesForm tksf = (TopKeywordsSearchesForm) target;
			if (tksf.getFromDate().equals("") || tksf.getToDate().equals(""))
			{
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fromDate",
						"field.required", "Please provide the start date");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "toDate",
						"field.required", "Please provide the end date");
			} else
			{
				Matcher matcher1 = pattern.matcher(tksf.getFromDate());
				Matcher matcher2 = pattern.matcher(tksf.getToDate());
				if (!matcher1.matches() || !matcher2.matches())
				{
					if (!matcher1.matches())
					{
						errors.rejectValue("fromDate", "wrong.format");
					} else
					{
						errors.rejectValue("toDate", "wrong.format");
					}
				} else
				{
					if (!tksf.getToDate().equals(tksf.getFromDate()))
					{
						if (!FormFormater.compareDates(
								FormFormater.formatDate(tksf.getToDate()),
								FormFormater.formatDate(tksf.getFromDate())))
						{
							errors.rejectValue("toDate", "wrong.date");
						}
					}
				}

			}

		}

	}

}
