package springMVC;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import reportDataObjects.TopDimensionValues;
import reports.Report;
import services.QueryThread;
import charts.BarChart;

import dbAccess.Dao;
import debug.ClassDebug;
import forms.TopDimensionValuesForm;

@SuppressWarnings("deprecation")
public class TopdimensionvaluesFormController extends SimpleFormController
{
	protected Dao dao;

	public void setDao(Dao dao)
	{
		this.dao = dao;
	}

	public int getTrendArray(String fromDateOfTrend, String toDateOfTrend,
			String[] incDimsOfTrend, String[] incValsOfTrend,
			String[] incDims1OfTrend, String[] incVals1OfTrend)
	{
		// TODO Auto-generated method stub
		return dao.getWeekTrend(FormFormater.formatDate(fromDateOfTrend),
				FormFormater.formatDate(toDateOfTrend), incDimsOfTrend,
				incValsOfTrend, incDims1OfTrend, incVals1OfTrend);
	}

	public LinkedHashMap<String, Object> getTrend(String fromDateOfTrend,
			String toDateOfTrend, String[] incDimsOfTrend,
			String[] incValsOfTrend, String[] incDims1OfTrend,
			String[] incVals1oFTrend)
	{
		// TODO Auto-generated method stub
		String lastDate = dao.getLastDate();
		String firstDate = dao.getFirstDate();
		if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDateOfTrend)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDateOfTrend), firstDate)))
		{
			fromDateOfTrend = FormFormater.formatDateTwo(firstDate);
			toDateOfTrend = FormFormater.formatDateTwo(lastDate);
		} else if ((!FormFormater.compareDates(
				FormFormater.formatDate(fromDateOfTrend), firstDate))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(toDateOfTrend)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(toDateOfTrend), lastDate)))
		{
			fromDateOfTrend = FormFormater.formatDateTwo(firstDate);
		} else if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDateOfTrend)))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(fromDateOfTrend)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDateOfTrend), lastDate)))
		{
			toDateOfTrend = FormFormater.formatDateTwo(lastDate);
		}
		LinkedHashMap<String, Object> trend = new LinkedHashMap<String, Object>();
		ArrayList<String> weeks = FormFormater.getWeeks(fromDateOfTrend,
				toDateOfTrend);
		for (int i = 0; i < weeks.size(); i = i + 3)
		{
			// String week = weeks.get(i);
			String startDate = weeks.get(i + 1);
			String endDate = weeks.get(i + 2);
			trend.put(
					startDate + " - " + endDate,
					getTrendArray(startDate, endDate, incDimsOfTrend,
							incValsOfTrend, incDims1OfTrend, incVals1oFTrend));
		}
		trend.put("date", fromDateOfTrend + " - " + toDateOfTrend);
		return trend;
	}

	public List<TopDimensionValues> getTopDimensionValues(String fromDate,
			String toDate, int numRecords, String depth, String[] incDims,
			String[] incVals, String[] incDims1, String[] incVals1)
	{
		// TODO Auto-generated method stub
		List<TopDimensionValues> topDimensionValues = dao
				.getTopDimensionValues(FormFormater.formatDate(fromDate),
						FormFormater.formatDate(toDate),
						FormFormater.formatDepth(depth), numRecords, incDims,
						incVals, incDims1, incVals1);

		return topDimensionValues;
	}

	public LinkedHashMap<String, Object> getTopDimVals(String fromDate,
			String toDate, int numRecords, String depth, String breakdownBy,
			String[] incDims, String[] incVals, String[] incDims1,
			String[] incVals1)
	{
		// TODO Auto-generated method stub
		String lastDate = dao.getLastDate();
		String firstDate = dao.getFirstDate();
		if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), firstDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
			toDate = FormFormater.formatDateTwo(lastDate);
		} else if ((!FormFormater.compareDates(
				FormFormater.formatDate(fromDate), firstDate))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(FormFormater.formatDate(toDate),
						lastDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
		} else if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(fromDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), lastDate)))
		{
			toDate = FormFormater.formatDateTwo(lastDate);
		}
		LinkedHashMap<String, Object> topDimVals = new LinkedHashMap<String, Object>();
		HashMap<Integer, Object> temp = new HashMap<Integer, Object>();
		List<QueryThread> qtList = new ArrayList<QueryThread>();
		ArrayList<String> dates = new ArrayList<String>();
		if (breakdownBy.equals("All"))
		{
			topDimVals.put(
					"All",
					getTopDimensionValues(fromDate, toDate, numRecords, depth,
							incDims, incVals, incDims1, incVals1));
			dates.add(" " + fromDate + " - " + toDate);
			topDimVals.put("dates", dates);
			return topDimVals;
		} else if (breakdownBy.equals("Weekly"))
		{
			ArrayList<String> weeks = FormFormater.getWeeks(fromDate, toDate);
			int size = weeks.size();
			int index = 0;
			for (int i = 0; i < size; i = i + 3)
			{
				String week = weeks.get(i);
				String startDate = weeks.get(i + 1);
				String endDate = weeks.get(i + 2);
				dates.add(" " + startDate + " - " + endDate);
				QueryThread qt = new QueryThread(index, dao, temp, week,
						startDate, endDate, numRecords, depth, incDims,
						incVals, incDims1, incVals1, 11);
				topDimVals.put(week, index);
				qt.start();
				qtList.add(qt);
				index++;
			}
			for (QueryThread qt : qtList)
			{
				try
				{
					qt.join();
				} catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
			for (String key : topDimVals.keySet())
			{
				topDimVals.put(key, temp.get((Integer) topDimVals.get(key)));
			}
			topDimVals.put("dates", dates);
			return topDimVals;
		} else if (breakdownBy.equals("Monthly"))
		{
			ArrayList<String> months = FormFormater.getMonths(fromDate, toDate);
			int size = months.size();
			int index = 0;
			for (int i = 0; i < size; i = i + 3)
			{
				String month = months.get(i);
				String startDate = months.get(i + 1);
				String endDate = months.get(i + 2);
				dates.add(" " + startDate + " - " + endDate);
				QueryThread qt = new QueryThread(index, dao, temp, month,
						startDate, endDate, numRecords, depth, incDims,
						incVals, incDims1, incVals1, 11);
				topDimVals.put(month, index);
				qt.start();
				qtList.add(qt);
				index++;
			}
			for (QueryThread qt : qtList)
			{
				try
				{
					qt.join();
				} catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
			for (String key : topDimVals.keySet())
			{
				topDimVals.put(key, temp.get((Integer) topDimVals.get(key)));
			}
			topDimVals.put("dates", dates);
			return topDimVals;
		} else if (breakdownBy.equals("Seasonal"))
		{
			ArrayList<String> seasons = FormFormater.getSeasons(fromDate,
					toDate);
			int size = seasons.size();
			int index = 0;
			for (int i = 0; i < size; i = i + 3)
			{
				String season = seasons.get(i);
				String startDate = seasons.get(i + 1);
				String endDate = seasons.get(i + 2);
				dates.add(" " + startDate + " - " + endDate);
				QueryThread qt = new QueryThread(index, dao, temp, season,
						startDate, endDate, numRecords, depth, incDims,
						incVals, incDims1, incVals1, 11);
				topDimVals.put(season, index);
				qt.start();
				qtList.add(qt);
				index++;
			}
			for (QueryThread qt : qtList)
			{
				try
				{
					qt.join();
				} catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
			for (String key : topDimVals.keySet())
			{
				topDimVals.put(key, temp.get((Integer) topDimVals.get(key)));
			}
			topDimVals.put("dates", dates);
			return topDimVals;
		}

		return null;
	}

	@Override
	protected HashMap<String, Object> referenceData(HttpServletRequest request)
			throws Exception
	{
		ClassDebug.print("TopdimensionvaluesFormController.referenceData()...");
		HashMap<String, Object> model = new HashMap<String, Object>();
		if (request.getSession().getAttribute("username") == null)
		{
			model.put("pageName", "login");
			model.put("value", "AdminLogin");
		} else
		{
			model.put("pageName", "logout");
			model.put("value", "Logout");
			HashMap<String, Object> common = new HashMap<String, Object>();
			common.put("title", "Admin Links");
			common.put("addPageName", "addlogfile");
			common.put("addValue", "Add Log Files");
			common.put("removePageName", "removelogfile");
			common.put("removeValue", "Remove Log Files");
			model.put("common", common);
		}
		if (request.getRequestURI().endsWith("topdimensionvalues"))
		{
			ArrayList<String> depth = new ArrayList<String>();
			depth.add("One");
			depth.add("Two");
			// only admin can choose option Three
			// if(request.getSession().getAttribute("username")!=null)
			depth.add("Three");
			model.put("depth", depth);
			ArrayList<String> numRecords = new ArrayList<String>();
			numRecords.add("5");
			numRecords.add("10");
			numRecords.add("20");
			numRecords.add("30");
			numRecords.add("40");
			if (request.getSession().getAttribute("username") != null)
			{
				numRecords.add("100");
				numRecords.add("1000");
				numRecords.add("10000");
			}
			model.put("numRecords", numRecords);
			ArrayList<String> breakdownBy = new ArrayList<String>();
			breakdownBy.add("All");
			breakdownBy.add("Weekly");
			breakdownBy.add("Monthly");
			breakdownBy.add("Seasonal");
			model.put("breakdownBy", breakdownBy);

			List<String> dims = dao.getDimensions();
			dims.add(0, "All");
			model.put("incDims", dims);
			List<String> vals = new ArrayList<String>();
			@SuppressWarnings("unchecked")
			HashMap<String, Object> HashMap_topDimensionValuesFormReport = (HashMap<String, Object>) request
					.getSession().getAttribute("topDimensionValuesForm");
			if (HashMap_topDimensionValuesFormReport != null)
			{
				String[] tmpS = (String[]) HashMap_topDimensionValuesFormReport
						.get("incDims");
				if (tmpS != null)
				{
					List<String> tmpList = dao.getDimensionValues(tmpS[0]);
					for (int i = tmpList.size() - 1; i >= 0; i--)
						vals.add(0, tmpList.get(i));
				}
			}
			vals.add(0, "All");
			model.put("incVals", vals);
			List<String> dims1 = dao.getDimensions();
			dims1.add(0, "All");
			model.put("incDims1", dims1);
			List<String> vals1 = new ArrayList<String>();
			if (HashMap_topDimensionValuesFormReport != null)
			{
				String[] tmpS = (String[]) HashMap_topDimensionValuesFormReport
						.get("incDims1");
				if (tmpS != null)
				{
					List<String> tmpList = dao.getDimensionValues(tmpS[0]);
					for (int i = tmpList.size() - 1; i >= 0; i--)
						vals1.add(0, tmpList.get(i));
				}
			}
			vals1.add(0, "All");
			model.put("incVals1", vals1);

			List<String> dimsOfTrend = dao.getDimensions();
			dimsOfTrend.add(0, "--Select--");
			model.put("incDimsOfTrend", dimsOfTrend);
			List<String> valsOfTrend = new ArrayList<String>();
			if (HashMap_topDimensionValuesFormReport != null)
			{
				String[] tmpS = (String[]) HashMap_topDimensionValuesFormReport
						.get("incDimsOfTrend");
				if (tmpS != null)
				{
					List<String> tmpList = dao.getDimensionValues(tmpS[0]);
					for (int i = tmpList.size() - 1; i >= 0; i--)
						valsOfTrend.add(0, tmpList.get(i));
				}
			}
			valsOfTrend.add(0, "--Select--");
			model.put("incValsOfTrend", valsOfTrend);
			List<String> dims1OfTrend = dao.getDimensions();
			dims1OfTrend.add(0, "--Select--");
			model.put("incDims1OfTrend", dims1OfTrend);
			List<String> vals1OfTrend = new ArrayList<String>();
			if (HashMap_topDimensionValuesFormReport != null)
			{
				String[] tmpS = (String[]) HashMap_topDimensionValuesFormReport
						.get("incDims1OfTrend");
				if (tmpS != null)
				{
					List<String> tmpList = dao.getDimensionValues(tmpS[0]);
					for (int i = tmpList.size() - 1; i >= 0; i--)
						vals1OfTrend.add(0, tmpList.get(i));
				}
			}
			vals1OfTrend.add(0, "--Select--");
			model.put("incVals1OfTrend", vals1OfTrend);
		}

		return model;
	}

	@Override
	protected Object formBackingObject(HttpServletRequest request)
			throws Exception
	{
		ClassDebug
				.print("TopdimensionvaluesFormController.formBackingObject()...");
		if (request.getRequestURI().endsWith("topdimensionvalues"))
		{
			setCommandClass(TopDimensionValuesForm.class);
			setCommandName("topDimensionValuesForm");
			TopDimensionValuesForm tdvf = new TopDimensionValuesForm();

			if (request.getParameter("Reset") == null)
			{
				@SuppressWarnings("unchecked")
				HashMap<String, Object> HashMap_topDimensionValuesFormReport = (HashMap<String, Object>) request
						.getSession().getAttribute("topDimensionValuesForm");
				if (HashMap_topDimensionValuesFormReport != null)
				{

					tdvf.setDepth((String) HashMap_topDimensionValuesFormReport
							.get("depth"));
					tdvf.setNumRecords((Integer) HashMap_topDimensionValuesFormReport
							.get("numRecords"));
					tdvf.setBreakdownBy((String) HashMap_topDimensionValuesFormReport
							.get("breakdownBy"));
					tdvf.setIncDims((String[]) HashMap_topDimensionValuesFormReport
							.get("incDims"));
					tdvf.setIncVals((String[]) HashMap_topDimensionValuesFormReport
							.get("incVals"));
					tdvf.setIncDims1((String[]) HashMap_topDimensionValuesFormReport
							.get("incDims1"));
					tdvf.setIncVals1((String[]) HashMap_topDimensionValuesFormReport
							.get("incVals1"));

					tdvf.setFromDateOfTrend((String) HashMap_topDimensionValuesFormReport
							.get("fromDateOfTrend"));
					tdvf.setToDateOfTrend((String) HashMap_topDimensionValuesFormReport
							.get("toDateOfTrend"));
					tdvf.setIncDimsOfTrend((String[]) HashMap_topDimensionValuesFormReport
							.get("incDimsOfTrend"));
					tdvf.setIncValsOfTrend((String[]) HashMap_topDimensionValuesFormReport
							.get("incValsOfTrend"));
					tdvf.setIncDims1OfTrend((String[]) HashMap_topDimensionValuesFormReport
							.get("incDims1OfTrend"));
					tdvf.setIncVals1OfTrend((String[]) HashMap_topDimensionValuesFormReport
							.get("incVals1OfTrend"));
				} else
				{
					tdvf.setFromDate(FormFormater.getPreviousMonth());
					tdvf.setToDate(FormFormater.getCurrentDate());
					tdvf.setDepth("Two");
					tdvf.setNumRecords(20);
					tdvf.setBreakdownBy("All");
					tdvf.setFromDateOfTrend(FormFormater.getPreviousMonth());
					tdvf.setToDateOfTrend(FormFormater.getCurrentDate());
				}

				if (request.getSession().getAttribute("fromDate") != null
						&& request.getSession().getAttribute("toDate") != null)
				{
					tdvf.setFromDate((String) request.getSession()
							.getAttribute("fromDate"));
					tdvf.setToDate((String) request.getSession().getAttribute(
							"toDate"));
				}

			} else
			{
				@SuppressWarnings("unchecked")
				HashMap<String, Object> HashMap_topDimensionValuesFormReport = (HashMap<String, Object>) request
						.getSession().getAttribute("topDimensionValuesForm");
				if (HashMap_topDimensionValuesFormReport != null)
					request.getSession().setAttribute("topDimensionValuesForm",
							null);
				request.getSession().removeAttribute("fromDate");
				request.getSession().removeAttribute("toDate");
				tdvf.setFromDate(FormFormater.getPreviousMonth());
				tdvf.setToDate(FormFormater.getCurrentDate());
				tdvf.setDepth("Two");
				tdvf.setNumRecords(20);
				tdvf.setBreakdownBy("All");
				tdvf.setFromDateOfTrend(FormFormater.getPreviousMonth());
				tdvf.setToDateOfTrend(FormFormater.getCurrentDate());
			}

			this.setFormView("InputView\\topdimensionvaluesInputView");
			return tdvf;
		}
		return null;
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception
	{
		ClassDebug.print("TopdimensionvaluesFormController.onSubmit()...");
		if (getCommandName().equals("topDimensionValuesForm"))
		{
			TopDimensionValuesForm form = (TopDimensionValuesForm) command;
			getValidator().validate(form, errors);

			long startTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Start the query at "
					+ ClassDebug.getCurrentDateAndTime());

			HashMap<String, Object> HashMap_topDimensionValuesFormReport = new HashMap<String, Object>();
			HashMap_topDimensionValuesFormReport.put("fromDate",
					form.getFromDate());
			HashMap_topDimensionValuesFormReport
					.put("toDate", form.getToDate());
			HashMap_topDimensionValuesFormReport.put("depth", form.getDepth());
			HashMap_topDimensionValuesFormReport.put("numRecords",
					form.getNumRecords());
			HashMap_topDimensionValuesFormReport.put("incDims",
					form.getIncDims());
			HashMap_topDimensionValuesFormReport.put("incVals",
					form.getIncVals());
			HashMap_topDimensionValuesFormReport.put("incDims1",
					form.getIncDims1());
			HashMap_topDimensionValuesFormReport.put("incVals1",
					form.getIncVals1());
			HashMap_topDimensionValuesFormReport.put("breakdownBy",
					form.getBreakdownBy());

			HashMap_topDimensionValuesFormReport.put("fromDateOfTrend",
					form.getFromDateOfTrend());
			HashMap_topDimensionValuesFormReport.put("toDateOfTrend",
					form.getToDateOfTrend());
			HashMap_topDimensionValuesFormReport.put("incDimsOfTrend",
					form.getIncDimsOfTrend());
			HashMap_topDimensionValuesFormReport.put("incValsOfTrend",
					form.getIncValsOfTrend());
			HashMap_topDimensionValuesFormReport.put("incDims1OfTrend",
					form.getIncDims1OfTrend());
			HashMap_topDimensionValuesFormReport.put("incVals1OfTrend",
					form.getIncVals1OfTrend());
			request.getSession().setAttribute("topDimensionValuesForm",
					HashMap_topDimensionValuesFormReport);
			request.getSession().setAttribute("fromDate", form.getFromDate());
			request.getSession().setAttribute("toDate", form.getToDate());

			HashMap<String, Object> model = new HashMap<String, Object>();
			if (request.getSession().getAttribute("username") == null)
			{
				model.put("pageName", "login");
				model.put("value", "AdminLogin");
			} else
			{
				model.put("pageName", "logout");
				model.put("value", "Logout");
				HashMap<String, Object> common = new HashMap<String, Object>();
				common.put("title", "Admin Links");
				common.put("addPageName", "addlogfile");
				common.put("addValue", "Add Log Files");
				common.put("removePageName", "removelogfile");
				common.put("removeValue", "Remove Log Files");
				model.put("common", common);
			}

			String filePath = this.getServletContext().getRealPath("/");
			if (form.getSubmitButton().equals("View Report"))
			{
				HashMap<String, Object> topValsSearchLists = getTopDimVals(
						form.getFromDate(), form.getToDate(),
						form.getNumRecords(), form.getDepth(),
						form.getBreakdownBy(), form.getIncDims(),
						form.getIncVals(), form.getIncDims1(),
						form.getIncVals1());
				model.put("dates", topValsSearchLists.get("dates"));
				topValsSearchLists.remove("dates");
				//
				response.setContentType("image/png");
				ArrayList<JFreeChart> chartList = BarChart
						.createBarChartForValues(topValsSearchLists);

				for (int i = 0; i < chartList.size(); i++)
				{
					ChartUtilities.saveChartAsJPEG(new File(filePath
							+ "pages/temp/" + i + ".jpg"), chartList.get(i),
							726, 400);
				}
				//
				model.put("topValsSearchLists", topValsSearchLists);
				model.put("reportName", "Report: Top " + form.getNumRecords()
						+ " " + form.getDepth() + "-Dimension Search Values");
				model.put("reportName1",
						"Breakdown by: " + form.getBreakdownBy());

				// start to generate reports (pdf,xls,csv)
				Report.createReports(filePath, model,
						"TopDimensionValuesReport");
				model.put("savedReportName", "TopDimensionValuesReport");
			} else if (form.getSubmitButton().equals("View Trend"))
			{

				HashMap<String, Object> trend = getTrend(
						form.getFromDateOfTrend(), form.getToDateOfTrend(),
						form.getIncDimsOfTrend(), form.getIncValsOfTrend(),
						form.getIncDims1OfTrend(), form.getIncVals1OfTrend());

				model.put("date", trend.get("date"));
				trend.remove("date");
				//
				JFreeChart chart = BarChart.createBarChartForTrend(trend);

				ChartUtilities.saveChartAsJPEG(new File(filePath
						+ "pages/temp/trend.jpg"), chart, 726, 400);
				//
				model.put("trend", trend);
				model.put("reportName",
						"User Trend Report With Dimension&Value Pairs:");
				model.put(
						"reportName1",
						form.getIncDimsOfTrend()[0] + " : "
								+ form.getIncValsOfTrend()[0] + " & "
								+ form.getIncDims1OfTrend()[0] + " : "
								+ form.getIncVals1OfTrend()[0]);
				model.put("img", "pages/temp/trend.jpg");

				// start to generate reports (pdf,xls,csv)
				Report.createReports(filePath, model, "UserTrendReport");
				model.put("savedReportName", "UserTrendReport");
			}

			// Check if the report was created to avoid problems in the View
			model.put(
					"pdfReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".pdf").exists());
			model.put(
					"csvReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".csv").exists());
			model.put(
					"xlsReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".xls").exists());

			long endTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Complete the Query at "
					+ ClassDebug.getCurrentDateAndTime());
			ClassDebug.print("Time Cost: "
					+ ClassDebug.timeFormat(endTime - startTime));

			return new ModelAndView("OutputView\\topdimensionvaluesOutputView",
					model);
		}

		return null;
	}
}
