package springMVC;

import java.io.File;
import java.util.*;

import javax.servlet.http.*;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.*;
import org.springframework.validation.BindException;

import others.TimeEstimation;

import reportDataObjects.SessionByMinutes;
import reports.SessionByMinutesReport;

import dbAccess.*;
import debug.*;
import forms.*;

@SuppressWarnings("deprecation")
public class SessionByMinutesFormController extends SimpleFormController
{
	protected Dao dao;

	public void setDao(Dao dao)
	{
		this.dao = dao;
	}

	@Override
	protected HashMap<String, Object> referenceData(HttpServletRequest request)
			throws Exception
	{
		ClassDebug.print("SessionByMinutesFormController.referenceData()...");
		HashMap<String, Object> model = new HashMap<String, Object>();
		if (request.getSession().getAttribute("username") == null)
		{
			model.put("pageName", "login");
			model.put("value", "AdminLogin");
		} else
		{
			model.put("pageName", "logout");
			model.put("value", "Logout");
			HashMap<String, Object> common = new HashMap<String, Object>();
			common.put("title", "Admin Links");
			common.put("addPageName", "addlogfile");
			common.put("addValue", "Add Log Files");
			common.put("removePageName", "removelogfile");
			common.put("removeValue", "Remove Log Files");
			model.put("common", common);
		}

		if (request.getRequestURI().endsWith("sessionbyminutes"))
		{
			ArrayList<String> ListNumberOfRecords = new ArrayList<String>();

			Integer tmpValue = 10;
			for (int i = 1; i <= 5; i++)
			{
				ListNumberOfRecords.add(tmpValue.toString());
				tmpValue *= 2;
			}

			model.put("ListNumberOfRecords", ListNumberOfRecords);
		}

		return model;
	}

	protected Object formBackingObject(HttpServletRequest request)
			throws Exception
	{
		ClassDebug
				.print("SessionByMinutesFormController.formBackingObject()...");
		if (request.getRequestURI().endsWith("sessionbyminutes"))
		{
			setCommandClass(SessionByMinutesForm.class);
			setCommandName("SessionByMinutesForm");
			SessionByMinutesForm tmpForm = new SessionByMinutesForm();

			@SuppressWarnings("unchecked")
			HashMap<String, Object> HashMap_SessionByMinutesForm = (HashMap<String, Object>) request
					.getSession().getAttribute("SessionByMinutesForm");
			if (request.getParameter("Reset") == null)
			{
				if (HashMap_SessionByMinutesForm != null)
				{
					tmpForm.setNumberOfRecords((Integer) HashMap_SessionByMinutesForm
							.get("numberOfRecords"));
				} else
				{
					tmpForm.setFromDate(FormFormater.getPreviousMonth());
					tmpForm.setToDate(FormFormater.getCurrentDate());
					tmpForm.setNumberOfRecords(100);
				}

				if (request.getSession().getAttribute("fromDate") != null
						&& request.getSession().getAttribute("toDate") != null)
				{
					tmpForm.setFromDate((String) request.getSession()
							.getAttribute("fromDate"));
					tmpForm.setToDate((String) request.getSession()
							.getAttribute("toDate"));
				}

			} else
			{
				if (HashMap_SessionByMinutesForm != null)
					request.getSession()
							.removeAttribute("SessionByMinutesForm");
				request.getSession().removeAttribute("fromDate");
				request.getSession().removeAttribute("toDate");
				tmpForm.setFromDate(FormFormater.getPreviousMonth());
				tmpForm.setToDate(FormFormater.getCurrentDate());
				tmpForm.setNumberOfRecords(100);
			}

			this.setFormView("InputView\\sessionbyminutesInputView");
			return tmpForm;
		} else
			return null;
	}

	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception
	{
		ClassDebug.print("SessionByMinutesFormController.onSubmit()...");
		if (getCommandName().equals("SessionByMinutesForm"))
		{
			SessionByMinutesForm form = (SessionByMinutesForm) command;

			String lastDate = dao.getLastDate();
			String firstDate = dao.getFirstDate();
			String fromDate = form.getFromDate();
			String toDate = form.getToDate();
			Integer numberOfRecords = form.getNumberOfRecords();

			HashMap<String, Object> HashMap_SessionByMinutesForm = new HashMap<String, Object>();
			HashMap_SessionByMinutesForm.put("fromDate", fromDate);
			HashMap_SessionByMinutesForm.put("toDate", toDate);
			HashMap_SessionByMinutesForm
					.put("numberOfRecords", numberOfRecords);

			request.getSession().setAttribute("SessionByMinutesForm",
					HashMap_SessionByMinutesForm);
			request.getSession().setAttribute("fromDate", form.getFromDate());
			request.getSession().setAttribute("toDate", form.getToDate());

			if ((!FormFormater.compareDates(lastDate,
					FormFormater.formatDate(toDate)))
					&& (!FormFormater.compareDates(
							FormFormater.formatDate(fromDate), firstDate)))
			{
				fromDate = FormFormater.formatDateTwo(firstDate);
				toDate = FormFormater.formatDateTwo(lastDate);
			} else if ((!FormFormater.compareDates(
					FormFormater.formatDate(form.getFromDate()), firstDate))
					&& (!FormFormater.compareDates(firstDate,
							FormFormater.formatDate(toDate)))
					&& (!FormFormater.compareDates(
							FormFormater.formatDate(toDate), lastDate)))
			{
				fromDate = FormFormater.formatDateTwo(firstDate);
			} else if ((!FormFormater.compareDates(lastDate,
					FormFormater.formatDate(toDate)))
					&& (!FormFormater.compareDates(firstDate,
							FormFormater.formatDate(fromDate)))
					&& (!FormFormater.compareDates(
							FormFormater.formatDate(fromDate), lastDate)))
			{
				toDate = FormFormater.formatDateTwo(lastDate);
			}
			/*
			 * TimeEstimation tmpTimeEstimation = new
			 * TimeEstimation(form.getFromDate(), form.getToDate(), dao,
			 * "SessionByMinutes"); double tmpTimeEstimated =
			 * tmpTimeEstimation.start(); ClassDebug.print(tmpTimeEstimated ==
			 * -1 ? "No enough history data and unable to make an estimation!" :
			 * "Approximate time cost: " +
			 * ClassDebug.timeFormat((int)tmpTimeEstimated));
			 */

			long startTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Start the query at "
					+ ClassDebug.getCurrentDateAndTime());

			HashMap<String, Object> model = new HashMap<String, Object>();

			fromDate = FormFormater.formatDate(fromDate);
			toDate = FormFormater.formatDate(toDate);

			ArrayList<SessionByMinutes> ArrayListResult = dao
					.getSessionByMinutes(fromDate, toDate);

			model.put("ArrayListResultIndex", ArrayListResult);
			model.put("ArrayListResultIndexSize", ArrayListResult.size());
			model.put("date", form.getFromDate() + " - " + form.getToDate());
			model.put("reportName", "Session By Minutes Report");
			model.put("numberOfRecords", numberOfRecords);

			if (request.getSession().getAttribute("username") == null)
			{
				model.put("pageName", "login");
				model.put("value", "AdminLogin");
			} else
			{
				model.put("pageName", "logout");
				model.put("value", "Logout");
				HashMap<String, Object> common = new HashMap<String, Object>();
				common.put("title", "Admin Links");
				common.put("addPageName", "addlogfile");
				common.put("addValue", "Add Log Files");
				common.put("removePageName", "removelogfile");
				common.put("removeValue", "Remove Log Files");
				model.put("common", common);
			}
			if (ArrayListResult.size() > 0)
			{
				// start to generate reports (pdf,xls,csv)
				String filePath = this.getServletContext().getRealPath("/");
				SessionByMinutesReport.createReports(filePath, model,
						"SessionByMinutesReport");
				model.put("savedReportName", "SessionByMinutesReport");

				// Check if the report was created to avoid problems in the View
				model.put("pdfReport", new File(filePath + "pages/reports/"
						+ model.get("savedReportName") + ".pdf").exists());
				model.put("csvReport", new File(filePath + "pages/reports/"
						+ model.get("savedReportName") + ".csv").exists());
				model.put("xlsReport", new File(filePath + "pages/reports/"
						+ model.get("savedReportName") + ".xls").exists());
			}

			long endTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Complete the Query at "
					+ ClassDebug.getCurrentDateAndTime());
			ClassDebug.print("Time Cost: "
					+ ClassDebug.timeFormat(endTime - startTime));
			/*
			 * tmpTimeEstimation.end(endTime - startTime);
			 */

			return new ModelAndView("OutputView\\sessionbyminutesOutputView",
					model);
		}
		return null;
	}
}
