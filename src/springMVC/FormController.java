package springMVC;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import charts.BarChart;
import charts.PieChart;
import debug.ClassDebug;

import forms.LoginForm;
import forms.RemoveForm;
import forms.RequestByHourForm;
import forms.RequestByTypeForm;
import forms.SessionByHourForm;
import forms.TopDimensionValuesForm;
import forms.TopKeywordsSearchesForm;
import forms.TopSearchesForm;
import forms.UploadForm;

import reportDataObjects.RequestByHour;
import reportDataObjects.SessionByHour;
import reportDataObjects.TopKeywordsSearch;
import reports.Report;
import services.Service;

/**
 * A FormController to process all the forms
 * 
 */
@SuppressWarnings("deprecation")
public class FormController extends SimpleFormController
{

	Service service;

	public void setService(Service service)
	{
		this.service = service;
	}

	@Override
	protected HashMap<String, Object> referenceData(HttpServletRequest request)
			throws Exception
	{
		ClassDebug.print("FormController.referenceData()...");
		HashMap<String, Object> model = new HashMap<String, Object>();
		if (request.getSession().getAttribute("username") == null)
		{
			model.put("pageName", "login");
			model.put("value", "AdminLogin");
		} else
		{
			model.put("pageName", "logout");
			model.put("value", "Logout");
			model.put("common", service.getCommon());
		}
		if (request.getRequestURI().endsWith("topsearches"))
		{
			ArrayList<String> depth = new ArrayList<String>();
			depth.add("One");
			depth.add("Two");
			depth.add("Three");
			model.put("depth", depth);
			ArrayList<String> numRecords = new ArrayList<String>();
			numRecords.add("5");
			numRecords.add("10");
			numRecords.add("20");
			numRecords.add("30");
			numRecords.add("40");
			if (request.getSession().getAttribute("username") != null)
			{
				numRecords.add("100");
				numRecords.add("1000");
				numRecords.add("10000");
			}
			model.put("numRecords", numRecords);
			ArrayList<String> breakdownBy = new ArrayList<String>();
			breakdownBy.add("All");
			breakdownBy.add("Weekly");
			breakdownBy.add("Monthly");
			breakdownBy.add("Seasonal");
			model.put("breakdownBy", breakdownBy);
			ArrayList<String> searchOrder = new ArrayList<String>();
			searchOrder.add("Yes");
			searchOrder.add("No");
			model.put("searchOrder", searchOrder);
			List<String> dims = service.getDimensions();
			dims.add(0, "All");
			model.put("incDims", dims);
		} else if (request.getRequestURI().endsWith("requestbytype"))
		{

		} else if (request.getRequestURI().endsWith("requestbyhour"))
		{

		} else if (request.getRequestURI().endsWith("sessionbyhour"))
		{

		} else if (request.getRequestURI().endsWith("topdimensionvalues"))
		{
			ArrayList<String> depth = new ArrayList<String>();
			depth.add("One");
			depth.add("Two");
			depth.add("Three");
			model.put("depth", depth);
			ArrayList<String> numRecords = new ArrayList<String>();
			numRecords.add("5");
			numRecords.add("10");
			numRecords.add("20");
			numRecords.add("30");
			numRecords.add("40");
			if (request.getSession().getAttribute("username") != null)
			{
				numRecords.add("100");
				numRecords.add("1000");
				numRecords.add("10000");
			}
			model.put("numRecords", numRecords);
			ArrayList<String> breakdownBy = new ArrayList<String>();
			breakdownBy.add("All");
			breakdownBy.add("Weekly");
			breakdownBy.add("Monthly");
			breakdownBy.add("Seasonal");
			model.put("breakdownBy", breakdownBy);

			List<String> dims = service.getDimensions();
			dims.add(0, "All");
			model.put("incDims", dims);
			List<String> vals = new ArrayList<String>();
			// =======================begin======================
			@SuppressWarnings("unchecked")
			HashMap<String, Object> HashMap_topDimensionValuesFormReport = (HashMap<String, Object>) request
					.getSession().getAttribute("topDimensionValuesForm");
			if (HashMap_topDimensionValuesFormReport != null)
			{
				String[] tmpS = (String[]) HashMap_topDimensionValuesFormReport
						.get("incDims");
				if (tmpS != null)
				{
					List<String> tmpList = service.getDimensionValues(tmpS[0]);
					for (int i = tmpList.size() - 1; i >= 0; i--)
						vals.add(0, tmpList.get(i));
				}
			}
			vals.add(0, "All");
			// =====================original======================
			// vals.add(0, "All");
			// =============end by Ricol at 04/10/2012============
			model.put("incVals", vals);
			List<String> dims1 = service.getDimensions();
			dims1.add(0, "All");
			model.put("incDims1", dims1);
			List<String> vals1 = new ArrayList<String>();
			// =======================begin======================
			if (HashMap_topDimensionValuesFormReport != null)
			{
				String[] tmpS = (String[]) HashMap_topDimensionValuesFormReport
						.get("incDims1");
				if (tmpS != null)
				{
					List<String> tmpList = service.getDimensionValues(tmpS[0]);
					for (int i = tmpList.size() - 1; i >= 0; i--)
						vals1.add(0, tmpList.get(i));
				}
			}
			vals1.add(0, "All");
			// =====================original======================
			// vals1.add(0, "All");
			// =============end by Ricol at 04/10/2012============
			model.put("incVals1", vals1);

			List<String> dimsOfTrend = service.getDimensions();
			dimsOfTrend.add(0, "--Select--");
			model.put("incDimsOfTrend", dimsOfTrend);
			List<String> valsOfTrend = new ArrayList<String>();
			// =======================begin======================
			if (HashMap_topDimensionValuesFormReport != null)
			{
				String[] tmpS = (String[]) HashMap_topDimensionValuesFormReport
						.get("incDimsOfTrend");
				if (tmpS != null)
				{
					List<String> tmpList = service.getDimensionValues(tmpS[0]);
					for (int i = tmpList.size() - 1; i >= 0; i--)
						valsOfTrend.add(0, tmpList.get(i));
				}
			}
			valsOfTrend.add(0, "--Select--");
			// =====================original======================
			// valsOfTrend.add(0,"--Select--");
			// =============end by Ricol at 04/10/2012============
			model.put("incValsOfTrend", valsOfTrend);
			List<String> dims1OfTrend = service.getDimensions();
			dims1OfTrend.add(0, "--Select--");
			model.put("incDims1OfTrend", dims1OfTrend);
			List<String> vals1OfTrend = new ArrayList<String>();
			// =======================begin======================
			if (HashMap_topDimensionValuesFormReport != null)
			{
				String[] tmpS = (String[]) HashMap_topDimensionValuesFormReport
						.get("incDims1OfTrend");
				if (tmpS != null)
				{
					List<String> tmpList = service.getDimensionValues(tmpS[0]);
					for (int i = tmpList.size() - 1; i >= 0; i--)
						vals1OfTrend.add(0, tmpList.get(i));
				}
			}
			vals1OfTrend.add(0, "--Select--");
			// =====================original======================
			// vals1OfTrend.add(0,"--Select--");
			// =============end by Ricol at 04/10/2012============
			model.put("incVals1OfTrend", vals1OfTrend);
		} else if (request.getRequestURI().endsWith("topkeywordssearches"))
		{
			ArrayList<String> resultType = new ArrayList<String>();
			resultType.add("All");
			resultType.add("Return Zero Records");
			resultType.add("Autocorrected");
			model.put("resultType", resultType);
			ArrayList<String> numRecords = new ArrayList<String>();
			numRecords.add("5");
			numRecords.add("10");
			numRecords.add("20");
			numRecords.add("30");
			numRecords.add("40");
			if (request.getSession().getAttribute("username") != null)
			{
				numRecords.add("100");
				numRecords.add("1000");
				numRecords.add("10000");
			}
			model.put("numRecords", numRecords);
		}

		return model;
	}

	@Override
	protected Object formBackingObject(HttpServletRequest request)
			throws Exception
	{
		ClassDebug.print("FormController.formBackingObject()...");
		if (request.getRequestURI().endsWith("addlogfile"))
		{
			setCommandClass(UploadForm.class);
			setCommandName("uploadForm");
			UploadForm uf = new UploadForm();
			uf.setFolderPath(" Eg: ( c:\\logserver_output )");
			uf.setButtonName("Browse...");
			return uf;
		} else if (request.getRequestURI().endsWith("removelogfile"))
		{
			setCommandClass(RemoveForm.class);
			setCommandName("removeForm");
			RemoveForm rf = new RemoveForm();
			return rf;
		} else if (request.getRequestURI().endsWith("login"))
		{
			setCommandClass(LoginForm.class);
			setCommandName("loginForm");
			LoginForm lf = new LoginForm();
			return lf;
		} else if (request.getRequestURI().endsWith("topsearches"))
		{
			setCommandClass(TopSearchesForm.class);
			setCommandName("topSearchesForm");
			TopSearchesForm tsf = new TopSearchesForm();

			// Kai 5/4/2012
			@SuppressWarnings("unchecked")
			HashMap<String, Object> HashMap_topSearchesForm = (HashMap<String, Object>) request
					.getSession().getAttribute("topSearchesForm");
			if (HashMap_topSearchesForm != null)
			{
				tsf.setFromDate((String) HashMap_topSearchesForm
						.get("fromDate"));
				tsf.setToDate((String) HashMap_topSearchesForm.get("toDate"));
				tsf.setDepth((String) HashMap_topSearchesForm.get("depth"));
				tsf.setBreakdownBy((String) HashMap_topSearchesForm
						.get("breakdownBy"));
				tsf.setIncDims1((String) HashMap_topSearchesForm
						.get("incDims1"));
				tsf.setIncDims2((String) HashMap_topSearchesForm
						.get("incDims2"));
				tsf.setIncDims3((String) HashMap_topSearchesForm
						.get("incDims3"));
				tsf.setSearchOrder((String) HashMap_topSearchesForm
						.get("searchOrder"));
				tsf.setNumRecords((Integer) HashMap_topSearchesForm
						.get("numRecords"));
			} else
			{
				tsf.setFromDate(FormFormater.getPreviousMonth());
				tsf.setToDate(FormFormater.getCurrentDate());
				tsf.setDepth("Two");
				tsf.setNumRecords(20);
				tsf.setBreakdownBy("All");
				tsf.setSearchOrder("Yes");
				tsf.setIncDims1("All");
				tsf.setIncDims2("All");
				tsf.setIncDims3("All");
			}
			;
			// Kai 5/4/2012

			return tsf;
		} else if (request.getRequestURI().endsWith("requestbytype"))
		{
			setCommandClass(RequestByTypeForm.class);
			setCommandName("requestbytypeForm");
			RequestByTypeForm rtf = new RequestByTypeForm();

			// =======================begin======================
			@SuppressWarnings("unchecked")
			HashMap<String, Object> HashMap_requestbytypeForm = (HashMap<String, Object>) request
					.getSession().getAttribute("requestbytypeForm");
			if (HashMap_requestbytypeForm != null)
			{
				rtf.setFromDate((String) HashMap_requestbytypeForm
						.get("fromDate"));
				rtf.setToDate((String) HashMap_requestbytypeForm.get("toDate"));
			} else
			{
				rtf.setFromDate(FormFormater.getPreviousMonth());
				rtf.setToDate(FormFormater.getCurrentDate());
			}
			// =====================original======================
			// rtf.setFromDate(FormFormater.getPreviousMonth());
			// rtf.setToDate(FormFormater.getCurrentDate());
			// =============end by Ricol at 04/10/2012============

			return rtf;
		} else if (request.getRequestURI().endsWith("requestbyhour"))
		{
			setCommandClass(RequestByHourForm.class);
			setCommandName("requestbyhourForm");
			RequestByHourForm rhf = new RequestByHourForm();
			// Kai 6/4/2012
			@SuppressWarnings("unchecked")
			HashMap<String, Object> HashMap_requestbyhourForm = (HashMap<String, Object>) request
					.getSession().getAttribute("requestbyhourForm");
			if (HashMap_requestbyhourForm != null)
			{
				rhf.setFromDate((String) HashMap_requestbyhourForm
						.get("fromDate"));
				rhf.setToDate((String) HashMap_requestbyhourForm.get("toDate"));
				rhf.setCompareFromDate((String) HashMap_requestbyhourForm
						.get("compareFromDate"));
				rhf.setCompareToDate((String) HashMap_requestbyhourForm
						.get("compareToDate"));
			} else
			{
				rhf.setFromDate(FormFormater.getPreviousMonth());
				rhf.setToDate(FormFormater.getCurrentDate());
			}
			// Kai 6/4/2012
			return rhf;
		} else if (request.getRequestURI().endsWith("sessionbyhour"))
		{
			setCommandClass(SessionByHourForm.class);
			setCommandName("sessionbyhourForm");
			SessionByHourForm shf = new SessionByHourForm();
			// =======================begin======================
			@SuppressWarnings("unchecked")
			HashMap<String, Object> HashMap_sessionbyhourForm = (HashMap<String, Object>) request
					.getSession().getAttribute("sessionbyhourForm");
			if (HashMap_sessionbyhourForm != null)
			{
				shf.setCompareFromDate((String) HashMap_sessionbyhourForm
						.get("compareFromDate"));
				shf.setCompareToDate((String) HashMap_sessionbyhourForm
						.get("compareToDate"));
				shf.setFromDate((String) HashMap_sessionbyhourForm
						.get("fromDate"));
				shf.setToDate((String) HashMap_sessionbyhourForm.get("toDate"));
			} else
			{
				shf.setFromDate(FormFormater.getPreviousMonth());
				shf.setToDate(FormFormater.getCurrentDate());
			}
			// =====================original======================
			// shf.setFromDate(FormFormater.getPreviousMonth());
			// shf.setToDate(FormFormater.getCurrentDate());
			// =============end by Ricol at 04/10/2012============
			return shf;
		} else if (request.getRequestURI().endsWith("topdimensionvalues"))
		{
			setCommandClass(TopDimensionValuesForm.class);
			setCommandName("topDimensionValuesForm");
			// ==================begin=============Mickey
			TopDimensionValuesForm tdvf = new TopDimensionValuesForm();
			@SuppressWarnings("unchecked")
			HashMap<String, Object> HashMap_topDimensionValuesFormReport = (HashMap<String, Object>) request
					.getSession().getAttribute("topDimensionValuesForm");
			if (HashMap_topDimensionValuesFormReport != null)
			{
				tdvf.setFromDate((String) HashMap_topDimensionValuesFormReport
						.get("fromDate"));
				tdvf.setToDate((String) HashMap_topDimensionValuesFormReport
						.get("toDate"));
				tdvf.setDepth((String) HashMap_topDimensionValuesFormReport
						.get("depth"));
				tdvf.setNumRecords((Integer) HashMap_topDimensionValuesFormReport
						.get("numRecords"));
				tdvf.setBreakdownBy((String) HashMap_topDimensionValuesFormReport
						.get("breakdownBy"));
				tdvf.setIncDims((String[]) HashMap_topDimensionValuesFormReport
						.get("incDims"));
				tdvf.setIncVals((String[]) HashMap_topDimensionValuesFormReport
						.get("incVals"));
				tdvf.setIncDims1((String[]) HashMap_topDimensionValuesFormReport
						.get("incDims1"));
				tdvf.setIncVals1((String[]) HashMap_topDimensionValuesFormReport
						.get("incVals1"));

				tdvf.setFromDateOfTrend((String) HashMap_topDimensionValuesFormReport
						.get("fromDateOfTrend"));
				tdvf.setToDateOfTrend((String) HashMap_topDimensionValuesFormReport
						.get("toDateOfTrend"));
				tdvf.setIncDimsOfTrend((String[]) HashMap_topDimensionValuesFormReport
						.get("incDimsOfTrend"));
				tdvf.setIncValsOfTrend((String[]) HashMap_topDimensionValuesFormReport
						.get("incValsOfTrend"));
				tdvf.setIncDims1OfTrend((String[]) HashMap_topDimensionValuesFormReport
						.get("incDims1OfTrend"));
				tdvf.setIncVals1OfTrend((String[]) HashMap_topDimensionValuesFormReport
						.get("incVals1OfTrend"));
			} else
			{
				tdvf.setFromDate(FormFormater.getPreviousMonth());
				tdvf.setToDate(FormFormater.getCurrentDate());
				tdvf.setDepth("Two");
				tdvf.setNumRecords(20);
				tdvf.setBreakdownBy("All");
				tdvf.setFromDateOfTrend(FormFormater.getPreviousMonth());
				tdvf.setToDateOfTrend(FormFormater.getCurrentDate());
			}
			// ==================end==============Mickey

			/*
			 * TopDimensionValuesForm tdvf = new TopDimensionValuesForm();
			 * tdvf.setFromDate(FormFormater.getPreviousMonth());
			 * tdvf.setToDate(FormFormater.getCurrentDate());
			 * tdvf.setDepth("Two"); tdvf.setNumRecords(20);
			 * tdvf.setBreakdownBy("All");
			 * tdvf.setFromDateOfTrend(FormFormater.getPreviousMonth());
			 * tdvf.setToDateOfTrend(FormFormater.getCurrentDate());
			 */
			return tdvf;
		} else if (request.getRequestURI().endsWith("topkeywordssearches"))
		{
			setCommandClass(TopKeywordsSearchesForm.class);
			setCommandName("topkeywordssearchesForm");
			TopKeywordsSearchesForm tksf = new TopKeywordsSearchesForm();

			// =======================begin======================
			@SuppressWarnings("unchecked")
			HashMap<String, Object> HashMap_topkeywordssearchesForm = (HashMap<String, Object>) request
					.getSession().getAttribute("topkeywordssearchesForm");
			if (HashMap_topkeywordssearchesForm != null)
			{
				tksf.setFromDate((String) HashMap_topkeywordssearchesForm
						.get("fromDate"));
				tksf.setToDate((String) HashMap_topkeywordssearchesForm
						.get("toDate"));
				tksf.setNumRecords((Integer) HashMap_topkeywordssearchesForm
						.get("numRecords"));
				tksf.setResultType((String) HashMap_topkeywordssearchesForm
						.get("resultType"));
			} else
			{
				tksf.setFromDate(FormFormater.getPreviousMonth());
				tksf.setToDate(FormFormater.getCurrentDate());
				tksf.setNumRecords(30);
			}
			// =====================original======================
			// tksf.setFromDate(FormFormater.getPreviousMonth());
			// tksf.setToDate(FormFormater.getCurrentDate());
			// tksf.setNumRecords(30);
			// =============end by Ricol at 04/10/2012============
			return tksf;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception
	{
		ClassDebug.print("FormController.onSubmit()...");
		if (getCommandName().equals("uploadForm"))
		{
			UploadForm form = (UploadForm) command;
			HashMap<String, Object> model = new HashMap<String, Object>();
			if (form.getButtonName().equals("Browse..."))
			{
				String path = FormFormater
						.openFolder("Please select the folder containing logs");
				if (path != null)
				{
					form.setFolderPath(path);
					form.setButtonName("Upload");
				}
				model.put("pageName", "logout");
				model.put("value", "Logout");
				model.put("common", service.getCommon());
				return new ModelAndView("addlogfile", model).addObject(command);
			} else
			{
				service.loadLogFiles(form.getFolderPath());
				response.getWriter()
						.write("<script>alert('Uploaded. Please refer to the Tomcat console for more info.');window.location='addlogfile';</script>");
			}

		} else if (getCommandName().equals("removeForm"))
		{
			RemoveForm form = (RemoveForm) command;
			getValidator().validate(form, errors);
			service.removeLogs(form.getStartDate(), form.getEndDate());
			response.getWriter()
					.write("<script>alert('Removed. Changes will take effect after the next upload.');window.location='removelogfile';</script>");
		} else if (getCommandName().equals("loginForm"))
		{
			LoginForm form = (LoginForm) command;
			getValidator().validate(form, errors);
			if (service.login(form.getUsername(), form.getPassword()))
			{
				request.getSession().setAttribute("username",
						form.getUsername());
				response.sendRedirect("home");
			} else
			{
				response.getWriter()
						.write("<script>alert('Wrong Username/Password.');window.location='login';</script>");
			}
		} else if (getCommandName().equals("topSearchesForm"))
		{
			TopSearchesForm form = (TopSearchesForm) command;
			getValidator().validate(form, errors);

			// Kai 5/4/2012
			HashMap<String, Object> HashMap_topSearchesForm = new HashMap<String, Object>();
			HashMap_topSearchesForm.put("fromDate", form.getFromDate());
			HashMap_topSearchesForm.put("toDate", form.getToDate());
			HashMap_topSearchesForm.put("depth", form.getDepth());
			HashMap_topSearchesForm.put("numRecords", form.getNumRecords());
			HashMap_topSearchesForm.put("breakdownBy", form.getBreakdownBy());
			HashMap_topSearchesForm.put("searchOrder", form.getSearchOrder());
			HashMap_topSearchesForm.put("incDims1", form.getIncDims1());
			HashMap_topSearchesForm.put("incDims2", form.getIncDims2());
			HashMap_topSearchesForm.put("incDims3", form.getIncDims3());
			request.getSession().setAttribute("topSearchesForm",
					HashMap_topSearchesForm);
			// Kai 5/4/2012

			HashMap<String, Object> model = new HashMap<String, Object>();
			String[] incDims =
			{ form.getIncDims1(), form.getIncDims2(), form.getIncDims3() };
			HashMap<String, Object> topDimsSearchLists = service
					.getTopDimsSearches(form.getFromDate(), form.getToDate(),
							form.getSearchOrder(), form.getNumRecords(),
							form.getDepth(), form.getBreakdownBy(), incDims);
			model.put("dates", topDimsSearchLists.get("dates"));
			topDimsSearchLists.remove("dates");
			//
			ArrayList<JFreeChart> chartList = BarChart
					.createBarChart(topDimsSearchLists);
			String filePath = this.getServletContext().getRealPath("/");
			for (int i = 0; i < chartList.size(); i++)
			{
				ChartUtilities.saveChartAsJPEG(new File(filePath
						+ "pages/temp/" + i + ".jpg"), chartList.get(i), 726,
						400);
			}
			//
			model.put("topDimsSearchLists", topDimsSearchLists);
			model.put("reportName", "Report: Top " + form.getNumRecords() + " "
					+ form.getDepth() + "-Dimension-Search");
			model.put("reportName1", "Breakdown by: " + form.getBreakdownBy()
					+ ", " + "Sequence enabled: " + form.getSearchOrder());

			if (request.getSession().getAttribute("username") == null)
			{
				model.put("pageName", "login");
				model.put("value", "AdminLogin");
			} else
			{
				model.put("pageName", "logout");
				model.put("value", "Logout");
				model.put("common", service.getCommon());
			}
			// start to generate reports (pdf,xls,csv)
			Report.createReports(filePath, model, "TopDimSearchReport");
			model.put("savedReportName", "TopDimSearchReport");
			return new ModelAndView("home", model);
		} else if (getCommandName().equals("requestbytypeForm"))
		{
			RequestByTypeForm form = (RequestByTypeForm) command;
			getValidator().validate(form, errors);

			// =======================begin======================
			HashMap<String, Object> HashMap_requestbytypeForm = new HashMap<String, Object>();
			HashMap_requestbytypeForm.put("fromDate", form.getFromDate());
			HashMap_requestbytypeForm.put("toDate", form.getToDate());
			request.getSession().setAttribute("requestbytypeForm",
					HashMap_requestbytypeForm);
			// =============end by Ricol at 04/10/2012============

			HashMap<String, Object> model = new HashMap<String, Object>();
			HashMap<String, Object> requestByTypeList = service
					.getRequestByType(form.getFromDate(), form.getToDate());
			model.put("date", requestByTypeList.get("date"));
			requestByTypeList.remove("date");
			//
			JFreeChart chart = PieChart.createPieChart(requestByTypeList);
			String filePath = this.getServletContext().getRealPath("/");
			ChartUtilities.saveChartAsJPEG(new File(filePath
					+ "pages/temp/piechart.jpg"), chart, 726, 400);
			//
			model.put("requestByTypeList", requestByTypeList);
			model.put("reportName", "Request By Type");
			model.put("img", "pages/temp/piechart.jpg");
			model.put("th1", "Type of Request");
			model.put("th2", "Requests");
			if (request.getSession().getAttribute("username") == null)
			{
				model.put("pageName", "login");
				model.put("value", "AdminLogin");
			} else
			{
				model.put("pageName", "logout");
				model.put("value", "Logout");
				model.put("common", service.getCommon());
			}
			// start to generate reports (pdf,xls,csv)
			Report.createReports(filePath, model, "RequestByTypeReport");
			model.put("savedReportName", "RequestByTypeReport");
			return new ModelAndView("home", model);
		} else if (getCommandName().equals("requestbyhourForm"))
		{
			RequestByHourForm form = (RequestByHourForm) command;
			getValidator().validate(form, errors);

			// Kai 6/4/2012
			HashMap<String, Object> HashMap_requestbyhourForm = new HashMap<String, Object>();
			HashMap_requestbyhourForm.put("fromDate", form.getFromDate());
			HashMap_requestbyhourForm.put("toDate", form.getToDate());
			HashMap_requestbyhourForm.put("compareFromDate",
					form.getCompareFromDate());
			HashMap_requestbyhourForm.put("compareToDate",
					form.getCompareToDate());
			request.getSession().setAttribute("requestbyhourForm",
					HashMap_requestbyhourForm);
			// Kai 6/4/2012

			HashMap<String, Object> model = new HashMap<String, Object>();
			HashMap<String, Object> requestByHour = service.getRequestByHour(
					form.getFromDate(), form.getToDate(),
					form.getCompareFromDate(), form.getCompareToDate());
			model.put("date", requestByHour.get("date"));
			requestByHour.remove("date");
			//
			JFreeChart chart = BarChart
					.createBarChartForRequestByHour(requestByHour);
			String filePath = this.getServletContext().getRealPath("/");
			ChartUtilities.saveChartAsJPEG(new File(filePath
					+ "pages/temp/barchart.jpg"), chart, 726, 400);
			//
			if (requestByHour.size() == 2)
			{
				int totalRequests = 0;
				int totalRequests1 = 0;
				ArrayList<RequestByHour> rbh = (ArrayList<RequestByHour>) requestByHour
						.get("requestByHourList");
				ArrayList<RequestByHour> rbh1 = (ArrayList<RequestByHour>) requestByHour
						.get("requestByHourList1");
				for (int i = 0; i < rbh.size(); i++)
				{
					totalRequests += rbh.get(i).getRequests();
				}
				for (int i = 0; i < rbh1.size(); i++)
				{
					totalRequests1 += rbh1.get(i).getRequests();
				}
				model.put("requestByHourList", rbh);
				model.put("requestByHourList1", rbh1);
				model.put("totalRequests", totalRequests);
				model.put("totalRequests1", totalRequests1);
				model.put("th1", "1st Hours");
				model.put("th2", "1st Requests");
				model.put("th3", "2nd Hours");
				model.put("th4", "2nd Requests");
				model.put("reportName", "Request By Hour");
				model.put("img", "pages/temp/barchart.jpg");
			} else
			{
				int totalRequests = 0;
				ArrayList<RequestByHour> rbh = (ArrayList<RequestByHour>) requestByHour
						.get("requestByHourList");
				for (int i = 0; i < rbh.size(); i++)
				{
					totalRequests += rbh.get(i).getRequests();
				}
				model.put("requestByHourList", rbh);
				model.put("totalRequests", totalRequests);
				model.put("th1", "Hours");
				model.put("th2", "Requests");
				model.put("reportName", "Request By Hour");
				model.put("img", "pages/temp/barchart.jpg");
			}

			if (request.getSession().getAttribute("username") == null)
			{
				model.put("pageName", "login");
				model.put("value", "AdminLogin");
			} else
			{
				model.put("pageName", "logout");
				model.put("value", "Logout");
				model.put("common", service.getCommon());
			}

			// start to generate reports (pdf,xls,csv)
			Report.createReports(filePath, model, "RequestByHourReport");
			model.put("savedReportName", "RequestByHourReport");
			return new ModelAndView("home", model);
		} else if (getCommandName().equals("sessionbyhourForm"))
		{
			SessionByHourForm form = (SessionByHourForm) command;
			getValidator().validate(form, errors);

			// =======================begin======================
			HashMap<String, Object> HashMap_sessionbyhourForm = new HashMap<String, Object>();
			HashMap_sessionbyhourForm.put("fromDate", form.getFromDate());
			HashMap_sessionbyhourForm.put("toDate", form.getToDate());
			HashMap_sessionbyhourForm.put("compareFromDate",
					form.getCompareFromDate());
			HashMap_sessionbyhourForm.put("compareToDate",
					form.getCompareToDate());
			request.getSession().setAttribute("sessionbyhourForm",
					HashMap_sessionbyhourForm);
			// =============end by Ricol at 04/10/2012============

			HashMap<String, Object> model = new HashMap<String, Object>();
			HashMap<String, Object> sessionByHour = service.getSessionByHour(
					form.getFromDate(), form.getToDate(),
					form.getCompareFromDate(), form.getCompareToDate());
			model.put("date", sessionByHour.get("date"));
			sessionByHour.remove("date");
			//
			JFreeChart chart = BarChart
					.createBarChartForSessionByHour(sessionByHour);
			String filePath = this.getServletContext().getRealPath("/");
			ChartUtilities.saveChartAsJPEG(new File(filePath
					+ "pages/temp/barchart.jpg"), chart, 726, 400);
			//
			if (sessionByHour.size() == 2)
			{
				int totalSessions = 0;
				int totalSessions1 = 0;
				ArrayList<SessionByHour> sbh = (ArrayList<SessionByHour>) sessionByHour
						.get("sessionByHourList");
				ArrayList<SessionByHour> sbh1 = (ArrayList<SessionByHour>) sessionByHour
						.get("sessionByHourList1");
				for (int i = 0; i < sbh.size(); i++)
				{
					totalSessions += sbh.get(i).getSessions();
				}
				for (int i = 0; i < sbh1.size(); i++)
				{
					totalSessions1 += sbh1.get(i).getSessions();
				}
				model.put("sessionByHourList", sbh);
				model.put("sessionByHourList1", sbh1);
				model.put("totalSessions", totalSessions);
				model.put("totalSessions1", totalSessions1);
				model.put("th1", "1st Hours");
				model.put("th2", "1st Sessions");
				model.put("th3", "2nd Hours");
				model.put("th4", "2nd Sessions");
				model.put("reportName", "Session By Hour");
				model.put("img", "pages/temp/barchart.jpg");
			} else
			{
				int totalSessions = 0;
				ArrayList<SessionByHour> sbh = (ArrayList<SessionByHour>) sessionByHour
						.get("sessionByHourList");
				for (int i = 0; i < sbh.size(); i++)
				{
					totalSessions += sbh.get(i).getSessions();
				}
				model.put("sessionByHourList", sbh);
				model.put("totalSessions", totalSessions);
				model.put("th1", "Hours");
				model.put("th2", "Sessions");
				model.put("reportName", "Session By Hour");
				model.put("img", "pages/temp/barchart.jpg");
			}

			if (request.getSession().getAttribute("username") == null)
			{
				model.put("pageName", "login");
				model.put("value", "AdminLogin");
			} else
			{
				model.put("pageName", "logout");
				model.put("value", "Logout");
				model.put("common", service.getCommon());
			}
			// start to generate reports (pdf,xls,csv)
			Report.createReports(filePath, model, "SessionByHourReport");
			model.put("savedReportName", "SessionByHourReport");
			return new ModelAndView("home", model);
		} else if (getCommandName().equals("topDimensionValuesForm"))
		{
			TopDimensionValuesForm form = (TopDimensionValuesForm) command;
			getValidator().validate(form, errors);

			// ================begin===============Mickey
			HashMap<String, Object> HashMap_topDimensionValuesFormReport = new HashMap<String, Object>();
			HashMap_topDimensionValuesFormReport.put("fromDate",
					form.getFromDate());
			HashMap_topDimensionValuesFormReport
					.put("toDate", form.getToDate());
			HashMap_topDimensionValuesFormReport.put("depth", form.getDepth());
			HashMap_topDimensionValuesFormReport.put("numRecords",
					form.getNumRecords());
			HashMap_topDimensionValuesFormReport.put("incDims",
					form.getIncDims());
			HashMap_topDimensionValuesFormReport.put("incVals",
					form.getIncVals());
			HashMap_topDimensionValuesFormReport.put("incDims1",
					form.getIncDims1());
			HashMap_topDimensionValuesFormReport.put("incVals1",
					form.getIncVals1());
			HashMap_topDimensionValuesFormReport.put("breakdownBy",
					form.getBreakdownBy());

			HashMap_topDimensionValuesFormReport.put("fromDateOfTrend",
					form.getFromDateOfTrend());
			HashMap_topDimensionValuesFormReport.put("toDateOfTrend",
					form.getToDateOfTrend());
			HashMap_topDimensionValuesFormReport.put("incDimsOfTrend",
					form.getIncDimsOfTrend());
			HashMap_topDimensionValuesFormReport.put("incValsOfTrend",
					form.getIncValsOfTrend());
			HashMap_topDimensionValuesFormReport.put("incDims1OfTrend",
					form.getIncDims1OfTrend());
			HashMap_topDimensionValuesFormReport.put("incVals1OfTrend",
					form.getIncVals1OfTrend());
			request.getSession().setAttribute("topDimensionValuesForm",
					HashMap_topDimensionValuesFormReport);

			// ================end==============Mickey

			HashMap<String, Object> model = new HashMap<String, Object>();
			if (request.getSession().getAttribute("username") == null)
			{
				model.put("pageName", "login");
				model.put("value", "AdminLogin");
			} else
			{
				model.put("pageName", "logout");
				model.put("value", "Logout");
				model.put("common", service.getCommon());
			}
			if (form.getSubmitButton().equals("View Report"))
			{
				HashMap<String, Object> topValsSearchLists = service
						.getTopDimVals(form.getFromDate(), form.getToDate(),
								form.getNumRecords(), form.getDepth(),
								form.getBreakdownBy(), form.getIncDims(),
								form.getIncVals(), form.getIncDims1(),
								form.getIncVals1());
				model.put("dates", topValsSearchLists.get("dates"));
				topValsSearchLists.remove("dates");
				//
				response.setContentType("image/png");
				ArrayList<JFreeChart> chartList = BarChart
						.createBarChartForValues(topValsSearchLists);
				String filePath = this.getServletContext().getRealPath("/");
				for (int i = 0; i < chartList.size(); i++)
				{
					ChartUtilities.saveChartAsJPEG(new File(filePath
							+ "pages/temp/" + i + ".jpg"), chartList.get(i),
							726, 400);
				}
				//
				model.put("topValsSearchLists", topValsSearchLists);
				model.put("reportName", "Report: Top " + form.getNumRecords()
						+ " " + form.getDepth() + "-Dimension & Value-Search");
				model.put("reportName1",
						"Breakdown by: " + form.getBreakdownBy());

				// start to generate reports (pdf,xls,csv)
				Report.createReports(filePath, model,
						"TopDimensionValuesReport");
				model.put("savedReportName", "TopDimensionValuesReport");
			} else if (form.getSubmitButton().equals("View Trend"))
			{

				HashMap<String, Object> trend = service.getTrend(
						form.getFromDateOfTrend(), form.getToDateOfTrend(),
						form.getIncDimsOfTrend(), form.getIncValsOfTrend(),
						form.getIncDims1OfTrend(), form.getIncVals1OfTrend());

				model.put("date", trend.get("date"));
				trend.remove("date");
				//
				JFreeChart chart = BarChart.createBarChartForTrend(trend);
				String filePath = this.getServletContext().getRealPath("/");
				ChartUtilities.saveChartAsJPEG(new File(filePath
						+ "pages/temp/trend.jpg"), chart, 726, 400);
				//
				model.put("trend", trend);
				model.put("reportName",
						"User Trend Report With Dimension&Value Pairs:");
				model.put(
						"reportName1",
						form.getIncDimsOfTrend()[0] + " : "
								+ form.getIncValsOfTrend()[0] + " & "
								+ form.getIncDims1OfTrend()[0] + " : "
								+ form.getIncVals1OfTrend()[0]);
				model.put("img", "pages/temp/trend.jpg");

				// start to generate reports (pdf,xls,csv)
				Report.createReports(filePath, model, "UserTrendReport");
				model.put("savedReportName", "UserTrendReport");
			}

			return new ModelAndView("home", model);
		} else if (getCommandName().equals("topkeywordssearchesForm"))
		{
			TopKeywordsSearchesForm form = (TopKeywordsSearchesForm) command;
			getValidator().validate(form, errors);
			// =======================begin======================
			HashMap<String, Object> HashMap_topkeywordssearchesForm = new HashMap<String, Object>();
			HashMap_topkeywordssearchesForm.put("fromDate", form.getFromDate());
			HashMap_topkeywordssearchesForm.put("toDate", form.getToDate());
			HashMap_topkeywordssearchesForm.put("resultType",
					form.getResultType());
			HashMap_topkeywordssearchesForm.put("numRecords",
					form.getNumRecords());
			request.getSession().setAttribute("topkeywordssearchesForm",
					HashMap_topkeywordssearchesForm);
			// =============end by Ricol at 04/10/2012============
			HashMap<String, Object> model = new HashMap<String, Object>();
			HashMap<String, Object> topKeywordsSearches = service
					.getTopKeywordsSearches(form.getFromDate(),
							form.getToDate(), form.getResultType(),
							form.getNumRecords());
			model.put("date", topKeywordsSearches.get("date"));

			topKeywordsSearches.remove("date");
			List<TopKeywordsSearch> topKeywordsSearchesList = (List<TopKeywordsSearch>) topKeywordsSearches
					.get("topKeywordsSearchesList");
			if (topKeywordsSearchesList.size() != 0)
			{
				if (topKeywordsSearchesList.get(0).getAutoCorrectTo() != null)
				{
					model.put("autocorrectto", "yes");
				} else
				{
					model.put("autocorrectto", "no");
				}
			}

			model.put("topKeywordsSearchesList", topKeywordsSearchesList);
			model.put("reportName", "Top Keywords Searches");

			if (request.getSession().getAttribute("username") == null)
			{
				model.put("pageName", "login");
				model.put("value", "AdminLogin");
			} else
			{
				model.put("pageName", "logout");
				model.put("value", "Logout");
				model.put("common", service.getCommon());
			}
			// start to generate reports (pdf,xls,csv)
			String filePath = this.getServletContext().getRealPath("/");
			Report.createReports(filePath, model, "TopKeywordsSearchReport");
			model.put("savedReportName", "TopKeywordsSearchReport");
			return new ModelAndView("home", model);
		}
		return null;

	}

}
