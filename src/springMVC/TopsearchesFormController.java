package springMVC;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import reportDataObjects.TopSearch;
import reports.Report;
import services.QueryThread;
import charts.BarChart;
import dbAccess.Dao;
import debug.ClassDebug;
import forms.TopSearchesForm;

@SuppressWarnings("deprecation")
public class TopsearchesFormController extends SimpleFormController
{
	private Dao dao;

	public void setDao(Dao dao)
	{
		this.dao = dao;
	}

	public List<TopSearch> getTopSearches(String fromDate, String toDate,
			String searchOrder, int numRecords, String depth, String[] incDims)
	{

		List<TopSearch> topDimensions = dao.getTopSearches(
				FormFormater.formatDate(fromDate),
				FormFormater.formatDate(toDate), searchOrder, numRecords,
				FormFormater.formatDepth(depth), incDims);
		return topDimensions;
	}

	public LinkedHashMap<String, Object> getTopDimsSearches(String fromDate,
			String toDate, String searchOrder, int numRecords, String depth,
			String breakdownBy, String[] incDims)
	{
		String lastDate = dao.getLastDate();
		String firstDate = dao.getFirstDate();
		if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), firstDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
			toDate = FormFormater.formatDateTwo(lastDate);
		} else if ((!FormFormater.compareDates(
				FormFormater.formatDate(fromDate), firstDate))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(FormFormater.formatDate(toDate),
						lastDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
		} else if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(fromDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), lastDate)))
		{
			toDate = FormFormater.formatDateTwo(lastDate);
		}
		LinkedHashMap<String, Object> topDimsSearches = new LinkedHashMap<String, Object>();
		HashMap<Integer, Object> temp = new HashMap<Integer, Object>();
		List<QueryThread> qtList = new ArrayList<QueryThread>();
		ArrayList<String> dates = new ArrayList<String>();
		if (breakdownBy.equals("All"))
		{
			topDimsSearches.put(
					"All",
					getTopSearches(fromDate, toDate, searchOrder, numRecords,
							depth, incDims));
			dates.add(" " + fromDate + " - " + toDate);
			topDimsSearches.put("dates", dates);
			return topDimsSearches;
		} else if (breakdownBy.equals("Weekly"))
		{
			ArrayList<String> weeks = FormFormater.getWeeks(fromDate, toDate);
			int size = weeks.size();
			int index = 0;
			for (int i = 0; i < size; i = i + 3)
			{
				String week = weeks.get(i);
				String startDate = weeks.get(i + 1);
				String endDate = weeks.get(i + 2);
				dates.add(" " + startDate + " - " + endDate);
				QueryThread qt = new QueryThread(index, dao, temp, week,
						startDate, endDate, numRecords, depth, searchOrder,
						incDims, 10);
				topDimsSearches.put(week, index);
				qt.start();
				qtList.add(qt);
				index++;

			}
			for (QueryThread qt : qtList)
			{
				try
				{
					qt.join();
				} catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
			for (String key : topDimsSearches.keySet())
			{
				topDimsSearches.put(key,
						temp.get((Integer) topDimsSearches.get(key)));
			}
			topDimsSearches.put("dates", dates);
			return topDimsSearches;
		} else if (breakdownBy.equals("Monthly"))
		{
			ArrayList<String> months = FormFormater.getMonths(fromDate, toDate);
			int size = months.size();
			int index = 0;
			for (int i = 0; i < size; i = i + 3)
			{
				String month = months.get(i);
				String startDate = months.get(i + 1);
				String endDate = months.get(i + 2);
				dates.add(" " + startDate + " - " + endDate);
				QueryThread qt = new QueryThread(index, dao, temp, month,
						startDate, endDate, numRecords, depth, searchOrder,
						incDims, 10);
				topDimsSearches.put(month, index);
				qt.start();
				qtList.add(qt);
				index++;
			}
			for (QueryThread qt : qtList)
			{
				try
				{
					qt.join();
				} catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
			for (String key : topDimsSearches.keySet())
			{
				topDimsSearches.put(key,
						temp.get((Integer) topDimsSearches.get(key)));
			}
			topDimsSearches.put("dates", dates);
			return topDimsSearches;
		} else if (breakdownBy.equals("Seasonal"))
		{
			ArrayList<String> seasons = FormFormater.getSeasons(fromDate,
					toDate);
			int size = seasons.size();
			int index = 0;
			for (int i = 0; i < size; i = i + 3)
			{
				String season = seasons.get(i);
				String startDate = seasons.get(i + 1);
				String endDate = seasons.get(i + 2);
				dates.add(" " + startDate + " - " + endDate);
				QueryThread qt = new QueryThread(index, dao, temp, season,
						startDate, endDate, numRecords, depth, searchOrder,
						incDims, 10);
				topDimsSearches.put(season, index);
				qt.start();
				qtList.add(qt);
				index++;

			}
			for (QueryThread qt : qtList)
			{
				try
				{
					qt.join();
				} catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
			for (String key : topDimsSearches.keySet())
			{
				topDimsSearches.put(key,
						temp.get((Integer) topDimsSearches.get(key)));
			}
			topDimsSearches.put("dates", dates);
			return topDimsSearches;
		}

		return null;
	}

	@Override
	protected HashMap<String, Object> referenceData(HttpServletRequest request)
			throws Exception
	{
		ClassDebug.print("TopsearchesFormController.referenceData()...");
		HashMap<String, Object> model = new HashMap<String, Object>();
		if (request.getSession().getAttribute("username") == null)
		{
			model.put("pageName", "login");
			model.put("value", "AdminLogin");
		} else
		{
			model.put("pageName", "logout");
			model.put("value", "Logout");
			HashMap<String, Object> common = new HashMap<String, Object>();
			common.put("title", "Admin Links");
			common.put("addPageName", "addlogfile");
			common.put("addValue", "Add Log Files");
			common.put("removePageName", "removelogfile");
			common.put("removeValue", "Remove Log Files");
			model.put("common", common);
		}
		if (request.getRequestURI().endsWith("topsearches"))
		{
			ArrayList<String> depth = new ArrayList<String>();
			depth.add("One");
			depth.add("Two");
			depth.add("Three");
			// depth.add("four");
			model.put("depthIndex", depth);
			ArrayList<String> numRecords = new ArrayList<String>();
			numRecords.add("5");
			numRecords.add("10");
			numRecords.add("20");
			numRecords.add("30");
			numRecords.add("40");
			if (request.getSession().getAttribute("username") != null)
			{
				numRecords.add("100");
				numRecords.add("1000");
				numRecords.add("10000");
			}
			model.put("numRecords", numRecords);
			ArrayList<String> breakdownBy = new ArrayList<String>();
			breakdownBy.add("All");
			breakdownBy.add("Weekly");
			breakdownBy.add("Monthly");
			breakdownBy.add("Seasonal");
			model.put("breakdownBy", breakdownBy);
			ArrayList<String> searchOrder = new ArrayList<String>();
			searchOrder.add("Yes");
			searchOrder.add("No");
			model.put("searchOrder", searchOrder);
			List<String> dims = dao.getDimensions();
			dims.add(0, "All");
			model.put("incDims1", dims);
			model.put("incDims2", dims);
			model.put("incDims3", dims);
		}
		return model;
	}

	@Override
	protected Object formBackingObject(HttpServletRequest request)
			throws Exception
	{
		ClassDebug.print("TopsearchesFormController.formBackingObject()...");
		if (request.getRequestURI().endsWith("topsearches"))
		{
			setCommandClass(TopSearchesForm.class);
			setCommandName("topSearchesForm");
			TopSearchesForm tsf = new TopSearchesForm();

			if (request.getParameter("Reset") == null)
			{
				@SuppressWarnings("unchecked")
				HashMap<String, Object> HashMap_topSearchesForm = (HashMap<String, Object>) request
						.getSession().getAttribute("topSearchesForm");
				if (HashMap_topSearchesForm != null)
				{
					tsf.setDepth((String) HashMap_topSearchesForm.get("depth"));
					tsf.setBreakdownBy((String) HashMap_topSearchesForm
							.get("breakdownBy"));
					tsf.setIncDims1((String) HashMap_topSearchesForm
							.get("incDims1"));
					tsf.setIncDims2((String) HashMap_topSearchesForm
							.get("incDims2"));
					tsf.setIncDims3((String) HashMap_topSearchesForm
							.get("incDims3"));
					tsf.setSearchOrder((String) HashMap_topSearchesForm
							.get("searchOrder"));
					tsf.setNumRecords((Integer) HashMap_topSearchesForm
							.get("numRecords"));
				} else
				{
					tsf.setFromDate(FormFormater.getPreviousMonth());
					tsf.setToDate(FormFormater.getCurrentDate());
					tsf.setDepth("One");
					tsf.setNumRecords(20);
					tsf.setBreakdownBy("All");
					tsf.setSearchOrder("Yes");
					tsf.setIncDims1("All");
					tsf.setIncDims2("All");
					tsf.setIncDims3("All");
				}

				if (request.getSession().getAttribute("fromDate") != null
						&& request.getSession().getAttribute("toDate") != null)
				{
					tsf.setFromDate((String) request.getSession().getAttribute(
							"fromDate"));
					tsf.setToDate((String) request.getSession().getAttribute(
							"toDate"));
				}
			} else
			{
				@SuppressWarnings("unchecked")
				HashMap<String, Object> HashMap_topSearchesForm = (HashMap<String, Object>) request
						.getSession().getAttribute("topSearchesForm");
				if (HashMap_topSearchesForm != null)
					request.getSession().setAttribute("topSearchesForm", null);
				request.getSession().removeAttribute("fromDate");
				request.getSession().removeAttribute("toDate");
				tsf.setFromDate(FormFormater.getPreviousMonth());
				tsf.setToDate(FormFormater.getCurrentDate());
				tsf.setDepth("one");
				tsf.setNumRecords(20);
				tsf.setBreakdownBy("All");
				tsf.setSearchOrder("Yes");
				tsf.setIncDims1("All");
				tsf.setIncDims2("All");
				tsf.setIncDims3("All");
			}

			this.setFormView("InputView\\topsearchesInputView");
			return tsf;
		}
		return null;
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception
	{
		ClassDebug.print("TopsearchesFormController.onSubmit()...");
		if (getCommandName().equals("topSearchesForm"))
		{
			TopSearchesForm form = (TopSearchesForm) command;
			getValidator().validate(form, errors);

			long startTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Start the query at "
					+ ClassDebug.getCurrentDateAndTime());

			HashMap<String, Object> HashMap_topSearchesForm = new HashMap<String, Object>();
			HashMap_topSearchesForm.put("fromDate", form.getFromDate());
			HashMap_topSearchesForm.put("toDate", form.getToDate());
			HashMap_topSearchesForm.put("depth", form.getDepth());
			HashMap_topSearchesForm.put("numRecords", form.getNumRecords());
			HashMap_topSearchesForm.put("breakdownBy", form.getBreakdownBy());
			HashMap_topSearchesForm.put("searchOrder", form.getSearchOrder());
			HashMap_topSearchesForm.put("incDims1", form.getIncDims1());
			HashMap_topSearchesForm.put("incDims2", form.getIncDims2());
			HashMap_topSearchesForm.put("incDims3", form.getIncDims3());
			request.getSession().setAttribute("topSearchesForm",
					HashMap_topSearchesForm);
			request.getSession().setAttribute("fromDate", form.getFromDate());
			request.getSession().setAttribute("toDate", form.getToDate());

			String fromDate = form.getFromDate();
			String toDate = form.getToDate();
			String searchOrder = form.getSearchOrder();
			int numRecords = form.getNumRecords();
			String depth = form.getDepth();
			String incDims1 = form.getIncDims1();
			String incDims2 = form.getIncDims2();
			String incDims3 = form.getIncDims3();
			String breakdownBy = form.getBreakdownBy();
			HashMap<String, Object> model = new HashMap<String, Object>();

			String[] incDimsList =
			{ incDims1, incDims2, incDims3 };
			HashMap<String, Object> topDimsSearchLists = this
					.getTopDimsSearches(fromDate, toDate, searchOrder,
							numRecords, depth, breakdownBy, incDimsList);

			model.put("dates", topDimsSearchLists.get("dates"));
			topDimsSearchLists.remove("dates");
			String filePath = this.getServletContext().getRealPath("/");

			ArrayList<JFreeChart> chartList = BarChart
					.createBarChart(topDimsSearchLists);
			for (int i = 0; i < chartList.size(); i++)
			{
				ChartUtilities.saveChartAsJPEG(new File(filePath
						+ "pages/temp/" + i + ".jpg"), chartList.get(i), 726,
						400);
			}

			model.put("topDimsSearchLists", topDimsSearchLists);
			model.put("reportName", "Report: Top " + form.getNumRecords() + " "
					+ form.getDepth() + "-Dimension-Search");
			model.put("reportName1", "Breakdown by: " + form.getBreakdownBy()
					+ ", " + "Sequence enabled: " + form.getSearchOrder());

			if (request.getSession().getAttribute("username") == null)
			{
				model.put("pageName", "login");
				model.put("value", "AdminLogin");
			} else
			{
				model.put("pageName", "logout");
				model.put("value", "Logout");
				HashMap<String, Object> common = new HashMap<String, Object>();
				common.put("title", "Admin Links");
				common.put("addPageName", "addlogfile");
				common.put("addValue", "Add Log Files");
				common.put("removePageName", "removelogfile");
				common.put("removeValue", "Remove Log Files");
				model.put("common", common);
			}
			// start to generate reports (pdf,xls,csv)
			Report.createReports(filePath, model, "TopDimSearchReport");
			model.put("savedReportName", "TopDimSearchReport");

			// Check if the report was created to avoid problems in the View
			model.put(
					"pdfReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".pdf").exists());
			model.put(
					"csvReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".csv").exists());
			model.put(
					"xlsReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".xls").exists());

			long endTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Complete the Query at "
					+ ClassDebug.getCurrentDateAndTime());
			ClassDebug.print("Time Cost: "
					+ ClassDebug.timeFormat(endTime - startTime));

			return new ModelAndView("OutputView\\topsearchesOutputView", model);
		}
		return null;
	}
}
