package springMVC;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.ModelAndView;
import dbAccess.Dao;
import debug.ClassDebug;
import forms.TopKeywordsSearchesForm;
import reportDataObjects.TopKeywordsSearch;
import reports.Report;

@SuppressWarnings("deprecation")
public class TopkeywordssearchesFormController extends SimpleFormController
{
	protected Dao dao;

	public void setDao(Dao dao)
	{
		this.dao = dao;
	}

	@Override
	protected HashMap<String, Object> referenceData(HttpServletRequest request)
			throws Exception
	{
		ClassDebug
				.print("TopkeywordssearchesFormController.referencesData()...");
		HashMap<String, Object> model = new HashMap<String, Object>();
		if (request.getSession().getAttribute("username") == null)
		{
			model.put("pageName", "login");
			model.put("value", "AdminLogin");
		} else
		{
			model.put("pageName", "logout");
			model.put("value", "Logout");
			HashMap<String, Object> common = new HashMap<String, Object>();
			common.put("title", "Admin Links");
			common.put("addPageName", "addlogfile");
			common.put("addValue", "Add Log Files");
			common.put("removePageName", "removelogfile");
			common.put("removeValue", "Remove Log Files");
			model.put("common", common);
		}
		if (request.getRequestURI().endsWith("topkeywordssearches"))
		{
			ArrayList<String> resultType = new ArrayList<String>();
			resultType.add("All");
			resultType.add("Return Zero Records");
			resultType.add("Autocorrected");
			model.put("resultType", resultType);
			ArrayList<String> numRecords = new ArrayList<String>();
			numRecords.add("5");
			numRecords.add("10");
			numRecords.add("20");
			numRecords.add("30");
			numRecords.add("40");
			if (request.getSession().getAttribute("username") != null)
			{
				numRecords.add("100");
				numRecords.add("1000");
				numRecords.add("10000");
			}
			model.put("numRecords", numRecords);
		}

		return model;
	}

	protected Object formBackingObject(HttpServletRequest request)
			throws Exception
	{
		ClassDebug
				.print("TopkeywordssearchesFormController.formBackingObject()...");
		if (request.getRequestURI().endsWith("topkeywordssearches"))
		{
			setCommandClass(TopKeywordsSearchesForm.class);
			setCommandName("topkeywordssearchesForm");
			TopKeywordsSearchesForm tksf = new TopKeywordsSearchesForm();

			if (request.getParameter("Reset") == null)
			{
				@SuppressWarnings("unchecked")
				HashMap<String, Object> HashMap_topkeywordssearchesForm = (HashMap<String, Object>) request
						.getSession().getAttribute("topkeywordssearchesForm");
				if (HashMap_topkeywordssearchesForm != null)
				{
					tksf.setFromDate((String) request.getSession()
							.getAttribute("fromDate"));
					tksf.setToDate((String) request.getSession().getAttribute(
							"toDate"));
					tksf.setNumRecords((Integer) HashMap_topkeywordssearchesForm
							.get("numRecords"));
					tksf.setResultType((String) HashMap_topkeywordssearchesForm
							.get("resultType"));
				} else
				{
					tksf.setFromDate(FormFormater.getPreviousMonth());
					tksf.setToDate(FormFormater.getCurrentDate());
					tksf.setNumRecords(30);
				}

				if (request.getSession().getAttribute("fromDate") != null
						&& request.getSession().getAttribute("toDate") != null)
				{
					tksf.setFromDate((String) request.getSession()
							.getAttribute("fromDate"));
					tksf.setToDate((String) request.getSession().getAttribute(
							"toDate"));
				}
			} else
			{
				@SuppressWarnings("unchecked")
				HashMap<String, Object> HashMap_topkeywordssearchesForm = (HashMap<String, Object>) request
						.getSession().getAttribute("topkeywordssearchesForm");
				if (HashMap_topkeywordssearchesForm != null)
					request.getSession().setAttribute(
							"topkeywordssearchesForm", null);
				request.getSession().removeAttribute("fromDate");
				request.getSession().removeAttribute("toDate");
				tksf.setFromDate(FormFormater.getPreviousMonth());
				tksf.setToDate(FormFormater.getCurrentDate());
				tksf.setNumRecords(30);
			}

			this.setFormView("InputView\\topkeywordssearchesInputView");
			return tksf;
		} else
			return null;
	}

	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception
	{
		ClassDebug.print("TopkeywordssearchesFormController.onSubmit()...");
		if (getCommandName().equals("topkeywordssearchesForm"))
		{
			TopKeywordsSearchesForm form = (TopKeywordsSearchesForm) command;
			getValidator().validate(form, errors);

			long startTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Start the query at "
					+ ClassDebug.getCurrentDateAndTime());

			HashMap<String, Object> HashMap_topkeywordssearchesForm = new HashMap<String, Object>();
			HashMap_topkeywordssearchesForm.put("fromDate", form.getFromDate());
			HashMap_topkeywordssearchesForm.put("toDate", form.getToDate());
			HashMap_topkeywordssearchesForm.put("resultType",
					form.getResultType());
			HashMap_topkeywordssearchesForm.put("numRecords",
					form.getNumRecords());
			request.getSession().setAttribute("topkeywordssearchesForm",
					HashMap_topkeywordssearchesForm);
			request.getSession().setAttribute("fromDate", form.getFromDate());
			request.getSession().setAttribute("toDate", form.getToDate());
			HashMap<String, Object> model = new HashMap<String, Object>();
			HashMap<String, Object> topKeywordsSearches = new HashMap<String, Object>();
			String lastDate = dao.getLastDate();
			String firstDate = dao.getFirstDate();
			String fromDate = form.getFromDate();
			String toDate = form.getToDate();
			String resultType = form.getResultType();
			int numRecords = form.getNumRecords();
			if ((!FormFormater.compareDates(lastDate,
					FormFormater.formatDate(toDate)))
					&& (!FormFormater.compareDates(
							FormFormater.formatDate(fromDate), firstDate)))
			{
				fromDate = FormFormater.formatDateTwo(firstDate);
				toDate = FormFormater.formatDateTwo(lastDate);
			} else if ((!FormFormater.compareDates(
					FormFormater.formatDate(form.getFromDate()), firstDate))
					&& (!FormFormater.compareDates(firstDate,
							FormFormater.formatDate(toDate)))
					&& (!FormFormater.compareDates(
							FormFormater.formatDate(toDate), lastDate)))
			{
				fromDate = FormFormater.formatDateTwo(firstDate);
			} else if ((!FormFormater.compareDates(lastDate,
					FormFormater.formatDate(toDate)))
					&& (!FormFormater.compareDates(firstDate,
							FormFormater.formatDate(fromDate)))
					&& (!FormFormater.compareDates(
							FormFormater.formatDate(fromDate), lastDate)))
			{
				toDate = FormFormater.formatDateTwo(lastDate);
			}
			String date = fromDate + " - " + toDate;
			topKeywordsSearches.put("date", date);
			topKeywordsSearches.put("topKeywordsSearchesList", dao
					.getTopKeywordsSearches(FormFormater.formatDate(fromDate),
							FormFormater.formatDate(toDate), resultType,
							numRecords));

			model.put("date", topKeywordsSearches.get("date"));

			topKeywordsSearches.remove("date");
			@SuppressWarnings("unchecked")
			List<TopKeywordsSearch> topKeywordsSearchesList = (List<TopKeywordsSearch>) topKeywordsSearches
					.get("topKeywordsSearchesList");
			if (topKeywordsSearchesList.size() != 0)
			{
				if (topKeywordsSearchesList.get(0).getAutoCorrectTo() != null)
				{
					model.put("autocorrectto", "yes");
				} else
				{
					model.put("autocorrectto", "no");
				}
			}

			model.put("topKeywordsSearchesList", topKeywordsSearchesList);
			model.put("reportName", "Report: Top " + form.getNumRecords());
			model.put("reportName1", "Result Type: " + form.getResultType());

			if (request.getSession().getAttribute("username") == null)
			{
				model.put("pageName", "login");
				model.put("value", "AdminLogin");
			} else
			{
				model.put("pageName", "logout");
				model.put("value", "Logout");
				HashMap<String, Object> common = new HashMap<String, Object>();
				common.put("title", "Admin Links");
				common.put("addPageName", "addlogfile");
				common.put("addValue", "Add Log Files");
				common.put("removePageName", "removelogfile");
				common.put("removeValue", "Remove Log Files");
				model.put("common", common);
			}
			// start to generate reports (pdf,xls,csv)
			String filePath = this.getServletContext().getRealPath("/");
			Report.createReports(filePath, model, "TopKeywordsSearchReport");
			model.put("savedReportName", "TopKeywordsSearchReport");

			// Check if the report was created to avoid problems in the View
			model.put(
					"pdfReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".pdf").exists());
			model.put(
					"csvReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".csv").exists());
			model.put(
					"xlsReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".xls").exists());

			long endTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Complete the Query at "
					+ ClassDebug.getCurrentDateAndTime());
			ClassDebug.print("Time Cost: "
					+ ClassDebug.timeFormat(endTime - startTime));

			return new ModelAndView(
					"OutputView\\topkeywordssearchesOutputView", model);
		}
		return null;
	}
}
