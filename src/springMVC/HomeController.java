package springMVC;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.FileCopyUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import debug.ClassDebug;

import services.Service;

/**
 * This controller class is used to return a static home page and process
 * requests of PDF, CSV, XLS reports
 * 
 */
public class HomeController extends AbstractController
{

	Service service;

	public void setService(Service service)
	{
		this.service = service;
	}

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
		ClassDebug.print("HomeController.handleRequestInternal()...");
		if (request.getRequestURI().endsWith("home")
				|| request.getRequestURI().endsWith("logout"))
		{
			HashMap<String, Object> model = new HashMap<String, Object>();
			model.put("reportName", "Quick Guide");
			model.put("introContent1",
					"Welcome to user trends analysis application for ConnectFurniture website!");
			model.put(
					"introContent2",
					"Please select a report type from the panel on the left. Below is a brief description of the available reporting schemes and options associated with each:");
			model.put("reports", "Description of reports");
			model.put("dimensionsTitle", "Top Dimensions Searches");
			model.put("hr", "aaaaaaa");
			model.put("dimensionsContent",
					"Depicting common usage paths users tend to follow when using the website");
			model.put("valuesTitle", "Top Dimension Values");
			model.put(
					"valuesContent",
					"Analysing popular values selected by users for different options and viewing weekly trends for specific values or pairs of values.");
			model.put("timeTitle", "Requests by Hour");
			model.put("timeContent",
					"Showing the website's usage volume based on the time of the day");
			model.put("sessionTitle", "Sessions by Hour");
			model.put(
					"sessionContent",
					"Showing how many search ephisodes the user has had based on the time of the day");
			model.put("typeTitle", "Requests by Type");
			model.put(
					"typeContent",
					"Displaying how many users prefer using the navigation toolbar vs searching for keywords");
			model.put("keywordTitle", "Top Keyword Searches");
			model.put("keywordContent",
					"Exposing the popular and non-popular words being searches on the website");
			model.put("options", "Description of options");
			model.put("datesTitle", "Dates");
			model.put("datesContent", "To pick the date range for the analysis");
			model.put("breakdownByTitle", "Breakdown By");
			model.put(
					"breakdownByContent",
					"To specify the unit to split the analysis and trends (weeks, months, seasons, etc.)");
			model.put("searchOrderTitle", "Search Order");
			model.put(
					"searchOrderContent",
					"To specify weather you wish the sequence of the user's behaviour to be considered");
			if (request.getSession().getAttribute("username") == null)
			{
				model.put("pageName", "login");
				model.put("value", "AdminLogin");
			} else
			{
				model.put("pageName", "logout");
				model.put("value", "Logout");
				model.put("common", service.getCommon());
			}
			return new ModelAndView("home", model);

		} else
		{
			String path = this.getServletContext().getRealPath("/");
			String fileName = request.getRequestURI().substring(
					(request.getRequestURI().lastIndexOf("/") + 1),
					request.getRequestURI().length());

			File file = new File(path + "pages/reports/" + fileName);
			int fSize = (int) file.length();
			BufferedInputStream in = new BufferedInputStream(
					new FileInputStream(file));
			String mimetype = this.getServletContext().getMimeType(fileName);
			response.setBufferSize(fSize);
			response.setContentType((mimetype != null) ? mimetype
					: "application/octet-stream");
			response.setHeader("Content-Disposition", "attachment; filename=\""
					+ fileName + "\"");
			response.setContentLength(fSize);
			FileCopyUtils.copy(in, response.getOutputStream());
			in.close();
			response.getOutputStream().flush();
			response.getOutputStream().close();
			return null;
		}

	}

}
