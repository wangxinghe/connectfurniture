package springMVC;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import others.TimeEstimation;

import charts.SessionByBarChart;

import reportDataObjects.SessionBy;
import reports.SessionByReport;

import dbAccess.Dao;
import debug.ClassDebug;
import forms.*;

/**
 * Session by form controller which control the form for the session by report.
 * 
 * @author yoursoft
 * 
 */
@SuppressWarnings("deprecation")
public class SessionByFormController extends SimpleFormController
{
	protected Dao dao;
	double timeConsume;
	double timeConsumeEstimated;
	double timeConsumeNew;

	/**
	 * Set the data access object.
	 * 
	 * @param dao
	 */
	public void setDao(Dao dao)
	{
		this.dao = dao;
	}

	/**
	 * Set the options and links you need to use in the input view.
	 * 
	 * @see org.springframework.web.servlet.mvc.SimpleFormController#referenceData(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	protected HashMap<String, Object> referenceData(HttpServletRequest request)
			throws Exception
	{
		ClassDebug.print("SessionbyFormController.referencesData()...");
		HashMap<String, Object> model = new HashMap<String, Object>();
		if (request.getSession().getAttribute("username") == null)
		{
			model.put("pageName", "login");
			model.put("value", "AdminLogin");
		} else
		{
			model.put("pageName", "logout");
			model.put("value", "Logout");
			HashMap<String, Object> common = new HashMap<String, Object>();
			common.put("title", "Admin Links");
			common.put("addPageName", "addlogfile");
			common.put("addValue", "Add Log Files");
			common.put("removePageName", "removelogfile");
			common.put("removeValue", "Remove Log Files");
			model.put("common", common);
		}
		if (request.getRequestURI().endsWith("sessionby"))
		{
			ArrayList<String> type = new ArrayList<String>();
			type.add("Hour of Day");
			type.add("Day of Month");
			type.add("Month of Year");
			type.add("None");
			model.put("type", type);
		}
		return model;
	}

	/**
	 * Retrieve a backing object for the current form from the given request.
	 * 
	 * @see org.springframework.web.servlet.mvc.AbstractFormController#formBackingObject(javax.servlet.http.HttpServletRequest)
	 */
	protected Object formBackingObject(HttpServletRequest request)
			throws Exception
	{
		ClassDebug.print("SessionbyFormController.formBackingObject()...");
		System.out.println(request.getParameter("View Report"));
		System.out.println(request.getParameter("Reset"));
		if (request.getRequestURI().endsWith("sessionby"))
		{
			SessionByForm sf = new SessionByForm();
			setCommandClass(SessionByForm.class);
			setCommandName("sessionbyForm");

			if (request.getParameter("Reset") == null)
			{
				/**
				 * This hashmap is for saving the information in the session.
				 */
				@SuppressWarnings("unchecked")
				HashMap<String, Object> HashMap_sessionbyForm = (HashMap<String, Object>) request
						.getSession().getAttribute("sessionbyForm");
				if (HashMap_sessionbyForm != null
						&& request.getSession().getAttribute("fromDate") != null
						&& request.getSession().getAttribute("toDate") != null)
				{
					sf.setCompareFromDate((String) HashMap_sessionbyForm
							.get("compareFromDate"));
					sf.setCompareToDate((String) HashMap_sessionbyForm
							.get("compareToDate"));
					sf.setFromDate((String) request.getSession().getAttribute(
							"fromDate"));
					sf.setToDate((String) request.getSession().getAttribute(
							"toDate"));
					sf.setType((String) HashMap_sessionbyForm.get("type"));
					sf.setIsCompared(new Boolean(
							(Boolean) HashMap_sessionbyForm.get("isCompared"))
							.booleanValue());
				} else
				{
					sf.setFromDate(FormFormater.getPreviousMonth());
					sf.setToDate(FormFormater.getCurrentDate());
					sf.setIsCompared(false);
				}
			} else
			{
				/**
				 * Set the information in the request for the first time.
				 */
				@SuppressWarnings("unchecked")
				HashMap<String, Object> HashMap_sessionbyForm = (HashMap<String, Object>) request
						.getSession().getAttribute("sessionbyForm");
				if (HashMap_sessionbyForm != null)
					request.getSession().setAttribute("sessionbyForm", null);
				request.getSession().removeAttribute("fromDate");
				request.getSession().removeAttribute("toDate");
				sf.setFromDate(FormFormater.getPreviousMonth());
				sf.setToDate(FormFormater.getCurrentDate());
				sf.setIsCompared(false);
			}

			this.setFormView("InputView\\sessionbyInputView");
			return sf;
		}
		return null;
	}

	/**
	 * Performing a submit action and rendering the specified success view.
	 * 
	 * @see org.springframework.web.servlet.mvc.SimpleFormController#onSubmit(javax.servlet.http.HttpServletRequest,
	 *      javax.servlet.http.HttpServletResponse, java.lang.Object,
	 *      org.springframework.validation.BindException)
	 */
	@SuppressWarnings("unchecked")
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception
	{

		ClassDebug.print("SessionbyFormController.onSubmit()...");
		System.out.println("onsubmit");

		if (getCommandName().equals("sessionbyForm"))
		{
			SessionByForm form = (SessionByForm) command;
			getValidator().validate(form, errors);

			// TimeEstimation tmpTimeEstimation = new
			// TimeEstimation(form.getFromDate(), form.getToDate(), dao,
			// "SessionBy");
			// double tmpTimeEstimated = tmpTimeEstimation.start();
			// ClassDebug.print(tmpTimeEstimated == -1 ?
			// "No enough history data and unable to make an estimation!" :
			// "Approximate time cost: " +
			// ClassDebug.timeFormat((int)tmpTimeEstimated));

			long startTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Start the query at "
					+ ClassDebug.getCurrentDateAndTime());
			/**
			 * This hashmap is for storing the request form.
			 */
			HashMap<String, Object> HashMap_sessionbyForm = new HashMap<String, Object>();
			HashMap_sessionbyForm.put("fromDate", form.getFromDate());
			HashMap_sessionbyForm.put("toDate", form.getToDate());
			HashMap_sessionbyForm.put("compareFromDate",
					form.getCompareFromDate());
			HashMap_sessionbyForm.put("compareToDate", form.getCompareToDate());
			HashMap_sessionbyForm.put("type", form.getType());
			HashMap_sessionbyForm.put("isCompared", form.getIsCompared());
			request.getSession().setAttribute("sessionbyForm",
					HashMap_sessionbyForm);
			request.getSession().setAttribute("fromDate", form.getFromDate());
			request.getSession().setAttribute("toDate", form.getToDate());

			// if "None" in the form is selected, it means that results are
			// group by date
			if (form.getIsCompared() == false)
			{
				ClassDebug.print("form.getIsCompared is false");
				form.setCompareFromDate("");
				form.setCompareToDate("");
			}
			if (form.getIsCompared() == true)
			{
				ClassDebug.print("form.getIsCompared is true");
			}
			if (form.getType().compareTo("None") == 0)
			{
				form.setType("Date");
			}

			/**
			 * This hashmap is for storing the model for the output view.
			 */
			HashMap<String, Object> model = new HashMap<String, Object>();
			/**
			 * This hashmap is for storing the data structure which stores the
			 * data retrieved from the database.
			 */
			HashMap<String, Object> sessionBy = new HashMap<String, Object>();// =
																				// service.getSessionByHour(form.getFromDate(),
																				// form.getToDate(),
																				// form.getCompareFromDate(),
																				// form.getCompareToDate());
			String lastDate = dao.getLastDate();
			String firstDate = dao.getFirstDate();
			String fromDate = form.getFromDate();
			String toDate = form.getToDate();
			String compareFromDate = form.getCompareFromDate();
			String compareToDate = form.getCompareToDate();
			if ((!FormFormater.compareDates(lastDate,
					FormFormater.formatDate(toDate)))
					&& (!FormFormater.compareDates(
							FormFormater.formatDate(fromDate), firstDate)))
			{
				fromDate = FormFormater.formatDateTwo(firstDate);
				toDate = FormFormater.formatDateTwo(lastDate);
			} else if ((!FormFormater.compareDates(
					FormFormater.formatDate(fromDate), firstDate))
					&& (!FormFormater.compareDates(firstDate,
							FormFormater.formatDate(toDate)))
					&& (!FormFormater.compareDates(
							FormFormater.formatDate(toDate), lastDate)))
			{
				fromDate = FormFormater.formatDateTwo(firstDate);
			} else if ((!FormFormater.compareDates(lastDate,
					FormFormater.formatDate(toDate)))
					&& (!FormFormater.compareDates(firstDate,
							FormFormater.formatDate(fromDate)))
					&& (!FormFormater.compareDates(
							FormFormater.formatDate(fromDate), lastDate)))
			{
				toDate = FormFormater.formatDateTwo(lastDate);
			}
			String date = fromDate + " - " + toDate;
			if ((!compareFromDate.equals("")) && (!compareToDate.equals("")))
			{
				if (FormFormater.compareDates(
						FormFormater.formatDate(compareToDate),
						dao.getLastDate()))
				{
					compareToDate = FormFormater.formatDateTwo(dao
							.getLastDate());
				}
				date += "  Compared With  " + compareFromDate + " - "
						+ compareToDate;
				List<SessionBy> sessionByList1 = dao.getSessionBy(
						FormFormater.formatDate(compareFromDate),
						FormFormater.formatDate(compareToDate), form.getType());
				sessionBy.put("sessionByList1", sessionByList1);
			}
			/**
			 * List (data structure) which stores the retrieved data.
			 */
			List<SessionBy> sessionByList = dao.getSessionBy(
					FormFormater.formatDate(fromDate),
					FormFormater.formatDate(toDate), form.getType());
			sessionBy.put("sessionByList", sessionByList);
			sessionBy.put("date", date);

			model.put("date", sessionBy.get("date"));
			sessionBy.remove("date");
			//
			JFreeChart chart = SessionByBarChart.createBarChartForSessionBy(
					sessionBy, form.getType());
			String filePath = this.getServletContext().getRealPath("/");
			ChartUtilities.saveChartAsJPEG(new File(filePath
					+ "pages/temp/barchart.jpg"), chart, 726, 400);
			//
			if (sessionBy.size() == 2)
			{
				int totalSessions = 0;
				int totalSessions1 = 0;
				ArrayList<SessionBy> sb = (ArrayList<SessionBy>) sessionBy
						.get("sessionByList");
				ArrayList<SessionBy> sb1 = (ArrayList<SessionBy>) sessionBy
						.get("sessionByList1");
				for (int i = 0; i < sb.size(); i++)
				{
					totalSessions += sb.get(i).getSessions();
				}
				for (int i = 0; i < sb1.size(); i++)
				{
					totalSessions1 += sb1.get(i).getSessions();
				}
				model.put("sessionByList", sb);
				model.put("sessionByList1", sb1);
				model.put("totalSessions", totalSessions);
				model.put("totalSessions1", totalSessions1);
				model.put("th1", "1st " + form.getType());
				model.put("th2", "1st Sessions");
				model.put("th3", "2nd " + form.getType());
				model.put("th4", "2nd Sessions");
				model.put("reportName", "Session By " + form.getType());
				model.put("img", "pages/temp/barchart.jpg");
			} else
			{
				int totalSessions = 0;
				ArrayList<SessionBy> sb = (ArrayList<SessionBy>) sessionBy
						.get("sessionByList");
				for (int i = 0; i < sb.size(); i++)
				{
					totalSessions += sb.get(i).getSessions();
				}
				model.put("sessionByList", sb);
				model.put("totalSessions", totalSessions);
				model.put("th1", form.getType());
				model.put("th2", "Sessions");
				model.put("reportName", "Session By " + form.getType());
				model.put("img", "pages/temp/barchart.jpg");
			}

			if (request.getSession().getAttribute("username") == null)
			{
				model.put("pageName", "login");
				model.put("value", "AdminLogin");
			} else
			{
				model.put("pageName", "logout");
				model.put("value", "Logout");
				HashMap<String, Object> common = new HashMap<String, Object>();
				common.put("title", "Admin Links");
				common.put("addPageName", "addlogfile");
				common.put("addValue", "Add Log Files");
				common.put("removePageName", "removelogfile");
				common.put("removeValue", "Remove Log Files");
				model.put("common", common);
			}
			// start to generate reports (pdf,xls,csv)
			SessionByReport.createReports(filePath, model, "SessionByReport");
			model.put("savedReportName", "SessionByReport");

			// Check if the report was created to avoid problems in the View
			model.put(
					"pdfReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".pdf").exists());
			model.put(
					"csvReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".csv").exists());
			model.put(
					"xlsReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".xls").exists());
			long endTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Complete the Query at "
					+ ClassDebug.getCurrentDateAndTime());
			ClassDebug.print("Time Cost: "
					+ ClassDebug.timeFormat(endTime - startTime));

			// tmpTimeEstimation.end(endTime - startTime);

			return new ModelAndView("OutputView\\sessionbyOutputView", model);
		}
		return null;
	}
}
