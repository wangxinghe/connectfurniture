package springMVC;

import java.io.File;
import java.util.*;

import javax.servlet.http.*;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.*;
import org.springframework.validation.BindException;

import charts.PieChart;

import reportDataObjects.RequestByUser_SearchOrNavigate;
import reportDataObjects.RequestByUser_All;
import reportDataObjects.RequestByUser_SearchAndNavigate;
import reports.RequestByUserReport;

import dbAccess.*;
import debug.*;
import forms.*;

@SuppressWarnings("deprecation")
public class RequestByUserFormController extends SimpleFormController
{
	protected Dao dao;

	public void setDao(Dao dao)
	{
		this.dao = dao;
	}

	@Override
	protected HashMap<String, Object> referenceData(HttpServletRequest request)
			throws Exception
	{
		ClassDebug.print("RequestByUserFormController.referencesData()...");
		HashMap<String, Object> model = new HashMap<String, Object>();
		if (request.getSession().getAttribute("username") == null)
		{
			model.put("pageName", "login");
			model.put("value", "AdminLogin");
		} else
		{
			model.put("pageName", "logout");
			model.put("value", "Logout");
			HashMap<String, Object> common = new HashMap<String, Object>();
			common.put("title", "Admin Links");
			common.put("addPageName", "addlogfile");
			common.put("addValue", "Add Log Files");
			common.put("removePageName", "removelogfile");
			common.put("removeValue", "Remove Log Files");
			model.put("common", common);
		}

		if (request.getRequestURI().endsWith("requestbyuser"))
		{
			ArrayList<String> ListRequestType = new ArrayList<String>();
			ListRequestType.add("All Requests");
			ListRequestType.add("Search Only Requests");
			ListRequestType.add("Navigate Only Requests");
			ListRequestType.add("Search And Navigate Requests");
			ListRequestType.add("All Requests With Graph");
			model.put("ListRequestType", ListRequestType);
		}

		return model;
	}

	protected Object formBackingObject(HttpServletRequest request)
			throws Exception
	{
		ClassDebug.print("RequestByUserFormController.formBackingObject()...");
		if (request.getRequestURI().endsWith("requestbyuser"))
		{
			setCommandClass(RequestByUserForm.class);
			setCommandName("RequestByUserForm");
			RequestByUserForm tmpForm = new RequestByUserForm();

			@SuppressWarnings("unchecked")
			HashMap<String, Object> HashMap_RequestByUserForm = (HashMap<String, Object>) request
					.getSession().getAttribute("RequestByUserForm");
			if (request.getParameter("Reset") == null)
			{
				if (HashMap_RequestByUserForm != null)
				{
					tmpForm.setRequestType((String) HashMap_RequestByUserForm
							.get("requestType"));
				} else
				{
					tmpForm.setFromDate(FormFormater.getPreviousMonth());
					tmpForm.setToDate(FormFormater.getCurrentDate());
					tmpForm.setRequestType("All Requests");
				}

				if (request.getSession().getAttribute("fromDate") != null
						&& request.getSession().getAttribute("toDate") != null)
				{
					tmpForm.setFromDate((String) request.getSession()
							.getAttribute("fromDate"));
					tmpForm.setToDate((String) request.getSession()
							.getAttribute("toDate"));
				}
			} else
			{
				if (HashMap_RequestByUserForm != null)
					request.getSession().removeAttribute("RequestByUserForm");
				request.getSession().removeAttribute("fromDate");
				request.getSession().removeAttribute("toDate");
				tmpForm.setFromDate(FormFormater.getPreviousMonth());
				tmpForm.setToDate(FormFormater.getCurrentDate());
				tmpForm.setRequestType("All Requests");
			}

			this.setFormView("InputView\\requestbyuserInputView");
			return tmpForm;
		} else
			return null;
	}

	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception
	{
		ClassDebug.print("RequestByUserFormController.onSubmit()...");
		if (getCommandName().equals("RequestByUserForm"))
		{
			RequestByUserForm form = (RequestByUserForm) command;

			String lastDate = dao.getLastDate();
			String firstDate = dao.getFirstDate();
			String fromDate = form.getFromDate();
			String toDate = form.getToDate();
			String requestType = form.getRequestType();

			HashMap<String, Object> HashMap_RequestByUserForm = new HashMap<String, Object>();
			HashMap_RequestByUserForm.put("fromDate", fromDate);
			HashMap_RequestByUserForm.put("toDate", toDate);
			HashMap_RequestByUserForm.put("requestType", requestType);

			request.getSession().setAttribute("RequestByUserForm",
					HashMap_RequestByUserForm);
			request.getSession().setAttribute("fromDate", form.getFromDate());
			request.getSession().setAttribute("toDate", form.getToDate());

			if ((!FormFormater.compareDates(lastDate,
					FormFormater.formatDate(toDate)))
					&& (!FormFormater.compareDates(
							FormFormater.formatDate(fromDate), firstDate)))
			{
				fromDate = FormFormater.formatDateTwo(firstDate);
				toDate = FormFormater.formatDateTwo(lastDate);
			} else if ((!FormFormater.compareDates(
					FormFormater.formatDate(form.getFromDate()), firstDate))
					&& (!FormFormater.compareDates(firstDate,
							FormFormater.formatDate(toDate)))
					&& (!FormFormater.compareDates(
							FormFormater.formatDate(toDate), lastDate)))
			{
				fromDate = FormFormater.formatDateTwo(firstDate);
			} else if ((!FormFormater.compareDates(lastDate,
					FormFormater.formatDate(toDate)))
					&& (!FormFormater.compareDates(firstDate,
							FormFormater.formatDate(fromDate)))
					&& (!FormFormater.compareDates(
							FormFormater.formatDate(fromDate), lastDate)))
			{
				toDate = FormFormater.formatDateTwo(lastDate);
			}

			long startTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Start the query at "
					+ ClassDebug.getCurrentDateAndTime());

			HashMap<String, Object> model = new HashMap<String, Object>();

			fromDate = FormFormater.formatDate(fromDate);
			toDate = FormFormater.formatDate(toDate);

			ClassDebug.print("fromDate: " + fromDate);
			ClassDebug.print("toDate: " + toDate);
			ClassDebug.print("requestType: " + requestType);

			String filePath = this.getServletContext().getRealPath("/");

			model.put("date", form.getFromDate() + " - " + form.getToDate());
			model.put("reportName", "Request By User Report");
			model.put("RequestType", requestType);

			if (request.getSession().getAttribute("username") == null)
			{
				model.put("pageName", "login");
				model.put("value", "AdminLogin");
			} else
			{
				model.put("pageName", "logout");
				model.put("value", "Logout");
				HashMap<String, Object> common = new HashMap<String, Object>();
				common.put("title", "Admin Links");
				common.put("addPageName", "addlogfile");
				common.put("addValue", "Add Log Files");
				common.put("removePageName", "removelogfile");
				common.put("removeValue", "Remove Log Files");
				model.put("common", common);
			}

			if (requestType.equals("Search Only Requests")
					|| requestType.equals("Navigate Only Requests"))
			{
				ArrayList<RequestByUser_SearchOrNavigate> ArrayListResult = dao
						.getRequestByUser_SearchOrNavigateOnlyRequests(
								fromDate, toDate, requestType);

				int TotalUsers = 0;
				for (int i = 0; i < ArrayListResult.size(); i++)
				{
					TotalUsers += Integer.valueOf(ArrayListResult.get(i)
							.getNumberOfUsers());
				}
				model.put("TotalUsers_SearchOrNavigateOnly", TotalUsers);

				if (requestType.equals("Search Only Requests"))
					model.put("savedReportName",
							"RequestByUserReport_SearchOnly");
				else
					model.put("savedReportName",
							"RequestByUserReport_NavigateOnly");
				model.put("ArrayListResultIndex", ArrayListResult);
				// start to generate reports (pdf,xls,csv)
				RequestByUserReport.createReports(filePath, model);
			} else if (requestType.equals("Search And Navigate Requests"))
			{
				ArrayList<RequestByUser_SearchAndNavigate> ArrayListSearchAndNavigateResult = dao
						.getRequestByUser_SearchAndNavigateRequests(fromDate,
								toDate);
				model.put("ArrayListSearchAndNavigateResultIndex",
						ArrayListSearchAndNavigateResult);

				int TotalUsers = 0;
				for (int i = 0; i < ArrayListSearchAndNavigateResult.size(); i++)
				{
					if (ArrayListSearchAndNavigateResult.get(i)
							.getNumberOfNavigates() == 0
							|| ArrayListSearchAndNavigateResult.get(i)
									.getNumberOfSearches() == 0)
						continue;
					else
						TotalUsers += ArrayListSearchAndNavigateResult.get(i)
								.getNumberOfUsers();
				}
				model.put("TotalUsers_SearchAndNavigate", TotalUsers);

				model.put("savedReportName",
						"RequestByUserReport_SearchAndNavigate");
				// start to generate reports (pdf,xls,csv)
				RequestByUserReport.createReports(filePath, model);
			} else if (requestType.equals("All Requests With Graph"))
			{
				ArrayList<RequestByUser_SearchOrNavigate> ArrayListSearchOnlyResult = dao
						.getRequestByUser_SearchOrNavigateOnlyRequests(
								fromDate, toDate, "Search Only Requests");
				ArrayList<RequestByUser_SearchOrNavigate> ArrayListNavigateOnlyResult = dao
						.getRequestByUser_SearchOrNavigateOnlyRequests(
								fromDate, toDate, "Navigate Only Requests");
				ArrayList<RequestByUser_SearchAndNavigate> ArrayListSearchAndNavigateResult = dao
						.getRequestByUser_SearchAndNavigateRequests(fromDate,
								toDate);

				// int tmpAllSearch =
				// dao.getRequestByUser_AllSearchUsers(fromDate, toDate);
				// int tmpAllNavigate =
				// dao.getRequestByUser_AllNavigateUsers(fromDate, toDate);

				int TotalUsers = 0;
				for (int i = 0; i < ArrayListSearchAndNavigateResult.size(); i++)
				{
					if (ArrayListSearchAndNavigateResult.get(i)
							.getNumberOfNavigates() == 0
							|| ArrayListSearchAndNavigateResult.get(i)
									.getNumberOfSearches() == 0)
						continue;
					else
						TotalUsers += ArrayListSearchAndNavigateResult.get(i)
								.getNumberOfUsers();
				}
				int tmpSearchAndNavigate = TotalUsers;// dao.getRequestByUser_SearchAndNavigateUsers(fromDate,
														// toDate);

				TotalUsers = 0;
				for (int i = 0; i < ArrayListSearchOnlyResult.size(); i++)
				{
					TotalUsers += Integer.valueOf(ArrayListSearchOnlyResult
							.get(i).getNumberOfUsers());
				}

				int tmpSearchOnly = TotalUsers;// tmpAllSearch -
												// tmpSearchAndNavigate;

				TotalUsers = 0;
				for (int i = 0; i < ArrayListNavigateOnlyResult.size(); i++)
				{
					TotalUsers += Integer.valueOf(ArrayListNavigateOnlyResult
							.get(i).getNumberOfUsers());
				}

				int tmpNavigateOnly = TotalUsers;// tmpAllNavigate -
													// tmpSearchAndNavigate;

				HashMap<String, Object> HashMap_RequestByUser = new HashMap<String, Object>();
				HashMap_RequestByUser.put("Search Only", tmpSearchOnly);
				HashMap_RequestByUser.put("Navigate Only", tmpNavigateOnly);
				HashMap_RequestByUser.put("Search And Navigate",
						tmpSearchAndNavigate);

				JFreeChart chart = PieChart
						.createPieChart(HashMap_RequestByUser);
				ChartUtilities.saveChartAsJPEG(new File(filePath
						+ "pages/temp/piechart.jpg"), chart, 726, 400);

				model.put("img", "pages/temp/piechart.jpg");
				model.put("tmpSearchOnly", tmpSearchOnly);
				model.put("tmpNavigateOnly", tmpNavigateOnly);
				model.put("tmpSearchAndNavigate", tmpSearchAndNavigate);
			} else if (requestType.equals("All Requests"))
			{
				ArrayList<RequestByUser_SearchOrNavigate> ArrayListSearchOnlyResult = dao
						.getRequestByUser_SearchOrNavigateOnlyRequests(
								fromDate, toDate, "Search Only Requests");
				ArrayList<RequestByUser_SearchOrNavigate> ArrayListNavigateOnlyResult = dao
						.getRequestByUser_SearchOrNavigateOnlyRequests(
								fromDate, toDate, "Navigate Only Requests");
				ArrayList<RequestByUser_SearchAndNavigate> ArrayListSearchAndNavigateResult = dao
						.getRequestByUser_SearchAndNavigateRequests(fromDate,
								toDate);

				ArrayList<RequestByUser_All> ArrayListAllRequest = new ArrayList<RequestByUser_All>();
				RequestByUser_All tmpRequestByUser_All;

				for (int i = 0; i < ArrayListSearchOnlyResult.size(); i++)
				{
					tmpRequestByUser_All = new RequestByUser_All();
					tmpRequestByUser_All
							.setNumberOfSearches(ArrayListSearchOnlyResult.get(
									i).getNumberOfRequests());
					tmpRequestByUser_All.setNumberOfNavigates(0);
					tmpRequestByUser_All
							.setNumberOfUsers(ArrayListSearchOnlyResult.get(i)
									.getNumberOfUsers());
					ArrayListAllRequest.add(tmpRequestByUser_All);
				}

				// add a separating mark by adding a record whose items are all
				// value of zero
				tmpRequestByUser_All = new RequestByUser_All();
				tmpRequestByUser_All.setNumberOfNavigates(0);
				tmpRequestByUser_All.setNumberOfSearches(0);
				tmpRequestByUser_All.setNumberOfUsers(0);
				ArrayListAllRequest.add(tmpRequestByUser_All);

				for (int i = 0; i < ArrayListNavigateOnlyResult.size(); i++)
				{
					tmpRequestByUser_All = new RequestByUser_All();
					tmpRequestByUser_All
							.setNumberOfNavigates(ArrayListNavigateOnlyResult
									.get(i).getNumberOfRequests());
					tmpRequestByUser_All.setNumberOfSearches(0);
					tmpRequestByUser_All
							.setNumberOfUsers(ArrayListNavigateOnlyResult
									.get(i).getNumberOfUsers());
					ArrayListAllRequest.add(tmpRequestByUser_All);
				}

				// add a separating mark by adding a record whose items are all
				// value of zero
				tmpRequestByUser_All = new RequestByUser_All();
				tmpRequestByUser_All.setNumberOfNavigates(0);
				tmpRequestByUser_All.setNumberOfSearches(0);
				tmpRequestByUser_All.setNumberOfUsers(0);
				ArrayListAllRequest.add(tmpRequestByUser_All);

				for (int i = 0; i < ArrayListSearchAndNavigateResult.size(); i++)
				{
					RequestByUser_SearchAndNavigate tmpSearchAndNavigate = ArrayListSearchAndNavigateResult
							.get(i);
					if (tmpSearchAndNavigate.getNumberOfNavigates() == 0
							|| tmpSearchAndNavigate.getNumberOfSearches() == 0)
						continue;
					tmpRequestByUser_All = new RequestByUser_All();
					tmpRequestByUser_All
							.setNumberOfNavigates(tmpSearchAndNavigate
									.getNumberOfNavigates());
					tmpRequestByUser_All
							.setNumberOfSearches(tmpSearchAndNavigate
									.getNumberOfSearches());
					tmpRequestByUser_All.setNumberOfUsers(tmpSearchAndNavigate
							.getNumberOfUsers());
					ArrayListAllRequest.add(tmpRequestByUser_All);
				}

				int TotalUsers = 0;
				for (int i = 0; i < ArrayListAllRequest.size(); i++)
				{
					TotalUsers += Integer.valueOf(ArrayListAllRequest.get(i)
							.getNumberOfUsers());
				}
				model.put("TotalUsers_All", TotalUsers);

				model.put("savedReportName", "RequestByUserReport_All");
				model.put("ArrayListAllRequestIndex", ArrayListAllRequest);
				RequestByUserReport.createReports(filePath, model);
			}

			// Check if the report was created to avoid problems in the View
			model.put(
					"pdfReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".pdf").exists());
			model.put(
					"csvReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".csv").exists());
			model.put(
					"xlsReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".xls").exists());

			long endTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Complete the Query at "
					+ ClassDebug.getCurrentDateAndTime());
			ClassDebug.print("Time Cost: "
					+ ClassDebug.timeFormat(endTime - startTime));

			return new ModelAndView("OutputView\\requestbyuserOutputView",
					model);
		}
		return null;
	}
}
