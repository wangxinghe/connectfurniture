package springMVC;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.JFileChooser;

/**
 * This class is used to change date format, divide a date range into weeks or
 * months or seasons. It acts as an assistant class
 * 
 */
public class FormFormater
{

	/**
	 * Opens a JFileChooser dialog box
	 * 
	 * @param dialog_name
	 *            the name of the dialog box
	 * @return the path been chosen
	 */
	public static String openFolder(String dialog_name)
	{
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileChooser.setDialogTitle(dialog_name);
		int ret = fileChooser.showOpenDialog(fileChooser);
		if (ret == JFileChooser.APPROVE_OPTION)
		{
			return fileChooser.getSelectedFile().getAbsolutePath();
		} else
			return null;
	}

	/**
	 * Changes the type of depth from String to Integer
	 * 
	 * @param depth
	 * @return the type of depth from String to Integer
	 */
	public static int formatDepth(String depth)
	{
		if (depth.equals("One"))
		{
			return 1;
		} else if (depth.equals("Two"))
		{
			return 2;
		} else if (depth.equals("Three"))
		{
			return 3;
		} else if (depth.equals("Four"))
		{
			return 4;
		} else if (depth.equals("Five"))
		{
			return 5;
		} else if (depth.equals("Six"))
		{
			return 6;
		}
		return 0;

	}

	/**
	 * Changes the format of a date from 'dd/MM/yyyy' to 'yyyy-MM-dd'
	 * 
	 * @param date
	 * @return date
	 */
	public static String formatDate(String date)
	{
		try
		{
			SimpleDateFormat inFmt = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat outFmt = new SimpleDateFormat("yyyy-MM-dd");
			return outFmt.format(inFmt.parse(date));
		} catch (ParseException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Changes the format of a date from 'yyyy-MM-dd' to 'dd/MM/yyyy'
	 * 
	 * @param date
	 * @return date
	 */
	public static String formatDateTwo(String date)
	{
		try
		{
			SimpleDateFormat inFmt = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat outFmt = new SimpleDateFormat("dd/MM/yyyy");
			return outFmt.format(inFmt.parse(date));
		} catch (ParseException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Returns the current date
	 * 
	 * @return the current date
	 */
	public static String getCurrentDate()
	{
		SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		return sf.format(cal.getTime());
	}

	/**
	 * Returns the corresponding date of last month
	 * 
	 * @return the same day of the last month
	 */
	public static String getPreviousMonth()
	{
		Calendar currentDate = new GregorianCalendar();
		Date date = new Date();
		currentDate.setTime(date);
		Calendar previousMonth = new GregorianCalendar();

		previousMonth.set(Calendar.YEAR, currentDate.get(Calendar.YEAR));
		previousMonth.set(Calendar.MONTH, currentDate.get(Calendar.MONTH));
		previousMonth.set(Calendar.DAY_OF_MONTH,
				currentDate.get(Calendar.DAY_OF_MONTH));
		return previousMonth.get(Calendar.DATE) + "/"
				+ previousMonth.get(Calendar.MONTH) + "/"
				+ previousMonth.get(Calendar.YEAR);
	}

	/**
	 * Compares two dates. Returns true if the first date is after the second
	 * date or false if not
	 * 
	 * @param dateOne
	 * @param dateTwo
	 * @return true or false
	 */
	public static boolean compareDates(String dateOne, String dateTwo)
	{
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		try
		{
			return fmt.parse(dateOne).after(fmt.parse(dateTwo));
		} catch (ParseException e)
		{
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Compares two dates. Returns true if the first date is before the second
	 * date or false if not
	 * 
	 * @param dateOne
	 * @param dateTwo
	 * @return true or false
	 */
	public static boolean isBefore(String dateOne, String dateTwo)
	{
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		try
		{
			return fmt.parse(dateOne).before(fmt.parse(dateTwo));
		} catch (ParseException e)
		{
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Divides a date range into weeks
	 * 
	 * @param fromDate
	 * @param toDate
	 * @return weeks
	 */
	public static ArrayList<String> getWeeks(String fromDate, String toDate)
	{

		ArrayList<String> weeks = new ArrayList<String>();
		try
		{
			SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
			Calendar start = new GregorianCalendar();
			Calendar end = new GregorianCalendar();
			start.setTime(fmt.parse(fromDate));
			end.setTime(fmt.parse(toDate));
			while (true)
			{
				if (start.get(Calendar.YEAR) == end.get(Calendar.YEAR)
						|| start.get(Calendar.WEEK_OF_YEAR) == end
								.get(Calendar.WEEK_OF_YEAR)
						&& (end.get(Calendar.YEAR) - start.get(Calendar.YEAR) == 1)
						&& (start.get(Calendar.MONTH) != end
								.get(Calendar.MONTH)))
				{
					while (true)
					{
						if (start.get(Calendar.WEEK_OF_YEAR) == end
								.get(Calendar.WEEK_OF_YEAR))
						{
							weeks.add("Week" + start.get(Calendar.WEEK_OF_YEAR)
									+ "-" + end.get(Calendar.YEAR));
							weeks.add(start.get(Calendar.DATE) + "/"
									+ (start.get(Calendar.MONTH) + 1) + "/"
									+ start.get(Calendar.YEAR));
							weeks.add(end.get(Calendar.DATE) + "/"
									+ (end.get(Calendar.MONTH) + 1) + "/"
									+ end.get(Calendar.YEAR));
							break;
						} else
						{
							weeks.add("Week" + start.get(Calendar.WEEK_OF_YEAR)
									+ "-" + start.get(Calendar.YEAR));
							weeks.add(start.get(Calendar.DATE) + "/"
									+ (start.get(Calendar.MONTH) + 1) + "/"
									+ start.get(Calendar.YEAR));
							start.add(
									Calendar.DAY_OF_YEAR,
									start.getActualMaximum(Calendar.DAY_OF_WEEK)
											- start.get(Calendar.DAY_OF_WEEK));
							weeks.add(start.get(Calendar.DATE) + "/"
									+ (start.get(Calendar.MONTH) + 1) + "/"
									+ start.get(Calendar.YEAR));
							start.add(Calendar.DAY_OF_YEAR, 1);
						}
					}
					break;
				} else
				{
					Calendar calendar = Calendar.getInstance();
					calendar.clear();
					calendar.set(Calendar.YEAR, start.get(Calendar.YEAR));
					calendar.set(Calendar.WEEK_OF_YEAR,
							calendar.getActualMaximum(Calendar.WEEK_OF_YEAR));
					calendar.roll(Calendar.DAY_OF_WEEK, -1);
					String currYearLast = calendar.get(Calendar.DATE) + "/"
							+ (calendar.get(Calendar.MONTH) + 1) + "/"
							+ calendar.get(Calendar.YEAR);
					end.setTime(fmt.parse(currYearLast));
					while (true)
					{
						if (start.get(Calendar.WEEK_OF_YEAR) == end
								.get(Calendar.WEEK_OF_YEAR))
						{

							weeks.add("Week" + start.get(Calendar.WEEK_OF_YEAR)
									+ "-" + start.get(Calendar.YEAR));
							weeks.add(start.get(Calendar.DATE) + "/"
									+ (start.get(Calendar.MONTH) + 1) + "/"
									+ start.get(Calendar.YEAR));
							weeks.add(end.get(Calendar.DATE) + "/"
									+ (end.get(Calendar.MONTH) + 1) + "/"
									+ end.get(Calendar.YEAR));
							start.add(
									Calendar.DAY_OF_YEAR,
									end.get(Calendar.DAY_OF_WEEK)
											- start.get(Calendar.DAY_OF_WEEK)
											+ 1);
							break;
						} else
						{
							if (start.get(Calendar.WEEK_OF_YEAR) == 1
									&& (start.get(Calendar.MONTH) == 11 || start
											.get(Calendar.MONTH) == 0))
							{
								weeks.add("Week"
										+ start.get(Calendar.WEEK_OF_YEAR)
										+ "-" + start.get(Calendar.YEAR));
								weeks.add(start.get(Calendar.DATE) + "/"
										+ (start.get(Calendar.MONTH) + 1) + "/"
										+ start.get(Calendar.YEAR));
								start.roll(Calendar.DAY_OF_WEEK, -1);
								if (start.getTimeInMillis() > fmt.parse(toDate)
										.getTime())
								{
									weeks.add(toDate);
									break;
								} else
								{
									start.add(Calendar.DAY_OF_WEEK,
											7 - start.get(Calendar.DAY_OF_WEEK));
									weeks.add(start.get(Calendar.DATE) + "/"
											+ (start.get(Calendar.MONTH) + 1)
											+ "/" + start.get(Calendar.YEAR));
									start.add(Calendar.DAY_OF_YEAR, 1);
								}
								break;
							}
							weeks.add("Week" + start.get(Calendar.WEEK_OF_YEAR)
									+ "-" + start.get(Calendar.YEAR));
							weeks.add(start.get(Calendar.DATE) + "/"
									+ (start.get(Calendar.MONTH) + 1) + "/"
									+ start.get(Calendar.YEAR));
							start.add(
									Calendar.DAY_OF_YEAR,
									start.getActualMaximum(Calendar.DAY_OF_WEEK)
											- start.get(Calendar.DAY_OF_WEEK));
							weeks.add(start.get(Calendar.DATE) + "/"
									+ (start.get(Calendar.MONTH) + 1) + "/"
									+ start.get(Calendar.YEAR));
							start.add(Calendar.DAY_OF_YEAR, 1);
						}
					}
					if (start.get(Calendar.WEEK_OF_YEAR) == 1
							&& (start.get(Calendar.MONTH) == 11 || start
									.get(Calendar.MONTH) == 0))
					{
						if (start.get(Calendar.WEEK_OF_YEAR) == 1
								&& start.get(Calendar.MONTH) == 11)
						{
							weeks.add("Week" + start.get(Calendar.WEEK_OF_YEAR)
									+ "-" + (start.get(Calendar.YEAR) + 1));
						} else
						{
							weeks.add("Week" + start.get(Calendar.WEEK_OF_YEAR)
									+ "-" + start.get(Calendar.YEAR));
						}

						weeks.add(start.get(Calendar.DATE) + "/"
								+ (start.get(Calendar.MONTH) + 1) + "/"
								+ start.get(Calendar.YEAR));
						start.roll(Calendar.DAY_OF_WEEK, -1);
						if (start.getTimeInMillis() > fmt.parse(toDate)
								.getTime())
						{
							weeks.add(toDate);
							break;
						} else
						{
							weeks.add(start.get(Calendar.DATE) + "/"
									+ (start.get(Calendar.MONTH) + 1) + "/"
									+ start.get(Calendar.YEAR));
						}
					}
					calendar.clear();
					calendar.set(Calendar.YEAR, start.get(Calendar.YEAR));
					calendar.set(Calendar.WEEK_OF_YEAR, 2);
					calendar.set(Calendar.DAY_OF_WEEK, 1);
					currYearLast = calendar.get(Calendar.DATE) + "/"
							+ (calendar.get(Calendar.MONTH) + 1) + "/"
							+ calendar.get(Calendar.YEAR);
					if (fmt.parse(currYearLast).getTime() > fmt.parse(toDate)
							.getTime())
					{
						break;
					}
					start.setTime(fmt.parse(currYearLast));
					end.setTime(fmt.parse(toDate));

				}
			}
		} catch (ParseException e)
		{
			e.printStackTrace();
		}
		return weeks;

	}

	/**
	 * Divides a date range into months
	 * 
	 * @param fromDate
	 * @param toDate
	 * @return months
	 */
	public static ArrayList<String> getMonths(String fromDate, String toDate)
	{

		ArrayList<String> months = new ArrayList<String>();
		try
		{
			boolean finish = false;
			SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
			Calendar start = new GregorianCalendar();
			Calendar end = new GregorianCalendar();
			start.setTime(fmt.parse(fromDate));
			end.setTime(fmt.parse(toDate));
			while (true)
			{
				if (start.get(Calendar.YEAR) == end.get(Calendar.YEAR))
				{
					while (true)
					{
						if (start.get(Calendar.MONTH) == end
								.get(Calendar.MONTH))
						{
							months.add(checkMonth(start.get(Calendar.MONTH))
									+ start.get(Calendar.YEAR));
							months.add(start.get(Calendar.DATE) + "/"
									+ (start.get(Calendar.MONTH) + 1) + "/"
									+ start.get(Calendar.YEAR));
							months.add(end.get(Calendar.DATE) + "/"
									+ (end.get(Calendar.MONTH) + 1) + "/"
									+ end.get(Calendar.YEAR));
							finish = true;
							break;
						} else
						{
							months.add(checkMonth(start.get(Calendar.MONTH))
									+ start.get(Calendar.YEAR));
							months.add(start.get(Calendar.DATE) + "/"
									+ (start.get(Calendar.MONTH) + 1) + "/"
									+ start.get(Calendar.YEAR));
							start.add(
									Calendar.DAY_OF_YEAR,
									start.getActualMaximum(Calendar.DAY_OF_MONTH)
											- start.get(Calendar.DAY_OF_MONTH));
							months.add(start.get(Calendar.DATE) + "/"
									+ (start.get(Calendar.MONTH) + 1) + "/"
									+ start.get(Calendar.YEAR));
							start.add(Calendar.DAY_OF_YEAR, 1);
						}

					}
				} else
				{
					Calendar calendar = Calendar.getInstance();
					calendar.clear();
					calendar.set(Calendar.YEAR, start.get(Calendar.YEAR));
					calendar.roll(Calendar.DAY_OF_YEAR, -1);
					String currYearLast = calendar.get(Calendar.DATE) + "/"
							+ (calendar.get(Calendar.MONTH) + 1) + "/"
							+ calendar.get(Calendar.YEAR);
					end.setTime(fmt.parse(currYearLast));

					while (true)
					{
						if (start.get(Calendar.MONTH) == end
								.get(Calendar.MONTH))
						{
							months.add(checkMonth(start.get(Calendar.MONTH))
									+ start.get(Calendar.YEAR));
							months.add(start.get(Calendar.DATE) + "/"
									+ (start.get(Calendar.MONTH) + 1) + "/"
									+ start.get(Calendar.YEAR));
							months.add(end.get(Calendar.DATE) + "/"
									+ (end.get(Calendar.MONTH) + 1) + "/"
									+ end.get(Calendar.YEAR));
							break;
						} else
						{
							months.add(checkMonth(start.get(Calendar.MONTH))
									+ start.get(Calendar.YEAR));
							months.add(start.get(Calendar.DATE) + "/"
									+ (start.get(Calendar.MONTH) + 1) + "/"
									+ start.get(Calendar.YEAR));
							start.add(
									Calendar.DAY_OF_YEAR,
									start.getActualMaximum(Calendar.DAY_OF_MONTH)
											- start.get(Calendar.DAY_OF_MONTH));
							months.add(start.get(Calendar.DATE) + "/"
									+ (start.get(Calendar.MONTH) + 1) + "/"
									+ start.get(Calendar.YEAR));
							start.add(Calendar.DAY_OF_YEAR, 1);
						}
					}
					calendar.clear();
					calendar.set(Calendar.YEAR, start.get(Calendar.YEAR) + 1);
					calendar.set(Calendar.DAY_OF_YEAR, 1);
					currYearLast = calendar.get(Calendar.DATE) + "/"
							+ (calendar.get(Calendar.MONTH) + 1) + "/"
							+ calendar.get(Calendar.YEAR);

					start.setTime(fmt.parse(currYearLast));
					end.setTime(fmt.parse(toDate));

				}
				if (finish == true)
				{
					break;
				}
			}

		} catch (ParseException e)
		{
			e.printStackTrace();
		}
		return months;

	}

	/**
	 * Divides a date range into seasons
	 * 
	 * @param fromDate
	 * @param toDate
	 * @return seasons
	 */
	public static ArrayList<String> getSeasons(String fromDate, String toDate)
	{

		ArrayList<String> seasons = new ArrayList<String>();
		try
		{
			boolean finish = false;
			SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
			Calendar start = new GregorianCalendar();
			Calendar end = new GregorianCalendar();
			start.setTime(fmt.parse(fromDate));
			end.setTime(fmt.parse(toDate));
			while (true)
			{
				if (start.get(Calendar.YEAR) == end.get(Calendar.YEAR)
						|| ((end.get(Calendar.YEAR) - start.get(Calendar.YEAR) == 1) && start
								.get(Calendar.MONTH) == 11)
						&& (end.get(Calendar.MONTH) == 1 || end
								.get(Calendar.MONTH) == 0))
				{
					while (true)
					{
						if (checkSeason(
								start.get(Calendar.DATE) + "/"
										+ (start.get(Calendar.MONTH) + 1) + "/"
										+ start.get(Calendar.YEAR)).equals(
								checkSeason(end.get(Calendar.DATE) + "/"
										+ (end.get(Calendar.MONTH) + 1) + "/"
										+ end.get(Calendar.YEAR))))
						{
							if (start.get(Calendar.MONTH) == 1
									|| start.get(Calendar.MONTH) == 0)
							{
								seasons.add(checkSeason(start
										.get(Calendar.DATE)
										+ "/"
										+ (start.get(Calendar.MONTH) + 1)
										+ "/"
										+ start.get(Calendar.YEAR))
										+ (start.get(Calendar.YEAR) - 1));
							} else
							{
								seasons.add(checkSeason(start
										.get(Calendar.DATE)
										+ "/"
										+ (start.get(Calendar.MONTH) + 1)
										+ "/"
										+ start.get(Calendar.YEAR))
										+ start.get(Calendar.YEAR));
							}
							seasons.add(start.get(Calendar.DATE) + "/"
									+ (start.get(Calendar.MONTH) + 1) + "/"
									+ start.get(Calendar.YEAR));
							seasons.add(end.get(Calendar.DATE) + "/"
									+ (end.get(Calendar.MONTH) + 1) + "/"
									+ end.get(Calendar.YEAR));
							finish = true;
							break;
						} else
						{
							seasons.add(checkSeason(start.get(Calendar.DATE)
									+ "/" + (start.get(Calendar.MONTH) + 1)
									+ "/" + start.get(Calendar.YEAR))
									+ start.get(Calendar.YEAR));
							seasons.add(start.get(Calendar.DATE) + "/"
									+ (start.get(Calendar.MONTH) + 1) + "/"
									+ start.get(Calendar.YEAR));
							Calendar calendar = start;
							String currentDate = calendar.get(Calendar.DATE)
									+ "/" + (calendar.get(Calendar.MONTH) + 1)
									+ "/" + calendar.get(Calendar.YEAR);
							while (true)
							{
								start.add(
										Calendar.DAY_OF_YEAR,
										start.getActualMaximum(Calendar.DAY_OF_MONTH)
												- start.get(Calendar.DAY_OF_MONTH));
								start.add(Calendar.DAY_OF_YEAR, 1);
								if (checkSeason(currentDate) != checkSeason(start
										.get(Calendar.DATE)
										+ "/"
										+ (start.get(Calendar.MONTH) + 1)
										+ "/"
										+ start.get(Calendar.YEAR)))
								{
									start.roll(Calendar.DAY_OF_YEAR, -1);
									break;
								}

							}
							seasons.add(start.get(Calendar.DATE) + "/"
									+ (start.get(Calendar.MONTH) + 1) + "/"
									+ start.get(Calendar.YEAR));
							start.add(Calendar.DAY_OF_YEAR, 1);
						}
					}
				} else
				{
					Calendar calendar = Calendar.getInstance();
					calendar.clear();
					calendar.set(Calendar.YEAR, start.get(Calendar.YEAR) + 1);
					calendar.set(Calendar.MONTH, 1);
					calendar.roll(Calendar.DAY_OF_MONTH, -1);
					String currYearLast = calendar.get(Calendar.DATE) + "/"
							+ (calendar.get(Calendar.MONTH) + 1) + "/"
							+ calendar.get(Calendar.YEAR);
					if (fmt.parse(toDate).after(fmt.parse(currYearLast)))
					{
						end.setTime(fmt.parse(currYearLast));
					}

					while (true)
					{
						if (checkSeason(
								start.get(Calendar.DATE) + "/"
										+ (start.get(Calendar.MONTH) + 1) + "/"
										+ start.get(Calendar.YEAR)).equals(
								checkSeason(end.get(Calendar.DATE) + "/"
										+ (end.get(Calendar.MONTH) + 1) + "/"
										+ end.get(Calendar.YEAR)))
								&& start.get(Calendar.YEAR) == end
										.get(Calendar.YEAR)
								|| ((end.get(Calendar.YEAR)
										- start.get(Calendar.YEAR) == 1) && start
										.get(Calendar.MONTH) == 11)
								&& (end.get(Calendar.MONTH) == 1 || end
										.get(Calendar.MONTH) == 0))
						{
							if (start.get(Calendar.MONTH) == 1
									|| start.get(Calendar.MONTH) == 0)
							{
								seasons.add(checkSeason(start
										.get(Calendar.DATE)
										+ "/"
										+ (start.get(Calendar.MONTH) + 1)
										+ "/"
										+ start.get(Calendar.YEAR))
										+ (start.get(Calendar.YEAR) - 1));
							} else
							{
								seasons.add(checkSeason(start
										.get(Calendar.DATE)
										+ "/"
										+ (start.get(Calendar.MONTH) + 1)
										+ "/"
										+ start.get(Calendar.YEAR))
										+ start.get(Calendar.YEAR));
							}
							seasons.add(start.get(Calendar.DATE) + "/"
									+ (start.get(Calendar.MONTH) + 1) + "/"
									+ start.get(Calendar.YEAR));
							seasons.add(end.get(Calendar.DATE) + "/"
									+ (end.get(Calendar.MONTH) + 1) + "/"
									+ end.get(Calendar.YEAR));
							break;
						} else
						{

							if (start.get(Calendar.MONTH) == 1
									|| start.get(Calendar.MONTH) == 0)
							{
								seasons.add(checkSeason(start
										.get(Calendar.DATE)
										+ "/"
										+ (start.get(Calendar.MONTH) + 1)
										+ "/"
										+ start.get(Calendar.YEAR))
										+ (start.get(Calendar.YEAR) - 1));
							} else
							{
								seasons.add(checkSeason(start
										.get(Calendar.DATE)
										+ "/"
										+ (start.get(Calendar.MONTH) + 1)
										+ "/"
										+ start.get(Calendar.YEAR))
										+ start.get(Calendar.YEAR));
							}
							seasons.add(start.get(Calendar.DATE) + "/"
									+ (start.get(Calendar.MONTH) + 1) + "/"
									+ start.get(Calendar.YEAR));
							Calendar tmp = start;
							String currentDate = tmp.get(Calendar.DATE) + "/"
									+ (tmp.get(Calendar.MONTH) + 1) + "/"
									+ tmp.get(Calendar.YEAR);
							while (true)
							{
								start.add(
										Calendar.DAY_OF_YEAR,
										start.getActualMaximum(Calendar.DAY_OF_MONTH)
												- start.get(Calendar.DAY_OF_MONTH));
								start.add(Calendar.DAY_OF_YEAR, 1);
								if (checkSeason(currentDate) != checkSeason(start
										.get(Calendar.DATE)
										+ "/"
										+ (start.get(Calendar.MONTH) + 1)
										+ "/"
										+ start.get(Calendar.YEAR)))
								{
									start.roll(Calendar.DAY_OF_YEAR, -1);
									break;
								}
							}
							seasons.add(start.get(Calendar.DATE) + "/"
									+ (start.get(Calendar.MONTH) + 1) + "/"
									+ start.get(Calendar.YEAR));
							start.add(Calendar.DAY_OF_YEAR, 1);
						}
					}
					calendar.clear();
					calendar.set(Calendar.YEAR, start.get(Calendar.YEAR) + 1);
					calendar.set(Calendar.MONTH, 2);
					currYearLast = calendar.get(Calendar.DATE) + "/"
							+ (calendar.get(Calendar.MONTH) + 1) + "/"
							+ calendar.get(Calendar.YEAR);
					if (fmt.parse(toDate).before(fmt.parse(currYearLast)))
					{
						break;
					} else
					{
						start.setTime(fmt.parse(currYearLast));
						end.setTime(fmt.parse(toDate));
					}

				}
				if (finish == true)
				{
					break;
				}
			}

		} catch (ParseException e)
		{
			e.printStackTrace();
		}
		return seasons;

	}

	/**
	 * Gets the season name of a certain date
	 * 
	 * @param date
	 * @return the season name of a certain date
	 */
	public static String checkSeason(String date)
	{

		try
		{
			SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(fmt.parse(date));
			switch (calendar.get(Calendar.MONTH))
			{
			case 0:
				return "Summer";
			case 1:
				return "Summer";
			case 2:
				return "Autumn";
			case 3:
				return "Autumn";
			case 4:
				return "Autumn";
			case 5:
				return "Winter";
			case 6:
				return "Winter";
			case 7:
				return "Winter";
			case 8:
				return "Spring";
			case 9:
				return "Spring";
			case 10:
				return "Spring";
			case 11:
				return "Summer";
			}
		} catch (ParseException e)
		{
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * Returns the name of a certain month
	 * 
	 * @param month
	 * @return the name of a certain month
	 */
	public static String checkMonth(int month)
	{

		switch (month)
		{
		case 0:
			return "Jan";
		case 1:
			return "Feb";
		case 2:
			return "Mar";
		case 3:
			return "Apr";
		case 4:
			return "May";
		case 5:
			return "Jun";
		case 6:
			return "Jul";
		case 7:
			return "Aug";
		case 8:
			return "Sep";
		case 9:
			return "Oct";
		case 10:
			return "Nov";
		case 11:
			return "Dec";

		}

		return null;

	}

	/**
	 * Returns the name of a certain month
	 * 
	 * @param month
	 *            (from 1-12)
	 * @return the name of a certain month
	 */
	public static String formateMonth(int month)
	{

		switch (month)
		{
		case 1:
			return "Jan";
		case 2:
			return "Feb";
		case 3:
			return "Mar";
		case 4:
			return "Apr";
		case 5:
			return "May";
		case 6:
			return "Jun";
		case 7:
			return "Jul";
		case 8:
			return "Aug";
		case 9:
			return "Sep";
		case 10:
			return "Oct";
		case 11:
			return "Nov";
		case 12:
			return "Dec";

		}

		return null;

	}
}
