package springMVC;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import DataMining.Accumulator;
import DataMining.DataMiningLogic;

import reportDataObjects.SimpleUserTrends;

import dbAccess.Dao;
import debug.ClassDebug;
import forms.SimpleUserTrendsForm;

@SuppressWarnings("deprecation")
public class SimpleUserTrendsFormController extends SimpleFormController
{

	Dao dao;

	public void setDao(Dao dao)
	{
		this.dao = dao;
	}

	@Override
	protected HashMap<String, Object> referenceData(HttpServletRequest request)
			throws Exception
	{
		ClassDebug.print("SimpleUserTrendFormController.referenceData()...");
		HashMap<String, Object> model = new HashMap<String, Object>();
		if (request.getSession().getAttribute("username") == null)
		{
			model.put("pageName", "login");
			model.put("value", "AdminLogin");
		} else
		{
			model.put("pageName", "logout");
			model.put("value", "Logout");
			model.put("common", getCommon());
		}
		if (request.getRequestURI().endsWith("simpleusertrends"))
		{
			List<String> dimensions = dao.getDimensions();
			model.put("dimensions", dimensions);
			ArrayList<String> mode = new ArrayList<String>();
			mode.add("Depreciation");
			mode.add("Derivative");
			model.put("mode", mode);
		}
		return model;
	}

	@Override
	protected Object formBackingObject(HttpServletRequest request)
			throws Exception
	{
		ClassDebug
				.print("SimpleUserTrendFormController.formBackingObject()...");
		if (request.getRequestURI().endsWith("simpleusertrends"))
		{
			setCommandClass(SimpleUserTrendsForm.class);
			setCommandName("SimpleUserTrendsForm");
			SimpleUserTrendsForm sutf = new SimpleUserTrendsForm();

			if (request.getParameter("Reset") == null)
			{
				@SuppressWarnings("unchecked")
				HashMap<String, Object> HashMap_SimpleUserTrendsForm = (HashMap<String, Object>) request
						.getSession().getAttribute("SimpleUserTrendsForm");
				if (HashMap_SimpleUserTrendsForm != null)
				{
					// sutf.setFromDate((String)
					// HashMap_SimpleUserTrendsForm.get("fromDate"));
					// sutf.setToDate((String)
					// HashMap_SimpleUserTrendsForm.get("toDate"));
					sutf.setDimension((String) HashMap_SimpleUserTrendsForm
							.get("dimension"));
					sutf.setMode((String) HashMap_SimpleUserTrendsForm
							.get("mode"));
					sutf.setThreshold((Integer) HashMap_SimpleUserTrendsForm
							.get("threshold"));
					if (request.getSession().getAttribute("username") != null)
					{
						sutf.setA((Double) HashMap_SimpleUserTrendsForm
								.get("a"));
						sutf.setDepreciationInterval((Integer) HashMap_SimpleUserTrendsForm
								.get("depreciationInterval"));
						sutf.setDepreciationRate((Double) HashMap_SimpleUserTrendsForm
								.get("depreciationRate"));
					}
				} else
				{
					// sutf.setFromDate(FormFormater.getPreviousMonth());
					// sutf.setToDate(FormFormater.getCurrentDate());
				}
			} else
			{
				@SuppressWarnings("unchecked")
				HashMap<String, Object> HashMap_requestbyhourForm = (HashMap<String, Object>) request
						.getSession().getAttribute("requestbyhourForm");
				if (HashMap_requestbyhourForm != null)
					request.getSession()
							.setAttribute("requestbyhourForm", null);
				// sutf.setDimension("Brand");
			}
			this.setFormView("InputView\\simpleusertrendsInputView");
			return sutf;
		}
		return null;
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception
	{
		ClassDebug.print("SimpleUserTrendsFormController.onSubmit()...");
		if (getCommandName().equals("SimpleUserTrendsForm"))
		{
			SimpleUserTrendsForm form = (SimpleUserTrendsForm) command;
			getValidator().validate(form, errors);

			long startTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Start the query at "
					+ ClassDebug.getCurrentDateAndTime());

			HashMap<String, Object> HashMap_SimpleUserTrendsForm = new HashMap<String, Object>();
			HashMap_SimpleUserTrendsForm.put("dimension", form.getDimension());
			HashMap_SimpleUserTrendsForm.put("mode", form.getMode());
			HashMap_SimpleUserTrendsForm.put("depreciationRate",
					form.getDepreciationRate());
			HashMap_SimpleUserTrendsForm.put("depreciationInterval",
					form.getDepreciationInterval());
			HashMap_SimpleUserTrendsForm.put("a", form.getA());
			HashMap_SimpleUserTrendsForm.put("threshold", form.getThreshold());
			request.getSession().setAttribute("SimpleUserTrendsForm",
					HashMap_SimpleUserTrendsForm);

			/*
			 * Call the getNormalizedDatas function in dao to get lists of
			 * SimpleUserTrends which storing normalized number of requests for
			 * every value in the specified dimension.
			 */
			HashMap<String, List<SimpleUserTrends>> normalizedSampleData = dao
					.getNormalizedDatas(form.getDimension());
			HashMap<String, Object> model = new HashMap<String, Object>();
			// create a new DataMiningLogic object and set the threshold (values
			// with an average number of
			// request per day less than the threshold will be ingnore before
			// ranking)
			DataMiningLogic datamining = new DataMiningLogic(
					form.getThreshold());
			System.out.println("threshold: " + form.getThreshold());

			// If user is not an Admin, he can't set any of the
			// depreciateInterval, the depreciationRate or the a.
			// Then use the preset values as follow
			if (form.getA() != 0)
			{
				System.out.println("parameter A set");
				datamining.setA(form.getA());
			} else
			{
				datamining.setA(1.01);
			}
			if (form.getDepreciationInterval() != 0)
			{
				System.out.println("parameter depreciateInterval set");
				datamining
						.setDepreciateInterval(form.getDepreciationInterval());
			} else
			{
				datamining.setDepreciateInterval(7);
			}
			if (form.getDepreciationRate() != 0)
			{
				System.out.println("parameter depreciationRate set");
				datamining.setDepreciationRate(form.getDepreciationRate());
			} else
			{
				datamining.setDepreciationRate(0.5);
			}

			// datamining.printTrends(normalizedSampleData,1);
			ArrayList<Accumulator> topResults = datamining.getSortedHeap(
					normalizedSampleData, form.getMode());
			model.put("topResults", topResults);
			System.out.println("topResults size " + topResults.size());
			model.put("reportName", "User Trends of " + form.getDimension()
					+ "Dimension");
			//
			if (request.getSession().getAttribute("username") == null)
			{
				model.put("pageName", "login");
				model.put("value", "AdminLogin");
			} else
			{
				model.put("pageName", "logout");
				model.put("value", "Logout");
				model.put("common", getCommon());
			}

			long endTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Complete the Query at "
					+ ClassDebug.getCurrentDateAndTime());
			ClassDebug.print("Time Cost: "
					+ ClassDebug.timeFormat(endTime - startTime));

			/*
			 * // start to generate reports (pdf,xls,csv)
			 * Report.createReports(filePath, model, "RequestByHourReport");
			 * model.put("savedReportName", "RequestByHourReport");
			 */
			return new ModelAndView("OutputView\\simpleusertrendsOutputView",
					model);
		}
		return null;
	}

	// customized methods
	public HashMap<String, Object> getCommon()
	{
		HashMap<String, Object> common = new HashMap<String, Object>();
		common.put("title", "Admin Links");
		common.put("addPageName", "addlogfile");
		common.put("addValue", "Add Log Files");
		common.put("removePageName", "removelogfile");
		common.put("removeValue", "Remove Log Files");

		return common;
	}
}
