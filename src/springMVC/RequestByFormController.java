package springMVC;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import others.TimeEstimation;

import charts.RequestByBarChart;

import dbAccess.Dao;
import debug.ClassDebug;

import forms.RequestByForm;

import reportDataObjects.RequestBy;

import reports.RequestByReport;

@SuppressWarnings("deprecation")
public class RequestByFormController extends SimpleFormController
{

	Dao dao;

	public void setDao(Dao dao)
	{
		this.dao = dao;
	}

	// Spring SimpleFormController workflow methods

	@Override
	protected HashMap<String, Object> referenceData(HttpServletRequest request)
			throws Exception
	{
		ClassDebug.print("RequestbyFormController.referenceData()...");
		HashMap<String, Object> model = new HashMap<String, Object>();
		if (request.getSession().getAttribute("username") == null)
		{
			model.put("pageName", "login");
			model.put("value", "AdminLogin");
		} else
		{
			model.put("pageName", "logout");
			model.put("value", "Logout");
			model.put("common", getCommon());
		}

		if (request.getRequestURI().endsWith("requestby"))
		{
			ArrayList<String> timeUnit = new ArrayList<String>();
			timeUnit.add("Hour of Day");
			timeUnit.add("Day of Month");
			timeUnit.add("Month of Year");
			timeUnit.add("None");
			model.put("timeUnit", timeUnit);
		}
		return model;
	}

	@Override
	protected Object formBackingObject(HttpServletRequest request)
			throws Exception
	{
		ClassDebug.print("RequestbyhourFormController.formBackingObject()...");
		if (request.getRequestURI().endsWith("requestby"))
		{
			setCommandClass(RequestByForm.class);
			setCommandName("requestbyForm");
			RequestByForm rbf = new RequestByForm();

			if (request.getParameter("Reset") == null)
			{
				@SuppressWarnings("unchecked")
				HashMap<String, Object> HashMap_RequestByForm = (HashMap<String, Object>) request
						.getSession().getAttribute("requestByForm");
				if (HashMap_RequestByForm != null)
				{
					rbf.setCompareFromDate((String) HashMap_RequestByForm
							.get("compareFromDate"));
					rbf.setCompareToDate((String) HashMap_RequestByForm
							.get("compareToDate"));
					rbf.setTimeUnit((String) HashMap_RequestByForm
							.get("timeUnit"));
					rbf.setIsCompared(new Boolean(
							(Boolean) HashMap_RequestByForm.get("isCompared"))
							.booleanValue());
				} else
				{
					rbf.setFromDate(FormFormater.getPreviousMonth());
					rbf.setToDate(FormFormater.getCurrentDate());
					rbf.setTimeUnit("hour");
					rbf.setIsCompared(false);
				}

				if (request.getSession().getAttribute("fromDate") != null
						&& request.getSession().getAttribute("toDate") != null)
				{
					rbf.setFromDate((String) request.getSession().getAttribute(
							"fromDate"));
					rbf.setToDate((String) request.getSession().getAttribute(
							"toDate"));
				}
			} else
			{
				@SuppressWarnings("unchecked")
				HashMap<String, Object> HashMap_RequestByForm = (HashMap<String, Object>) request
						.getSession().getAttribute("requestByForm");
				if (HashMap_RequestByForm != null)
					request.getSession().setAttribute("requestByForm", null);

				request.getSession().removeAttribute("fromDate");
				request.getSession().removeAttribute("toDate");
				rbf.setFromDate(FormFormater.getPreviousMonth());
				rbf.setToDate(FormFormater.getCurrentDate());
				rbf.setTimeUnit("hour");
				rbf.setIsCompared(false);
			}

			this.setFormView("InputView\\requestbyInputView");
			return rbf;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception
	{
		ClassDebug.print("RequestbyFormController.onSubmit()...");
		if (getCommandName().equals("requestbyForm"))
		{
			RequestByForm form = (RequestByForm) command;
			getValidator().validate(form, errors);

			/*
			 * TimeEstimation tmpTimeEstimation = new
			 * TimeEstimation(form.getFromDate(), form.getToDate(), dao,
			 * "RequestBy"); double tmpTimeEstimated =
			 * tmpTimeEstimation.start(); ClassDebug.print(tmpTimeEstimated ==
			 * -1 ? "No enough history data and unable to make an estimation!" :
			 * "Approximate time cost: " +
			 * ClassDebug.timeFormat((int)tmpTimeEstimated));
			 */

			long startTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Start the query at "
					+ ClassDebug.getCurrentDateAndTime());

			// *************************
			HashMap<String, Object> HashMap_RequestByForm = new HashMap<String, Object>();
			HashMap_RequestByForm.put("fromDate", form.getFromDate());
			HashMap_RequestByForm.put("toDate", form.getToDate());
			HashMap_RequestByForm.put("compareFromDate",
					form.getCompareFromDate());
			HashMap_RequestByForm.put("compareToDate", form.getCompareToDate());
			HashMap_RequestByForm.put("timeUnit", form.getTimeUnit());
			HashMap_RequestByForm.put("isCompared", form.getIsCompared());
			request.getSession().setAttribute("requestByForm",
					HashMap_RequestByForm);
			request.getSession().setAttribute("fromDate", form.getFromDate());
			request.getSession().setAttribute("toDate", form.getToDate());

			// ************************

			HashMap<String, Object> model = new HashMap<String, Object>();
			if (form.getIsCompared() == false)
			{
				ClassDebug.print("form.getIsCompared is false");
				form.setCompareFromDate("");
				form.setCompareToDate("");
			}
			if (form.getIsCompared() == true)
			{
				ClassDebug.print("form.getIsCompared is true");
			}
			// if "None" in the form is selected, it means that results are
			// group by date
			if (form.getTimeUnit().compareTo("None") == 0)
			{
				form.setTimeUnit("Date");
			}
			HashMap<String, Object> requestBy = getRequestBy(
					form.getFromDate(), form.getToDate(),
					form.getCompareFromDate(), form.getCompareToDate(),
					form.getTimeUnit());
			model.put("date", requestBy.get("date"));
			requestBy.remove("date");
			//
			JFreeChart chart = RequestByBarChart.createBarChartForRequestBy(
					requestBy, form.getTimeUnit());
			String filePath = this.getServletContext().getRealPath("/");
			ChartUtilities.saveChartAsJPEG(new File(filePath
					+ "pages/temp/barchart.jpg"), chart, 726, 400);
			//
			if (requestBy.size() == 2)
			{
				int totalRequests = 0;
				int totalRequests1 = 0;
				ArrayList<RequestBy> rb = (ArrayList<RequestBy>) requestBy
						.get("requestByList");
				ArrayList<RequestBy> rb1 = (ArrayList<RequestBy>) requestBy
						.get("requestByList1");
				for (int i = 0; i < rb.size(); i++)
				{
					totalRequests += rb.get(i).getRequests();
				}
				for (int i = 0; i < rb1.size(); i++)
				{
					totalRequests1 += rb1.get(i).getRequests();
				}
				model.put("requestByList", rb);
				model.put("requestByList1", rb1);
				model.put("totalRequests", totalRequests);
				model.put("totalRequests1", totalRequests1);
				model.put("th1", "1st " + form.getTimeUnit());
				model.put("th2", "1st Number of Requests");
				model.put("th3", "2nd " + form.getTimeUnit());
				model.put("th4", "2nd Number of Requests");
				model.put("reportName", "Request By " + form.getTimeUnit());
				model.put("img", "pages/temp/barchart.jpg");
			} else
			{
				int totalRequests = 0;
				ArrayList<RequestBy> rb = (ArrayList<RequestBy>) requestBy
						.get("requestByList");
				for (int i = 0; i < rb.size(); i++)
				{
					totalRequests += rb.get(i).getRequests();
				}
				model.put("requestByList", rb);
				model.put("totalRequests", totalRequests);
				model.put("th1", form.getTimeUnit());
				model.put("th2", "Number of Requests");
				model.put("reportName", "Request By " + form.getTimeUnit());
				model.put("img", "pages/temp/barchart.jpg");
			}

			if (request.getSession().getAttribute("username") == null)
			{
				model.put("pageName", "login");
				model.put("value", "AdminLogin");
			} else
			{
				model.put("pageName", "logout");
				model.put("value", "Logout");
				model.put("common", getCommon());
			}

			// start to generate reports (pdf,xls,csv)
			RequestByReport.createReports(filePath, model, "RequestByReport");
			model.put("savedReportName", "RequestByReport");

			// Check if the report was created to avoid problems in the View
			model.put(
					"pdfReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".pdf").exists());
			model.put(
					"csvReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".csv").exists());
			model.put(
					"xlsReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".xls").exists());

			long endTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Complete the Query at "
					+ ClassDebug.getCurrentDateAndTime());
			ClassDebug.print("Time Cost: "
					+ ClassDebug.timeFormat(endTime - startTime));

			/*
			 * tmpTimeEstimation.end(endTime - startTime);
			 */

			return new ModelAndView("OutputView\\requestbyOutputView", model);
		}
		return null;
	}

	// customized methods
	public HashMap<String, Object> getCommon()
	{
		HashMap<String, Object> common = new HashMap<String, Object>();
		common.put("title", "Admin Links");
		common.put("addPageName", "addlogfile");
		common.put("addValue", "Add Log Files");
		common.put("removePageName", "removelogfile");
		common.put("removeValue", "Remove Log Files");

		return common;
	}

	//
	public HashMap<String, Object> getRequestBy(String fromDate, String toDate,
			String compareFromDate, String compareToDate, String timeUnit)
	{
		ClassDebug.print("RequestByFormController.getRequestBy()...");
		String lastDate = dao.getLastDate();
		String firstDate = dao.getFirstDate();
		if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), firstDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
			toDate = FormFormater.formatDateTwo(lastDate);
		} else if ((!FormFormater.compareDates(
				FormFormater.formatDate(fromDate), firstDate))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(FormFormater.formatDate(toDate),
						lastDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
		} else if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(fromDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), lastDate)))
		{
			toDate = FormFormater.formatDateTwo(lastDate);
		}
		HashMap<String, Object> requestBy = new HashMap<String, Object>();
		String date = fromDate + " - " + toDate;
		if ((!compareFromDate.equals("")) && (!compareToDate.equals("")))
		{
			if (FormFormater.compareDates(
					FormFormater.formatDate(compareToDate), dao.getLastDate()))
			{
				compareToDate = FormFormater.formatDateTwo(dao.getLastDate());
			}
			date += "  Compared With  " + compareFromDate + " - "
					+ compareToDate;
			List<RequestBy> requestByList1 = dao.getRequestBy(
					FormFormater.formatDate(compareFromDate),
					FormFormater.formatDate(compareToDate), timeUnit);
			requestBy.put("requestByList1", requestByList1);
		}
		List<RequestBy> requestByList = dao.getRequestBy(
				FormFormater.formatDate(fromDate),
				FormFormater.formatDate(toDate), timeUnit);
		requestBy.put("requestByList", requestByList);
		requestBy.put("date", date);
		return requestBy;
	}
}
