package springMVC;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import reports.KeywordSearchReport;

import reportDataObjects.*;

import dbAccess.Dao;
import debug.ClassDebug;
import forms.KeywordSearchForm;

@SuppressWarnings("deprecation")
public class KeywordSearchFormController extends SimpleFormController
{
	protected Dao dao;

	public void setDao(Dao dao)
	{
		this.dao = dao;
	}

	protected static ArrayList<String> ArrayListTableNames = new ArrayList<String>();

	protected static synchronized void AddToTableNames(String tmpTableName)
	{
		if (Contain(tmpTableName))
			return;
		ArrayListTableNames.add(tmpTableName);
	}

	protected static synchronized boolean Contain(String tmpTableName)
	{
		for (int i = 0; i < ArrayListTableNames.size(); i++)
		{
			String tmpS = ArrayListTableNames.get(i);
			if (tmpS.equals(tmpTableName))
				return true;
		}
		return false;
	}

	protected static synchronized void RemoveFromTableNames(String tmpTableName)
	{
		if (Contain(tmpTableName))
		{
			ArrayListTableNames.remove(tmpTableName);
		}
	}

	protected static synchronized int GetSize()
	{
		return ArrayListTableNames.size();
	}

	protected static synchronized int GetIndex(String tmpTableName)
	{
		for (int i = 0; i < ArrayListTableNames.size(); i++)
		{
			String tmpS = ArrayListTableNames.get(i);
			if (tmpS.equals(tmpTableName))
				return i;
		}
		return -1;
	}

	protected static synchronized String GetString(int index)
	{
		return ArrayListTableNames.get(index);
	}

	@Override
	protected HashMap<String, Object> referenceData(HttpServletRequest request)
			throws Exception
	{
		ClassDebug.print("KeywordSearchFormController.referencesData()...");
		HashMap<String, Object> model = new HashMap<String, Object>();
		if (request.getSession().getAttribute("username") == null)
		{
			model.put("pageName", "login");
			model.put("value", "AdminLogin");
		} else
		{
			model.put("pageName", "logout");
			model.put("value", "Logout");
			HashMap<String, Object> common = new HashMap<String, Object>();
			common.put("title", "Admin Links");
			common.put("addPageName", "addlogfile");
			common.put("addValue", "Add Log Files");
			common.put("removePageName", "removelogfile");
			common.put("removeValue", "Remove Log Files");
			model.put("common", common);
		}

		if (request.getRequestURI().endsWith("keywordsearch"))
		{
			ArrayList<String> ListNumberOfRecords = new ArrayList<String>();
			int tmpValue = 100;
			for (int i = 1; i <= 10; i++)
			{
				ListNumberOfRecords.add(Integer.toString(tmpValue));
				tmpValue *= 2;
			}
			model.put("ListNumberOfRecords", ListNumberOfRecords);

			ArrayList<String> ListValue = new ArrayList<String>();
			tmpValue = 10;
			for (int i = 1; i <= 10; i++)
			{
				ListValue.add(Integer.toString(tmpValue));
				tmpValue += 5;
			}
			model.put("ListValue", ListValue);

			ArrayList<String> ListLengthOfUnit = new ArrayList<String>();
			tmpValue = 50;
			for (int i = 1; i <= 10; i++)
			{
				ListLengthOfUnit.add(Integer.toString(tmpValue));
				tmpValue += 50;
			}
			model.put("ListLengthOfUnit", ListLengthOfUnit);

		}

		return model;
	}

	protected Object formBackingObject(HttpServletRequest request)
			throws Exception
	{
		ClassDebug.print("KeywordSearchFormController.formBackingObject()...");
		setCommandClass(KeywordSearchForm.class);
		setCommandName("KeywordSearchForm");
		KeywordSearchForm tmpForm = new KeywordSearchForm();

		if (request.getParameter("Reset") == null)
		{

			if (request.getSession().getAttribute("fromDate") != null
					&& request.getSession().getAttribute("toDate") != null)
			{
				tmpForm.setFromDate((String) request.getSession().getAttribute(
						"fromDate"));
				tmpForm.setToDate((String) request.getSession().getAttribute(
						"toDate"));
			} else
			{
				tmpForm.setFromDate(FormFormater.getPreviousMonth());
				tmpForm.setToDate(FormFormater.getCurrentDate());

			}
			tmpForm.setUnitChangeType("KeepConstant");
		} else
		{
			request.getSession().removeAttribute("fromDate");
			request.getSession().removeAttribute("toDate");
			tmpForm.setFromDate(FormFormater.getPreviousMonth());
			tmpForm.setToDate(FormFormater.getCurrentDate());

		}

		this.setFormView("InputView\\keywordSearchInputView");
		return tmpForm;
	}

	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception
	{
		ClassDebug.print("KeywordSearchFormController.onSubmit()...");
		if (request.getParameter("llimit") != null
				&& request.getParameter("hlimit") != null
				&& request.getParameter("detail") != null)
		{
			ClassDebug.print("KeywordSearchFormController.onSubmit() Limit...");
			int llimit = Integer.parseInt(request.getParameter("llimit")
					.toString());
			int hlimit = Integer.parseInt(request.getParameter("hlimit")
					.toString());

			HashMap<String, Object> model = new HashMap<String, Object>();

			KeywordSearchForm form = (KeywordSearchForm) command;
			getValidator().validate(form, errors);

			String lastDate = dao.getLastDate();
			String firstDate = dao.getFirstDate();
			String fromDate = form.getFromDate();
			String toDate = form.getToDate();

			if ((!FormFormater.compareDates(lastDate,
					FormFormater.formatDate(toDate)))
					&& (!FormFormater.compareDates(
							FormFormater.formatDate(fromDate), firstDate)))
			{
				fromDate = FormFormater.formatDateTwo(firstDate);
				toDate = FormFormater.formatDateTwo(lastDate);
			} else if ((!FormFormater.compareDates(
					FormFormater.formatDate(form.getFromDate()), firstDate))
					&& (!FormFormater.compareDates(firstDate,
							FormFormater.formatDate(toDate)))
					&& (!FormFormater.compareDates(
							FormFormater.formatDate(toDate), lastDate)))
			{
				fromDate = FormFormater.formatDateTwo(firstDate);
			} else if ((!FormFormater.compareDates(lastDate,
					FormFormater.formatDate(toDate)))
					&& (!FormFormater.compareDates(firstDate,
							FormFormater.formatDate(fromDate)))
					&& (!FormFormater.compareDates(
							FormFormater.formatDate(fromDate), lastDate)))
			{
				toDate = FormFormater.formatDateTwo(lastDate);
			}

			fromDate = FormFormater.formatDate(fromDate);
			toDate = FormFormater.formatDate(toDate);

			long startTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Start the query at "
					+ ClassDebug.getCurrentDateAndTime());

			List<KeywordSearch> keywordSearchList = dao.getKeywordSearchResult(
					fromDate, toDate, llimit, hlimit, false);

			long endTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Complete the Query at "
					+ ClassDebug.getCurrentDateAndTime());
			ClassDebug.print("Time Cost: "
					+ ClassDebug.timeFormat(endTime - startTime));

			model.put("keywordSearchList", keywordSearchList);
			model.put("keywordSearchListSize", keywordSearchList.size());
			model.put("size", keywordSearchList.size());
			model.put("date", form.getFromDate() + " - " + form.getToDate());
			if (llimit == 0)
			{
				model.put("reportName", "Report: Keyword Search Details");
				model.put("reportName1", "No Records Returned");
			} else if (llimit == hlimit)
			{
				model.put("reportName", "Report: Keyword Search Details");
				model.put("reportName1", "Records Returned " + llimit + "+");
			} else
			{
				model.put("reportName", "Report: Keyword Search Details");
				model.put("reportName1", "Records Returned [" + llimit + "-"
						+ hlimit + "]");
			}
			model.put("fromDate", fromDate);
			model.put("toDate", toDate);
			if (request.getSession().getAttribute("username") == null)
			{
				model.put("pageName", "login");
				model.put("value", "AdminLogin");
			} else
			{
				model.put("pageName", "logout");
				model.put("value", "Logout");
				HashMap<String, Object> common = new HashMap<String, Object>();
				common.put("title", "Admin Links");
				common.put("addPageName", "addlogfile");
				common.put("addValue", "Add Log Files");
				common.put("removePageName", "removelogfile");
				common.put("removeValue", "Remove Log Files");
				model.put("common", common);
			}

			// start to generate reports (pdf,xls,csv)
			String filePath = this.getServletContext().getRealPath("/");

			if (keywordSearchList.size() > 0)
			{
				KeywordSearchReport.createReports(filePath, model,
						"KeywordSearchReport_" + llimit + "_" + hlimit);
				model.put("savedReportName", "KeywordSearchReport_" + llimit
						+ "_" + hlimit);

				// Check if the report was created to avoid problems in the View
				model.put("pdfReport", new File(filePath + "pages/reports/"
						+ model.get("savedReportName") + ".pdf").exists());
				model.put("csvReport", new File(filePath + "pages/reports/"
						+ model.get("savedReportName") + ".csv").exists());
				model.put("xlsReport", new File(filePath + "pages/reports/"
						+ model.get("savedReportName") + ".xls").exists());
			}
			return new ModelAndView("OutputView\\keywordSearchOutputView",
					model);
		} else
		{
			KeywordSearchForm form = (KeywordSearchForm) command;
			getValidator().validate(form, errors);

			String lastDate = dao.getLastDate();
			String firstDate = dao.getFirstDate();
			String fromDate = form.getFromDate();
			String toDate = form.getToDate();

			HashMap<String, Object> HashMap_KeywordSearchForm = new HashMap<String, Object>();
			HashMap_KeywordSearchForm.put("fromDate", fromDate);
			HashMap_KeywordSearchForm.put("toDate", toDate);
			request.getSession().setAttribute("fromDate", fromDate);
			request.getSession().setAttribute("toDate", toDate);

			HashMap<String, Object> model = new HashMap<String, Object>();

			model.put("fromDate", fromDate);
			model.put("toDate", toDate);

			if ((!FormFormater.compareDates(lastDate,
					FormFormater.formatDate(toDate)))
					&& (!FormFormater.compareDates(
							FormFormater.formatDate(fromDate), firstDate)))
			{
				fromDate = FormFormater.formatDateTwo(firstDate);
				toDate = FormFormater.formatDateTwo(lastDate);
			} else if ((!FormFormater.compareDates(
					FormFormater.formatDate(form.getFromDate()), firstDate))
					&& (!FormFormater.compareDates(firstDate,
							FormFormater.formatDate(toDate)))
					&& (!FormFormater.compareDates(
							FormFormater.formatDate(toDate), lastDate)))
			{
				fromDate = FormFormater.formatDateTwo(firstDate);
			} else if ((!FormFormater.compareDates(lastDate,
					FormFormater.formatDate(toDate)))
					&& (!FormFormater.compareDates(firstDate,
							FormFormater.formatDate(fromDate)))
					&& (!FormFormater.compareDates(
							FormFormater.formatDate(fromDate), lastDate)))
			{
				toDate = FormFormater.formatDateTwo(lastDate);
			}

			fromDate = FormFormater.formatDate(fromDate);
			toDate = FormFormater.formatDate(toDate);

			long startTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Start the query at "
					+ ClassDebug.getCurrentDateAndTime());

			List<KeywordSearchSummary> keywordSearchArrayList = dao
					.getKeywordSearchResultSummary(fromDate, toDate);

			List<KeywordSearch> keywordSearchList = dao.getKeywordSearchResult(
					fromDate, toDate, 0, 0, true);

			long endTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Complete the Query at "
					+ ClassDebug.getCurrentDateAndTime());
			ClassDebug.print("Time Cost: "
					+ ClassDebug.timeFormat(endTime - startTime));

			model.put("keywordSearchArrayListIndex", keywordSearchArrayList);
			model.put("keywordSearchList", keywordSearchList);
			model.put("keywordSearchListSize", keywordSearchList.size());
			model.put("size", keywordSearchArrayList.size());
			model.put("date", form.getFromDate() + " - " + form.getToDate());
			model.put("reportName", "Report: Keyword Search Summary");

			if (request.getSession().getAttribute("username") == null)
			{
				model.put("pageName", "login");
				model.put("value", "AdminLogin");
			} else
			{
				model.put("pageName", "logout");
				model.put("value", "Logout");
				HashMap<String, Object> common = new HashMap<String, Object>();
				common.put("title", "Admin Links");
				common.put("addPageName", "addlogfile");
				common.put("addValue", "Add Log Files");
				common.put("removePageName", "removelogfile");
				common.put("removeValue", "Remove Log Files");
				model.put("common", common);
			}
			// start to generate reports (pdf,xls,csv)
			String filePath = this.getServletContext().getRealPath("/");
			KeywordSearchReport.createReportsSummary(filePath, model,
					"KeywordSearchReportSummary");
			model.put("savedReportName", "KeywordSearchReportSummary");

			// Check if the report was created to avoid problems in the View
			model.put(
					"pdfReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".pdf").exists());
			model.put(
					"csvReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".csv").exists());
			model.put(
					"xlsReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".xls").exists());

			return new ModelAndView(
					"OutputView\\keywordSearchSummaryOutputView", model);
		}
	}
}
