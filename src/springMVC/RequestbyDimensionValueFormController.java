package springMVC;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import others.FileZipper;

import charts.RequestByBarChart;

import dbAccess.Dao;
import debug.ClassDebug;

import forms.RequestByDimensionValueForm;
import reportDataObjects.RequestBy;
import reports.Report;
import reports.RequestByReport;

/**
 * This form controller takes charge of the "Request By Dimension Value"
 * function.
 * 
 * @author yoursoft
 * 
 */
@SuppressWarnings("deprecation")
public class RequestbyDimensionValueFormController extends SimpleFormController
{

	Dao dao;

	public void setDao(Dao dao)
	{
		this.dao = dao;
	}

	// Spring SimpleFormController workflow methods
	/**
	 * @see referenceData() in SimpleFormController Spring Framework
	 * 
	 */
	@Override
	protected HashMap<String, Object> referenceData(HttpServletRequest request)
			throws Exception
	{
		ClassDebug
				.print("RequestbyDimensionValueFormController.referenceData()...");
		HashMap<String, Object> model = new HashMap<String, Object>();
		if (request.getSession().getAttribute("username") == null)
		{
			model.put("pageName", "login");
			model.put("value", "AdminLogin");
		} else
		{
			model.put("pageName", "logout");
			model.put("value", "Logout");
			model.put("common", getCommon());
		}
		if (request.getRequestURI().endsWith("requestbydimensionvalue"))
		{
			List<String> dims = dao.getDimensions();
			model.put("dims", dims);
			List<String> vals = new ArrayList<String>();
			@SuppressWarnings("unchecked")
			HashMap<String, Object> HashMap_requestbyDimensionValueFormReport = (HashMap<String, Object>) request
					.getSession().getAttribute("requestbyDimensionValueForm");

			// get the previouse selected dimension to load values of that
			// dimension
			if (HashMap_requestbyDimensionValueFormReport != null)
			{
				String[] tmpS = (String[]) HashMap_requestbyDimensionValueFormReport
						.get("incDims");
				if (tmpS != null)
				{
					List<String> tmpList = dao.getDimensionValues(tmpS[0]);
					for (int i = tmpList.size() - 1; i >= 0; i--)
						vals.add(0, tmpList.get(i));
				}
			}
			vals.add(0, "All");
			model.put("vals", vals);
			List<String> breakdownby = new ArrayList<String>();
			breakdownby.add("Month");
			breakdownby.add("Week");
			model.put("breakdownby", breakdownby);
		}
		return model;
	}

	/**
	 * @see formBackingObject() in AbstractFormControll Spring Framework
	 */
	@Override
	protected Object formBackingObject(HttpServletRequest request)
			throws Exception
	{
		ClassDebug
				.print("RequestbyDimensionValueFormController.formBackingObject()...");
		if (request.getRequestURI().endsWith("requestbydimensionvalue"))
		{
			setCommandClass(RequestByDimensionValueForm.class);
			setCommandName("RequestByDimensionValueForm");
			RequestByDimensionValueForm rbdvf = new RequestByDimensionValueForm();

			if (request.getParameter("Reset") == null)
			{
				@SuppressWarnings("unchecked")
				HashMap<String, Object> requestbyDimensionValueForm = (HashMap<String, Object>) request
						.getSession().getAttribute(
								"requestbyDimensionValueForm");
				if (requestbyDimensionValueForm != null)
				{
					rbdvf.setFromDate((String) requestbyDimensionValueForm
							.get("fromDate"));
					rbdvf.setToDate((String) requestbyDimensionValueForm
							.get("toDate"));
					rbdvf.setIncDims((String[]) requestbyDimensionValueForm
							.get("incDims"));
					rbdvf.setIncVals((String[]) requestbyDimensionValueForm
							.get("incVals"));
				} else
				{
					rbdvf.setFromDate(FormFormater.getPreviousMonth());
					rbdvf.setToDate(FormFormater.getCurrentDate());
				}
			} else
			{
				@SuppressWarnings("unchecked")
				HashMap<String, Object> HashMap_requestbyhourForm = (HashMap<String, Object>) request
						.getSession().getAttribute("requestbyhourForm");
				if (HashMap_requestbyhourForm != null)
					request.getSession()
							.setAttribute("requestbyhourForm", null);
				rbdvf.setFromDate(FormFormater.getPreviousMonth());
				rbdvf.setToDate(FormFormater.getCurrentDate());
			}

			this.setFormView("InputView\\requestbyDimensionValueInputView");
			return rbdvf;
		}
		return null;
	}

	/**
	 * @see onSubmit() in SimpleFormController Spring Framework
	 */
	@Override
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception
	{
		ClassDebug.print("RequestbyDimensionValueFormController.onSubmit()...");
		if (getCommandName().equals("RequestByDimensionValueForm"))
		{
			RequestByDimensionValueForm form = (RequestByDimensionValueForm) command;
			getValidator().validate(form, errors);

			long startTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Start the query at "
					+ ClassDebug.getCurrentDateAndTime());

			HashMap<String, Object> HashMap_requestbyDimensionValueForm = new HashMap<String, Object>();
			HashMap_requestbyDimensionValueForm.put("fromDate",
					form.getFromDate());
			HashMap_requestbyDimensionValueForm.put("toDate", form.getToDate());
			HashMap_requestbyDimensionValueForm.put("incDims",
					form.getIncDims());
			HashMap_requestbyDimensionValueForm.put("incVals",
					form.getIncVals());
			request.getSession().setAttribute("requestbyDimensionValueForm",
					HashMap_requestbyDimensionValueForm);

			String[] values = form.getIncVals();
			HashMap<String, Object> model = new HashMap<String, Object>();
			HashMap<String, Object> requestByDimVal = new HashMap<String, Object>();
			String filePath = this.getServletContext().getRealPath("/");

			/*
			 * If the user choose all in the value dropdown list, retreive data
			 * for each value and create charts.At the end, zip the charts into
			 * one file.
			 */
			if (values[0].compareTo("All") == 0)
			{
				String[] dims = form.getIncDims();
				List<String> valuesList = dao.getDimensionValues(dims[0]);
				File newFile = null;
				String newFilePath = null;
				for (String s : valuesList)
				{
					System.out.println(s);
					String[] val =
					{ s };
					System.out.println(s);
					requestByDimVal = getDimVals(form.getFromDate(),
							form.getToDate(), form.getIncDims(), val);
					model.put("dates", requestByDimVal.get("dates"));
					requestByDimVal.remove("dates");
					model.put("th1", "Date");
					model.put("th2", "Request");
					//
					JFreeChart chart = RequestByBarChart
							.createBarChartForRequestBy(requestByDimVal, "Date");
					newFile = new File(filePath + "pages/temp/" + dims[0] + "/");
					newFile.mkdirs();
					newFilePath = newFile.getAbsolutePath() + "/";
					System.out.println(newFilePath + " is created...");
					ChartUtilities.saveChartAsJPEG(new File(newFilePath + s
							+ ".jpg"), chart, 726, 400);
				}
				// System.out.println(newFile.getAbsolutePath());
				FileZipper fz = new FileZipper(form.getZipFileName(),
						newFilePath, filePath + "pages/temp/");
				fz.zipFile();
				System.out
						.println("pages/temp/" + fz.getZipFileName() + ".zip");
				model.put("zipFileName", fz.getZipFileName());
				model.put("zipFile", "pages/temp/" + fz.getZipFileName());
			}
			/*
			 * If a specific value is chosen, then do the regular process of
			 * generating charts and datas to show on the screen.
			 */
			else
			{
				requestByDimVal = getDimVals(form.getFromDate(),
						form.getToDate(), form.getIncDims(), form.getIncVals());
				model.put("dates", requestByDimVal.get("dates"));
				requestByDimVal.remove("dates");

				HashMap<String, Object> statisticalData = getStatisticalData(
						(List<RequestBy>) requestByDimVal.get("requestByList"),
						form.getFromDate(), form.getToDate(),
						form.getBreakdownby());
				double averageRequest = (Double) statisticalData.get("mean");
				RequestBy highest = (RequestBy) statisticalData.get("highest");
				RequestBy lowest = (RequestBy) statisticalData.get("lowest");
				//
				JFreeChart chart = RequestByBarChart
						.createBarChartForRequestBy(requestByDimVal, "Date");
				ChartUtilities.saveChartAsJPEG(new File(filePath
						+ "pages/temp/barchart.jpg"), chart, 726, 400);
				model.put("requestByList", requestByDimVal.get("requestByList"));
				model.put("th1", "Average Requests for the period");
				model.put("th2", "Highest Request");
				model.put("th3", "Lowest Request");
				model.put("th4", "Ranking");
				model.put("th5", "Period");
				model.put("th6", "Average Requests");
				model.put("th7", "Date");
				model.put("th8", "Request");
				model.put("img", "pages/temp/barchart.jpg");
				model.put("averageRequest", averageRequest);
				model.put("highest", highest);
				model.put("lowest", lowest);
				model.put("periodRequests",
						statisticalData.get("periodRequests"));
			}
			String[] dimName = form.getIncDims();
			String[] valName = form.getIncVals();
			String reportName = "Request By " + dimName[0] + ": " + valName[0];
			model.put("reportName", reportName);
			if (request.getSession().getAttribute("username") == null)
			{
				model.put("pageName", "login");
				model.put("value", "AdminLogin");
			} else
			{
				model.put("pageName", "logout");
				model.put("value", "Logout");
				model.put("common", getCommon());
			}

			// start to generate reports (pdf,xls,csv)
			Report.createReports(filePath, model,
					"RequestByDimensionValueReport");
			model.put("savedReportName", "RequestByDimensionValueReport");

			// start to generate reports (pdf,xls,csv)
			RequestByReport.createReports(filePath, model,
					"RequestByDimensionValueReport");
			model.put("savedReportName", "RequestByDimensionValueReport");

			System.out.println("report path : " + filePath);
			// Check if the report was created to avoid problems in the View
			model.put(
					"pdfReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".pdf").exists());
			model.put(
					"csvReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".csv").exists());
			model.put(
					"xlsReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".xls").exists());

			long endTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Complete the Query at "
					+ ClassDebug.getCurrentDateAndTime());
			ClassDebug.print("Time Cost: "
					+ ClassDebug.timeFormat(endTime - startTime));
			return new ModelAndView(
					"OutputView\\requestbydimensionvalueOutpuView", model);
		}
		return null;
	}

	// customized methods
	public HashMap<String, Object> getCommon()
	{
		HashMap<String, Object> common = new HashMap<String, Object>();
		common.put("title", "Admin Links");
		common.put("addPageName", "addlogfile");
		common.put("addValue", "Add Log Files");
		common.put("removePageName", "removelogfile");
		common.put("removeValue", "Remove Log Files");

		return common;
	}

	//
	public HashMap<String, Object> getDimVals(String fromDate, String toDate,
			String[] incDims, String[] incVals)
	{
		// TODO Auto-generated method stub
		String lastDate = dao.getLastDate();
		String firstDate = dao.getFirstDate();
		if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), firstDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
			toDate = FormFormater.formatDateTwo(lastDate);
		} else if ((!FormFormater.compareDates(
				FormFormater.formatDate(fromDate), firstDate))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(FormFormater.formatDate(toDate),
						lastDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
		} else if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(fromDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), lastDate)))
		{
			toDate = FormFormater.formatDateTwo(lastDate);
		}
		LinkedHashMap<String, Object> DimVals = new LinkedHashMap<String, Object>();
		ArrayList<String> dates = new ArrayList<String>();
		DimVals.put("requestByList", dao.getRequestByDimVal(
				FormFormater.formatDate(fromDate),
				FormFormater.formatDate(toDate), incDims[0], incVals[0]));
		dates.add(" " + fromDate + " - " + toDate);
		DimVals.put("dates", dates);
		DimVals.put("Title", incDims[0] + ": " + incVals[0]);
		return DimVals;
	}

	/**
	 * This method returns a map of statistical datas of the chosen value(sum of
	 * number of request, average of number of request, the highest and lowest
	 * number of request on a single day).
	 * 
	 * @param result
	 *            A list of RequestBy reportDataObject.
	 * @return HashMap<String, Double> data.
	 */
	public HashMap<String, Object> getStatisticalData(List<RequestBy> result,
			String fromDate, String toDate, String breakdownby)
	{
		double sum = 0;
		double mean = 0;
		double highest = 0;
		double lowest = Integer.MAX_VALUE;
		RequestBy high = new RequestBy();
		RequestBy low = new RequestBy();
		ArrayList<RequestBy> periodRequests = new ArrayList<RequestBy>();
		ArrayList<String> periods = new ArrayList<String>();
		int index = 0;
		String startDate, endDate;
		int subTotal = 0;
		boolean byMonth = true;

		String lastDate = dao.getLastDate();
		String firstDate = dao.getFirstDate();
		if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), firstDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
			toDate = FormFormater.formatDateTwo(lastDate);
		} else if ((!FormFormater.compareDates(
				FormFormater.formatDate(fromDate), firstDate))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(FormFormater.formatDate(toDate),
						lastDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
		} else if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(fromDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), lastDate)))
		{
			toDate = FormFormater.formatDateTwo(lastDate);
		}

		if (breakdownby.compareTo("Month") == 0)
		{
			System.out.println("in breakdownby month");
			periods = FormFormater.getMonths(fromDate, toDate);
		}
		if (breakdownby.compareTo("Week") == 0)
		{
			System.out.println("in breakdownby week");
			byMonth = false;
			periods = FormFormater.getWeeks(fromDate, toDate);
		}
		startDate = periods.get(index + 1);
		endDate = periods.get(index + 2);
		for (RequestBy rb : result)
		{
			boolean flag = false;
			while (flag != true)
			{
				if (!FormFormater.compareDates(rb.getTime(),
						FormFormater.formatDate(endDate))
						&& !FormFormater.isBefore(rb.getTime(),
								FormFormater.formatDate(startDate)))
				{
					subTotal += rb.getRequests();
					flag = true;
				} else
				{
					if (byMonth == true)
					{
						// assume that a month has 30 days
						subTotal = subTotal / 30;
					} else
					{
						subTotal = subTotal / 7;
					}
					RequestBy item = new RequestBy();
					item.setTime(periods.get(index) + ":"
							+ periods.get(index + 1) + "-"
							+ periods.get(index + 2));
					item.setRequests(subTotal);
					subTotal = 0;
					periodRequests.add(item);
					index += 3;
					startDate = periods.get(index + 1);
					endDate = periods.get(index + 2);
				}
			}
			// calculate the highest request and lowest request
			sum += rb.getRequests();
			if (highest < rb.getRequests())
			{
				highest = rb.getRequests();
				high = rb;
			}
			if (lowest > rb.getRequests())
			{
				lowest = rb.getRequests();
				low = rb;
			}
		}
		ArrayList<RequestBy> pRequests = merge_sort(periodRequests);
		mean = sum / result.size();
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("sum", sum);
		data.put("mean", mean);
		data.put("highest", high);
		data.put("lowest", low);
		data.put("periodRequests", pRequests);
		return data;
	}

	public ArrayList<RequestBy> merge_sort(ArrayList<RequestBy> m)
	{
		// if list size is 1, consider it sorted and return it
		if (m.size() <= 1)
			return m;
		// else list size is > 1, so split the list into two sublists
		ArrayList<RequestBy> left = new ArrayList<RequestBy>();
		ArrayList<RequestBy> right = new ArrayList<RequestBy>();
		int middle = m.size() / 2;
		for (int i = 0; i < middle; i++)
		{
			left.add(m.get(i));
		}
		for (int i = m.size() - 1; i >= middle; i--)
		{
			right.add(m.get(i));
		}
		// recursively call merge_sort() to further split each sublist
		// until sublist size is 1
		left = merge_sort(left);
		right = merge_sort(right);
		// merge the sublists returned from prior calls to merge_sort()
		// and return the resulting merged sublist
		return merge(left, right);
	}

	public ArrayList<RequestBy> merge(ArrayList<RequestBy> left,
			ArrayList<RequestBy> right)
	{
		ArrayList<RequestBy> result = new ArrayList<RequestBy>();
		while (left.size() > 0 || right.size() > 0)
		{
			if (left.size() > 0 && right.size() > 0)
			{
				if (left.get(0).getRequests() >= right.get(0).getRequests())
				{
					result.add(left.get(0));
					left.remove(0);
				} else
				{
					result.add(right.get(0));
					right.remove(0);
				}
			} else if (left.size() > 0)
			{
				result.add(left.get(0));
				left.remove(0);
			} else if (right.size() > 0)
			{
				result.add(right.get(0));
				right.remove(0);
			}
		}
		return result;
	}
}
