package springMVC;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import others.TimeEstimation;

import reports.Report;
import services.QueryThread;
import charts.PieChart;

import dbAccess.Dao;
import debug.ClassDebug;
import forms.RequestByTypeForm;

@SuppressWarnings("deprecation")
public class RequestbytypeFormController extends SimpleFormController
{
	private Dao dao;

	public void setDao(Dao dao)
	{
		this.dao = dao;
	}

	@Override
	protected HashMap<String, Object> referenceData(HttpServletRequest request)
			throws Exception
	{
		ClassDebug.print("RequestbytypeFormController.referenceData()...");
		HashMap<String, Object> model = new HashMap<String, Object>();
		if (request.getSession().getAttribute("username") == null)
		{
			model.put("pageName", "login");
			model.put("value", "AdminLogin");
		} else
		{
			model.put("pageName", "logout");
			model.put("value", "Logout");
			HashMap<String, Object> common = new HashMap<String, Object>();
			common.put("title", "Admin Links");
			common.put("addPageName", "addlogfile");
			common.put("addValue", "Add Log Files");
			common.put("removePageName", "removelogfile");
			common.put("removeValue", "Remove Log Files");
			model.put("common", common);
		}
		if (request.getRequestURI().endsWith("requestbytype"))
		{
			ArrayList<String> Threads = new ArrayList<String>();
			Threads.add("YES");
			Threads.add("NO");
			model.put("Threads", Threads);
		}

		return model;
	}

	@Override
	protected Object formBackingObject(HttpServletRequest request)
			throws Exception
	{
		ClassDebug.print("RequestbytypeFormController.formBackingObject()...");
		if (request.getRequestURI().endsWith("requestbytype"))
		{
			setCommandClass(RequestByTypeForm.class);
			setCommandName("requestbytypeForm");
			RequestByTypeForm rtf = new RequestByTypeForm();

			if (request.getParameter("Reset") == null)
			{
				@SuppressWarnings("unchecked")
				HashMap<String, Object> HashMap_requestbytypeForm = (HashMap<String, Object>) request
						.getSession().getAttribute("requestbytypeForm");
				if (HashMap_requestbytypeForm != null)
				{
					rtf.setThreads((String) HashMap_requestbytypeForm
							.get("Threads"));
				} else
				{
					rtf.setFromDate(FormFormater.getPreviousMonth());
					rtf.setToDate(FormFormater.getCurrentDate());
					rtf.setThreads("YES");
				}

				if (request.getSession().getAttribute("fromDate") != null
						&& request.getSession().getAttribute("toDate") != null)
				{
					rtf.setFromDate((String) request.getSession().getAttribute(
							"fromDate"));
					rtf.setToDate((String) request.getSession().getAttribute(
							"toDate"));
				}
			} else
			{
				@SuppressWarnings("unchecked")
				HashMap<String, Object> HashMap_requestbytypeForm = (HashMap<String, Object>) request
						.getSession().getAttribute("requestbytypeForm");
				if (HashMap_requestbytypeForm != null)
					request.getSession()
							.setAttribute("requestbytypeForm", null);
				request.getSession().removeAttribute("fromDate");
				request.getSession().removeAttribute("toDate");
				rtf.setFromDate(FormFormater.getPreviousMonth());
				rtf.setToDate(FormFormater.getCurrentDate());
				rtf.setThreads("YES");
			}

			this.setFormView("InputView\\requestbytypeInputView");
			return rtf;
		}
		return null;
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception
	{
		ClassDebug.print("RequestbytypeFormController.onSubmit()...");
		if (getCommandName().equals("requestbytypeForm"))
		{
			RequestByTypeForm form = (RequestByTypeForm) command;
			getValidator().validate(form, errors);

			/*
			 * TimeEstimation tmpTimeEstimation = new
			 * TimeEstimation(form.getFromDate(), form.getToDate(), dao,
			 * "RequestByType"); double tmpTimeEstimated =
			 * tmpTimeEstimation.start(); ClassDebug.print(tmpTimeEstimated ==
			 * -1 ? "No enough history data and unable to make an estimation!" :
			 * "Approximate time cost: " +
			 * ClassDebug.timeFormat((int)tmpTimeEstimated));
			 */

			long startTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Start the query at "
					+ ClassDebug.getCurrentDateAndTime());

			HashMap<String, Object> HashMap_requestbytypeForm = new HashMap<String, Object>();
			HashMap_requestbytypeForm.put("fromDate", form.getFromDate());
			HashMap_requestbytypeForm.put("toDate", form.getToDate());
			HashMap_requestbytypeForm.put("Threads", form.getThreads());
			request.getSession().setAttribute("requestbytypeForm",
					HashMap_requestbytypeForm);
			request.getSession().setAttribute("fromDate", form.getFromDate());
			request.getSession().setAttribute("toDate", form.getToDate());

			HashMap<String, Object> model = new HashMap<String, Object>();
			HashMap<String, Object> requestByTypeList = new HashMap<String, Object>();
			String fromDate = form.getFromDate();
			String toDate = form.getToDate();
			String Threads = form.getThreads();
			String lastDate = dao.getLastDate();
			String firstDate = dao.getFirstDate();

			if ((!FormFormater.compareDates(lastDate,
					FormFormater.formatDate(toDate)))
					&& (!FormFormater.compareDates(
							FormFormater.formatDate(fromDate), firstDate)))
			{
				fromDate = FormFormater.formatDateTwo(firstDate);
				toDate = FormFormater.formatDateTwo(lastDate);
			} else if ((!FormFormater.compareDates(
					FormFormater.formatDate(fromDate), firstDate))
					&& (!FormFormater.compareDates(firstDate,
							FormFormater.formatDate(toDate)))
					&& (!FormFormater.compareDates(
							FormFormater.formatDate(toDate), lastDate)))
			{
				fromDate = FormFormater.formatDateTwo(firstDate);
			} else if ((!FormFormater.compareDates(lastDate,
					FormFormater.formatDate(toDate)))
					&& (!FormFormater.compareDates(firstDate,
							FormFormater.formatDate(fromDate)))
					&& (!FormFormater.compareDates(
							FormFormater.formatDate(fromDate), lastDate)))
			{
				toDate = FormFormater.formatDateTwo(lastDate);
			}

			ClassDebug.print("Threads: " + Threads);

			if (Threads.equals("YES"))
			{
				List<QueryThread> qtList = new ArrayList<QueryThread>();
				QueryThread qt0 = new QueryThread(dao, requestByTypeList,
						"Search-Only Requests",
						FormFormater.formatDate(fromDate),
						FormFormater.formatDate(toDate), 12);
				qt0.start();
				qtList.add(qt0);
				QueryThread qt1 = new QueryThread(dao, requestByTypeList,
						"Navigate-Only Requests",
						FormFormater.formatDate(fromDate),
						FormFormater.formatDate(toDate), 13);
				qt1.start();
				qtList.add(qt1);
				// QueryThread qt2 = new
				// QueryThread(dao,requestByTypeList,"Search-And-Navigate Requests",FormFormater.formatDate(fromDate),FormFormater.formatDate(toDate),14);
				// qt2.start();
				// qtList.add(qt2);

				for (QueryThread qt : qtList)
				{
					try
					{
						qt.join();
					} catch (InterruptedException e)
					{
						e.printStackTrace();
					}
				}
			} else
			{
				int tmpSearchOnly = dao.getSearchRequest(
						FormFormater.formatDate(fromDate),
						FormFormater.formatDate(toDate));
				ClassDebug.print("Search-Only Requests: " + tmpSearchOnly);
				int tmpNavigateOnly = dao.getNaviRequest(
						FormFormater.formatDate(fromDate),
						FormFormater.formatDate(toDate));
				ClassDebug.print("Navigate-Only Requests: " + tmpNavigateOnly);
				int tmpBoth = dao.getBothRequest(
						FormFormater.formatDate(fromDate),
						FormFormater.formatDate(toDate));
				ClassDebug.print("Both Requests: " + tmpBoth);
				requestByTypeList.put("Search-Only Requests", tmpSearchOnly);
				requestByTypeList
						.put("Navigate-Only Requests", tmpNavigateOnly);
				// requestByTypeList.put("Search-And-Navigate Requests",
				// tmpBoth);
			}

			model.put("date", fromDate + " - " + toDate);
			//
			JFreeChart chart = PieChart.createPieChart(requestByTypeList);
			String filePath = this.getServletContext().getRealPath("/");
			ChartUtilities.saveChartAsJPEG(new File(filePath
					+ "pages/temp/piechart.jpg"), chart, 726, 400);
			//
			model.put("requestByTypeList", requestByTypeList);
			model.put("reportName", "Request By Type");
			model.put("img", "pages/temp/piechart.jpg");
			model.put("th1", "Type of Request");
			model.put("th2", "Requests");
			if (request.getSession().getAttribute("username") == null)
			{
				model.put("pageName", "login");
				model.put("value", "AdminLogin");
			} else
			{
				model.put("pageName", "logout");
				model.put("value", "Logout");
				HashMap<String, Object> common = new HashMap<String, Object>();
				common.put("title", "Admin Links");
				common.put("addPageName", "addlogfile");
				common.put("addValue", "Add Log Files");
				common.put("removePageName", "removelogfile");
				common.put("removeValue", "Remove Log Files");
				model.put("common", common);
			}
			// start to generate reports (pdf,xls,csv)
			Report.createReports(filePath, model, "RequestByTypeReport");
			model.put("savedReportName", "RequestByTypeReport");

			// Check if the report was created to avoid problems in the View
			model.put(
					"pdfReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".pdf").exists());
			model.put(
					"csvReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".csv").exists());
			model.put(
					"xlsReport",
					new File(filePath + "pages/reports/"
							+ model.get("savedReportName") + ".xls").exists());

			long endTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Complete the Query at "
					+ ClassDebug.getCurrentDateAndTime());
			ClassDebug.print("Time Cost: "
					+ ClassDebug.timeFormat(endTime - startTime));
			/*
			 * tmpTimeEstimation.end(endTime - startTime);
			 */
			return new ModelAndView("OutputView\\requestbytypeOutputView",
					model);
		}
		return null;
	}
}
