package springMVC;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import charts.BarChart;

import dbAccess.Dao;
import debug.ClassDebug;

import forms.RequestByHourForm;

import reportDataObjects.RequestByHour;

import reports.Report;

@SuppressWarnings("deprecation")
public class RequestbyhourFormController extends SimpleFormController
{

	Dao dao;

	public void setDao(Dao dao)
	{
		this.dao = dao;
	}

	// Spring SimpleFormController workflow methods

	@Override
	protected HashMap<String, Object> referenceData(HttpServletRequest request)
			throws Exception
	{
		ClassDebug.print("RequestbyhourFormController.referenceData()...");
		HashMap<String, Object> model = new HashMap<String, Object>();
		if (request.getSession().getAttribute("username") == null)
		{
			model.put("pageName", "login");
			model.put("value", "AdminLogin");
		} else
		{
			model.put("pageName", "logout");
			model.put("value", "Logout");
			model.put("common", getCommon());
		}
		return model;
	}

	@Override
	protected Object formBackingObject(HttpServletRequest request)
			throws Exception
	{
		ClassDebug.print("RequestbyhourFormController.formBackingObject()...");
		if (request.getRequestURI().endsWith("requestbyhour"))
		{
			setCommandClass(RequestByHourForm.class);
			setCommandName("requestbyhourForm");
			RequestByHourForm rhf = new RequestByHourForm();

			if (request.getParameter("Reset") == null)
			{
				@SuppressWarnings("unchecked")
				HashMap<String, Object> HashMap_requestbyhourForm = (HashMap<String, Object>) request
						.getSession().getAttribute("requestbyhourForm");
				if (HashMap_requestbyhourForm != null)
				{
					rhf.setFromDate((String) HashMap_requestbyhourForm
							.get("fromDate"));
					rhf.setToDate((String) HashMap_requestbyhourForm
							.get("toDate"));
					rhf.setCompareFromDate((String) HashMap_requestbyhourForm
							.get("compareFromDate"));
					rhf.setCompareToDate((String) HashMap_requestbyhourForm
							.get("compareToDate"));
				} else
				{
					rhf.setFromDate(FormFormater.getPreviousMonth());
					rhf.setToDate(FormFormater.getCurrentDate());
				}
			} else
			{
				@SuppressWarnings("unchecked")
				HashMap<String, Object> HashMap_requestbyhourForm = (HashMap<String, Object>) request
						.getSession().getAttribute("requestbyhourForm");
				if (HashMap_requestbyhourForm != null)
					request.getSession()
							.setAttribute("requestbyhourForm", null);
				rhf.setFromDate(FormFormater.getPreviousMonth());
				rhf.setToDate(FormFormater.getCurrentDate());
			}

			this.setFormView("InputView\\requestbyhourInputView");
			return rhf;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception
	{
		ClassDebug.print("RequestbyhourFormController.onSubmit()...");
		if (getCommandName().equals("requestbyhourForm"))
		{
			RequestByHourForm form = (RequestByHourForm) command;
			getValidator().validate(form, errors);

			long startTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Start the query at "
					+ ClassDebug.getCurrentDateAndTime());

			HashMap<String, Object> HashMap_requestbyhourForm = new HashMap<String, Object>();
			HashMap_requestbyhourForm.put("fromDate", form.getFromDate());
			HashMap_requestbyhourForm.put("toDate", form.getToDate());
			HashMap_requestbyhourForm.put("compareFromDate",
					form.getCompareFromDate());
			HashMap_requestbyhourForm.put("compareToDate",
					form.getCompareToDate());
			request.getSession().setAttribute("requestbyhourForm",
					HashMap_requestbyhourForm);

			HashMap<String, Object> model = new HashMap<String, Object>();
			HashMap<String, Object> requestByHour = getRequestByHour(
					form.getFromDate(), form.getToDate(),
					form.getCompareFromDate(), form.getCompareToDate());
			model.put("date", requestByHour.get("date"));
			requestByHour.remove("date");
			//
			JFreeChart chart = BarChart
					.createBarChartForRequestByHour(requestByHour);
			String filePath = this.getServletContext().getRealPath("/");
			ChartUtilities.saveChartAsJPEG(new File(filePath
					+ "pages/temp/barchart.jpg"), chart, 726, 400);
			//
			if (requestByHour.size() == 2)
			{
				int totalRequests = 0;
				int totalRequests1 = 0;
				ArrayList<RequestByHour> rbh = (ArrayList<RequestByHour>) requestByHour
						.get("requestByHourList");
				ArrayList<RequestByHour> rbh1 = (ArrayList<RequestByHour>) requestByHour
						.get("requestByHourList1");
				for (int i = 0; i < rbh.size(); i++)
				{
					totalRequests += rbh.get(i).getRequests();
				}
				for (int i = 0; i < rbh1.size(); i++)
				{
					totalRequests1 += rbh1.get(i).getRequests();
				}
				model.put("requestByHourList", rbh);
				model.put("requestByHourList1", rbh1);
				model.put("totalRequests", totalRequests);
				model.put("totalRequests1", totalRequests1);
				model.put("th1", "1st Hours");
				model.put("th2", "1st Requests");
				model.put("th3", "2nd Hours");
				model.put("th4", "2nd Requests");
				model.put("reportName", "Request By Hour");
				model.put("img", "pages/temp/barchart.jpg");
			} else
			{
				int totalRequests = 0;
				ArrayList<RequestByHour> rbh = (ArrayList<RequestByHour>) requestByHour
						.get("requestByHourList");
				for (int i = 0; i < rbh.size(); i++)
				{
					totalRequests += rbh.get(i).getRequests();
				}
				model.put("requestByHourList", rbh);
				model.put("totalRequests", totalRequests);
				model.put("th1", "Hours");
				model.put("th2", "Requests");
				model.put("reportName", "Request By Hour");
				model.put("img", "pages/temp/barchart.jpg");
			}

			if (request.getSession().getAttribute("username") == null)
			{
				model.put("pageName", "login");
				model.put("value", "AdminLogin");
			} else
			{
				model.put("pageName", "logout");
				model.put("value", "Logout");
				model.put("common", getCommon());
			}

			long endTime = ClassDebug.getCurrentTime();
			ClassDebug.print("Complete the Query at "
					+ ClassDebug.getCurrentDateAndTime());
			ClassDebug.print("Time Cost: "
					+ ClassDebug.timeFormat(endTime - startTime));

			// start to generate reports (pdf,xls,csv)
			Report.createReports(filePath, model, "RequestByHourReport");
			model.put("savedReportName", "RequestByHourReport");
			return new ModelAndView("OutputView\\requestbyhourOutputView",
					model);
		}
		return null;
	}

	// customized methods
	public HashMap<String, Object> getCommon()
	{
		HashMap<String, Object> common = new HashMap<String, Object>();
		common.put("title", "Admin Links");
		common.put("addPageName", "addlogfile");
		common.put("addValue", "Add Log Files");
		common.put("removePageName", "removelogfile");
		common.put("removeValue", "Remove Log Files");

		return common;
	}

	//
	public HashMap<String, Object> getRequestByHour(String fromDate,
			String toDate, String compareFromDate, String compareToDate)
	{
		ClassDebug.print("RequestbyhourFormController.getRequestByHour()...");
		String lastDate = dao.getLastDate();
		String firstDate = dao.getFirstDate();
		if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), firstDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
			toDate = FormFormater.formatDateTwo(lastDate);
		} else if ((!FormFormater.compareDates(
				FormFormater.formatDate(fromDate), firstDate))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(FormFormater.formatDate(toDate),
						lastDate)))
		{
			fromDate = FormFormater.formatDateTwo(firstDate);
		} else if ((!FormFormater.compareDates(lastDate,
				FormFormater.formatDate(toDate)))
				&& (!FormFormater.compareDates(firstDate,
						FormFormater.formatDate(fromDate)))
				&& (!FormFormater.compareDates(
						FormFormater.formatDate(fromDate), lastDate)))
		{
			toDate = FormFormater.formatDateTwo(lastDate);
		}
		HashMap<String, Object> requestByHour = new HashMap<String, Object>();
		String date = fromDate + " - " + toDate;
		if ((!compareFromDate.equals("")) && (!compareToDate.equals("")))
		{
			if (FormFormater.compareDates(
					FormFormater.formatDate(compareToDate), dao.getLastDate()))
			{
				compareToDate = FormFormater.formatDateTwo(dao.getLastDate());
			}
			date += "  Compared With  " + compareFromDate + " - "
					+ compareToDate;
			List<RequestByHour> requestByHourList1 = dao.getRequestByHours(
					FormFormater.formatDate(compareFromDate),
					FormFormater.formatDate(compareToDate));
			requestByHour.put("requestByHourList1", requestByHourList1);
		}
		List<RequestByHour> requestByHourList = dao.getRequestByHours(
				FormFormater.formatDate(fromDate),
				FormFormater.formatDate(toDate));
		requestByHour.put("requestByHourList", requestByHourList);
		requestByHour.put("date", date);
		return requestByHour;
	}
}
