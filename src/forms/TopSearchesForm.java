package forms;

/**
 * This class is used to store information get from the top dimension search
 * form
 * 
 */
public class TopSearchesForm
{

	String fromDate;
	String toDate;
	String depth;
	int numRecords;
	String breakdownBy;
	String searchOrder;
	String incDims1;
	String incDims2;
	String incDims3;

	public String getIncDims1()
	{
		return incDims1;
	}

	public void setIncDims1(String incDims1)
	{
		this.incDims1 = incDims1;
	}

	public String getIncDims2()
	{
		return incDims2;
	}

	public void setIncDims2(String incDims2)
	{
		this.incDims2 = incDims2;
	}

	public String getIncDims3()
	{
		return incDims3;
	}

	public void setIncDims3(String incDims3)
	{
		this.incDims3 = incDims3;
	}

	public String getFromDate()
	{
		return fromDate;
	}

	public void setFromDate(String fromDate)
	{
		this.fromDate = fromDate;
	}

	public String getToDate()
	{
		return toDate;
	}

	public void setToDate(String toDate)
	{
		this.toDate = toDate;
	}

	public String getDepth()
	{
		return depth;
	}

	public void setDepth(String depth)
	{
		this.depth = depth;
	}

	public int getNumRecords()
	{
		return numRecords;
	}

	public void setNumRecords(int numRecords)
	{
		this.numRecords = numRecords;
	}

	public String getBreakdownBy()
	{
		return breakdownBy;
	}

	public void setBreakdownBy(String breakdownBy)
	{
		this.breakdownBy = breakdownBy;
	}

	public String getSearchOrder()
	{
		return searchOrder;
	}

	public void setSearchOrder(String searchOrder)
	{
		this.searchOrder = searchOrder;
	}

}
