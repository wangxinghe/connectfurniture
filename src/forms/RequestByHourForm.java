package forms;

/**
 * This class is used to store information get from the request by hour form
 * 
 */
public class RequestByHourForm
{

	String fromDate;
	String toDate;
	String compareFromDate;
	String compareToDate;

	public String getCompareFromDate()
	{
		return compareFromDate;
	}

	public void setCompareFromDate(String compareFromDate)
	{
		this.compareFromDate = compareFromDate;
	}

	public String getCompareToDate()
	{
		return compareToDate;
	}

	public void setCompareToDate(String compareToDate)
	{
		this.compareToDate = compareToDate;
	}

	public String getFromDate()
	{
		return fromDate;
	}

	public void setFromDate(String fromDate)
	{
		this.fromDate = fromDate;
	}

	public String getToDate()
	{
		return toDate;
	}

	public void setToDate(String toDate)
	{
		this.toDate = toDate;
	}

}
