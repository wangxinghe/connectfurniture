package forms;

/**
 * This class is used to store information get from the administrator login form
 * 
 */
public class LoginForm
{

	String username;
	String password;

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getUsername()
	{
		return username;

	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getPassword()
	{
		return password;

	}

}
