package forms;

public class RequestByDimensionValueForm
{
	String fromDate;
	String toDate;
	String[] incVals;
	String[] incDims;
	String submitButton;
	private String breakdownby;
	private String zipFileName;

	public String getSubmitButton()
	{
		return submitButton;
	}

	public void setSubmitButton(String submitButton)
	{
		this.submitButton = submitButton;
	}

	public String getFromDate()
	{
		return fromDate;
	}

	public void setFromDate(String fromDate)
	{
		this.fromDate = fromDate;
	}

	public String getToDate()
	{
		return toDate;
	}

	public void setToDate(String toDate)
	{
		this.toDate = toDate;
	}

	public String[] getIncDims()
	{
		return incDims;
	}

	public void setIncDims(String[] incDims)
	{
		this.incDims = incDims;
	}

	public String[] getIncVals()
	{
		return incVals;
	}

	public void setIncVals(String[] incVals)
	{
		this.incVals = incVals;
	}

	public String getZipFileName()
	{
		return zipFileName;
	}

	public void setZipFileName(String zipFileName)
	{
		this.zipFileName = zipFileName;
	}

	public void setBreakdownby(String breakdownby)
	{
		this.breakdownby = breakdownby;
	}

	public String getBreakdownby()
	{
		return breakdownby;
	}
}
