package forms;

/**
 * This class is used to store information get from the top keywords search form
 * 
 */
public class TopKeywordsSearchesForm
{

	String fromDate;
	String toDate;
	String resultType;
	int numRecords;

	public String getFromDate()
	{
		return fromDate;
	}

	public void setFromDate(String fromDate)
	{
		this.fromDate = fromDate;
	}

	public String getToDate()
	{
		return toDate;
	}

	public void setToDate(String toDate)
	{
		this.toDate = toDate;
	}

	public String getResultType()
	{
		return resultType;
	}

	public void setResultType(String resultType)
	{
		this.resultType = resultType;
	}

	public int getNumRecords()
	{
		return numRecords;
	}

	public void setNumRecords(int numRecords)
	{
		this.numRecords = numRecords;
	}

}
