package forms;

/**
 * This class is used to store information get from the request by type form
 * 
 */
public class RequestByTypeForm
{

	String fromDate;
	String toDate;
	String Threads;

	public String getFromDate()
	{
		return fromDate;
	}

	public void setFromDate(String fromDate)
	{
		this.fromDate = fromDate;
	}

	public String getToDate()
	{
		return toDate;
	}

	public void setToDate(String toDate)
	{
		this.toDate = toDate;
	}

	public void setThreads(String thread)
	{
		Threads = thread;
	}

	public String getThreads()
	{
		return Threads;
	}
}
