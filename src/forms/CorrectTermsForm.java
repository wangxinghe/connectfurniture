package forms;

/**
 * This class is used to store information get from the Correct Term form
 * 
 * @author yoursoft
 * 
 */
public class CorrectTermsForm
{

	String fromDate;
	String toDate;
	int topNum;

	/**
	 * Get the start date.
	 * 
	 * @return the start date.
	 */
	public String getFromDate()
	{
		return fromDate;
	}

	/**
	 * Set the start date to the form.
	 * 
	 * @param fromDate
	 *            the start date of the request.
	 */
	public void setFromDate(String fromDate)
	{
		this.fromDate = fromDate;
	}

	/**
	 * Get the end date.
	 * 
	 * @return the end date.
	 */
	public String getToDate()
	{
		return toDate;
	}

	/**
	 * Set the end date to the form.
	 * 
	 * @param toDate
	 *            the end date of the request.
	 */
	public void setToDate(String toDate)
	{
		this.toDate = toDate;
	}

	/**
	 * Get the Number of how many results you need.
	 * 
	 * @return
	 */
	public int getTopNum()
	{
		return topNum;
	}

	public void setTopNum(int topNum)
	{
		this.topNum = topNum;
	}
}
