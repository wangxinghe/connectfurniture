package forms;

/**
 * This class is used to store information get from the remove logs form
 * 
 */
public class RemoveForm
{

	String startDate;
	String endDate;

	public void setStartDate(String startDate)
	{
		this.startDate = startDate;
	}

	public String getStartDate()
	{
		return startDate;

	}

	public void setEndDate(String endDate)
	{
		this.endDate = endDate;
	}

	public String getEndDate()
	{
		return endDate;

	}

}
