package forms;

public class SessionByMinutesForm
{
	String fromDate;
	String toDate;
	Integer numberOfRecords;

	public String getFromDate()
	{
		return this.fromDate;
	}

	public String getToDate()
	{
		return this.toDate;
	}

	public Integer getNumberOfRecords()
	{
		return this.numberOfRecords;
	}

	public void setFromDate(String fromDate)
	{
		this.fromDate = fromDate;
	}

	public void setToDate(String toDate)
	{
		this.toDate = toDate;
	}

	public void setNumberOfRecords(Integer numberOfRecords)
	{
		this.numberOfRecords = numberOfRecords;
	}
}
