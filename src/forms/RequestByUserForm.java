package forms;

public class RequestByUserForm
{
	String fromDate;
	String toDate;
	String requestType;

	public void setFromDate(String fromDate)
	{
		this.fromDate = fromDate;
	}

	public void setToDate(String toDate)
	{
		this.toDate = toDate;
	}

	public void setRequestType(String requestType)
	{
		this.requestType = requestType;
	}

	public String getFromDate()
	{
		return this.fromDate;
	}

	public String getToDate()
	{
		return this.toDate;
	}

	public String getRequestType()
	{
		return this.requestType;
	}
}
