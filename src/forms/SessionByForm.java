package forms;

/**
 * This class is used to store information get from the session by form
 * 
 * @author yoursoft
 * 
 */
public class SessionByForm
{

	String fromDate;
	String toDate;
	String compareFromDate;
	String compareToDate;
	String type;
	boolean isCompared;

	/**
	 * Get the compare start date.
	 * 
	 * @return the date which is used for compare.
	 */
	public String getCompareFromDate()
	{
		return compareFromDate;
	}

	/**
	 * Set the compare start date to the form.
	 * 
	 * @param compareFromDate
	 *            the compare start date.
	 */
	public void setCompareFromDate(String compareFromDate)
	{
		this.compareFromDate = compareFromDate;
	}

	/**
	 * Get the compare end date.
	 * 
	 * @return the date which is used for compare.
	 */
	public String getCompareToDate()
	{
		return compareToDate;
	}

	/**
	 * Set the compare end date to the form.
	 * 
	 * @param compareToDate
	 *            the compare end date.
	 */
	public void setCompareToDate(String compareToDate)
	{
		this.compareToDate = compareToDate;
	}

	/**
	 * Get the start date.
	 * 
	 * @return the start date.
	 */
	public String getFromDate()
	{
		return fromDate;
	}

	/**
	 * Set the start date to the form.
	 * 
	 * @param fromDate
	 *            the start date.
	 */
	public void setFromDate(String fromDate)
	{
		this.fromDate = fromDate;
	}

	/**
	 * Get the end date.
	 * 
	 * @return the end date.
	 */
	public String getToDate()
	{
		return toDate;
	}

	/**
	 * Set the end date to the form.
	 * 
	 * @param toDate
	 *            the end date.
	 */
	public void setToDate(String toDate)
	{
		this.toDate = toDate;
	}

	/**
	 * Get the time range type.
	 * 
	 * @return the time range type.
	 */
	public String getType()
	{
		return type;
	}

	/**
	 * Set the time range type to the form.
	 * 
	 * @param type
	 */
	public void setType(String type)
	{
		this.type = type;
	}

	/**
	 * Check whether the compare function is used or not.
	 * 
	 * @return
	 */
	public boolean getIsCompared()
	{
		return isCompared;
	}

	/**
	 * Set the compare condition (compare or not compare) to the form.
	 * 
	 * @param isCompared
	 */
	public void setIsCompared(boolean isCompared)
	{
		this.isCompared = isCompared;
	}
}