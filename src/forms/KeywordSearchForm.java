package forms;

public class KeywordSearchForm
{
	String fromDate;
	String toDate;
	int numberOfRecords;
	int lengthOfUnit;
	int value;
	String unitChangeType;

	public String getUnitChangeType()
	{
		return unitChangeType;
	}

	public void setUnitChangeType(String unitChangeType)
	{
		this.unitChangeType = unitChangeType;
	}

	public int getLengthOfUnit()
	{
		return lengthOfUnit;
	}

	public int getValue()
	{
		return value;
	}

	public void setLengthOfUnit(int lengthOfUnit)
	{
		this.lengthOfUnit = lengthOfUnit;
	}

	public void setValue(int value)
	{
		this.value = value;
	}

	public String getFromDate()
	{
		return fromDate;
	}

	public void setFromDate(String fromDate)
	{
		this.fromDate = fromDate;
	}

	public String getToDate()
	{
		return toDate;
	}

	public void setToDate(String toDate)
	{
		this.toDate = toDate;
	}

	public int getNumberOfRecords()
	{
		return numberOfRecords;
	}

	public void setNumberOfRecords(int numberOfRecords)
	{
		this.numberOfRecords = numberOfRecords;
	}
}
