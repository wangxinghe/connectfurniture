package forms;

/**
 * This class is used to store information get from the request by hour form
 * 
 * @author yoursoft
 * 
 */
public class RequestByForm
{

	/** The start date selected by the user. */
	String fromDate;
	/** The end date selected by the user. */
	String toDate;
	/** The compared start date selected by the user. */
	String compareFromDate;
	/** The compared end date selected by the user. */
	String compareToDate;
	/** The time unit selected by the user in group by option in the form. */
	String timeUnit;
	/**
	 * The tick box in the form to indicate if compared dates are added to the
	 * sql request.
	 */
	boolean isCompared;

	/** A getter of compareFromDate. */
	public String getCompareFromDate()
	{
		return compareFromDate;
	}

	/** A setter of compareFromDate. */
	public void setCompareFromDate(String compareFromDate)
	{
		this.compareFromDate = compareFromDate;
	}

	/** A getter of compareToDate. */
	public String getCompareToDate()
	{
		return compareToDate;
	}

	/** A setter of compareToDate. */
	public void setCompareToDate(String compareToDate)
	{
		this.compareToDate = compareToDate;
	}

	/** A getter of fromDate. */
	public String getFromDate()
	{
		return fromDate;
	}

	/** A setter of fromDate. */
	public void setFromDate(String fromDate)
	{
		this.fromDate = fromDate;
	}

	/** A getter of toDate. */
	public String getToDate()
	{
		return toDate;
	}

	/** A setter of toDate. */
	public void setToDate(String toDate)
	{
		this.toDate = toDate;
	}

	/** A setter of timeUnit. */
	public void setTimeUnit(String timeUnit)
	{
		this.timeUnit = timeUnit;
	}

	/** A getter of timeUnit. */
	public String getTimeUnit()
	{
		return timeUnit;
	}

	/** A getter of isCompared. */
	public boolean getIsCompared()
	{
		return isCompared;
	}

	/** A setter of isCompared. */
	public void setIsCompared(boolean isCompared)
	{
		this.isCompared = isCompared;
	}
}
