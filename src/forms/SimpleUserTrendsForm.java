package forms;

public class SimpleUserTrendsForm
{
	private String dimension;
	private String mode;
	private double depreciationRate;
	private int depreciationInterval;
	private double a;
	private int threshold;

	public String getDimension()
	{
		return dimension;
	}

	public void setDimension(String dimension)
	{
		this.dimension = dimension;
	}

	public void setMode(String mode)
	{
		this.mode = mode;
	}

	public String getMode()
	{
		return mode;
	}

	public double getDepreciationRate()
	{
		return depreciationRate;
	}

	public void setDepreciationRate(double depreciationRate)
	{
		this.depreciationRate = depreciationRate;
	}

	public int getDepreciationInterval()
	{
		return depreciationInterval;
	}

	public void setDepreciationInterval(int depreciationInterval)
	{
		this.depreciationInterval = depreciationInterval;
	}

	public double getA()
	{
		return a;
	}

	public void setA(double a)
	{
		this.a = a;
	}

	public void setThreshold(int threshold)
	{
		this.threshold = threshold;
	}

	public int getThreshold()
	{
		return threshold;
	}
}
