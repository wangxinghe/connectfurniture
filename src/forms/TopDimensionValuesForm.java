package forms;

/**
 * This class is used to store information get from the top dimension-value form
 * 
 */
public class TopDimensionValuesForm
{

	String fromDate;
	String toDate;
	String depth;
	int numRecords;
	String breakdownBy;
	String[] incVals;
	String[] incDims;
	String[] incVals1;
	String[] incDims1;

	String fromDateOfTrend;
	String toDateOfTrend;
	String[] incValsOfTrend;
	String[] incDimsOfTrend;
	String[] incVals1OfTrend;
	String[] incDims1OfTrend;

	String submitButton;

	public String getSubmitButton()
	{
		return submitButton;
	}

	public void setSubmitButton(String submitButton)
	{
		this.submitButton = submitButton;
	}

	public String getFromDateOfTrend()
	{
		return fromDateOfTrend;
	}

	public void setFromDateOfTrend(String fromDateOfTrend)
	{
		this.fromDateOfTrend = fromDateOfTrend;
	}

	public String getToDateOfTrend()
	{
		return toDateOfTrend;
	}

	public void setToDateOfTrend(String toDateOfTrend)
	{
		this.toDateOfTrend = toDateOfTrend;
	}

	public String[] getIncValsOfTrend()
	{
		return incValsOfTrend;
	}

	public void setIncValsOfTrend(String[] incValsOfTrend)
	{
		this.incValsOfTrend = incValsOfTrend;
	}

	public String[] getIncDimsOfTrend()
	{
		return incDimsOfTrend;
	}

	public void setIncDimsOfTrend(String[] incDimsOfTrend)
	{
		this.incDimsOfTrend = incDimsOfTrend;
	}

	public String[] getIncVals1OfTrend()
	{
		return incVals1OfTrend;
	}

	public void setIncVals1OfTrend(String[] incVals1OfTrend)
	{
		this.incVals1OfTrend = incVals1OfTrend;
	}

	public String[] getIncDims1OfTrend()
	{
		return incDims1OfTrend;
	}

	public void setIncDims1OfTrend(String[] incDims1OfTrend)
	{
		this.incDims1OfTrend = incDims1OfTrend;
	}

	public String[] getIncVals1()
	{
		return incVals1;
	}

	public void setIncVals1(String[] incVals1)
	{
		this.incVals1 = incVals1;
	}

	public String[] getIncDims1()
	{
		return incDims1;
	}

	public void setIncDims1(String[] incDims1)
	{
		this.incDims1 = incDims1;
	}

	public String[] getIncDims()
	{
		return incDims;
	}

	public void setIncDims(String[] incDims)
	{
		this.incDims = incDims;
	}

	public String[] getIncVals()
	{
		return incVals;
	}

	public void setIncVals(String[] incVals)
	{
		this.incVals = incVals;
	}

	public String getFromDate()
	{
		return fromDate;
	}

	public void setFromDate(String fromDate)
	{
		this.fromDate = fromDate;
	}

	public String getToDate()
	{
		return toDate;
	}

	public void setToDate(String toDate)
	{
		this.toDate = toDate;
	}

	public String getDepth()
	{
		return depth;
	}

	public void setDepth(String depth)
	{
		this.depth = depth;
	}

	public int getNumRecords()
	{
		return numRecords;
	}

	public void setNumRecords(int numRecords)
	{
		this.numRecords = numRecords;
	}

	public String getBreakdownBy()
	{
		return breakdownBy;
	}

	public void setBreakdownBy(String breakdownBy)
	{
		this.breakdownBy = breakdownBy;
	}
}
