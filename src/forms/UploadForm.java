package forms;

/**
 * This class is used to store information get from the upload log files form
 * 
 */
public class UploadForm
{

	// textbox
	String folderPath;
	String buttonName;

	public String getButtonName()
	{
		return buttonName;
	}

	public void setButtonName(String buttonName)
	{
		this.buttonName = buttonName;
	}

	public void setFolderPath(String folderPath)
	{
		this.folderPath = folderPath;
	}

	public String getFolderPath()
	{
		return folderPath;
	}

}
