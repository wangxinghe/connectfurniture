package dbAccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import DataMining.StandardDeviation;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcDaoSupport;

import reportDataObjects.*;

public class DataMiningDao extends SimpleJdbcDaoSupport
{
	@SuppressWarnings("null")
	public HashMap<String, List<SimpleUserTrends>> getNormalizedDatas(
			String Dimension)
	{
		long start = System.currentTimeMillis();
		HashMap<String, List<SimpleUserTrends>> normalizedSampleData = new HashMap<String, List<SimpleUserTrends>>();
		int SumOfDimensionRequests = 0;
		int SumOfNumberOfDataEntries = 0;
		ArrayList<Double> requestsArray = new ArrayList<Double>();
		String query1 = "SELECT value FROM valuetable WHERE dimension = ?";
		List<String> valueNames = getSimpleJdbcTemplate().query(query1,
				new RowMapper<String>()
				{
					public String mapRow(ResultSet rs, int rowNum)
							throws SQLException
					{
						return rs.getString("value");
					}
				}, Dimension);

		long breakpoint1 = System.currentTimeMillis();
		System.out.println("First SQL finished: " + (breakpoint1 - start)
				/ 1000 + "s");

		String query2 = "SELECT numofoccurences, date FROM mat_dimensionvalues_one WHERE dimension LIKE ? and value LIKE ? ORDER BY date";
		for (String name : valueNames)
		{
			List<SimpleUserTrends> result = getSimpleJdbcTemplate().query(
					query2, new RowMapper<SimpleUserTrends>()
					{
						public SimpleUserTrends mapRow(ResultSet rs, int rowNum)
								throws SQLException
						{
							SimpleUserTrends valueRequestsByDate = new SimpleUserTrends();
							valueRequestsByDate.setRequest(rs.getInt(1));
							valueRequestsByDate.setDate(rs.getString(2));
							DateFormat formatter = new SimpleDateFormat(
									"yyyy-MM-dd");
							Date date = new Date();
							try
							{
								date = (Date) formatter.parse(rs.getString(2));
							} catch (ParseException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							valueRequestsByDate.setLongdate(date.getTime());
							return valueRequestsByDate;
						}
					}, Dimension, name);
			SumOfNumberOfDataEntries += result.size();
			for (SimpleUserTrends dmo : result)
			{
				requestsArray.add(dmo.getRequest());
				SumOfDimensionRequests += dmo.getRequest();
			}
			normalizedSampleData.put(name, result);
		}
		long breakpoint2 = System.currentTimeMillis();
		System.out.println("Second SQL finished: "
				+ (breakpoint2 - breakpoint1) / 1000 + "s");

		double meanOfSample = SumOfDimensionRequests / SumOfNumberOfDataEntries;
		StandardDeviation sdcalculater = new StandardDeviation(true);
		double standardDeviation = sdcalculater.calculateStandardDeviation(
				requestsArray, meanOfSample);
		// normalized data
		for (String dimensionValue : valueNames)
		{
			for (SimpleUserTrends dmo : normalizedSampleData
					.get(dimensionValue))
			{
				dmo.getRequest();
				double newValue = 0;
				newValue = normalizeValues(dmo.getRequest(), meanOfSample,
						standardDeviation);
				dmo.setNormalizedRequest(newValue);
			}
		}
		long end = System.currentTimeMillis();
		System.out.println("Time Spent: " + (end - start) / 1000 + "s");
		// end
		return normalizedSampleData;
	}

	// normalized function
	private double normalizeValues(double unnormal, double average,
			double standdiv)
	{
		double normal = 0;
		normal = (unnormal - average) / standdiv;
		return normal;
	}
	// end
}
