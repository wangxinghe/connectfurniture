package dbAccess;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import others.TimeEstimation;

import reportDataObjects.*;

/** Database Access Interface
 * This interface interacts with the database to provide information for the Service class
 *
 */
/**
 * @author yoursoft
 * 
 */
public interface Dao
{
	/**
	 * Returns the max id of table mainlogs
	 * 
	 * @return the max id of table mainlogs
	 */
	int getMainLogsRowId();

	/**
	 * Returns how many rows of table mainlogs
	 * 
	 * @return Integer
	 */
	int checkMainLogsRowId();

	/**
	 * Inserts dimensions and values parsed from the logs to valuetable
	 * 
	 * @param dim
	 *            dimension to be inserted
	 * @param val
	 *            value to be inserted
	 */
	void insertValueTable(String dim, String val);

	/**
	 * Return 1 if there is a row which has dim and val in the valuetable or
	 * return 0 if not
	 * 
	 * @param dim
	 *            dimension
	 * @param val
	 *            value
	 * @return Integer
	 */
	int checkValueTable(String dim, String val);

	/**
	 * Returns the row id of valuetable where column dimension equals dim and
	 * column value equals val
	 * 
	 * @param dim
	 *            dimension
	 * @param val
	 *            value
	 * @return Integer
	 */
	int getValueTableId(String dim, String val);

	/**
	 * Parse the log files and insert them into the database
	 * 
	 * @param filePath
	 *            The absolute path of the folder containing log files
	 */
	void loadLogs(String filePath);

	/**
	 * Creates the admin table
	 * 
	 */
	void createTableAdmin();

	/**
	 * Creates the mainlogs table
	 * 
	 */
	void createTableMainlogs();

	/**
	 * Creates the dimensions table
	 * 
	 */
	void createTableDimensions();

	/**
	 * Creates the file table
	 * 
	 */
	void createTableFile();

	/**
	 * Creates the valuetable table
	 * 
	 */
	void createTableValues();

	/**
	 * Deletes rows in the mainlogs table from date sDate to date eDate
	 * 
	 * @param sDate
	 *            the from date input
	 * @param eDate
	 *            the to date input
	 */
	void deleteFromMainlogsTable(String sDate, String eDate);

	/**
	 * Deletes rows in the dimensions table which not in the mainlogs table
	 * 
	 */
	void deleteFromDimensionsTable();

	/**
	 * Returns true if uname and pwd in table admin or returns false if not
	 * 
	 * @param uname
	 *            the username input
	 * @param pwd
	 *            the password input
	 * @return boolean
	 */
	boolean checkAdmin(String uname, String pwd);

	/**
	 * Returns a list of type TopSearch for Top Dimension Search report
	 * 
	 * @param sDate
	 *            the from date input
	 * @param eDate
	 *            the to date input
	 * @param searchOrder
	 *            yes for order important, no for order not important
	 * @param depth
	 *            1 for one dimension, 2 for two dimensions, 3 for three
	 *            dimensions
	 * @param numberOfRecords
	 *            the number of records input
	 * @param searchedDimensions
	 *            the including dimensions input.
	 * @return List<TopSearch>
	 */
	List<TopSearch> getTopSearches(String sDate, String eDate,
			String searchOrder, int depth, int numberOfRecords,
			String[] searchedDimensions);

	/**
	 * Returns a list of type RequestByHour for Request By Hour report
	 * 
	 * @param sDate
	 *            the from date input
	 * @param eDate
	 *            the to date input
	 * @return List<RequestByHour>
	 */
	List<RequestByHour> getRequestByHours(String sDate, String eDate);

	/**
	 * Returns a list of type RequestBy for Request By report
	 * 
	 * @param sDate
	 *            the from date input
	 * @param eDate
	 *            the to date input
	 * @return List<RequestBy>
	 */
	List<RequestBy> getRequestBy(String sDate, String eDate, String sTime);

	/**
	 * Returns a list of type RequestBy for Request By report
	 * 
	 * @param sDate
	 *            the from date input
	 * @param eDate
	 *            the to date input
	 * @return List<RequestBy>
	 */
	List<CorrectTerms> getAutoCorrectedSearchTerms(String sDate, String eDate);

	/**
	 * Returns all the distinct dimensions in the valuetable table
	 * 
	 * @return an arraylist of dimensions
	 */
	List<String> getDimensions();

	/**
	 * Return the last date exists in the mainlogs table
	 * 
	 * @return last date
	 */
	String getLastDate();

	/**
	 * Returns the number of navigate-only requests
	 * 
	 * @param fDate
	 * @param tDate
	 * @return number of navigate-only requests
	 */
	int getNaviRequest(String fDate, String tDate);

	/**
	 * Returns the number of search-only requests
	 * 
	 * @param fDate
	 * @param tDate
	 * @return number of search-only requests
	 */
	int getSearchRequest(String fDate, String tDate);

	/**
	 * Returns the number of navigate then search requests
	 * 
	 * @param fDate
	 * @param tDate
	 * @return number of navigate then search requests
	 */
	int getBothRequest(String fDate, String tDate);

	/**
	 * Creates a materialized table for Top Dimension Search with number of
	 * dimensions equals 1 and search order important equals yes
	 */
	void createDimensionsJoinedOneWithSeq();

	/**
	 * Creates a materialized table for Top Dimension Search with number of
	 * dimensions equals 2 and search order important equals yes
	 */
	void createDimensionsJoinedTwoWithSeq();

	/**
	 * Creates a materialized table for Top Dimension Search with number of
	 * dimensions equals 3 and search order important equals yes
	 */
	void createDimensionsJoinedThreeWithSeq();

	/**
	 * Creates a materialized table for Top Dimension Search with number of
	 * dimensions equals 1 and search order important equals no
	 */
	void createDimensionsJoinedOneNoSeq();

	/**
	 * Creates a materialized table for Top Dimension Search with number of
	 * dimensions equals 2 and search order important equals no
	 */
	void createDimensionsJoinedTwoNoSeq();

	/**
	 * Creates a materialized table for Top Dimension Search with number of
	 * dimensions equals 3 and search order important equals no
	 */
	void createDimensionsJoinedThreeNoSeq();

	/**
	 * Creates a materialized table for Top Dimension-Value Search with number
	 * of dimension-value pairs equals 1
	 */
	void createDimensionValuesJoinedOne();

	/**
	 * Creates a materialized table for Top Dimension-Value Search with number
	 * of dimension-value pairs equals 2
	 */
	void createDimensionValuesJoinedTwo();

	/**
	 * Creates a materialized table for Top Dimension-Value Search with number
	 * of dimension-value pairs equals 3
	 */
	void createDimensionValuesJoinedThree();

	/*
	 * Creates a materialized table for request by a user per day (search,
	 * navigate and all)
	 */
	void createRequestByUserTable();

	boolean createTimePerformanceTable();

	boolean destroyTimePerformanceTable();

	/**
	 * Returns a list of dimension & value pairs from the valuetable table using
	 * ":" to separate them
	 * 
	 * @return an arraylist of dimension & value pairs
	 */

	List<String> getDimensionValues();

	/**
	 * Returns a list of distinct values for a selected dimension
	 * 
	 * @param dim
	 *            the dimension selected
	 * @return an arraylist of values
	 */
	List<String> getDimensionValues(String dim);

	/**
	 * Returns a list of type SessionByHour for the Session By Hour report
	 * 
	 * @param sDate
	 *            the from date input
	 * @param eDate
	 *            the to date input
	 * @return an arraylist of results
	 */
	List<SessionByHour> getSessionByHours(String sDate, String eDate);

	/**
	 * Returns a list of type SessionByTime for the Session By report
	 * 
	 * @param sDate
	 *            the from date input
	 * @param eDate
	 *            the to date input
	 * @param sTime
	 *            the breakdown selection
	 * @return an arraylist of results
	 */
	List<SessionBy> getSessionBy(String sDate, String eDate, String sTime);

	/**
	 * Returns a list of type RequestByTime for the Request By report
	 * 
	 * @param sDate
	 *            the from date input
	 * @param eDate
	 *            the to date input
	 * @param sTime
	 *            the breakdown selection
	 * @return an arraylist of results
	 */
	// =======================begin=======================
	// List<RequestBy> getRequestBy(String sDate, String eDate, String sTime);
	// =============end by Tim at 04/19/2012============
	/**
	 * Returns a list of type TopDimensionValues for the Top Dimension-Value
	 * Search report
	 * 
	 * @param sDate
	 *            the from date input
	 * @param eDate
	 *            the to date input
	 * @param depth
	 *            the number of dimension-value pairs (1, 2, 3)
	 * @param numberOfRecords
	 *            the number of records input
	 * @param searchedDimensions
	 *            the dimension selected of the first dimension-value pair
	 * @param searchedValues
	 *            the value selected of the first dimension-value pair
	 * @param searchedDimensions1
	 *            the dimension selected of the second dimension-value pair
	 * @param searchedValues1
	 *            the value selected of the second dimension-value pair
	 * @return an arraylist of results
	 */
	List<TopDimensionValues> getTopDimensionValues(String sDate, String eDate,
			int depth, int numberOfRecords, String[] searchedDimensions,
			String[] searchedValues, String[] searchedDimensions1,
			String[] searchedValues1);

	/**
	 * Returns a list of type TopKeywordsSearch for the Top Keywords Search
	 * report
	 * 
	 * @param fromDate
	 *            the form date input
	 * @param toDate
	 *            the to date input
	 * @param resultType
	 *            the type selected (All, ReturnZeroRecords, AutoCorrected)
	 * @param numRecords
	 *            the number of records input
	 * @return an arraylist of results
	 */
	List<TopKeywordsSearch> getTopKeywordsSearches(String fromDate,
			String toDate, String resultType, int numRecords);

	/**
	 * Returns number of requests for each week between the from date and the to
	 * date
	 * 
	 * @param fromDateOfTrend
	 *            the from date of trend input
	 * @param toDateOfTrend
	 *            the to date of trend input
	 * @param incDimsOfTrend
	 *            the dimension selected of the first dimension-value pair
	 * @param incValsOfTrend
	 *            the value selected of the first dimension-value pair
	 * @param incDims1OfTrend
	 *            the dimension selected of the second dimension-value pair
	 * @param incVals1OfTrend
	 *            the value selected of the second dimension-value pair
	 * @return an integer of week number
	 */

	/**
	 * Returns the keyword search result between a range date
	 * 
	 * @param fromDate
	 *            the from date of keyword search result
	 * @param toDate
	 *            the to date of keyword search result
	 * @return a list of KeywordSearchResults
	 */
	List<KeywordSearch> getKeywordSearchResult(String fromDate, String toDate,
			int llimit, int hlimit, boolean nolimit);

	/**
	 * Returns the keyword search result between a range date
	 * 
	 * @param fromDate
	 *            the from date of keyword search result
	 * @param toDate
	 *            the to date of keyword search result
	 * @return a list of KeywordSearchResults
	 */
	List<KeywordSearchSummary> getKeywordSearchResultSummary(String fromDate,
			String toDate);

	/**
	 * Returns a list of number of requests in type of search only/navigation
	 * only/both by users between a time range
	 * 
	 * @param fromDate
	 *            the start of the time range
	 * @param toDate
	 *            the end of the time range
	 * @param requestType
	 *            the request Type could be search only/navigation only/both
	 * @return a list of RequestByUser_SearchOrNavigate
	 */
	ArrayList<RequestByUser_SearchOrNavigate> getRequestByUser_SearchOrNavigateOnlyRequests(
			String fromDate, String toDate, String requestType);

	/**
	 * Returns the count of distinct users who do search
	 * 
	 * @param fromDate
	 *            the start of the time range
	 * @param toDate
	 *            the end of the time range
	 * @return the count of distinct users who do search
	 */
	int getRequestByUser_AllSearchUsers(String fromDate, String toDate);

	/**
	 * Returns the count of distinct users who do navigation
	 * 
	 * @param fromDate
	 *            the start of the time range
	 * @param toDate
	 *            the end of the time range
	 * @return the count of distinct users who do navigation
	 */
	int getRequestByUser_AllNavigateUsers(String fromDate, String toDate);

	/**
	 * Returns the count of distinct users who do both search and navigation
	 * 
	 * @param fromDate
	 *            the start of the time range
	 * @param toDate
	 *            the end of the time range
	 * @return the count of distinct users who do both search and navigation
	 */
	int getRequestByUser_SearchAndNavigateUsers(String fromDate, String toDate);

	/**
	 * Returns a list of RequestByUser_SearchAndNavigate which contains
	 * numberOfUsers, numberOfSearches, numberOfNavigates amount of user, amount
	 * of search, amount of navigate
	 * 
	 * @param fromDate
	 *            the start of the time range
	 * @param toDate
	 *            the end of the time range
	 * @return a list of RequestByUser_SearchAndNavigate
	 */
	ArrayList<RequestByUser_SearchAndNavigate> getRequestByUser_SearchAndNavigateRequests(
			String fromDate, String toDate);

	/**
	 * Returns a list of SessionByMinutes which contain minutes and
	 * corresponding amount of sessions.
	 * 
	 * @param fromDate
	 *            the start of the time range
	 * @param toDate
	 *            the end of the time range
	 * @return a list of SessionByMinutes
	 */
	ArrayList<SessionByMinutes> getSessionByMinutes(String fromDate,
			String toDate);

	/**
	 * @param fromDateOfTrend
	 *            start of the time range
	 * @param toDateOfTrend
	 *            end of the time range
	 * @param incDimsOfTrend
	 *            array of dimensions group included
	 * @param incValsOfTrend
	 *            array of values group included
	 * @param incDims1OfTrend
	 *            array of second dimension group included
	 * @param incVals1OfTrend
	 *            array of second value group included
	 * @return 0 by default, or
	 * @return total amount of numofoccurences
	 */
	int getWeekTrend(String fromDateOfTrend, String toDateOfTrend,
			String[] incDimsOfTrend, String[] incValsOfTrend,
			String[] incDims1OfTrend, String[] incVals1OfTrend);

	/**
	 * Returns the earliest date exists in the mainlogs table
	 * 
	 * @return the earliest date exists in the mainlogs table
	 */
	String getFirstDate();

	/**
	 * Changes the MySQL engine to MyISAM
	 * 
	 */
	void changeEngineToMyISAM();

	/**
	 * Changes the MySQL engine to InnoDB
	 * 
	 */
	void changeEngineToInnoDB();

	/**
	 * Returns a list of RequestBy which contains time, ftime and requests.
	 * 
	 * @param sDate
	 * @param eDate
	 * @param dimension
	 * @param value
	 * @return a list of RequestBy
	 */
	List<RequestBy> getRequestByDimVal(String sDate, String eDate,
			String dimension, String value);

	/**
	 * Returns a HashMap which contain string and corresponding SimpleUserTrends
	 * object
	 * 
	 * @param Dimension
	 * @return a HashMap which contain string and corresponding SimpleUserTrends
	 *         object
	 */
	public HashMap<String, List<SimpleUserTrends>> getNormalizedDatas(
			String Dimension);

	double getTimeConsume(String Identifier, int TimeScope,
			TimeEstimation.Type type);

	List<TimePerformance> getAllTimeScopeAndTimeConsume(String Identifier,
			TimeEstimation.Type type);

	void UpdateTimeConsumeTable(String Identifier, int TimeScope,
			double TimeConsume1, double TimeConsume2, double TimeConsume3);

}
