package dbAccess;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcDaoSupport;

import others.TimeEstimation;
import others.TimeEstimation.Type;

import DataMining.StandardDeviation;

import reportDataObjects.*;
import springMVC.FormFormater;

/**
 * An implementation of interface Dao. This class accesses the database, runs
 * queries then returns results.
 * 
 * @author
 * @version 2
 * @modified by Tim 20/4/2012
 */
public class JdbcDaoImp extends SimpleJdbcDaoSupport implements Dao
{

	ArrayList<String> attributes = new ArrayList<String>();
	ArrayList<Object> values = new ArrayList<Object>();
	ArrayList<String> metadata = new ArrayList<String>();
	ArrayList<String> dimensions = new ArrayList<String>();
	ArrayList<String> dvalues = new ArrayList<String>();
	ArrayList<Integer> valueTableIds = new ArrayList<Integer>();

	@Override
	public int checkMainLogsRowId()
	{
		int id = getSimpleJdbcTemplate().queryForInt(
				"select count(*) from mainlogs");
		return id;
	}

	@Override
	public int getMainLogsRowId()
	{
		int id = getSimpleJdbcTemplate().queryForInt(
				"select max(Id) from mainlogs");
		return id;
	}

	@Override
	public void insertValueTable(String dim, String val)
	{
		getSimpleJdbcTemplate().update(
				"insert into valuetable (Dimension,Value) values (?,?)", dim,
				val);
	}

	@Override
	public int getValueTableId(String dim, String val)
	{
		int id = getSimpleJdbcTemplate().queryForInt(
				"select Id from valuetable where Dimension = ? and Value = ?",
				new Object[]
				{ dim, val });
		return id;
	}

	@Override
	public int checkValueTable(String dim, String val)
	{
		int id = getSimpleJdbcTemplate()
				.queryForInt(
						"select count(*) from valuetable where Dimension = ? and Value = ?",
						new Object[]
						{ dim, val });
		return id;
	}

	@Override
	public void loadLogs(String filePath)
	{
		try
		{
			File linkFile = new File(filePath);
			String[] fname = linkFile.list();
			int fnameLength = fname.length;
			for (int i = 0; i < fnameLength; i++)
			{
				if (fname[i].startsWith("LogServer"))
				{
					if (getSimpleJdbcTemplate().queryForInt(
							"select count(*) from files where File = ?",
							new Object[]
							{ fname[i] }) == 0)
					{
						System.out.println(fname[i]);
						FileInputStream fstream = new FileInputStream(filePath
								+ "\\" + fname[i]);
						BufferedReader input = new BufferedReader(
								new InputStreamReader(fstream));
						String next = input.readLine();
						while (true)
						{
							if (next == null)
							{
								fstream.close();
								input.close();
								break;
							}
							while (next != null)
							{
								attributes.clear();
								values.clear();
								dimensions.clear();
								dvalues.clear();
								valueTableIds.clear();
								String cols = "";
								String qMark = "";
								String tempValue = "";
								for (String substring : next.split("\\{|,|\\}"))
								{
									if (substring
											.matches("L\\d{4}_\\d{2}_\\d{2}\\.\\d{2}_\\d{2}_\\d{2}"))
									{
										for (String date : substring
												.split("\\."))
										{
											if (date.startsWith("L"))
											{
												for (String day : date
														.split("L"))
												{
													if (!day.isEmpty())
													{
														attributes.add("Date");
														values.add(day);
													}
												}
											} else
											{
												attributes.add("Time");
												date = date
														.replaceAll("_", ":");
												values.add(date);
											}
										}
									} else if (substring.startsWith("DIMS="))
									{
										for (String dims : substring
												.split("DIMS=|%2c"))
										{
											if (!dims.isEmpty())
											{
												dims = dims.replaceAll(" ", "");
												dimensions.add(dims);
											}
										}
									} else if (substring.startsWith("DVALS="))
									{
										for (String dvals : substring
												.split("DVALS=|%2c"))
										{
											if (!dvals.isEmpty())
											{
												if (dvals
														.startsWith("/Category/"))
												{
													for (String lastValue : dvals
															.split("/Category/"))
													{
														tempValue = lastValue;
													}
													dvalues.add(tempValue);
												} else
												{
													for (String lastValue : dvals
															.split("/"))
													{
														tempValue = lastValue;
													}
													dvalues.add(tempValue);
												}

											}
										}
									} else if (substring
											.startsWith("NUM_RECORDS="))
									{
										for (String num : substring
												.split("NUM_RECORDS="))
										{
											if (!num.isEmpty())
											{
												attributes
														.add("NumberofRecords");
												values.add(Integer
														.parseInt(num));
											}
										}
									} else if (substring
											.startsWith("SEARCH_TERMS="))
									{
										for (String sTerm : substring
												.split("SEARCH_TERMS="))
										{
											if (!sTerm.isEmpty())
											{
												attributes.add("SearchTerms");
												values.add(sTerm);
											}
										}
									} else if (substring
											.startsWith("AUTOCORRECT_TO="))
									{
										for (String cTerm : substring
												.split("AUTOCORRECT_TO="))
										{
											if (!cTerm.isEmpty())
											{
												attributes.add("AutoCorrectTo");
												values.add(cTerm);
											}
										}
									} else if (substring
											.startsWith("NUMREFINEMENTS="))
									{
										for (String nRefinement : substring
												.split("NUMREFINEMENTS="))
										{
											if (!nRefinement.isEmpty())
											{
												attributes
														.add("NumberofRefinements");
												values.add(Integer
														.parseInt(nRefinement));
											}
										}
									} else if (substring
											.startsWith("SESSION_ID="))
									{
										for (String SID : substring
												.split("SESSION_ID="))
										{
											if (!SID.isEmpty())
											{
												attributes.add("SessionID");
												values.add(SID);
											}
										}
									}
								}

								if (attributes.size() < 5)
								{
									next = input.readLine();
									break;
								}

								int dimensionSize = dimensions.size();
								for (int r = 0; r < dimensionSize; r++)
								{
									int o = checkValueTable(dimensions.get(r),
											dvalues.get(r));
									if (o == 0)
									{
										insertValueTable(dimensions.get(r),
												dvalues.get(r));
									}
									o = getValueTableId(dimensions.get(r),
											dvalues.get(r));
									valueTableIds.add(o);
								}

								int attributeSize = attributes.size();
								for (int n = 0; n < attributeSize; n++)
								{
									cols += attributes.get(n) + ",";
									qMark += "?,";
								}
								cols = cols.substring(0, cols.length() - 1);
								qMark = qMark.substring(0, qMark.length() - 1);
								final String query = "insert into mainlogs("
										+ cols + ") values(" + qMark + ")";
								getSimpleJdbcTemplate().getJdbcOperations()
										.update(new PreparedStatementCreator()
										{
											public PreparedStatement createPreparedStatement(
													Connection connection)
													throws SQLException
											{
												PreparedStatement ps = connection
														.prepareStatement(query);
												int valuesSize = values.size();
												for (int k = 0; k < valuesSize; k++)
												{
													ps.setObject(k + 1,
															values.get(k));
												}
												return ps;
											}
										});
								for (int q = 0; q < dimensionSize; q++)
								{
									getSimpleJdbcTemplate()
											.update("insert into dimensions (LogId, SequenceId, ValueTableId) values (?,?,?)",
													new Object[]
													{
															getMainLogsRowId(),
															q + 1,
															valueTableIds
																	.get(q) });
								}
								next = input.readLine();
							}
						}
						getSimpleJdbcTemplate().update(
								"insert into files (File) values(?)",
								new Object[]
								{ fname[i] });

						this.getConnection().commit();
					}
				}
			}
		} catch (Exception e)
		{

		}

	}

	@Override
	public void createTableAdmin()
	{
		String query = "create table if not exists admin(Id int not null primary key auto_increment,UNAME VARCHAR(20),PWD VARCHAR(20)) ENGINE=INNODB";
		getSimpleJdbcTemplate().update(query);
		System.out.println("table admin created.");
	}

	@Override
	public void createTableMainlogs()
	{
		String query = "create table if not exists mainlogs(Id int not null primary key auto_increment, Date DATE, Time TIME, NumberofRefinements int, NumberofRecords int,SearchTerms VARCHAR(100),AutoCorrectTo VARCHAR(100), SessionID VARCHAR(60)) ENGINE=INNODB";
		String query1 = "CREATE INDEX idx_mainlogs_date ON mainlogs(date)";
		getSimpleJdbcTemplate().update(query);
		getSimpleJdbcTemplate().update(query1);
		System.out.println("table mainlogs created.");
	}

	@Override
	public void createTableDimensions()
	{
		String query = "create table if not exists dimensions (Id int not null primary key auto_increment,LogId int,SequenceId int,ValueTableId int) ENGINE=INNODB";
		String query1 = "create index idx_dimensions_logid on dimensions(logid)";
		String query2 = "create index idx_dimensions_valuetableid on dimensions(valuetableid)";
		getSimpleJdbcTemplate().update(query);
		getSimpleJdbcTemplate().update(query1);
		getSimpleJdbcTemplate().update(query2);
		System.out
				.println("table dimensions created with dimensions.logid, dimensions.valuetableid indexed.");
	}

	@Override
	public void createTableFile()
	{
		String query = "create table if not exists files(Id int not null primary key auto_increment,File VARCHAR(30)) ENGINE=INNODB";
		getSimpleJdbcTemplate().update(query);
		System.out.println("table files created.");
	}

	@Override
	public void createTableValues()
	{
		String query = "create table if not exists valuetable(Id int not null primary key auto_increment,Dimension varchar(50),Value varchar(100)) ENGINE=INNODB";
		getSimpleJdbcTemplate().update(query);
		System.out.println("table valuetable created.");
	}

	@Override
	public void deleteFromMainlogsTable(String sDate, String eDate)
	{
		String query = "delete from mainlogs where Date >= ? and Date <= ?";
		getSimpleJdbcTemplate().update(query, new Object[]
		{ sDate, eDate });
	}

	@Override
	public void deleteFromDimensionsTable()
	{
		String query = "delete from dimensions where LogId not in (select Id from mainlogs)";
		getSimpleJdbcTemplate().update(query);
	}

	@Override
	public boolean checkAdmin(String uname, String pwd)
	{
		boolean exist = false;
		int id = getSimpleJdbcTemplate().queryForInt(
				"select count(*) from admin where uname = ? and pwd = ?",
				new Object[]
				{ uname, pwd });
		if (id != 0)
		{
			exist = true;
		}
		return exist;
	}

	@Override
	public List<TopSearch> getTopSearches(String sDate, String eDate,
			final String searchOrder, final int numberOfRecords, int depth,
			final String[] searchedDimensions)
	{
		String query = "";
		String totalQuery = "";
		String searchedDimensionsClause = "";
		/*
		 * if(!searchedDimensions[0].equals("All")) { searchedDimensionsClause
		 * += " AND"; int numOfSearchedDimensions = depth;
		 * if(numOfSearchedDimensions==1) { searchedDimensionsClause +=
		 * " dimension LIKE '%" + searchedDimensions[0] + "%' AND"; } else {
		 * for(int i=0; i < numOfSearchedDimensions; i++) {
		 * searchedDimensionsClause += " dim" + Integer.toString(i+1);
		 * searchedDimensionsClause += " LIKE '%" + searchedDimensions[i] +
		 * "%' AND"; } } searchedDimensionsClause =
		 * searchedDimensionsClause.substring(0,
		 * searchedDimensionsClause.length() - 4); }
		 */
		int numOfSearchedDimensions = depth;
		if (numOfSearchedDimensions == 1
				&& !searchedDimensions[0].equals("All"))
		{
			System.out.println("in if");
			searchedDimensionsClause += "AND dimension LIKE '%"
					+ searchedDimensions[0] + "%'";
		} else if (numOfSearchedDimensions > 1)
		{
			System.out.println("in else if");
			searchedDimensionsClause += " AND";
			for (int i = 0; i < numOfSearchedDimensions; i++)
			{
				if (!searchedDimensions[i].equals("All"))
				{
					searchedDimensionsClause += " dim"
							+ Integer.toString(i + 1);
					searchedDimensionsClause += " LIKE '%"
							+ searchedDimensions[i] + "%' AND";
				}
			}
			searchedDimensionsClause = searchedDimensionsClause.substring(0,
					searchedDimensionsClause.length() - 4);
		}
		String query1 = "SELECT dimension AS dim1, SUM(numofoccurences) AS numofocc FROM mat_dimensions_one WHERE Date>=? AND Date<=?"
				+ searchedDimensionsClause
				+ " GROUP BY dim1 ORDER BY numofocc DESC LIMIT ?";
		String query1total = "SELECT SUM(numofoccurences) FROM mat_dimensions_one WHERE Date>=? AND Date<=?";
		String query2 = "SELECT dim1, dim2, SUM(numofoccurences) AS numofocc FROM mat_dimensions_two WHERE Date>=? AND Date<=?"
				+ searchedDimensionsClause
				+ " GROUP BY dim1,dim2 ORDER BY numofocc DESC LIMIT ?";
		String query2total = "SELECT SUM(numofoccurences) FROM mat_dimensions_two WHERE Date>=? AND Date<=?";
		String query3 = "SELECT dim1, dim2, dim3, SUM(numofoccurences) AS numofocc FROM mat_dimensions_three WHERE Date>=? AND Date<=?"
				+ searchedDimensionsClause
				+ " GROUP BY dim1,dim2,dim3 ORDER BY numofocc DESC LIMIT ?";
		String query3total = "SELECT SUM(numofoccurences) FROM mat_dimensions_three WHERE Date>=? AND Date<=?";
		String query1s = "SELECT dimension AS dim1, SUM(numofoccurences) AS numofocc FROM mat_dimensions_one_seq WHERE Date>=? AND Date<=?"
				+ searchedDimensionsClause
				+ " GROUP BY dim1 ORDER BY numofocc DESC LIMIT ?";
		String query1stotal = "SELECT SUM(numofoccurences) FROM mat_dimensions_one_seq WHERE Date>=? AND Date<=?";
		String query2s = "SELECT dim1, dim2, SUM(numofoccurences) AS numofocc FROM mat_dimensions_two_seq WHERE Date>=? AND Date<=?"
				+ searchedDimensionsClause
				+ " GROUP BY dim1,dim2 ORDER BY numofocc DESC LIMIT ?";
		String query2stotal = "SELECT SUM(numofoccurences) FROM mat_dimensions_two_seq WHERE Date>=? AND Date<=?";
		String query3s = "SELECT dim1, dim2, dim3, SUM(numofoccurences) AS numofocc FROM mat_dimensions_three_seq WHERE Date>=? AND Date<=?"
				+ searchedDimensionsClause
				+ " GROUP BY dim1,dim2,dim3 ORDER BY numofocc DESC LIMIT ?";
		String query3stotal = "SELECT SUM(numofoccurences) FROM mat_dimensions_three_seq WHERE Date>=? AND Date<=?";

		if (depth == 1 && searchOrder.equals("No"))
		{
			query = query1;
			totalQuery = query1total;
		} else if (depth == 2 && searchOrder.equals("No"))
		{
			query = query2;
			totalQuery = query2total;
		} else if (depth == 3 && searchOrder.equals("No"))
		{
			query = query3;
			totalQuery = query3total;
		} else if (depth == 1 && searchOrder.equals("Yes"))
		{
			query = query1s;
			totalQuery = query1stotal;
		} else if (depth == 2 && searchOrder.equals("Yes"))
		{
			query = query2s;
			totalQuery = query2stotal;
		} else if (depth == 3 && searchOrder.equals("Yes"))
		{
			query = query3s;
			totalQuery = query3stotal;
		}
		System.out.println(query);
		System.out.println(totalQuery);
		// System.out.println(query);
		List<TopSearch> tempTopDimensions = getSimpleJdbcTemplate().query(
				query, new RowMapper<TopSearch>()
				{
					public TopSearch mapRow(ResultSet rs, int rowNum)
							throws SQLException
					{
						TopSearch td = new TopSearch();
						if (rs == null)
						{
							return null;
						}
						ResultSetMetaData rsMetaData = rs.getMetaData();
						int depth = (rsMetaData.getColumnCount() - 1);
						String[] dims = new String[depth];

						for (int i = 0; i < depth; i++)
						{
							dims[i] = rs.getString(i + 1);
						}
						td.setDimensions(dims);
						td.setCount(rs.getInt(rsMetaData.getColumnCount()));
						return td;
					}
				}, new Object[]
				{ sDate, eDate, numberOfRecords });

		// Getting the total number of records
		int total = getSimpleJdbcTemplate().queryForInt(totalQuery,
				new Object[]
				{ sDate, eDate });

		// List<TopSearch> topDimensions = new ArrayList<TopSearch>();

		int numberOfExistingRecords = tempTopDimensions.size();

		for (int i = 0; i < numberOfExistingRecords; i++)
		{
			tempTopDimensions.get(i).setTotal(total);
		}
		return tempTopDimensions;
	}

	@Override
	public List<String> getDimensions()
	{
		String query = "select distinct dimension from valuetable order by dimension";
		List<String> dims = getSimpleJdbcTemplate().query(query,
				new RowMapper<String>()
				{
					public String mapRow(ResultSet rs, int rowNum)
							throws SQLException
					{
						return rs.getString("dimension");
					}
				});
		return dims;
	}

	@Override
	public String getLastDate()
	{
		String query = "select max(date) from mainlogs";
		return getSimpleJdbcTemplate().queryForObject(query, String.class);
	}

	@Override
	public int getNaviRequest(String fDate, String tDate)
	{
		int id = getSimpleJdbcTemplate()
				.queryForInt(
						"select count(*) from mainlogs where searchterms is null and date>=? and date<=?",
						fDate, tDate);
		return id;
	}

	@Override
	public int getSearchRequest(String fDate, String tDate)
	{
		int id = getSimpleJdbcTemplate()
				.queryForInt(
						"select count(*) from (select id from mainlogs where id not in(select logid "
								+ "from dimensions) and searchterms is not null and date>=? and date<=?) t1",
						fDate, tDate);
		return id;
	}

	@Override
	public int getBothRequest(String fDate, String tDate)
	{
		int id = getSimpleJdbcTemplate()
				.queryForInt(
						"select count(*) from (select id from mainlogs where id in(select logid "
								+ "from dimensions) and searchterms is not null and date>=? and date<=?) t1",
						fDate, tDate);
		return id;
	}

	@Override
	public void createDimensionsJoinedOneWithSeq()
	{
		String query0 = "DROP TABLE IF EXISTS mat_dimensions_one_seq_temp";
		String query = "CREATE TABLE mat_dimensions_one_seq_temp ENGINE=MyISAM SELECT date, dimension, COUNT(*) AS numofoccurences FROM dimensions, valuetable, mainlogs WHERE mainlogs.id=dimensions.LogId AND dimensions.valuetableid=valuetable.id AND dimensions.sequenceid=1 GROUP BY date, dimension ORDER BY NULL";
		String query1 = "DROP TABLE IF EXISTS mat_dimensions_one_seq";
		String query2 = "RENAME TABLE mat_dimensions_one_seq_temp TO mat_dimensions_one_seq";
		String query3 = "ALTER TABLE mat_dimensions_one_seq PARTITION BY RANGE(TO_DAYS(date)) (PARTITION p1 VALUES LESS THAN (TO_DAYS('2010-07-01')),PARTITION p2 VALUES LESS THAN (TO_DAYS('2010-08-01')),PARTITION p3 VALUES LESS THAN (TO_DAYS('2010-09-01')),PARTITION p4 VALUES LESS THAN (TO_DAYS('2010-10-01')),PARTITION p5 VALUES LESS THAN (TO_DAYS('2010-11-01')),PARTITION p6 VALUES LESS THAN (TO_DAYS('2010-12-01')),PARTITION p7 VALUES LESS THAN (TO_DAYS('2011-01-01')),PARTITION p8 VALUES LESS THAN (TO_DAYS('2011-02-01')),PARTITION p9 VALUES LESS THAN (TO_DAYS('2011-03-01')),PARTITION p10 VALUES LESS THAN (TO_DAYS('2011-04-01')),PARTITION p11 VALUES LESS THAN (TO_DAYS('2011-05-01')),PARTITION p12 VALUES LESS THAN (TO_DAYS('2011-06-01')),PARTITION p13 VALUES LESS THAN (TO_DAYS('2011-07-01')),PARTITION p14 VALUES LESS THAN (TO_DAYS('2011-08-01')),PARTITION p15 VALUES LESS THAN (TO_DAYS('2011-09-01')),PARTITION p16 VALUES LESS THAN (TO_DAYS('2011-10-01')),PARTITION p17 VALUES LESS THAN (TO_DAYS('2011-11-01')),PARTITION p18 VALUES LESS THAN (TO_DAYS('2011-12-01')),PARTITION p19 VALUES LESS THAN (TO_DAYS('2012-01-01')),PARTITION p20 VALUES LESS THAN (TO_DAYS('2012-02-01')),PARTITION p21 VALUES LESS THAN (TO_DAYS('2012-03-01')),PARTITION p22 VALUES LESS THAN (TO_DAYS('2012-04-01')),PARTITION p23 VALUES LESS THAN (TO_DAYS('2012-05-01')),PARTITION p24 VALUES LESS THAN (TO_DAYS('2012-06-01')),PARTITION p25 VALUES LESS THAN (TO_DAYS('2012-07-01')),PARTITION p26 VALUES LESS THAN (TO_DAYS('2012-08-01')),PARTITION p27 VALUES LESS THAN (TO_DAYS('2012-09-01')),PARTITION p28 VALUES LESS THAN (TO_DAYS('2012-10-01')),PARTITION p29 VALUES LESS THAN (TO_DAYS('2012-11-01')),PARTITION p30 VALUES LESS THAN (TO_DAYS('2012-12-01')),PARTITION p31 VALUES LESS THAN (MAXVALUE))";
		getSimpleJdbcTemplate().update(query0);
		getSimpleJdbcTemplate().update(query);
		getSimpleJdbcTemplate().update(query1);
		getSimpleJdbcTemplate().update(query2);
		getSimpleJdbcTemplate().update(query3);
	}

	@Override
	public void createDimensionsJoinedTwoWithSeq()
	{
		String query0 = "DROP TABLE IF EXISTS mat_dimensions_two_seq_temp";
		String query = "CREATE TABLE mat_dimensions_two_seq_temp ENGINE=MyISAM SELECT date, v1.dimension AS dim1, v2.dimension AS dim2, COUNT(*) AS numofoccurences FROM dimensions t1, dimensions t2, valuetable v1, valuetable v2, mainlogs WHERE t1.logid = t2.logid AND t1.id != t2.id AND mainlogs.id=t1.LogId AND t1.valuetableid=v1.id AND t2.valuetableid=v2.id AND t1.sequenceid=1 AND t2.sequenceid=2 GROUP BY date, dim1, dim2 ORDER BY NULL";
		String query1 = "DROP TABLE IF EXISTS mat_dimensions_two_seq";
		String query2 = "RENAME TABLE mat_dimensions_two_seq_temp TO mat_dimensions_two_seq";
		String query3 = "ALTER TABLE mat_dimensions_two_seq PARTITION BY RANGE(TO_DAYS(date)) (PARTITION p1 VALUES LESS THAN (TO_DAYS('2010-07-01')),PARTITION p2 VALUES LESS THAN (TO_DAYS('2010-08-01')),PARTITION p3 VALUES LESS THAN (TO_DAYS('2010-09-01')),PARTITION p4 VALUES LESS THAN (TO_DAYS('2010-10-01')),PARTITION p5 VALUES LESS THAN (TO_DAYS('2010-11-01')),PARTITION p6 VALUES LESS THAN (TO_DAYS('2010-12-01')),PARTITION p7 VALUES LESS THAN (TO_DAYS('2011-01-01')),PARTITION p8 VALUES LESS THAN (TO_DAYS('2011-02-01')),PARTITION p9 VALUES LESS THAN (TO_DAYS('2011-03-01')),PARTITION p10 VALUES LESS THAN (TO_DAYS('2011-04-01')),PARTITION p11 VALUES LESS THAN (TO_DAYS('2011-05-01')),PARTITION p12 VALUES LESS THAN (TO_DAYS('2011-06-01')),PARTITION p13 VALUES LESS THAN (TO_DAYS('2011-07-01')),PARTITION p14 VALUES LESS THAN (TO_DAYS('2011-08-01')),PARTITION p15 VALUES LESS THAN (TO_DAYS('2011-09-01')),PARTITION p16 VALUES LESS THAN (TO_DAYS('2011-10-01')),PARTITION p17 VALUES LESS THAN (TO_DAYS('2011-11-01')),PARTITION p18 VALUES LESS THAN (TO_DAYS('2011-12-01')),PARTITION p19 VALUES LESS THAN (TO_DAYS('2012-01-01')),PARTITION p20 VALUES LESS THAN (TO_DAYS('2012-02-01')),PARTITION p21 VALUES LESS THAN (TO_DAYS('2012-03-01')),PARTITION p22 VALUES LESS THAN (TO_DAYS('2012-04-01')),PARTITION p23 VALUES LESS THAN (TO_DAYS('2012-05-01')),PARTITION p24 VALUES LESS THAN (TO_DAYS('2012-06-01')),PARTITION p25 VALUES LESS THAN (TO_DAYS('2012-07-01')),PARTITION p26 VALUES LESS THAN (TO_DAYS('2012-08-01')),PARTITION p27 VALUES LESS THAN (TO_DAYS('2012-09-01')),PARTITION p28 VALUES LESS THAN (TO_DAYS('2012-10-01')),PARTITION p29 VALUES LESS THAN (TO_DAYS('2012-11-01')),PARTITION p30 VALUES LESS THAN (TO_DAYS('2012-12-01')),PARTITION p31 VALUES LESS THAN (MAXVALUE))";
		getSimpleJdbcTemplate().update(query0);
		getSimpleJdbcTemplate().update(query);
		getSimpleJdbcTemplate().update(query1);
		getSimpleJdbcTemplate().update(query2);
		getSimpleJdbcTemplate().update(query3);
	}

	@Override
	public void createDimensionsJoinedThreeWithSeq()
	{
		String query0 = "DROP TABLE IF EXISTS mat_dimensions_three_seq_temp";
		String query = "CREATE TABLE mat_dimensions_three_seq_temp ENGINE=MyISAM SELECT date, v1.dimension AS dim1, v2.dimension AS dim2, v3.dimension AS dim3, COUNT(*) AS numofoccurences FROM dimensions t1, dimensions t2, dimensions t3, valuetable v1, valuetable v2, valuetable v3, mainlogs WHERE t1.logid = t2.logid AND t2.logid=t3.logid AND t1.id != t2.id AND t2.id != t3.id AND t1.id != t3.id AND mainlogs.id=t1.LogId AND t1.valuetableid=v1.id AND t2.valuetableid=v2.id AND t3.valuetableid=v3.id AND t1.sequenceid=1 AND t2.sequenceid=2 AND t3.sequenceid=3 GROUP BY date, dim1, dim2, dim3 ORDER BY NULL";
		String query1 = "DROP TABLE IF EXISTS mat_dimensions_three_seq";
		String query2 = "RENAME TABLE mat_dimensions_three_seq_temp TO mat_dimensions_three_seq";
		String query3 = "ALTER TABLE mat_dimensions_three_seq PARTITION BY RANGE(TO_DAYS(date)) (PARTITION p1 VALUES LESS THAN (TO_DAYS('2010-07-01')),PARTITION p2 VALUES LESS THAN (TO_DAYS('2010-08-01')),PARTITION p3 VALUES LESS THAN (TO_DAYS('2010-09-01')),PARTITION p4 VALUES LESS THAN (TO_DAYS('2010-10-01')),PARTITION p5 VALUES LESS THAN (TO_DAYS('2010-11-01')),PARTITION p6 VALUES LESS THAN (TO_DAYS('2010-12-01')),PARTITION p7 VALUES LESS THAN (TO_DAYS('2011-01-01')),PARTITION p8 VALUES LESS THAN (TO_DAYS('2011-02-01')),PARTITION p9 VALUES LESS THAN (TO_DAYS('2011-03-01')),PARTITION p10 VALUES LESS THAN (TO_DAYS('2011-04-01')),PARTITION p11 VALUES LESS THAN (TO_DAYS('2011-05-01')),PARTITION p12 VALUES LESS THAN (TO_DAYS('2011-06-01')),PARTITION p13 VALUES LESS THAN (TO_DAYS('2011-07-01')),PARTITION p14 VALUES LESS THAN (TO_DAYS('2011-08-01')),PARTITION p15 VALUES LESS THAN (TO_DAYS('2011-09-01')),PARTITION p16 VALUES LESS THAN (TO_DAYS('2011-10-01')),PARTITION p17 VALUES LESS THAN (TO_DAYS('2011-11-01')),PARTITION p18 VALUES LESS THAN (TO_DAYS('2011-12-01')),PARTITION p19 VALUES LESS THAN (TO_DAYS('2012-01-01')),PARTITION p20 VALUES LESS THAN (TO_DAYS('2012-02-01')),PARTITION p21 VALUES LESS THAN (TO_DAYS('2012-03-01')),PARTITION p22 VALUES LESS THAN (TO_DAYS('2012-04-01')),PARTITION p23 VALUES LESS THAN (TO_DAYS('2012-05-01')),PARTITION p24 VALUES LESS THAN (TO_DAYS('2012-06-01')),PARTITION p25 VALUES LESS THAN (TO_DAYS('2012-07-01')),PARTITION p26 VALUES LESS THAN (TO_DAYS('2012-08-01')),PARTITION p27 VALUES LESS THAN (TO_DAYS('2012-09-01')),PARTITION p28 VALUES LESS THAN (TO_DAYS('2012-10-01')),PARTITION p29 VALUES LESS THAN (TO_DAYS('2012-11-01')),PARTITION p30 VALUES LESS THAN (TO_DAYS('2012-12-01')),PARTITION p31 VALUES LESS THAN (MAXVALUE))";
		getSimpleJdbcTemplate().update(query0);
		getSimpleJdbcTemplate().update(query);
		getSimpleJdbcTemplate().update(query1);
		getSimpleJdbcTemplate().update(query2);
		getSimpleJdbcTemplate().update(query3);
	}

	@Override
	public void createDimensionsJoinedOneNoSeq()
	{
		String query0 = "DROP TABLE IF EXISTS mat_dimensions_one_temp";
		String query = "CREATE TABLE mat_dimensions_one_temp ENGINE=MyISAM SELECT date, dimension, COUNT(*) AS numofoccurences FROM dimensions, valuetable, mainlogs WHERE mainlogs.id=dimensions.LogId AND dimensions.valuetableid=valuetable.id GROUP BY date, dimension ORDER BY NULL";
		String query1 = "DROP TABLE IF EXISTS mat_dimensions_one";
		String query2 = "RENAME TABLE mat_dimensions_one_temp TO mat_dimensions_one";
		String query3 = "ALTER TABLE mat_dimensions_one PARTITION BY RANGE(TO_DAYS(date)) (PARTITION p1 VALUES LESS THAN (TO_DAYS('2010-07-01')),PARTITION p2 VALUES LESS THAN (TO_DAYS('2010-08-01')),PARTITION p3 VALUES LESS THAN (TO_DAYS('2010-09-01')),PARTITION p4 VALUES LESS THAN (TO_DAYS('2010-10-01')),PARTITION p5 VALUES LESS THAN (TO_DAYS('2010-11-01')),PARTITION p6 VALUES LESS THAN (TO_DAYS('2010-12-01')),PARTITION p7 VALUES LESS THAN (TO_DAYS('2011-01-01')),PARTITION p8 VALUES LESS THAN (TO_DAYS('2011-02-01')),PARTITION p9 VALUES LESS THAN (TO_DAYS('2011-03-01')),PARTITION p10 VALUES LESS THAN (TO_DAYS('2011-04-01')),PARTITION p11 VALUES LESS THAN (TO_DAYS('2011-05-01')),PARTITION p12 VALUES LESS THAN (TO_DAYS('2011-06-01')),PARTITION p13 VALUES LESS THAN (TO_DAYS('2011-07-01')),PARTITION p14 VALUES LESS THAN (TO_DAYS('2011-08-01')),PARTITION p15 VALUES LESS THAN (TO_DAYS('2011-09-01')),PARTITION p16 VALUES LESS THAN (TO_DAYS('2011-10-01')),PARTITION p17 VALUES LESS THAN (TO_DAYS('2011-11-01')),PARTITION p18 VALUES LESS THAN (TO_DAYS('2011-12-01')),PARTITION p19 VALUES LESS THAN (TO_DAYS('2012-01-01')),PARTITION p20 VALUES LESS THAN (TO_DAYS('2012-02-01')),PARTITION p21 VALUES LESS THAN (TO_DAYS('2012-03-01')),PARTITION p22 VALUES LESS THAN (TO_DAYS('2012-04-01')),PARTITION p23 VALUES LESS THAN (TO_DAYS('2012-05-01')),PARTITION p24 VALUES LESS THAN (TO_DAYS('2012-06-01')),PARTITION p25 VALUES LESS THAN (TO_DAYS('2012-07-01')),PARTITION p26 VALUES LESS THAN (TO_DAYS('2012-08-01')),PARTITION p27 VALUES LESS THAN (TO_DAYS('2012-09-01')),PARTITION p28 VALUES LESS THAN (TO_DAYS('2012-10-01')),PARTITION p29 VALUES LESS THAN (TO_DAYS('2012-11-01')),PARTITION p30 VALUES LESS THAN (TO_DAYS('2012-12-01')),PARTITION p31 VALUES LESS THAN (MAXVALUE))";
		getSimpleJdbcTemplate().update(query0);
		getSimpleJdbcTemplate().update(query);
		getSimpleJdbcTemplate().update(query1);
		getSimpleJdbcTemplate().update(query2);
		getSimpleJdbcTemplate().update(query3);
	}

	@Override
	public void createDimensionsJoinedTwoNoSeq()
	{
		String query0 = "DROP TABLE IF EXISTS mat_dimensions_two_temp";
		String query = "CREATE TABLE mat_dimensions_two_temp ENGINE=MyISAM SELECT date, v1.dimension AS dim1, v2.dimension AS dim2, COUNT(*) AS numofoccurences FROM dimensions t1, dimensions t2, valuetable v1, valuetable v2, mainlogs WHERE t1.logid = t2.logid AND t1.id != t2.id AND mainlogs.id=t1.LogId AND t1.valuetableid=v1.id AND t2.valuetableid=v2.id AND (v1.dimension<v2.dimension OR (v1.dimension=v2.dimension AND t1.sequenceid < t2.sequenceid)) GROUP BY date, dim1, dim2 ORDER BY NULL";
		String query1 = "DROP TABLE IF EXISTS mat_dimensions_two";
		String query2 = "RENAME TABLE mat_dimensions_two_temp TO mat_dimensions_two";
		String query3 = "ALTER TABLE mat_dimensions_two PARTITION BY RANGE(TO_DAYS(date)) (PARTITION p1 VALUES LESS THAN (TO_DAYS('2010-07-01')),PARTITION p2 VALUES LESS THAN (TO_DAYS('2010-08-01')),PARTITION p3 VALUES LESS THAN (TO_DAYS('2010-09-01')),PARTITION p4 VALUES LESS THAN (TO_DAYS('2010-10-01')),PARTITION p5 VALUES LESS THAN (TO_DAYS('2010-11-01')),PARTITION p6 VALUES LESS THAN (TO_DAYS('2010-12-01')),PARTITION p7 VALUES LESS THAN (TO_DAYS('2011-01-01')),PARTITION p8 VALUES LESS THAN (TO_DAYS('2011-02-01')),PARTITION p9 VALUES LESS THAN (TO_DAYS('2011-03-01')),PARTITION p10 VALUES LESS THAN (TO_DAYS('2011-04-01')),PARTITION p11 VALUES LESS THAN (TO_DAYS('2011-05-01')),PARTITION p12 VALUES LESS THAN (TO_DAYS('2011-06-01')),PARTITION p13 VALUES LESS THAN (TO_DAYS('2011-07-01')),PARTITION p14 VALUES LESS THAN (TO_DAYS('2011-08-01')),PARTITION p15 VALUES LESS THAN (TO_DAYS('2011-09-01')),PARTITION p16 VALUES LESS THAN (TO_DAYS('2011-10-01')),PARTITION p17 VALUES LESS THAN (TO_DAYS('2011-11-01')),PARTITION p18 VALUES LESS THAN (TO_DAYS('2011-12-01')),PARTITION p19 VALUES LESS THAN (TO_DAYS('2012-01-01')),PARTITION p20 VALUES LESS THAN (TO_DAYS('2012-02-01')),PARTITION p21 VALUES LESS THAN (TO_DAYS('2012-03-01')),PARTITION p22 VALUES LESS THAN (TO_DAYS('2012-04-01')),PARTITION p23 VALUES LESS THAN (TO_DAYS('2012-05-01')),PARTITION p24 VALUES LESS THAN (TO_DAYS('2012-06-01')),PARTITION p25 VALUES LESS THAN (TO_DAYS('2012-07-01')),PARTITION p26 VALUES LESS THAN (TO_DAYS('2012-08-01')),PARTITION p27 VALUES LESS THAN (TO_DAYS('2012-09-01')),PARTITION p28 VALUES LESS THAN (TO_DAYS('2012-10-01')),PARTITION p29 VALUES LESS THAN (TO_DAYS('2012-11-01')),PARTITION p30 VALUES LESS THAN (TO_DAYS('2012-12-01')),PARTITION p31 VALUES LESS THAN (MAXVALUE))";
		getSimpleJdbcTemplate().update(query0);
		getSimpleJdbcTemplate().update(query);
		getSimpleJdbcTemplate().update(query1);
		getSimpleJdbcTemplate().update(query2);
		getSimpleJdbcTemplate().update(query3);
	}

	@Override
	public void createDimensionsJoinedThreeNoSeq()
	{
		String query0 = "DROP TABLE IF EXISTS mat_dimensions_three_temp";
		String query = "CREATE TABLE mat_dimensions_three_temp ENGINE=MyISAM SELECT date, v1.dimension AS dim1, v2.dimension AS dim2, v3.dimension AS dim3, COUNT(*) AS numofoccurences FROM dimensions t1, dimensions t2, dimensions t3, valuetable v1, valuetable v2, valuetable v3, mainlogs WHERE t1.logid = t2.logid AND t2.logid=t3.logid AND t1.id != t2.id AND t2.id != t3.id AND t1.id != t3.id AND mainlogs.id=t1.LogId AND t1.valuetableid=v1.id AND t2.valuetableid=v2.id AND t3.valuetableid=v3.id AND ((v1.dimension<v2.dimension AND v2.dimension<v3.dimension) OR (v1.dimension=v2.dimension AND v2.dimension=v3.dimension AND t1.sequenceid < t2.sequenceid AND t2.sequenceid < t3.sequenceid) OR (v1.dimension=v2.dimension AND t1.sequenceid<t2.sequenceid AND v1.dimension!=v3.dimension)) GROUP BY date, dim1, dim2, dim3 ORDER BY NULL";
		String query1 = "DROP TABLE IF EXISTS mat_dimensions_three";
		String query2 = "RENAME TABLE mat_dimensions_three_temp TO mat_dimensions_three";
		String query3 = "ALTER TABLE mat_dimensions_three PARTITION BY RANGE(TO_DAYS(date)) (PARTITION p1 VALUES LESS THAN (TO_DAYS('2010-07-01')),PARTITION p2 VALUES LESS THAN (TO_DAYS('2010-08-01')),PARTITION p3 VALUES LESS THAN (TO_DAYS('2010-09-01')),PARTITION p4 VALUES LESS THAN (TO_DAYS('2010-10-01')),PARTITION p5 VALUES LESS THAN (TO_DAYS('2010-11-01')),PARTITION p6 VALUES LESS THAN (TO_DAYS('2010-12-01')),PARTITION p7 VALUES LESS THAN (TO_DAYS('2011-01-01')),PARTITION p8 VALUES LESS THAN (TO_DAYS('2011-02-01')),PARTITION p9 VALUES LESS THAN (TO_DAYS('2011-03-01')),PARTITION p10 VALUES LESS THAN (TO_DAYS('2011-04-01')),PARTITION p11 VALUES LESS THAN (TO_DAYS('2011-05-01')),PARTITION p12 VALUES LESS THAN (TO_DAYS('2011-06-01')),PARTITION p13 VALUES LESS THAN (TO_DAYS('2011-07-01')),PARTITION p14 VALUES LESS THAN (TO_DAYS('2011-08-01')),PARTITION p15 VALUES LESS THAN (TO_DAYS('2011-09-01')),PARTITION p16 VALUES LESS THAN (TO_DAYS('2011-10-01')),PARTITION p17 VALUES LESS THAN (TO_DAYS('2011-11-01')),PARTITION p18 VALUES LESS THAN (TO_DAYS('2011-12-01')),PARTITION p19 VALUES LESS THAN (TO_DAYS('2012-01-01')),PARTITION p20 VALUES LESS THAN (TO_DAYS('2012-02-01')),PARTITION p21 VALUES LESS THAN (TO_DAYS('2012-03-01')),PARTITION p22 VALUES LESS THAN (TO_DAYS('2012-04-01')),PARTITION p23 VALUES LESS THAN (TO_DAYS('2012-05-01')),PARTITION p24 VALUES LESS THAN (TO_DAYS('2012-06-01')),PARTITION p25 VALUES LESS THAN (TO_DAYS('2012-07-01')),PARTITION p26 VALUES LESS THAN (TO_DAYS('2012-08-01')),PARTITION p27 VALUES LESS THAN (TO_DAYS('2012-09-01')),PARTITION p28 VALUES LESS THAN (TO_DAYS('2012-10-01')),PARTITION p29 VALUES LESS THAN (TO_DAYS('2012-11-01')),PARTITION p30 VALUES LESS THAN (TO_DAYS('2012-12-01')),PARTITION p31 VALUES LESS THAN (MAXVALUE))";
		getSimpleJdbcTemplate().update(query0);
		getSimpleJdbcTemplate().update(query);
		getSimpleJdbcTemplate().update(query1);
		getSimpleJdbcTemplate().update(query2);
		getSimpleJdbcTemplate().update(query3);
	}

	@Override
	public void createDimensionValuesJoinedOne()
	{
		String query0 = "DROP TABLE IF EXISTS mat_dimensionvalues_one_temp";
		String query = "CREATE TABLE mat_dimensionvalues_one_temp ENGINE=MyISAM SELECT date, v1.dimension, v1.value, COUNT(*) AS numofoccurences FROM dimensions, mainlogs, valuetable v1 WHERE dimensions.logid=mainlogs.id AND dimensions.valuetableid=v1.id GROUP BY date, v1.dimension, v1.value ORDER BY NULL";
		String query1 = "DROP TABLE IF EXISTS mat_dimensionvalues_one";
		String query2 = "RENAME TABLE mat_dimensionvalues_one_temp TO mat_dimensionvalues_one";
		String query3 = "ALTER TABLE mat_dimensionvalues_one PARTITION BY RANGE(TO_DAYS(date)) (PARTITION p1 VALUES LESS THAN (TO_DAYS('2010-07-01')),PARTITION p2 VALUES LESS THAN (TO_DAYS('2010-08-01')),PARTITION p3 VALUES LESS THAN (TO_DAYS('2010-09-01')),PARTITION p4 VALUES LESS THAN (TO_DAYS('2010-10-01')),PARTITION p5 VALUES LESS THAN (TO_DAYS('2010-11-01')),PARTITION p6 VALUES LESS THAN (TO_DAYS('2010-12-01')),PARTITION p7 VALUES LESS THAN (TO_DAYS('2011-01-01')),PARTITION p8 VALUES LESS THAN (TO_DAYS('2011-02-01')),PARTITION p9 VALUES LESS THAN (TO_DAYS('2011-03-01')),PARTITION p10 VALUES LESS THAN (TO_DAYS('2011-04-01')),PARTITION p11 VALUES LESS THAN (TO_DAYS('2011-05-01')),PARTITION p12 VALUES LESS THAN (TO_DAYS('2011-06-01')),PARTITION p13 VALUES LESS THAN (TO_DAYS('2011-07-01')),PARTITION p14 VALUES LESS THAN (TO_DAYS('2011-08-01')),PARTITION p15 VALUES LESS THAN (TO_DAYS('2011-09-01')),PARTITION p16 VALUES LESS THAN (TO_DAYS('2011-10-01')),PARTITION p17 VALUES LESS THAN (TO_DAYS('2011-11-01')),PARTITION p18 VALUES LESS THAN (TO_DAYS('2011-12-01')),PARTITION p19 VALUES LESS THAN (TO_DAYS('2012-01-01')),PARTITION p20 VALUES LESS THAN (TO_DAYS('2012-02-01')),PARTITION p21 VALUES LESS THAN (TO_DAYS('2012-03-01')),PARTITION p22 VALUES LESS THAN (TO_DAYS('2012-04-01')),PARTITION p23 VALUES LESS THAN (TO_DAYS('2012-05-01')),PARTITION p24 VALUES LESS THAN (TO_DAYS('2012-06-01')),PARTITION p25 VALUES LESS THAN (TO_DAYS('2012-07-01')),PARTITION p26 VALUES LESS THAN (TO_DAYS('2012-08-01')),PARTITION p27 VALUES LESS THAN (TO_DAYS('2012-09-01')),PARTITION p28 VALUES LESS THAN (TO_DAYS('2012-10-01')),PARTITION p29 VALUES LESS THAN (TO_DAYS('2012-11-01')),PARTITION p30 VALUES LESS THAN (TO_DAYS('2012-12-01')),PARTITION p31 VALUES LESS THAN (MAXVALUE))";
		getSimpleJdbcTemplate().update(query0);
		getSimpleJdbcTemplate().update(query);
		getSimpleJdbcTemplate().update(query1);
		getSimpleJdbcTemplate().update(query2);
		getSimpleJdbcTemplate().update(query3);
	}

	@Override
	public void createDimensionValuesJoinedTwo()
	{
		String query0 = "DROP TABLE IF EXISTS mat_dimensionvalues_two_temp";
		String query = "CREATE TABLE mat_dimensionvalues_two_temp ENGINE=MyISAM SELECT date, v1.dimension AS dim1, v1.value AS val1, v2.dimension AS dim2, v2.value AS val2, COUNT(*) AS numofoccurences FROM dimensions t1, dimensions t2, valuetable v1, valuetable v2, mainlogs WHERE t1.logid = t2.logid AND t1.id != t2.id AND mainlogs.id=t1.LogId AND t1.valuetableid=v1.id AND t2.valuetableid=v2.id AND ((CONCAT_WS(':',v1.dimension,v1.value)<CONCAT_WS(':',v2.dimension,v2.value)) OR (v1.dimension=v2.dimension AND v1.value=v2.value AND t1.sequenceid < t2.sequenceid)) GROUP BY  date, dim1, val1, dim2, val2 ORDER BY NULL";
		String query1 = "DROP TABLE IF EXISTS mat_dimensionvalues_two";
		String query2 = "RENAME TABLE mat_dimensionvalues_two_temp TO mat_dimensionvalues_two";
		String query3 = "ALTER TABLE mat_dimensionvalues_two PARTITION BY RANGE(TO_DAYS(date)) (PARTITION p1 VALUES LESS THAN (TO_DAYS('2010-07-01')),PARTITION p2 VALUES LESS THAN (TO_DAYS('2010-08-01')),PARTITION p3 VALUES LESS THAN (TO_DAYS('2010-09-01')),PARTITION p4 VALUES LESS THAN (TO_DAYS('2010-10-01')),PARTITION p5 VALUES LESS THAN (TO_DAYS('2010-11-01')),PARTITION p6 VALUES LESS THAN (TO_DAYS('2010-12-01')),PARTITION p7 VALUES LESS THAN (TO_DAYS('2011-01-01')),PARTITION p8 VALUES LESS THAN (TO_DAYS('2011-02-01')),PARTITION p9 VALUES LESS THAN (TO_DAYS('2011-03-01')),PARTITION p10 VALUES LESS THAN (TO_DAYS('2011-04-01')),PARTITION p11 VALUES LESS THAN (TO_DAYS('2011-05-01')),PARTITION p12 VALUES LESS THAN (TO_DAYS('2011-06-01')),PARTITION p13 VALUES LESS THAN (TO_DAYS('2011-07-01')),PARTITION p14 VALUES LESS THAN (TO_DAYS('2011-08-01')),PARTITION p15 VALUES LESS THAN (TO_DAYS('2011-09-01')),PARTITION p16 VALUES LESS THAN (TO_DAYS('2011-10-01')),PARTITION p17 VALUES LESS THAN (TO_DAYS('2011-11-01')),PARTITION p18 VALUES LESS THAN (TO_DAYS('2011-12-01')),PARTITION p19 VALUES LESS THAN (TO_DAYS('2012-01-01')),PARTITION p20 VALUES LESS THAN (TO_DAYS('2012-02-01')),PARTITION p21 VALUES LESS THAN (TO_DAYS('2012-03-01')),PARTITION p22 VALUES LESS THAN (TO_DAYS('2012-04-01')),PARTITION p23 VALUES LESS THAN (TO_DAYS('2012-05-01')),PARTITION p24 VALUES LESS THAN (TO_DAYS('2012-06-01')),PARTITION p25 VALUES LESS THAN (TO_DAYS('2012-07-01')),PARTITION p26 VALUES LESS THAN (TO_DAYS('2012-08-01')),PARTITION p27 VALUES LESS THAN (TO_DAYS('2012-09-01')),PARTITION p28 VALUES LESS THAN (TO_DAYS('2012-10-01')),PARTITION p29 VALUES LESS THAN (TO_DAYS('2012-11-01')),PARTITION p30 VALUES LESS THAN (TO_DAYS('2012-12-01')),PARTITION p31 VALUES LESS THAN (MAXVALUE))";
		getSimpleJdbcTemplate().update(query0);
		getSimpleJdbcTemplate().update(query);
		getSimpleJdbcTemplate().update(query1);
		getSimpleJdbcTemplate().update(query2);
		getSimpleJdbcTemplate().update(query3);
	}

	@Override
	public void createDimensionValuesJoinedThree()
	{
		String query0 = "DROP TABLE IF EXISTS mat_dimensionvalues_three_temp";
		String query4 = "CREATE TABLE mat_dimensionvalues_three_temp ENGINE=MyISAM SELECT date, v1.dimension AS dim1, v1.value AS val1, v2.dimension AS dim2, v2.value AS val2, v3.dimension AS dim3, v3.value AS val3, COUNT(*) AS numofoccurences FROM dimensions t1, dimensions t2, dimensions t3, valuetable v1, valuetable v2, valuetable v3, mainlogs WHERE t1.logid = t2.logid AND t2.logid=t3.logid AND t1.id != t2.id AND t2.id != t3.id AND t1.id != t3.id AND mainlogs.id=t1.LogId AND t1.valuetableid=v1.id AND t2.valuetableid=v2.id AND t3.valuetableid=v3.id AND CONCAT_WS(':',v1.dimension,v1.value)<CONCAT_WS(':',v2.dimension,v2.value) AND CONCAT_WS(':',v2.dimension,v2.value)<CONCAT_WS(':',v3.dimension,v3.value) GROUP BY date, dim1, val1, dim2, val2, dim3, val3 ORDER BY NULL";
		String query5 = "DROP TABLE IF EXISTS mat_dimensionvalues_three";
		String query6 = "RENAME TABLE mat_dimensionvalues_three_temp TO mat_dimensionvalues_three";
		String query7 = "ALTER TABLE mat_dimensionvalues_three PARTITION BY RANGE(TO_DAYS(date)) (PARTITION p1 VALUES LESS THAN (TO_DAYS('2010-07-01')),PARTITION p2 VALUES LESS THAN (TO_DAYS('2010-08-01')),PARTITION p3 VALUES LESS THAN (TO_DAYS('2010-09-01')),PARTITION p4 VALUES LESS THAN (TO_DAYS('2010-10-01')),PARTITION p5 VALUES LESS THAN (TO_DAYS('2010-11-01')),PARTITION p6 VALUES LESS THAN (TO_DAYS('2010-12-01')),PARTITION p7 VALUES LESS THAN (TO_DAYS('2011-01-01')),PARTITION p8 VALUES LESS THAN (TO_DAYS('2011-02-01')),PARTITION p9 VALUES LESS THAN (TO_DAYS('2011-03-01')),PARTITION p10 VALUES LESS THAN (TO_DAYS('2011-04-01')),PARTITION p11 VALUES LESS THAN (TO_DAYS('2011-05-01')),PARTITION p12 VALUES LESS THAN (TO_DAYS('2011-06-01')),PARTITION p13 VALUES LESS THAN (TO_DAYS('2011-07-01')),PARTITION p14 VALUES LESS THAN (TO_DAYS('2011-08-01')),PARTITION p15 VALUES LESS THAN (TO_DAYS('2011-09-01')),PARTITION p16 VALUES LESS THAN (TO_DAYS('2011-10-01')),PARTITION p17 VALUES LESS THAN (TO_DAYS('2011-11-01')),PARTITION p18 VALUES LESS THAN (TO_DAYS('2011-12-01')),PARTITION p19 VALUES LESS THAN (TO_DAYS('2012-01-01')),PARTITION p20 VALUES LESS THAN (TO_DAYS('2012-02-01')),PARTITION p21 VALUES LESS THAN (TO_DAYS('2012-03-01')),PARTITION p22 VALUES LESS THAN (TO_DAYS('2012-04-01')),PARTITION p23 VALUES LESS THAN (TO_DAYS('2012-05-01')),PARTITION p24 VALUES LESS THAN (TO_DAYS('2012-06-01')),PARTITION p25 VALUES LESS THAN (TO_DAYS('2012-07-01')),PARTITION p26 VALUES LESS THAN (TO_DAYS('2012-08-01')),PARTITION p27 VALUES LESS THAN (TO_DAYS('2012-09-01')),PARTITION p28 VALUES LESS THAN (TO_DAYS('2012-10-01')),PARTITION p29 VALUES LESS THAN (TO_DAYS('2012-11-01')),PARTITION p30 VALUES LESS THAN (TO_DAYS('2012-12-01')),PARTITION p31 VALUES LESS THAN (MAXVALUE))";
		getSimpleJdbcTemplate().update(query0);
		getSimpleJdbcTemplate().update(query4);
		getSimpleJdbcTemplate().update(query5);
		getSimpleJdbcTemplate().update(query6);
		getSimpleJdbcTemplate().update(query7);

	}

	@Override
	public void createRequestByUserTable()
	{
		String query1 = "DROP TABLE IF EXISTS mat_sessions_request_temp";
		String query2 = "CREATE TABLE mat_sessions_request_temp ENGINE=MyISAM SELECT date, sessionid, SUM(IF(SearchTerms is not null,1,0)) AS searchrequest, SUM(IF(SearchTerms is null,1,0)) AS navrequest,  COUNT(*) AS totalrequest FROM mainlogs GROUP BY date, sessionid";
		String query3 = "DROP TABLE IF EXISTS mat_sessions_request";
		String query4 = "RENAME TABLE mat_sessions_request_temp TO mat_sessions_request";
		getSimpleJdbcTemplate().update(query1);
		getSimpleJdbcTemplate().update(query2);
		getSimpleJdbcTemplate().update(query3);
		getSimpleJdbcTemplate().update(query4);
	}

	@Override
	public List<RequestByHour> getRequestByHours(String sDate, String eDate)
	{
		String query = "select hour(time)+1 as hour, count(*) as requests from mainlogs where date>=? and date <=? group by hour";
		List<RequestByHour> requestByHours = getSimpleJdbcTemplate().query(
				query, new RowMapper<RequestByHour>()
				{
					public RequestByHour mapRow(ResultSet rs, int rowNum)
							throws SQLException
					{
						RequestByHour rbh = new RequestByHour();
						rbh.setHour(rs.getInt(1));
						rbh.setRequests(rs.getInt(2));
						return rbh;
					}
				}, new Object[]
				{ sDate, eDate });
		return requestByHours;
	}

	@Override
	public List<String> getDimensionValues(String dim)
	{
		// This method gets a list of values for a selected dimension
		String query = "SELECT value FROM valuetable WHERE dimension=? ORDER BY value";
		List<String> vals = getSimpleJdbcTemplate().query(query,
				new RowMapper<String>()
				{
					public String mapRow(ResultSet rs, int rowNum)
							throws SQLException
					{
						return rs.getString("value");
					}
				}, new Object[]
				{ dim });
		return vals;
	}

	@Override
	public List<String> getDimensionValues()
	{
		// TODO Auto-generated method stub
		String query = "SELECT DISTINCT WS_CONCAT(':', dimension, value) AS dimensionvalues FROM valuetable";
		List<String> vals = getSimpleJdbcTemplate().query(query,
				new RowMapper<String>()
				{
					public String mapRow(ResultSet rs, int rowNum)
							throws SQLException
					{
						return rs.getString("dimensionvalues");
					}
				});
		return vals;
	}

	@Override
	public List<SessionByHour> getSessionByHours(String sDate, String eDate)
	{
		String query = "select hour(time)+1 as hour, count(distinct sessionid) as sessions from mainlogs where date>=? and date <=? group by hour";
		List<SessionByHour> sessionByHours = getSimpleJdbcTemplate().query(
				query, new RowMapper<SessionByHour>()
				{
					public SessionByHour mapRow(ResultSet rs, int rowNum)
							throws SQLException
					{
						SessionByHour sbh = new SessionByHour();
						sbh.setHour(rs.getInt(1));
						sbh.setSessions(rs.getInt(2));
						return sbh;
					}
				}, new Object[]
				{ sDate, eDate });
		return sessionByHours;
	}

	@Override
	public List<SessionBy> getSessionBy(String sDate, String eDate, String sTime)
	{
		String query = "";
		if (sTime.compareTo("Hour of Day") == 0)
		{
			query = "select hour(time)+1 as hour, count(distinct sessionid) as sessions from mainlogs where date>=? and date <=? group by hour;";
		} else if (sTime.compareTo("Day of Month") == 0)
		{
			query = "select Day(Date) as day_group, count(distinct sessionid) as sessions from mainlogs where date>=? and date<=? group by day_group;";
		} else if (sTime.compareTo("Month of Year") == 0)
		{
			query = "select month(Date) as month, count(distinct sessionid) as sessions from mainlogs where date>=? and date<=? group by month;";
			List<SessionBy> sessionBy = getSimpleJdbcTemplate().query(query,
					new RowMapper<SessionBy>()
					{
						public SessionBy mapRow(ResultSet rs, int rowNum)
								throws SQLException
						{
							SessionBy sbh = new SessionBy();
							String formatedMonth = FormFormater.formateMonth(rs
									.getInt(1));
							sbh.setTime(formatedMonth);
							sbh.setFtime(formatedMonth);
							sbh.setSessions(rs.getInt(2));
							return sbh;
						}
					}, new Object[]
					{ sDate, eDate });
			return sessionBy;
		} else if (sTime.compareTo("Date") == 0)
		{
			query = "select Date, count(distinct sessionid) as sessions from mainlogs where date>=? and date<=? group by Date;";
			List<SessionBy> sessionBy = getSimpleJdbcTemplate().query(query,
					new RowMapper<SessionBy>()
					{
						public SessionBy mapRow(ResultSet rs, int rowNum)
								throws SQLException
						{
							SessionBy sbh = new SessionBy();
							sbh.setTime(rs.getString(1));
							String formatedDate = FormFormater.formatDateTwo(rs
									.getString(1));
							sbh.setFtime(formatedDate);
							sbh.setSessions(rs.getInt(2));
							return sbh;
						}
					}, new Object[]
					{ sDate, eDate });
			return sessionBy;
		}
		List<SessionBy> sessionBy = getSimpleJdbcTemplate().query(query,
				new RowMapper<SessionBy>()
				{
					public SessionBy mapRow(ResultSet rs, int rowNum)
							throws SQLException
					{
						SessionBy sbh = new SessionBy();
						sbh.setTime(rs.getInt(1));
						sbh.setFtime(rs.getInt(1));
						System.out.println(sbh.getFtime());
						sbh.setSessions(rs.getInt(2));
						return sbh;
					}
				}, new Object[]
				{ sDate, eDate });
		return sessionBy;
	}

	@Override
	public List<RequestBy> getRequestBy(String sDate, String eDate, String sTime)
	{
		System.out.println("Dao.getRequestBy()...");
		String query = "";
		if (sTime.compareTo("Hour of Day") == 0)
		{
			query = "select hour(time)+1 as hour, count(*) as requests from mainlogs where date>=? and date <=? group by hour;";
			System.out.println("query: " + query);
		} else if (sTime.compareTo("Day of Month") == 0)
		{
			query = "select Day(Date) as day_group, count(*) as requests from mainlogs where date>=? and date<=? group by day_group;";
			System.out.println("query: " + query);
		} else if (sTime.compareTo("Month of Year") == 0)
		{
			query = "select month(Date) as month, count(*) as requests from mainlogs where date>=? and date<=? group by month;";
			System.out.println("query: " + query);
			List<RequestBy> requestBy = getSimpleJdbcTemplate().query(query,
					new RowMapper<RequestBy>()
					{
						public RequestBy mapRow(ResultSet rs, int rowNum)
								throws SQLException
						{
							RequestBy rb = new RequestBy();
							String formatedMonth = FormFormater.formateMonth(rs
									.getInt(1));
							rb.setTime(formatedMonth);
							rb.setFtime(formatedMonth);
							rb.setRequests(rs.getInt(2));
							return rb;
						}
					}, new Object[]
					{ sDate, eDate });
			return requestBy;
		} else if (sTime.compareTo("Date") == 0)
		{
			query = "select Date, count(*) as requests from mainlogs where date>=? and date<=? group by Date;";
			System.out.println("query: " + query);
			List<RequestBy> requestBy = getSimpleJdbcTemplate().query(query,
					new RowMapper<RequestBy>()
					{
						public RequestBy mapRow(ResultSet rs, int rowNum)
								throws SQLException
						{
							RequestBy rb = new RequestBy();
							rb.setTime(rs.getString(1));
							String formatedDate = FormFormater.formatDateTwo(rs
									.getString(1));
							rb.setFtime(formatedDate);
							rb.setRequests(rs.getInt(2));
							return rb;
						}
					}, new Object[]
					{ sDate, eDate });
			return requestBy;
		}
		List<RequestBy> requestBy = getSimpleJdbcTemplate().query(query,
				new RowMapper<RequestBy>()
				{
					public RequestBy mapRow(ResultSet rs, int rowNum)
							throws SQLException
					{
						RequestBy rb = new RequestBy();
						rb.setTime(rs.getInt(1));
						rb.setFtime(rs.getInt(1));
						rb.setRequests(rs.getInt(2));
						return rb;
					}
				}, new Object[]
				{ sDate, eDate });

		return requestBy;
	}

	@Override
	public List<CorrectTerms> getAutoCorrectedSearchTerms(String sDate,
			String eDate)
	{
		String query = "select SearchTerms, AutoCorrectTo, count(SearchTerms) as requests from mainlogs where AutoCorrectTo is not null and date>=? and date<=? group by SearchTerms order by requests desc;";
		List<CorrectTerms> terms = getSimpleJdbcTemplate().query(query,
				new RowMapper<CorrectTerms>()
				{
					public CorrectTerms mapRow(ResultSet rs, int rowNum)
							throws SQLException
					{
						CorrectTerms sbh = new CorrectTerms();
						sbh.setSearchterm(rs.getString(1));
						sbh.setCorrectto(rs.getString(2));
						sbh.setRequest(rs.getInt(3));
						return sbh;
					}
				}, new Object[]
				{ sDate, eDate });

		return terms;
	}

	@Override
	public List<TopDimensionValues> getTopDimensionValues(String sDate,
			String eDate, int depth, final int numberOfRecords,
			final String[] searchedDimensions, final String[] searchedValues,
			final String[] searchedDimensions1, final String[] searchedValues1)
	{
		// TODO Auto-generated method stub
		String query1 = "SELECT dimension, value, SUM(numofoccurences) AS numofocc FROM mat_dimensionvalues_one WHERE Date>=? AND Date<=? GROUP BY dimension, value ORDER BY numofocc DESC";
		String query1total = "SELECT SUM(numofoccurences) FROM mat_dimensionvalues_one WHERE Date>=? AND Date<=?";
		String query2 = "SELECT dim1, val1, dim2, val2, SUM(numofoccurences) AS numofocc FROM mat_dimensionvalues_two WHERE Date>=? AND Date<=? GROUP BY dim1, val1, dim2, val2 ORDER BY numofocc DESC";
		String query2total = "SELECT SUM(numofoccurences) FROM mat_dimensionvalues_two WHERE Date>=? AND Date<=?";
		String query3 = "SELECT dim1, val1, dim2, val2, dim3, val3, SUM(numofoccurences) AS numofocc FROM mat_dimensionvalues_three WHERE Date>=? AND Date<=? GROUP BY dim1, val1, dim2, val2, dim3, val3 ORDER BY numofocc DESC";
		String query3total = "SELECT SUM(numofoccurences) FROM mat_dimensionvalues_three WHERE Date>=? AND Date<=?";

		String query = "";
		String queryTotal = "";
		if (depth == 1)
		{
			query = query1;
			queryTotal = query1total;
		} else if (depth == 2)
		{
			query = query2;
			queryTotal = query2total;
		} else
		{
			query = query3;
			queryTotal = query3total;
		}

		List<TopDimensionValues> tempTopDimensionValues = getSimpleJdbcTemplate()
				.query(query, new RowMapper<TopDimensionValues>()
				{
					public TopDimensionValues mapRow(ResultSet rs, int rowNum)
							throws SQLException
					{
						TopDimensionValues tdv = new TopDimensionValues();
						if (rs == null)
						{
							return null;
						}
						ResultSetMetaData rsMetaData = rs.getMetaData();
						int depth = (rsMetaData.getColumnCount() - 1) / 2;
						String[] dims = new String[depth];
						String[] vals = new String[depth];
						int count = 1;
						for (int i = 0; i < depth; i++)
						{
							dims[i] = rs.getString(count);
							vals[i] = rs.getString(count + 1);
							count += 2;
						}
						tdv.setDimensions(dims);
						tdv.setValues(vals);
						tdv.setCount(rs.getInt(rsMetaData.getColumnCount()));
						return tdv;
					}
				}, new Object[]
				{ sDate, eDate });

		// Getting the total number of records
		int total = getSimpleJdbcTemplate().queryForInt(queryTotal,
				new Object[]
				{ sDate, eDate });
		List<TopDimensionValues> topDimensionValues = new ArrayList<TopDimensionValues>();

		// Adding the total number of records to the list of objects
		if (total != 0)
		{

			int numberOfExistingRecords = tempTopDimensionValues.size();
			for (int m = 0; m < numberOfExistingRecords; m++)
			{
				if (topDimensionValues.size() < numberOfRecords)
				{
					String[] dims = tempTopDimensionValues.get(m)
							.getDimensions();
					String[] vals = tempTopDimensionValues.get(m).getValues();
					if (searchedDimensions[0].equals("All")
							&& searchedDimensions1[0].equals("All"))
					{
						topDimensionValues.add(tempTopDimensionValues.get(m));
					} else
					{
						if (searchedDimensions[0].equals("All")
								&& (!searchedDimensions1[0].equals("All")))
						{
							if (searchedValues1[0].equals("All"))
							{
								for (int i = 0; i < dims.length; i++)
								{
									if (searchedDimensions1[0].equals(dims[i]))
									{
										topDimensionValues
												.add(tempTopDimensionValues
														.get(m));
									}
								}
							} else
							{
								for (int i = 0; i < dims.length; i++)
								{
									if (searchedDimensions1[0].equals(dims[i])
											&& searchedValues1[0]
													.equals(vals[i]))
									{
										topDimensionValues
												.add(tempTopDimensionValues
														.get(m));
									}
								}
							}

						} else if ((!searchedDimensions[0].equals("All"))
								&& searchedDimensions1[0].equals("All"))
						{
							if (searchedValues[0].equals("All"))
							{
								for (int i = 0; i < dims.length; i++)
								{
									if (searchedDimensions[0].equals(dims[i]))
									{
										topDimensionValues
												.add(tempTopDimensionValues
														.get(m));
									}
								}
							} else
							{
								for (int i = 0; i < dims.length; i++)
								{
									if (searchedDimensions[0].equals(dims[i])
											&& searchedValues[0]
													.equals(vals[i]))
									{
										topDimensionValues
												.add(tempTopDimensionValues
														.get(m));
									}
								}
							}

						} else if ((!searchedDimensions[0].equals("All"))
								&& (!searchedDimensions1[0].equals("All")))
						{

							if (searchedValues[0].equals("All")
									&& searchedValues1[0].equals("All"))
							{
								boolean contains1 = false;
								boolean contains = false;
								for (int i = 0; i < dims.length; i++)
								{
									if (searchedDimensions1[0].equals(dims[i]))
									{
										contains1 = true;
									}
									if (searchedDimensions[0].equals(dims[i]))
									{
										contains = true;
									}
								}
								if (contains1 && contains)
								{
									topDimensionValues
											.add(tempTopDimensionValues.get(m));
								}
							} else if (searchedValues[0].equals("All")
									&& (!searchedValues1[0].equals("All")))
							{
								boolean contains1 = false;
								boolean contains = false;
								for (int i = 0; i < dims.length; i++)
								{
									if (searchedDimensions1[0].equals(dims[i])
											&& searchedValues1[0]
													.equals(vals[i]))
									{
										contains1 = true;
									}
								}
								for (int i = 0; i < dims.length; i++)
								{
									if (searchedDimensions[0].equals(dims[i]))
									{
										contains = true;
									}
								}
								if (contains1 && contains)
								{
									topDimensionValues
											.add(tempTopDimensionValues.get(m));
								}
							} else if ((!searchedValues[0].equals("All"))
									&& searchedValues1[0].equals("All"))
							{
								boolean contains1 = false;
								boolean contains = false;
								for (int i = 0; i < dims.length; i++)
								{
									if (searchedDimensions1[0].equals(dims[i]))
									{
										contains1 = true;
									}
								}
								for (int i = 0; i < dims.length; i++)
								{
									if (searchedDimensions[0].equals(dims[i])
											&& searchedValues[0]
													.equals(vals[i]))
									{
										contains = true;
									}
								}
								if (contains1 && contains)
								{
									topDimensionValues
											.add(tempTopDimensionValues.get(m));
								}
							} else if ((!searchedValues[0].equals("All"))
									&& ((!searchedValues1[0].equals("All"))))
							{
								boolean contains1 = false;
								boolean contains = false;
								for (int i = 0; i < dims.length; i++)
								{
									if (searchedDimensions1[0].equals(dims[i])
											&& searchedValues1[0]
													.equals(vals[i]))
									{
										contains1 = true;
									}
								}
								for (int i = 0; i < dims.length; i++)
								{
									if (searchedDimensions[0].equals(dims[i])
											&& searchedValues[0]
													.equals(vals[i]))
									{
										contains = true;
									}
								}
								if (contains1 && contains)
								{
									topDimensionValues
											.add(tempTopDimensionValues.get(m));
								}
							}
						}
					}
				} else
				{
					break;
				}
			}
		}
		for (int i = 0; i < topDimensionValues.size(); i++)
		{
			topDimensionValues.get(i).setTotal(total);
		}
		return topDimensionValues;
	}

	@Override
	public List<TopKeywordsSearch> getTopKeywordsSearches(String fromDate,
			String toDate, final String resultType, int numRecords)
	{
		String query = "";
		if (resultType.equals("All"))
		{
			query = "select searchterms,count(*) from mainlogs where searchterms is not null and date>=? and date<=? group by searchterms order by count(*) desc limit ?";
		} else if (resultType.equals("Return Zero Records"))
		{
			query = "select searchterms,count(*) from mainlogs where searchterms is not null and date>=? and date<=? and numberofrecords=0 group by searchterms order by count(*) desc limit ?";
		} else if (resultType.equals("Autocorrected"))
		{
			query = "select searchterms,autocorrectto,count(*) from mainlogs where autocorrectto is not null and date>=? and date<=? group by searchterms,autocorrectto order by count(*) desc limit ?";
		}

		List<TopKeywordsSearch> topKeywordsSearches = getSimpleJdbcTemplate()
				.query(query, new RowMapper<TopKeywordsSearch>()
				{
					public TopKeywordsSearch mapRow(ResultSet rs, int rowNum)
							throws SQLException
					{
						TopKeywordsSearch tks = new TopKeywordsSearch();
						tks.setSearchTerms(rs.getString(1));
						if (!resultType.equals("Autocorrected"))
						{
							tks.setCount(rs.getInt(2));
						} else
						{
							tks.setAutoCorrectTo(rs.getString(2));
							tks.setCount(rs.getInt(3));
						}

						return tks;
					}
				}, new Object[]
				{ fromDate, toDate, numRecords });

		return topKeywordsSearches;
	}

	@Override
	public int getWeekTrend(String fromDateOfTrend, String toDateOfTrend,
			String[] incDimsOfTrend, String[] incValsOfTrend,
			String[] incDims1OfTrend, String[] incVals1OfTrend)
	{
		// TODO Auto-generated method stub
		String whereClause = "";
		String query = "";
		if ((!incDimsOfTrend[0].equals("--Select--"))
				&& (!incDims1OfTrend[0].equals("--Select--")))
		{
			whereClause = "((dim1=? AND val1=? AND dim2=? AND val2=?) OR (dim1=? AND val1=? AND dim2=? AND val2=?))";
			query = "SELECT SUM(numofoccurences) AS numofocc FROM mat_dimensionvalues_two WHERE Date>=? AND Date<=? AND "
					+ whereClause + " ORDER BY numofocc DESC";
			return getSimpleJdbcTemplate().queryForInt(
					query,
					new Object[]
					{ fromDateOfTrend, toDateOfTrend, incDimsOfTrend[0],
							incValsOfTrend[0], incDims1OfTrend[0],
							incVals1OfTrend[0], incDims1OfTrend[0],
							incVals1OfTrend[0], incDimsOfTrend[0],
							incValsOfTrend[0] });
		} else if (!incDimsOfTrend[0].equals("--Select--"))
		{
			whereClause = "dimension=? AND value=?";
			query = "SELECT SUM(numofoccurences) AS numofocc FROM mat_dimensionvalues_one WHERE Date>=? AND Date<=? AND "
					+ whereClause + " ORDER BY numofocc DESC";
			return getSimpleJdbcTemplate().queryForInt(
					query,
					new Object[]
					{ fromDateOfTrend, toDateOfTrend, incDimsOfTrend[0],
							incValsOfTrend[0] });
		} else if (!incDims1OfTrend[0].equals("--Select--"))
		{
			whereClause = "dimension=? AND value=?";
			query = "SELECT SUM(numofoccurences) AS numofocc FROM mat_dimensionvalues_one WHERE Date>=? AND Date<=? AND "
					+ whereClause + " ORDER BY numofocc DESC";
			return getSimpleJdbcTemplate().queryForInt(
					query,
					new Object[]
					{ fromDateOfTrend, toDateOfTrend, incDims1OfTrend[0],
							incVals1OfTrend[0] });
		}
		return 0;
	}

	@Override
	public List<KeywordSearch> getKeywordSearchResult(String fromDate,
			String toDate, int llimit, int hlimit, boolean nolimit)
	{
		String strSQL = "";
		String tmpSQL = "";

		Object[] tmpObject = null;

		if (!nolimit)
		{
			if (llimit != 0 && llimit == hlimit)
			{
				tmpSQL = "AND NumberofRecords  >= ? ";
				tmpObject = new Object[]
				{ fromDate, toDate, llimit };
			} else
			{
				tmpSQL = "AND NumberofRecords  >= ? AND NumberofRecords  <= ? ";
				tmpObject = new Object[]
				{ fromDate, toDate, llimit, hlimit };
			}
		} else
		{
			tmpObject = new Object[]
			{ fromDate, toDate };
		}

		strSQL = "SELECT searchterms, NumberofRecords, count(*) as requests "
				+ "FROM mainlogs "
				+ "WHERE searchterms is not null  AND date >= ? AND date <= ? "
				+ tmpSQL + "GROUP BY NumberofRecords, searchterms "
				+ "ORDER BY NumberofRecords, requests DESC";

		List<KeywordSearch> tmpKSS = getSimpleJdbcTemplate().query(strSQL,
				new RowMapper<KeywordSearch>()
				{
					public KeywordSearch mapRow(ResultSet rs, int rowNum)
							throws SQLException
					{
						KeywordSearch tmpKeywordSearch = new KeywordSearch();
						tmpKeywordSearch.setSearchterms(rs.getString(1));
						tmpKeywordSearch.setRecords(rs.getInt(2));
						tmpKeywordSearch.setRequests(rs.getInt(3));
						return tmpKeywordSearch;
					}
				}, tmpObject);

		return tmpKSS;
	}

	@Override
	public List<KeywordSearchSummary> getKeywordSearchResultSummary(
			String fromDate, String toDate)
	{
		int[][] range = new int[7][2];

		String strSQL;
		String tmpSQL;

		range[0][0] = 0;
		range[0][1] = 0;
		range[1][0] = 1;
		range[1][1] = 20;
		range[2][0] = 21;
		range[2][1] = 50;
		range[3][0] = 51;
		range[3][1] = 100;
		range[4][0] = 101;
		range[4][1] = 250;
		range[5][0] = 251;
		range[5][1] = 500;
		range[6][0] = 501;
		range[6][1] = 501;

		List<KeywordSearchSummary> tmpKSS = new ArrayList<KeywordSearchSummary>();

		for (int i = 0; i <= 6; i++)
		{
			if (range[i][0] == 0 && range[i][1] == 0)
				tmpSQL = "AND NumberofRecords  = ? ";
			else if (range[i][0] > 0 && range[i][0] == range[i][1])
				tmpSQL = "AND NumberofRecords  >= ? ";
			else
				tmpSQL = "AND NumberofRecords  >= ? AND NumberofRecords  <= ? ";

			strSQL = "SELECT count(distinct(searchterms)) as countterms, ?, ? "
					+ "FROM mainlogs "
					+ "WHERE searchterms is not null  AND date >= ? AND date <= ? "
					+ tmpSQL;

			if (range[i][0] == 0 || range[i][0] == range[i][1])
			{
				tmpKSS.addAll(getSimpleJdbcTemplate().query(
						strSQL,
						new RowMapper<KeywordSearchSummary>()
						{
							public KeywordSearchSummary mapRow(ResultSet rs,
									int rowNum) throws SQLException
							{
								KeywordSearchSummary tmpKeywordSearch = new KeywordSearchSummary();
								tmpKeywordSearch.setRecords(rs.getInt(1));
								tmpKeywordSearch.setLlimit(rs.getInt(2));
								tmpKeywordSearch.setHlimit(rs.getInt(3));
								return tmpKeywordSearch;
							}
						},
						new Object[]
						{ range[i][0], range[i][1], fromDate, toDate,
								range[i][0] }));
			} else
			{
				tmpKSS.addAll(getSimpleJdbcTemplate().query(
						strSQL,
						new RowMapper<KeywordSearchSummary>()
						{
							public KeywordSearchSummary mapRow(ResultSet rs,
									int rowNum) throws SQLException
							{
								KeywordSearchSummary tmpKeywordSearch = new KeywordSearchSummary();
								tmpKeywordSearch.setRecords(rs.getInt(1));
								tmpKeywordSearch.setLlimit(rs.getInt(2));
								tmpKeywordSearch.setHlimit(rs.getInt(3));
								return tmpKeywordSearch;
							}
						},
						new Object[]
						{ range[i][0], range[i][1], fromDate, toDate,
								range[i][0], range[i][1] }));
			}

		}

		return tmpKSS;
	}

	@Override
	public ArrayList<RequestByUser_SearchOrNavigate> getRequestByUser_SearchOrNavigateOnlyRequests(
			String fromDate, String toDate, String requestType)
	{
		ArrayList<RequestByUser_SearchOrNavigate> tmpListResult = new ArrayList<RequestByUser_SearchOrNavigate>();
		String strSQL = "";

		if (requestType.equals("Search Only Requests"))
		{
			/*
			 * strSQL = "select request_count, count(sessionId) " + "from " +
			 * "( " + "select count(*) as request_count, sessionId " +
			 * "from mainlogs " + "where searchterms is not null " +
			 * "and date >= ? " + "and date <= ? " + "and sessionId not in " +
			 * "( " + "select s.sessionId from " + "( " +
			 * "select distinct sessionId " + "from mainlogs " +
			 * "where searchterms is not null " + "and date >= ? " +
			 * "and date <= ? " + ") as s, " + "( " +
			 * "select distinct sessionId " + "from mainlogs " +
			 * "where searchterms is null " + "and date >= ? " +
			 * "and date <= ? " + ") as n " + "where s.sessionId = n.sessionId "
			 * + ") " + "group by sessionId " + ") as t " +
			 * "group by request_count ";
			 */
			strSQL = "SELECT ssr, count(*) FROM ( "
					+ "SELECT sessionid, SUM(searchrequest) as ssr, sum(navrequest) snv "
					+ "FROM mat_sessions_request "
					+ "WHERE date >= ? and date <= ? " + "GROUP BY sessionid "
					+ ")  AS tmprequest " + "WHERE snv = 0 " + "GROUP BY ssr ";
		} else if (requestType.equals("Navigate Only Requests"))
		{
			/*
			 * strSQL = "select request_count, count(sessionId) " + "from " +
			 * "( " + "select count(*) as request_count, sessionId  " +
			 * "from mainlogs " + "where searchterms is null " +
			 * "and date >= ? " + "and date <= ? " + "and sessionId not in " +
			 * "( " + "select s.sessionId " + "from " + "( " +
			 * "select distinct sessionId  " + "from mainlogs " +
			 * "where searchterms is not null " + "and date >= ? " +
			 * "and date <= ? " + ") as s, " + "( " +
			 * "select distinct sessionId " + "from mainlogs " +
			 * "where searchterms is null " + "and date >= ? " +
			 * "and date <= ? " + ") as n " + "where s.sessionId = n.sessionId "
			 * + ") " + "group by sessionId " + ") as t " +
			 * "group by request_count ";
			 */
			strSQL = "SELECT snv, count(*) FROM ( "
					+ "SELECT sessionid, SUM(searchrequest) as ssr, sum(navrequest) snv "
					+ "FROM mat_sessions_request "
					+ "WHERE date >= ? and date <= ? " + "GROUP BY sessionid "
					+ ")  AS tmprequest " + "WHERE ssr = 0 " + "GROUP BY snv ";

		} else
			return null;

		tmpListResult = (ArrayList<RequestByUser_SearchOrNavigate>) getSimpleJdbcTemplate()
				.query(strSQL, new RowMapper<RequestByUser_SearchOrNavigate>()
				{
					public RequestByUser_SearchOrNavigate mapRow(ResultSet rs,
							int rowNum) throws SQLException
					{
						RequestByUser_SearchOrNavigate tmpRequestByUser = new RequestByUser_SearchOrNavigate();
						tmpRequestByUser.setNumberOfRequests(rs.getInt(1));
						tmpRequestByUser.setNumberOfUsers(rs.getInt(2));
						return tmpRequestByUser;
					}
				}, new Object[]
				{ fromDate, toDate });
		// new Object[] { fromDate, toDate, fromDate, toDate, fromDate, toDate
		// });

		return tmpListResult;
	}

	@Override
	public int getRequestByUser_AllSearchUsers(String fromDate, String toDate)
	{
		int tmpResult = 0;
		/*
		 * String strSQL = "select count(distinct sessionId) as users_count " +
		 * "from mainlogs " + "where date >= ? " + "and date <= ? " +
		 * "and searchterms is not null";
		 */

		String strSQL = "SELECT ssr, count(*) FROM ( "
				+ "SELECT sessionid, SUM(searchrequest) as ssr, sum(navrequest) snv "
				+ "FROM mat_sessions_request "
				+ "WHERE date >= ? and date <= ? " + "GROUP BY sessionid "
				+ ")  AS tmprequest " + "WHERE snv = 0 " + "GROUP BY ssr ";

		tmpResult = getSimpleJdbcTemplate().queryForInt(strSQL, fromDate,
				toDate);

		return tmpResult;
	}

	@Override
	public int getRequestByUser_AllNavigateUsers(String fromDate, String toDate)
	{
		int tmpResult = 0;
		String strSQL = "select count(distinct sessionId) as users_count "
				+ "from mainlogs " + "where date >= ? " + "and date <= ? "
				+ "and searchterms is null";

		tmpResult = getSimpleJdbcTemplate().queryForInt(strSQL, fromDate,
				toDate);

		return tmpResult;
	}

	@Override
	public int getRequestByUser_SearchAndNavigateUsers(String fromDate,
			String toDate)
	{
		int tmpResult = 0;
		String strSQL = "select count(distinct s.sessionId) " + "from " + "( "
				+ "select distinct sessionId " + "from mainlogs "
				+ "where searchterms is not null " + "and date >= ? "
				+ "and date <= ? " + ") as s, " + "( "
				+ "select distinct sessionId " + "from mainlogs "
				+ "where searchterms is null " + "and date >= ? "
				+ "and date <= ? " + ") as n "
				+ "where s.sessionId = n.sessionId ";

		tmpResult = getSimpleJdbcTemplate().queryForInt(strSQL, fromDate,
				toDate, fromDate, toDate);

		return tmpResult;
	}

	@Override
	public ArrayList<RequestByUser_SearchAndNavigate> getRequestByUser_SearchAndNavigateRequests(
			String fromDate, String toDate)
	{
		ArrayList<RequestByUser_SearchAndNavigate> tmpListResult = new ArrayList<RequestByUser_SearchAndNavigate>();
		/*
		 * String strSQL =
		 * "select count(s.sessionId), s.NumOfSearch, n.NumOfNav " + "from " +
		 * "( " + "select count(sessionId) as NumOfSearch, sessionId " +
		 * "from mainlogs " + "where searchterms is not null " +
		 * "and date >= ? " + "and date <= ? " + "group by sessionId " +
		 * ") as s, " + "( " + "select count(sessionId) as NumOfNav, sessionId "
		 * + "from mainlogs " + "where searchterms is null " + "and date >= ? "
		 * + "and date <= ? " + "group by sessionId " + ") as n " +
		 * "where s.sessionId = n.sessionId " +
		 * "group by NumOfSearch, NumOfNav with rollup ";
		 */

		String strSQL = "SELECT count(*), ssr, snv FROM ( "
				+ "SELECT sessionid, SUM(searchrequest) as ssr, sum(navrequest) snv "
				+ "FROM mat_sessions_request "
				+ "WHERE date >= ? and date <= ? " + "GROUP BY sessionid "
				+ ")  AS tmprequest " + "WHERE snv > 0 AND ssr > 0 "
				+ "GROUP BY ssr, snv WITH ROLLUP";

		tmpListResult = (ArrayList<RequestByUser_SearchAndNavigate>) getSimpleJdbcTemplate()
				.query(strSQL, new RowMapper<RequestByUser_SearchAndNavigate>()
				{
					public RequestByUser_SearchAndNavigate mapRow(ResultSet rs,
							int rowNum) throws SQLException
					{
						RequestByUser_SearchAndNavigate tmpRequestByUser_SearchAndNavigate = new RequestByUser_SearchAndNavigate();
						tmpRequestByUser_SearchAndNavigate.setNumberOfUsers(rs
								.getInt(1));
						tmpRequestByUser_SearchAndNavigate
								.setNumberOfSearches(rs.getInt(2));
						tmpRequestByUser_SearchAndNavigate
								.setNumberOfNavigates(rs.getInt(3));
						return tmpRequestByUser_SearchAndNavigate;
					}
				}, new Object[]
				{ fromDate, toDate });// new Object[] { fromDate, toDate,
										// fromDate, toDate });

		return tmpListResult;
	}

	@Override
	public ArrayList<SessionByMinutes> getSessionByMinutes(String fromDate,
			String toDate)
	{
		ArrayList<SessionByMinutes> tmpResult = new ArrayList<SessionByMinutes>();

		/*
		 * String strSQL =
		 * "select len_minutes, count(sessionId) as NumberOfSessions " +
		 * "from  " + "( " +
		 * "select min(minutes) as min_minutes, max(minutes) as max_minutes, max(minutes) - min(minutes) + 1 as len_minutes, sessionId "
		 * + "from  " + "( " +
		 * "select DateToMinutes + TimeToMinutes as minutes, sessionId " +
		 * "from  " + "( " +
		 * "select date, (DATEDIFF(mainlogs.date, \"2010-05-31\") - 1) * 24 * 60 as DateToMinutes, time, round(time_to_sec(time) / 60) as TimeToMinutes, sessionId "
		 * + "from mainlogs " + "where date >= ? " + "and date <= ? " +
		 * ") as ttt " + ") as tt " + "group by sessionId " + ") as t " +
		 * "group by len_minutes";
		 */
		String strSQL = "SELECT datediff, count(*) "
				+ "FROM ("
				+ "SELECT sessionid, TIMESTAMPDIFF(MINUTE, min(CONCAT(`date`, ' ', `time`)) , max(CONCAT(`date`, ' ', `time`))) as datediff from mainlogs where date >= ? and date <= ? GROUP BY sessionid "
				+ ") as tbtmp " + "GROUP BY datediff ORDER BY datediff";

		tmpResult = (ArrayList<SessionByMinutes>) getSimpleJdbcTemplate()
				.query(strSQL, new RowMapper<SessionByMinutes>()
				{
					public SessionByMinutes mapRow(ResultSet rs, int rowNum)
							throws SQLException
					{
						SessionByMinutes tmpSessionByMinutes = new SessionByMinutes();
						tmpSessionByMinutes.setMinutes(rs.getInt(1));
						tmpSessionByMinutes.setNumberOfSessions(rs.getInt(2));
						return tmpSessionByMinutes;
					}
				}, new Object[]
				{ fromDate, toDate });

		return tmpResult;
	}

	@Override
	public String getFirstDate()
	{
		// TODO Auto-generated method stub
		String query = "select MIN(date) from mainlogs";
		return getSimpleJdbcTemplate().queryForObject(query, String.class);
	}

	@Override
	public void changeEngineToMyISAM()
	{
		// TODO Auto-generated method stub
		String query = "ALTER TABLE dimensions ENGINE=MyISAM";
		String query1 = "ALTER TABLE mainlogs ENGINE=MyISAM";
		String query2 = "ALTER TABLE valuetable ENGINE=MyISAM";
		getSimpleJdbcTemplate().update(query);
		getSimpleJdbcTemplate().update(query1);
		getSimpleJdbcTemplate().update(query2);

	}

	@Override
	public void changeEngineToInnoDB()
	{
		// TODO Auto-generated method stub
		String query = "ALTER TABLE dimensions ENGINE=InnoDB";
		String query1 = "ALTER TABLE mainlogs ENGINE=InnoDB";
		String query2 = "ALTER TABLE valuetable ENGINE=InnoDB";
		getSimpleJdbcTemplate().update(query);
		getSimpleJdbcTemplate().update(query1);
		getSimpleJdbcTemplate().update(query2);

	}

	@Override
	public List<RequestBy> getRequestByDimVal(String sDate, String eDate,
			String dimension, String value)
	{
		String query = "";
		query = "SELECT numofoccurences, Date FROM mat_dimensionvalues_one WHERE date >= ? and date <= ? and dimension LIKE ? and value LIKE ? ORDER BY date";
		System.out.println("query: " + query);
		List<RequestBy> result = getSimpleJdbcTemplate().query(query,
				new RowMapper<RequestBy>()
				{
					public RequestBy mapRow(ResultSet rs, int rowNum)
							throws SQLException
					{
						RequestBy result = new RequestBy();
						result.setRequests(rs.getInt(1));
						result.setTime(rs.getString(2));
						return result;
					}
				}, sDate, eDate, dimension, value);
		return result;
	}

	@Override
	public HashMap<String, List<SimpleUserTrends>> getNormalizedDatas(
			String Dimension)
	{
		long start = System.currentTimeMillis();
		HashMap<String, List<SimpleUserTrends>> normalizedSampleData = new HashMap<String, List<SimpleUserTrends>>();
		int SumOfDimensionRequests = 0;
		int SumOfNumberOfDataEntries = 0;
		ArrayList<Double> requestsArray = new ArrayList<Double>();
		String query1 = "SELECT value FROM valuetable WHERE dimension = ?";
		List<String> valueNames = getSimpleJdbcTemplate().query(query1,
				new RowMapper<String>()
				{
					public String mapRow(ResultSet rs, int rowNum)
							throws SQLException
					{
						return rs.getString("value");
					}
				}, Dimension);

		long breakpoint1 = System.currentTimeMillis();
		System.out.println("First SQL finished: " + (breakpoint1 - start)
				/ 1000 + "s");

		String query2 = "SELECT numofoccurences, date FROM mat_dimensionvalues_one WHERE dimension LIKE ? and value LIKE ? ORDER BY date";
		for (String name : valueNames)
		{
			List<SimpleUserTrends> result = getSimpleJdbcTemplate().query(
					query2, new RowMapper<SimpleUserTrends>()
					{
						public SimpleUserTrends mapRow(ResultSet rs, int rowNum)
								throws SQLException
						{
							SimpleUserTrends valueRequestsByDate = new SimpleUserTrends();
							valueRequestsByDate.setRequest(rs.getInt(1));
							valueRequestsByDate.setDate(rs.getString(2));
							DateFormat formatter = new SimpleDateFormat(
									"yyyy-MM-dd");
							Date date = new Date();
							try
							{
								date = (Date) formatter.parse(rs.getString(2));
							} catch (ParseException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							valueRequestsByDate.setLongdate(date.getTime());
							return valueRequestsByDate;
						}
					}, Dimension, name);
			SumOfNumberOfDataEntries += result.size();
			for (SimpleUserTrends dmo : result)
			{
				requestsArray.add(dmo.getRequest());
				SumOfDimensionRequests += dmo.getRequest();
			}
			normalizedSampleData.put(name, result);
		}
		long breakpoint2 = System.currentTimeMillis();
		System.out.println("Second SQL finished: "
				+ (breakpoint2 - breakpoint1) / 1000 + "s");

		double meanOfSample = SumOfDimensionRequests / SumOfNumberOfDataEntries;
		StandardDeviation sdcalculater = new StandardDeviation(true);
		double standardDeviation = sdcalculater.calculateStandardDeviation(
				requestsArray, meanOfSample);
		// normalized data
		for (String dimensionValue : valueNames)
		{
			for (SimpleUserTrends dmo : normalizedSampleData
					.get(dimensionValue))
			{
				dmo.getRequest();
				double newValue = 0;
				newValue = normalizeValues(dmo.getRequest(), meanOfSample,
						standardDeviation);
				dmo.setNormalizedRequest(newValue);
			}
		}
		long end = System.currentTimeMillis();
		System.out.println("Time Spent: " + (end - start) / 1000 + "s");
		// end
		return normalizedSampleData;
	}

	// calculate standard score for each <code>unnormal</code> in order to
	// normalize the data set
	private double normalizeValues(double unnormal, double average,
			double standdiv)
	{
		double normal = 0;
		normal = (unnormal - average) / standdiv;
		return normal;
	}

	@Override
	public double getTimeConsume(String Identifier, int TimeScope,
			TimeEstimation.Type type)
	{
		String result = "-1";
		String tmpSQL = "";
		if (type == Type.TYPE_MIN)
			tmpSQL = "select TimeConsumeMin from mat_TimePerformance_Table where Identifier = ? and TimeScope = ?";
		else if (type == Type.TYPE_MAX)
			tmpSQL = "select TimeConsumeMax from mat_TimePerformance_Table where Identifier = ? and TimeScope = ?";
		else
			tmpSQL = "select TimeConsumeRecent from mat_TimePerformance_Table where Identifier = ? and TimeScope = ?";
		try
		{
			result = getSimpleJdbcTemplate().queryForObject(tmpSQL,
					String.class, Identifier, TimeScope);
		} catch (DataAccessException e)
		{
			result = "-1";
		}
		return Double.parseDouble(result);
	}

	@Override
	public void UpdateTimeConsumeTable(String Identifier, int TimeScope,
			double TimeConsume1, double TimeConsume2, double TimeConsume3)
	{
		String tmpSQL = "";
		tmpSQL = "delete from mat_TimePerformance_Table where Identifier = ? and TimeScope = ?";
		getSimpleJdbcTemplate().update(tmpSQL, Identifier, TimeScope);

		tmpSQL = "insert into mat_TimePerformance_Table(Identifier, TimeScope, TimeConsumeMin, TimeConsumeMax, TimeConsumeRecent) values(?, ?, ?, ?, ?);";
		getSimpleJdbcTemplate().update(tmpSQL, Identifier, TimeScope,
				TimeConsume1, TimeConsume2, TimeConsume3);
	}

	@Override
	public List<TimePerformance> getAllTimeScopeAndTimeConsume(
			String Identifier, TimeEstimation.Type type)
	{
		String tmpSQL = "";
		if (type == Type.TYPE_MIN)
			tmpSQL = "select TimeScope, TimeConsumeMin from mat_TimePerformance_Table where Identifier = ?";
		else if (type == Type.TYPE_MAX)
			tmpSQL = "select TimeScope, TimeConsumeMax from mat_TimePerformance_Table where Identifier = ?";
		else
			tmpSQL = "select TimeScope, TimeConsumeRecent from mat_TimePerformance_Table where Identifier = ?";

		List<TimePerformance> result = getSimpleJdbcTemplate().query(tmpSQL,
				new RowMapper<TimePerformance>()
				{
					public TimePerformance mapRow(ResultSet rs, int rowNum)
							throws SQLException
					{
						TimePerformance result = new TimePerformance();
						result.setTimeScope(rs.getDouble(1));
						result.setTimeConsume(rs.getDouble(2));
						return result;
					}
				}, Identifier);

		return result;
	}

	@Override
	public boolean createTimePerformanceTable()
	{
		String tmpSQL = "create table if not exists mat_TimePerformance_Table(Identifier char(30), TimeScope int, TimeConsumeMin char(30), TimeConsumeMax char(30), TimeConsumeRecent char(30), primary key(Identifier, TimeScope))";
		try
		{
			getSimpleJdbcTemplate().update(tmpSQL);
		} catch (DataAccessException e)
		{
			return false;
		}
		return true;
	}

	@Override
	public boolean destroyTimePerformanceTable()
	{
		String tmpSQL = "drop table if exists mat_TimePerformance_Table";
		try
		{
			getSimpleJdbcTemplate().update(tmpSQL);
		} catch (DataAccessException e)
		{
			return false;
		}
		return true;
	}
}