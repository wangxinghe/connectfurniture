		$(document).ready(function() {

			//select all the a tag with name equal to modal
			$('a[name=modal]').click(function(e) {
				//Cancel the link behavior
				e.preventDefault();

				//Get the A tag
				var id = $(this).attr('href');

				//Get the screen height and width
				var maskHeight = $(document).height();
				var maskWidth = $(window).width();

				//Set heigth and width to mask to fill up the whole screen
				$('#mask').css({
					'width' : maskWidth,
					'height' : maskHeight
				});

				//transition effect		
				//$('#mask').fadeIn(200);
				$('#mask').fadeTo("slow", 0.2);

				//Get the window height and width
				var winH = $(window).height();
				var winW = $(window).width();

				//Set the popup window to center
				$(id).css('top', (winH-20) / 2 - $(id).height() / 2);
				$(id).css('left', (winW-20) / 2 - $(id).width() / 2);

				//transition effect
				//$(id).fadeIn(2000);
				$(id).fadeTo("slow", 0.8);

			});

			//JQuery show more
			$(".flip").click(function() {
				$(".panel").slideToggle(100);
			});
		});
		function formsubmit() {
			var popup = document.getElementById("popup");
			clickLink(popup);
		}
		function clickLink(link) {
			var cancelled = false;
			if (document.createEvent) {
				var event = document.createEvent("MouseEvents");
				event.initMouseEvent("click", true, true, window, 0, 0, 0, 0,
						0, false, false, false, false, 0, null);
				cancelled = !link.dispatchEvent(event);
			} else if (link.fireEvent) {
				cancelled = !link.fireEvent("onclick");
			}
			if (!cancelled) {
				window.location = link.href;
			}
		}