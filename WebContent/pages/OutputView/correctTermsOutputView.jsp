<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ConnectFurniture - Auto-Correct Terms</title>
<link type="text/css" href="css/datepicker.css" rel="stylesheet" />
<link type="text/css" href="css/Style.css" rel="stylesheet" />
<link rel="shortcut icon"
	href="http://connectfurniture.com.au/favicon.ico" />
<script type="text/javascript" src="js/datepicker.js"></script>
<script type="text/javascript" src="js/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="js/jquery.tabify.js"></script>
<script type="text/javascript" src="js/OutputCommon.js"></script> 
</head>
<body>
	<!-- wrap starts here -->
	<div id="wrap">

		<!--header -->
		<div id="header"></div>

		<!-- menu -->
		<div id="menu">
			<p class="align-right">
				<a class="login" href=<c:out value="${pageName}" />><c:out
						value="${value}" /> </a>
			</p>
			<p class="breadcrumb">
				<a href="home">Home</a>
				<a href="correctterms">Auto-Corrected Terms</a>
				<a href="#" onclick="return false;">Result</a>
			</p>
		</div>
		<br />
		<!-- content-wrap starts here -->
		<div id="content-wrap">

			<div id="main">
				<div class="title_div">
					<h1>
						<c:out value="${reportName}" />
						<br />
						<c:out value="${reportName1}" />
					</h1>
				</div>
				<div class="modify_search">
					<input class="button" type="button" value="Modify Search" onclick="location.href='correctterms'">
				</div>
				<div style="clear:both;"></div> 
				<!-- Auto correct terms-->
				<c:if
					test="${CorrectTermsList!=null}">
					<h2>${date}</h2>
					<c:if test="${img!=null}">
						<img src=<c:out value="${img}" />>
						<br />
						<br />
					</c:if>
					<table class="report">
						<tr>
							<c:if
								test="${CorrectTermsList!=null}">
								<th width="5%">#</th>
								<th width="30%"><c:out value="${th1}" /></th>
								<th width="30%"><c:out value="${th2}" /></th>
								<th><c:out value="${th3}" /></th>
							</c:if>
						</tr>
						<c:forEach var="correctTerms" items="${CorrectTermsList}"
							varStatus="status">
							<tr>
								<td><c:out value="${status.index+1}" /></td>
								<td><c:out value="${correctTerms.searchterm}" /></td>
								<td><c:out value="${correctTerms.correctto}" /></td>
								<td><fmt:formatNumber value="${correctTerms.request}" /></td>
							</tr>
						</c:forEach>
						<c:if test="${totalRequests1!=null}">
							<tr>
								<td>Total Requests</td>
								<td><c:out value="${totalRequests1}" /></td>
							</tr>
						</c:if>
					</table>
					<br />
				</c:if>
			</div>

			<div id="sidebar">
				<jsp:include page="../includes/sidebar.jsp" />

			</div>
		</div>
		<!-- content-wrap ends here -->

		<!--footer starts here-->
		<div id="footer">
			<jsp:include page="../includes/footer.jsp" />
		</div>


	</div>
	<!-- wrap ends here -->
</body>
</html>