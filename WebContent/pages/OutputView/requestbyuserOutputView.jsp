<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ConnectFurniture - Request By User</title>
<link type="text/css" href="css/datepicker.css" rel="stylesheet" />
<link type="text/css" href="css/Style.css" rel="stylesheet" />
<link rel="shortcut icon"
	href="http://connectfurniture.com.au/favicon.ico" />
<script type="text/javascript" src="js/datepicker.js"></script>
<script type="text/javascript" src="js/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="js/jquery.tabify.js"></script>
<script type="text/javascript" src="js/OutputCommon.js"></script> 
</head>
<body>
<!-- wrap starts here -->
	<div id="wrap">

		<!--header -->
		<div id="header"></div>

		<!-- menu -->
		<div id="menu">
			<p class="align-right">
				<a class="login" href=<c:out value="${pageName}" />><c:out
						value="${value}" /> </a>
			</p>
			<p class="breadcrumb">
				<a href="home">Home</a>
				<a href="requestbyuser">Request By User</a>
				<a href="#" onclick="return false;">Result</a>
			</p>
		</div>
		<br />
		<!-- content-wrap starts here -->
		<div id="content-wrap">
			<!-- keywordSearch Result starts here --> 
			<div id="main">
				<div class="title_div">
					<h1>
						<c:out value="${reportName}" />
						<br />
						<c:out value="${reportName1}" />
					</h1>
				</div>
				<div class="modify_search">
					<input class="button" type="button" value="Modify Search" onclick="location.href='requestbyuser'">
				</div>
				<div style="clear:both;"></div> 
				<h2>${date}</h2>
				<!-- Search Only Requests Result and Navigate Only Requests Result -->
				<c:if test="${ArrayListResultIndex != null}">
					<table class="report">
						<tr>
							<th align="left">Number Of Requests[${RequestType}]</th>
							<th align="right">Number Of Users</th>
						</tr>
						<c:forEach var="RequestByUser" items="${ArrayListResultIndex}" varStatus="status">
							<tr>
								<td align="left"><fmt:formatNumber type="number" value="${RequestByUser.numberOfRequests}" /></td>
								<td align="right"><fmt:formatNumber type="number" value="${RequestByUser.numberOfUsers}" /></td>
							</tr>
						</c:forEach>
						<tr>
							<td colspan="3" height="1px" bgcolor="#88CF00"></td>
						</tr>
						<tr>
							<th align="left">Total Users</th>
							<td align="right"><fmt:formatNumber type="number" value="${TotalUsers_SearchOrNavigateOnly}" /></td>
						</tr>
					</table>
					<br />
				</c:if>
				<!-- Search And Navigate Requests Result -->
				<c:if test="${ArrayListSearchAndNavigateResultIndex!=null}">
					<table class="report">
						<tr>
							<th align="left">Number Of Searches</th>
							<th align="center">Number Of Navigates</th>
							<th align="right">Number Of Users</th>
						</tr>
						<c:forEach var="RequestByUser_SearchAndNavigate" items="${ArrayListSearchAndNavigateResultIndex}" varStatus="status">
							<c:if test="${RequestByUser_SearchAndNavigate.numberOfSearches!=0&&RequestByUser_SearchAndNavigate.numberOfNavigates!=0}">
								<tr>
									<td align="left"><fmt:formatNumber type="number" value="${RequestByUser_SearchAndNavigate.numberOfSearches}" /></td>
									<td align="center"><fmt:formatNumber type="number" value="${RequestByUser_SearchAndNavigate.numberOfNavigates}" /></td>
									<td align="right"><fmt:formatNumber type="number" value="${RequestByUser_SearchAndNavigate.numberOfUsers}" /></td>
								</tr>
							</c:if>
						</c:forEach>
						<tr>
							<td colspan="3" height="1px" bgcolor="#88CF00"></td>
						</tr>
						<tr>
							<th align="left">Total Users</th>
							<td></td>
							<td align="right"><fmt:formatNumber type="number" value="${TotalUsers_SearchAndNavigate}" /></td>
						</tr>
					</table>
				</c:if>
				<!-- All Requests Result -->
				<c:if test="${ArrayListAllRequestIndex!=null}">
					<table class="report">
						<tr>
							<th align="left">Number Of Searches</th>
							<th align="center">Number Of Navigates</th>
							<th align="right">Number Of Users</th>
						</tr>
						<c:forEach var="RequestByUser_All" items="${ArrayListAllRequestIndex}" varStatus="status">
							<c:if test="${RequestByUser_All.numberOfSearches!=0||RequestByUser_All.numberOfNavigates!=0||RequestByUser_All.numberOfUsers!=0}">
								<tr>
									<td align="left"><fmt:formatNumber type="number" value="${RequestByUser_All.numberOfSearches}" /></td>
									<td align="center"><fmt:formatNumber type="number" value="${RequestByUser_All.numberOfNavigates}" /></td>
									<td align="right"><fmt:formatNumber type="number" value="${RequestByUser_All.numberOfUsers}" /></td>
								</tr>
							</c:if>
							<c:if test="${RequestByUser_All.numberOfSearches==0&&RequestByUser_All.numberOfNavigates==0&&RequestByUser_All.numberOfUsers==0}">
								<tr>
									<td colspan="3" height="1px" bgcolor="#88CF00"></td>
								</tr>
							</c:if>
						</c:forEach>
						<tr>
							<td colspan="3" height="1px" bgcolor="#88CF00"></td>
						</tr>
						<tr>
							<th align="left">Total Users</th>
							<td></td>
							<td align="right"><fmt:formatNumber type="number" value="${TotalUsers_All}" /></td>
						</tr>
					</table>
				</c:if>
				<!-- All Requests With Graph Result -->
				<c:if test="${img!=null}">
					<img src=<c:out value="${img}" />>
					<br />
					<table class="report">
						<tr>
							<th width="50%">Type</th>
							<th align = "right">Number Of Users</th>
						</tr>
						<tr>
							<td>Search Only</td>
							<td align = "right">
								<fmt:formatNumber type="number" value="${tmpSearchOnly}" />
							</td>
						</tr>
						<tr>
							<td>Navigate Only</td>
							<td align = "right">
								<fmt:formatNumber type="number" value="${tmpNavigateOnly}" />
							</td>
						</tr>
						<tr>
							<td>Search And Navigate</td>
							<td align = "right">
								<fmt:formatNumber type="number" value="${tmpSearchAndNavigate}" />
							</td>
						</tr>
						<tr>
							<td colspan="3" height="1px" bgcolor="#88CF00"></td>
						</tr>
						<tr>
							<th>Total Users</th>
							<td align = "right">
								<fmt:formatNumber type="number" value="${tmpSearchAndNavigate + tmpNavigateOnly + tmpSearchOnly}" />
							</td>
						</tr>
					</table>
				</c:if> 
			</div>
			<!-- keywordSearch Result ends here -->

			<div id="sidebar">
				<jsp:include page="../includes/sidebar.jsp" />

			</div>
		</div>
		<!-- content-wrap ends here -->

		<!--footer starts here-->
		<div id="footer">
			<jsp:include page="../includes/footer.jsp" />
		</div>


	</div>
	<!-- wrap ends here -->
</body>
</html>
