<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ConnectFurniture - Session By Minutes</title>
<link type="text/css" href="css/datepicker.css" rel="stylesheet" />
<link type="text/css" href="css/Style.css" rel="stylesheet" />
<link rel="shortcut icon" href="http://connectfurniture.com.au/favicon.ico" />
<script type="text/javascript" src="js/datepicker.js"></script>
<script type="text/javascript" src="js/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="js/jquery.tabify.js"></script>
<script type="text/javascript" src="js/OutputCommon.js"></script> 
</head>
<body>
<!-- wrap starts here -->
	<div id="wrap">

		<!--header -->
		<div id="header"></div>

		<!-- menu -->
		<div id="menu">
			<p class="align-right">
				<a class="login" href=<c:out value="${pageName}" />><c:out
						value="${value}" /> </a>
			</p>
			<p class="breadcrumb">
				<a href="home">Home</a>
				<a href="sessionbyminutes">Session By Minutes</a>
				<a href="#" onclick="return false;">Result</a>
			</p>
		</div>
		<br />
		<!-- content-wrap starts here -->
		<div id="content-wrap">
			<!-- SessionByMinutes Result starts here --> 
			<div id="main">
				<div class="title_div">
					<h1>
						<c:out value="${reportName}" />
						<br />
						<c:out value="${reportName1}" />
					</h1>
				</div>
				<div class="modify_search">
					<input class="button" type="button" value="Modify Search" onclick="location.href='sessionbyminutes'">
				</div>
				<div style="clear:both;"></div> 
				<c:choose>
					<c:when test="${(ArrayListResultIndex != null) && (ArrayListResultIndexSize != 0)}">
					<h2>${date}</h2>
					<table class="report">
						<tr>
							<!--th width="30%" align="left">#[Top ${numberOfRecords}]</th-->
							<th width="30%" align="center">Minutes</th>
							<th align="center">Number Of Sessions</th>
						</tr>
						<c:forEach var="SessionByMinutes" items="${ArrayListResultIndex}" varStatus="status">
							<!--c:if test="${status.index < numberOfRecords}"-->
								<tr>
									<!-- td><fmt:formatNumber type="number"  value="${status.index+1}" /></td-->
									<td align="center">
										<fmt:formatNumber type="number" value="${SessionByMinutes.minutes}" /></td>
									<td align="center">
										<fmt:formatNumber type="number" value="${SessionByMinutes.numberOfSessions}" /></td>
								</tr>
							<!--/c:if-->
						</c:forEach>
					</table>
					<br />
					</c:when>
					<c:otherwise>
						<br/>
						<p>No Results Found</p>
					</c:otherwise>
				</c:choose>
			</div>
			<!-- SessionByMinutes Result ends here -->

			<div id="sidebar">
				<jsp:include page="../includes/sidebar.jsp" />

			</div>
		</div>
		<!-- content-wrap ends here -->

		<!--footer starts here-->
		<div id="footer">
			<jsp:include page="../includes/footer.jsp" />
		</div>


	</div>
	<!-- wrap ends here -->
</body>
</html>