<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ConnectFurniture - Keyword Search</title>
<link type="text/css" href="css/datepicker.css" rel="stylesheet" />
<link type="text/css" href="css/Style.css" rel="stylesheet" />
<link rel="shortcut icon"
	href="http://connectfurniture.com.au/favicon.ico" />
<script type="text/javascript" src="js/datepicker.js"></script>
<script type="text/javascript" src="js/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="js/jquery.tabify.js"></script>
<script type="text/javascript" src="js/OutputCommon.js"></script> 
<script type="text/javascript">
	function Show(Parameter)
	{
		$(".moredetail" + Parameter).slideDown(200);
	}
	
	function Hide(Parameter)
	{
		$(".moredetail" + Parameter).slideUp(200);
	}
</script> 
</head>
<body>
<!-- wrap starts here -->
	<div id="wrap">

		<!--header -->
		<div id="header"></div>

		<!-- menu -->
		<div id="menu">
			<p class="align-right">
				<a class="login" href=<c:out value="${pageName}" />><c:out
						value="${value}" /> </a>
			</p>
			<p class="breadcrumb">
				<a href="home">Home</a>
				<a href="keywordsearch">Keyword Search</a>
				<a href="#" onclick="return false;">Result</a>
			</p>
		</div>
		<br />
		<!-- content-wrap starts here -->
		<div id="content-wrap">
			<!-- keywordSearch Result starts here --> 
			<div id="main">
				<h1>
					<c:out value="${reportName}" />
					<br />
					<c:out value="${reportName1}" />
				</h1>
				<h2>${date}</h2>
				<c:choose>
					<c:when test="${(keywordSearchList != null) && (keywordSearchListSize != 0)}">
						<table class="report">
							<tr>
								<th width="35px">#</th>
								<th width="280px">Search Term</th>
								<th width="245px">Number of Results</th>
								<th>Number of Request</th>
							</tr>
							<c:forEach var="keywordDetail" items="${keywordSearchList}" varStatus="status">
								<tr>
									<td><c:out value="${status.index+1}" /></td>
									<td><c:out value="${keywordDetail.searchterms}" /></td>
									<td><fmt:formatNumber value="${keywordDetail.records}" /></td>
									<td><fmt:formatNumber value="${keywordDetail.requests}" /></td>
								</tr>
							</c:forEach>
						</table>
						<br />
					</c:when>
					<c:otherwise>
						<br/>
						<p>No Results Found</p>
					</c:otherwise>
				</c:choose>
			</div>
			<!-- keywordSearch Result ends here -->

			<div id="sidebar">
				<jsp:include page="../includes/sidebar.jsp" />

			</div>
		</div>
		<!-- content-wrap ends here -->

		<!--footer starts here-->
		<div id="footer">
			<jsp:include page="../includes/footer.jsp" />
		</div>


	</div>
	<!-- wrap ends here -->
</body>
</html>