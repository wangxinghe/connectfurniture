<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ConnectFurniture - Top Dimension Searches</title>
<link type="text/css" href="css/datepicker.css" rel="stylesheet" />
<link type="text/css" href="css/Style.css" rel="stylesheet" />
<link rel="shortcut icon"
	href="http://connectfurniture.com.au/favicon.ico" />
<script type="text/javascript" src="js/datepicker.js"></script>
<script type="text/javascript" src="js/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="js/jquery.tabify.js"></script>
<script type="text/javascript" src="js/OutputCommon.js"></script> 
</head>
<body>
	<!-- wrap starts here -->
	<div id="wrap">

		<!--header -->
		<div id="header"></div>

		<!-- menu -->
		<div id="menu">
			<p class="align-right">
				<a class="login" href=<c:out value="${pageName}" />><c:out
						value="${value}" /> </a>
			</p>
			<p class="breadcrumb">
				<a href="home">Home</a>
				<a href="topsearches">Top Searches</a>
				<a href="#" onclick="return false;">Result</a>
			</p>
		</div>
		<br />
		<!-- content-wrap starts here -->
		<div id="content-wrap">

			<div id="main">
				<div class="title_div">
					<h1>
						<c:out value="${reportName}" />
						<br />
						<c:out value="${reportName1}" />
					</h1>
				</div>
				<div class="modify_search">
					<input class="button" type="button" value="Modify Search" onclick="location.href='topsearches'">
				</div>
				<div style="clear:both;"></div>
				<!-- top searches starts here -->
				<c:if test="${topDimsSearchLists!=null}">
					<div class='slider'>
						<button class='prev'>&lt;</button>
						<button class='next'>&gt;</button>
						<div>
							<ul id="menu1" class="menu1">
								<c:forEach var="topDimsSearchList" items="${topDimsSearchLists}"
									varStatus="status">
									<c:if test="${status.index==0}">
										<li class="active"><a
											href=<c:out value="#${topDimsSearchList.key}${status.index}" />><c:out
													value="${topDimsSearchList.key}" /> </a></li>
									</c:if>
									<c:if test="${status.index!=0}">
										<li><a
											href=<c:out value="#${topDimsSearchList.key}${status.index}" />><c:out
													value="${topDimsSearchList.key}" /> </a></li>
									</c:if>
								</c:forEach>
							</ul>
						</div>
					</div>


					<c:forEach var="topDimsSearchList" items="${topDimsSearchLists}"
						varStatus="status">
						<div id="${topDimsSearchList.key}${status.index}" class="content1">
							<h2>${topDimsSearchList.key}:${dates[status.index]}</h2>
							<img src=<c:out value="pages/temp/${status.index}.jpg" />><br />
							<table class="report">
								<tr>
									<th width="5%">#</th>
									<th width="65%">Dimensions</th>
									<th width="15%">Requests</th>
									<th width="15%">% of Total</th>
								</tr>
								<c:forEach var="topDimsSearch"
									items="${topDimsSearchList.value}" varStatus="status">
									<tr>
										<td><c:out value="${status.index+1}" /></td>
										<td><c:out value="${topDimsSearch.allDimensions}" /></td>
										<td style="margin-right: 10px;"><fmt:formatNumber type="number" 
             value="${topDimsSearch.count}" />
												</td>
										<td><c:out value="${topDimsSearch.percentage}" /></td>
									</tr>
								</c:forEach>
							</table>
						</div>
					</c:forEach>
				</c:if>
				<!-- top searches end here -->
			</div>

			<div id="sidebar">
				<jsp:include page="../includes/sidebar.jsp" />

			</div>
		</div>
		<!-- content-wrap ends here -->

		<!--footer starts here-->
		<div id="footer">
			<jsp:include page="../includes/footer.jsp" />
		</div>


	</div>
	<!-- wrap ends here -->
</body>
</html>