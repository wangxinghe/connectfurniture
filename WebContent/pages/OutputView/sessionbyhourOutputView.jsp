<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>sessionbyhourOutputView</title>
<link type="text/css" href="css/datepicker.css" rel="stylesheet" />
<link type="text/css" href="css/Style.css" rel="stylesheet" />
<link rel="shortcut icon"
	href="http://connectfurniture.com.au/favicon.ico" />
<script type="text/javascript" src="js/datepicker.js"></script>
<script type="text/javascript" src="js/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="js/jquery.tabify.js"></script>
<script type="text/javascript" src="js/OutputCommon.js"></script> 
</head>
<body>
	<!-- wrap starts here -->
	<div id="wrap">

		<!--header -->
		<div id="header"></div>

		<!-- menu -->
		<div id="menu">
			<p class="align-right">
				<a class="login" href=<c:out value="${pageName}" />><c:out
						value="${value}" /> </a>
			</p>
			<p class="breadcrumb">
				<a href="home">Home</a>
			</p>
		</div>
		<br />
		<!-- content-wrap starts here -->
		<div id="content-wrap">

			<div id="main">
				<h1>
					<c:out value="${reportName}" />
					<br />
					<c:out value="${reportName1}" />
				</h1>
				<br>
				<!-- Request by hour & Request by type & Session by hour -->
				<c:if
					test="${requestByTypeList!=null || requestByHourList!=null || sessionByHourList!=null}">
					<h2>${date}</h2>
					<c:if test="${img!=null}">
						<img src=<c:out value="${img}" />>
						<br />
						<br />
					</c:if>
					<table class="report">
						<tr>
							<c:if test="${requestByTypeList!=null}">
								<th width="85%"><c:out value="${th1}" /></th>
								<th width="15%"><c:out value="${th2}" /></th>
							</c:if>
							<c:if
								test="${requestByHourList!=null && requestByHourList1==null}">
								<th width="85%"><c:out value="${th1}" /></th>
								<th width="15%"><c:out value="${th2}" /></th>
							</c:if>
							<c:if test="${requestByHourList1!=null}">
								<th width="25%"><c:out value="${th1}" /></th>
								<th width="25%"><c:out value="${th2}" /></th>
								<th width="25%"><c:out value="${th3}" /></th>
								<th width="25%"><c:out value="${th4}" /></th>
							</c:if>
							<c:if
								test="${sessionByHourList!=null && sessionByHourList1==null}">
								<th width="85%"><c:out value="${th1}" /></th>
								<th width="15%"><c:out value="${th2}" /></th>
							</c:if>
							<c:if test="${sessionByHourList1!=null}">
								<th width="25%"><c:out value="${th1}" /></th>
								<th width="25%"><c:out value="${th2}" /></th>
								<th width="25%"><c:out value="${th3}" /></th>
								<th width="25%"><c:out value="${th4}" /></th>
							</c:if>
						</tr>
						<c:forEach var="request" items="${requestByTypeList}"
							varStatus="status">
							<tr>
								<td><c:out value="${request.key}" /></td>
								<td><c:out value="${request.value}" /></td>
							</tr>
						</c:forEach>
						<c:forEach var="request" items="${requestByHourList}"
							varStatus="status">
							<tr>
								<td><c:out value="${request.hour}" /></td>
								<td><c:out value="${request.requests}" /></td>

								<c:if test="${requestByHourList1!=null}">
									<td><c:out
											value="${requestByHourList1[status.index].hour}" /></td>
									<td><c:out
											value="${requestByHourList1[status.index].requests}" /></td>
								</c:if>
							</tr>
						</c:forEach>
						<c:forEach var="session" items="${sessionByHourList}"
							varStatus="status">
							<tr>
								<td><c:out value="${session.hour}" /></td>
								<td><c:out value="${session.sessions}" /></td>

								<c:if test="${sessionByHourList1!=null}">
									<td><c:out
											value="${sessionByHourList1[status.index].hour}" /></td>
									<td><c:out
											value="${sessionByHourList1[status.index].sessions}" /></td>
								</c:if>
							</tr>
						</c:forEach>
						<c:if test="${totalRequests!=null}">
							<tr>
								<td>Total Requests</td>
								<td><c:out value="${totalRequests}" /></td>
								<c:if test="${totalRequests1!=null}">
									<td>Total Requests</td>
									<td><c:out value="${totalRequests1}" /></td>
								</c:if>
							</tr>
						</c:if>
						<c:if test="${totalSessions!=null}">
							<tr>
								<td>Total Sessions</td>
								<td><c:out value="${totalSessions}" /></td>
								<c:if test="${totalSessions1!=null}">
									<td>Total Sessions</td>
									<td><c:out value="${totalSessions1}" /></td>
								</c:if>
							</tr>
						</c:if>
					</table>
					<br />
				</c:if>
			</div>

			<div id="sidebar">
				<jsp:include page="../includes/sidebar.jsp" />

			</div>
		</div>
		<!-- content-wrap ends here -->

		<!--footer starts here-->
		<div id="footer">
			<jsp:include page="../includes/footer.jsp" />
		</div>


	</div>
	<!-- wrap ends here -->
</body>
</html>