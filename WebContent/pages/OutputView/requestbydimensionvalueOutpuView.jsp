<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ConnectFurniture - Requests by Dimension Value</title>
<link type="text/css" href="css/datepicker.css" rel="stylesheet" />
<link type="text/css" href="css/Style.css" rel="stylesheet" />
<link rel="shortcut icon"
	href="http://connectfurniture.com.au/favicon.ico" />
<script type="text/javascript" src="js/datepicker.js"></script>
<script type="text/javascript" src="js/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="js/jquery.tabify.js"></script>
<script type="text/javascript" src="js/OutputCommon.js"></script>
</head>
<body>
	<!-- wrap starts here -->
	<div id="wrap">

		<!--header -->
		<div id="header"></div>

		<!-- menu -->
		<div id="menu">
			<p class="align-right">
				<a class="login" href=<c:out value="${pageName}" />><c:out
						value="${value}" /> </a>
			</p>
			<p class="breadcrumb">
				<a href="home">Home</a>
				<a href="topdimensionvalues">Top Value Searches</a>
				<a href="#" onclick="return false;">Result</a>
			</p>
		</div>
		<br />
		<!-- content-wrap starts here -->
		<div id="content-wrap">

			<div id="main">
				<div class="title_div">
					<h1>
						<c:out value="${reportName}" />
						<br />
						<c:out value="${reportName1}" />
					</h1>
				</div>
				<div class="modify_search">
					<input class="button" type="button" value="Modify Search" onclick="location.href='requestbydimensionvalue'">
				</div>
				<div style="clear:both;"></div>
				<!-- TopDimensionValuesBegin -->
				<h2>${dates}</h2>
					<c:if test="${img!=null}">
						<img src=<c:out value="${img}" />>
						<br />
						<br />
					<table class="report">
						<tr>
							<th width="33%"><c:out value="${th1}"></c:out></th>
							<th width="33%"><c:out value="${th2}"></c:out></th>
							<th width="34%"><c:out value="${th3}"></c:out></th>
						</tr>
						<tr>
							<td><c:out value="${averageRequest}"></c:out></td>
							<td><c:out value="${highest.requests}"></c:out></td>
							<td><c:out value="${lowest.requests}"></c:out></td>
						</tr>
						<tr>
							<c:if
								test="${periodRequests!=null}">
								<th width="33%"><c:out value="${th4}" /></th>
								<th width="33%"><c:out value="${th5}" /></th>
								<th width="33%"><c:out value="${th6}" /></th>
							</c:if>
						</tr>
						<c:forEach var="prequest" items="${periodRequests}" varStatus = "status">
						<tr>
							<td><c:out value="${status.index+1}" /></td>
							<td><c:out value="${prequest.time}" /></td>
							<td><fmt:formatNumber value="${prequest.requests}" /></td>
						</tr>
						</c:forEach>
						<tr>
							<c:if
								test="${requestByList!=null}">
								<th width="50%"><c:out value="${th7}" /></th>
								<th width="50%"><c:out value="${th8}" /></th>
							</c:if>
						</tr>
						<c:forEach var="request" items="${requestByList}"
							varStatus="status">
							<tr>
								<td><c:out value="${request.time}" /></td>
								<td><fmt:formatNumber value="${request.requests}" /></td>
							</tr>
						</c:forEach>
						<c:if test="${totalRequests!=null}">
							<tr>
								<td>Total Requests</td>
								<td><fmt:formatNumber type ="number" value="${totalRequests}" /></td>
							</tr>
						</c:if>
					</table>
					</c:if>
					<c:if test="${zipFileName!=null}">
						<p>Please click the following link to download the zip file</p>
						<a href ="${zipFile}"><span>${zipFileName}</span></a>
					</c:if>
					<form ACTION="requestbydimensionvalue"><input class="button" TYPE="submit" VALUE="Back To Input"></form>
					<br />
				<!-- TopDimensionValuesEnd -->
			</div>

			<div id="sidebar">
				<jsp:include page="../includes/sidebar.jsp" />

			</div>
		</div>
		<!-- content-wrap ends here -->

		<!--footer starts here-->
		<div id="footer">
			<jsp:include page="../includes/footer.jsp" />
		</div>


	</div>
	<!-- wrap ends here -->
</body>
</html>