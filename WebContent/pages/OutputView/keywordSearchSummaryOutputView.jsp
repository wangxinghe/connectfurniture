<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ConnectFurniture - Keyword Search</title>
<link type="text/css" href="css/datepicker.css" rel="stylesheet" />
<link type="text/css" href="css/Style.css" rel="stylesheet" />
<link rel="shortcut icon"
	href="http://connectfurniture.com.au/favicon.ico" />
<script type="text/javascript" src="js/datepicker.js"></script>
<script type="text/javascript" src="js/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="js/jquery.tabify.js"></script>
<script type="text/javascript" src="js/OutputCommon.js"></script> 
<script type="text/javascript">
	$(document).ready(function () {
	  $("#KeywordSearchForm").attr("target","_blank");
	});
	
	function Show(Parameter)
	{
		$(".moredetail" + Parameter).slideDown(200);
	}
	
	function Hide(Parameter)
	{
		$(".moredetail" + Parameter).slideUp(200);
	}
	
	function DetailView(llimit, hlimit)
	{
		$("#llimit").val(llimit);
		$("#hlimit").val(hlimit);
		
		$("#KeywordSearchForm").submit();
	}
</script> 
</head>
<body>
<!-- wrap starts here -->
	<div id="wrap">

		<!--header -->
		<div id="header"></div>

		<!-- menu -->
		<div id="menu">
			<p class="align-right">
				<a class="login" href=<c:out value="${pageName}" />><c:out
						value="${value}" /> </a>
			</p>
			<p class="breadcrumb">
				<a href="home">Home</a>
				<a href="keywordsearch">Keyword Search</a>
				<a href="#" onclick="return false;">Summary</a>
			</p>
		</div>
		<br />
		<!-- content-wrap starts here -->
		<div id="content-wrap">
			<!-- keywordSearch Result starts here --> 
			<div id="main">
				<div class="title_div">
					<h1>
						<c:out value="${reportName}" />
						<br />
						<c:out value="${reportName1}" />
					</h1>
				</div>
				<div class="modify_search">
					<input class="button" type="button" value="Modify Search" onclick="location.href='keywordsearch'">
				</div>
				<div style="clear:both;"></div> 
				<c:if test="${keywordSearchArrayListIndex != null}">
					<h2>${date}</h2>
					<form:form method="POST" commandName="KeywordSearchForm" id="KeywordSearchForm">
						<input type="hidden" name="llimit" id="llimit" />
						<input type="hidden" name="hlimit" id="hlimit" />
						<input type="hidden" name="detail" id="detail" />
						<input type="hidden" name="fromDate" id="fromDate" value="${fromDate}" />
						<input type="hidden" name="toDate" id="toDate" value="${toDate}" />
						<table class="report">
							<tr>
								<!--th width="5%">#</th-->
								<th width="40%">No of Records Returned</th>
								<th width="40%">Number of Keywords</th>
								<th width="20%">&nbsp;</th>
							</tr>
							<c:forEach var="Unit" items="${keywordSearchArrayListIndex}" varStatus="status">
								<tr>
									<!--td><c:out value="${status.index+1}" /></td-->
									<td>
									<c:choose>
   										<c:when test="${Unit.llimit == 0}">[<c:out value="${Unit.llimit}" />]</c:when>
   										<c:when test="${Unit.llimit == Unit.hlimit}">[<c:out value="${Unit.llimit}" />+]</c:when>
   										<c:otherwise>[<c:out value="${Unit.llimit}" /> - <c:out value="${Unit.hlimit}" />]</c:otherwise>
									</c:choose>
									</td>
									
																			
									<td><fmt:formatNumber value="${Unit.records}" /></td>
									<td>
										<input class="button" type="button" value="Details" onclick="DetailView(${Unit.llimit},${Unit.hlimit})" />
									</td>
								</tr>
							</c:forEach>
						</table>
					</form:form>
					<br />
				</c:if>
			</div>
			<!-- keywordSearch Result ends here -->

			<div id="sidebar">
				<jsp:include page="../includes/sidebar.jsp" />

			</div>
		</div>
		<!-- content-wrap ends here -->

		<!--footer starts here-->
		<div id="footer">
			<jsp:include page="../includes/footer.jsp" />
		</div>


	</div>
	<!-- wrap ends here -->
</body>
</html>