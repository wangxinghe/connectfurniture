<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>requestbyhourInputView</title>
<link type="text/css" href="css/datepicker.css" rel="stylesheet" />
<link type="text/css" href="css/Style.css" rel="stylesheet" />
<link rel="shortcut icon" href="http://connectfurniture.com.au/favicon.ico" />
<script type="text/javascript" src="js/datepicker.js"></script>
<script type="text/javascript" src="js/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="js/jquery.tabify.js"></script>
<script type='text/javascript' src="/ConnectFurniture/dwr/engine.js"></script>
<script type='text/javascript' src="/ConnectFurniture/dwr/interface/service.js"></script>
<script type='text/javascript' src='/ConnectFurniture/dwr/util.js'></script>
</head>
<body>
<script type="text/javascript">
$(document).ready(function() {

	//select all the a tag with name equal to modal
	$('a[name=modal]').click(function(e) {
		//Cancel the link behavior
		e.preventDefault();

		//Get the A tag
		var id = $(this).attr('href');

		//Get the screen height and width
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();

		//Set heigth and width to mask to fill up the whole screen
		$('#mask').css({
			'width' : maskWidth,
			'height' : maskHeight
		});

		//transition effect		
		//$('#mask').fadeIn(200);
		$('#mask').fadeTo("slow", 0.2);

		//Get the window height and width
		var winH = $(window).height();
		var winW = $(window).width();

		//Set the popup window to center
		$(id).css('top', (winH - 20) / 2 - $(id).height() / 2);
		$(id).css('left', (winW - 20) / 2 - $(id).width() / 2);

		//transition effect
		//$(id).fadeIn(2000);
		$(id).fadeTo("slow", 0.8);

	});

	// devide into tabs
	$('#menu1').tabify();
	
	//refresh the parameters input fields
	showHideOptions();

});

//If the user login as an Admin, he can set the parameters for the datamining algorithm
function showHideOptions()
{
	var i =$('#login').text();
	
	if(i.trim()=="Logout")
	{
		if($('#q_status').val()=="Depreciation")
		{
			$('.depreciate').show();
			$('.derivertive').hide();
			//$('#depreciationRate').show();
			//$('#depreciationInterval').show();
		}
		if($('#q_status').val()=="Derivative")
		{
			$('.derivertive').show();
			$('.depreciate').hide();
			//$('#a').show();
		}
	}
}

function formsubmit() {
	var popup = document.getElementById("popup");
	clickLink(popup);
}
</script>
<!-- wrap starts here -->
	<div id="wrap">

		<!--header -->
		<div id="header"></div>

		<!-- menu -->
		<div id="menu">
			<p class="align-right">
				<a class="login" id ="login" href=<c:out value="${pageName}" />><c:out
						value="${value}" /> </a>
			</p>
			<p class="breadcrumb">
				<a href="home">Home</a><a href="simpleusertrends">Simple User Trends</a>
			</p>
		</div>
		<br />
		<!-- content-wrap starts here -->
		<div id="content-wrap">

			<div id="main">
				<h1>Simple User Trends</h1>
				<br />
				<h2>
					Ranks values under the specified dimension according to their number of requests
				</h2>
				<br/>
				<form:form method="POST" commandName="SimpleUserTrendsForm">
					<table id="hour">
						<%--
						<!-- 
						<tr>
							<td>From <form:input path="fromDate"
									class="w16em dateformat-d-sl-m-sl-Y show-weeks statusformat-l-cc-sp-d-sp-F-sp-Y  disable-drag"
									id="dp-8" name="dp-8" size="13" />
							</td>
							<td>To <form:input path="toDate"
									class="w16em dateformat-d-sl-m-sl-Y show-weeks statusformat-l-cc-sp-d-sp-F-sp-Y  disable-drag"
									id="dp-9" name="dp-9" size="13" />
							</td>
						</tr>
						<tr>
							<td><form:errors path="fromDate" class="error" />
							</td>
							<td><form:errors path="toDate" class="error" />
							</td>
						</tr>
						-->
						--%>
						<tr>
							<td>Choose A Dimension
							</td>
						</tr>
						<tr>
								<td colspan="2"><p>
										Dimension:<br />
										<form:select path="dimension" items="${dimensions}" multiple="false" />
										<form:select path="mode" items="${mode}" id="q_status"
											multiple="false" onchange="showHideOptions()"/><br />
										Eliminate DimensionValues with average number of requests per day under:<br />
										<form:input path="threshold" value="20"/><br />
										<span class="depreciate" style="display: none">Depreciation Rate:
										<form:input path="depreciationRate" id="depreciationRate" /> ("0" for using preset value as "0.5")
										<br />
										Depreciate Interval in day(s):
										<form:input path="depreciationInterval" id ="depreciationInterval" /> ("0" for using preset value as "7")
										<br /></span>
										<span class="derivertive" style="display: none">A in Logarithm:
										<form:input path="a" id="a" /> ("0" for using preset value as "1.01")
										</span>
									</p>
								</td>
							</tr>	
						<tr>
							<td><input class="button" type="submit" value="View Trends"
								onclick="formsubmit()" /></td>
							<td>
								<input class="button" type="button" value="Reset" onclick="location.href='simpleusertrends?Reset'" />
							</td>
						</tr>
					</table>
				</form:form>

			</div>

			<div id="sidebar">
				<jsp:include page="../includes/sidebar.jsp" />

			</div>

		</div>
		<!-- content-wrap ends here -->

		<!--footer starts here-->
		<div id="footer">
			<jsp:include page="../includes/footer.jsp" />
		</div>


	</div>
	<!-- wrap ends here -->

	<a id="popup" href="#dialog" name="modal"></a>
	<!--Popup starts here-->
	<div id="boxes">

		<div id="dialog" class="window">
			<br /> <br />
			<div style="font-size: 20px; text-align: center; color: white">Generating report, this may take several minutes...</div>
		</div>
		<!-- Mask to cover the whole screen -->
		<div id="mask"></div>
	</div>
	<!--Popup ends here-->
</body>
</html>