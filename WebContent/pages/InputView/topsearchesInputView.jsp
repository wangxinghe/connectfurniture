<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ConnectFurniture - Top Dimension Searches</title>
<link type="text/css" href="css/datepicker.css" rel="stylesheet" />
<link type="text/css" href="css/Style.css" rel="stylesheet" />
<link rel="shortcut icon"
	href="http://connectfurniture.com.au/favicon.ico" />
<script type="text/javascript" src="js/datepicker.js"></script>
<script type="text/javascript" src="js/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="js/InputCommon.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#numRecords option").each( function() {
		$(this).text("Top "+$(this).val());
	});
	
	UpdateSelect();
 });

function UpdateSelect() {
	select_value = document.report_form.selection_1.value;
	var id = 'two';
	var obj2 = '';
	var obj3 = '';
	obj2 =  /*$("#"+id)*/(document.getElementById) ? document.getElementById(id)
		: ((document.all) ? document.all[id]: ((document.layers) ? document.layers[id] : false));
	var id = 'three';
	obj3 = (document.getElementById) ? document.getElementById(id)
			: ((document.all) ? document.all[id]
					: ((document.layers) ? document.layers[id] : false));
	if (select_value == 'One') {
		obj2.style.display = 'none';
		obj3.style.display = 'none';
	} else if (select_value == 'Two') {
		obj2.style.display = 'table-row';//Show Fields
		//obj2.style.display='table-row';
		obj3.style.display = 'none';
	} else if (select_value == 'Three') {
		obj2.style.display = 'table-row';
		obj3.style.display = 'table-row';
	}
}
</script>
</head>
<body>
	<!-- wrap starts here -->
	<div id="wrap">

		<!--header -->
		<div id="header"></div>

		<!-- menu -->
		<div id="menu">
			<p class="align-right">
				<a class="login" href=<c:out value="${pageName}" />><c:out
						value="${value}" /> </a>
			</p>
			<p class="breadcrumb">
				<a href="home">Home</a><a href="topsearches">Top Searches</a>
			</p>
		</div>
		<br />
		<!-- content-wrap starts here -->
		<div id="content-wrap">

			<div id="main">
				<h1>Top Searches</h1>
				<br />
				<h2>Shows the top searches conducted by users</h2>
				<br />
				<form:form method="POST" commandName="topSearchesForm" name="report_form" id="report_form">
					<table id="search1">
						<tr>
							<td>From <form:input path="fromDate"
									class="w16em dateformat-d-sl-m-sl-Y show-weeks statusformat-l-cc-sp-d-sp-F-sp-Y  disable-drag"
									id="dp-8" name="dp-8" size="13" />
							</td>
							<td>To <form:input path="toDate"
									class="w16em dateformat-d-sl-m-sl-Y show-weeks statusformat-l-cc-sp-d-sp-F-sp-Y  disable-drag"
									id="dp-9" name="dp-9" size="13" />
							</td>
						</tr>
						<tr>
							<td><form:errors path="fromDate" class="error" /></td>
							<td><form:errors path="toDate" class="error" /></td>
						</tr>
						<tr>
							<td>Number Of Search Dimensions:</td>
							<td><form:select path="depth" name="selection_1" id="selection_1" onChange="UpdateSelect()">
									<form:options items="${depthIndex}" />
								</form:select></td>
						</tr>
						<tr id="one">
							<td><p title="Specify one or more dimension filters">Dimension 1</p></td>
							<td><form:select path="incDims1">
									<form:options items="${incDims1}" />
								</form:select></td>

						</tr>
						<tr id="two" style="display:none">
							<td><p title="Specify one or more dimension filters">Dimension 2</p></td>
							<td><form:select path="incDims2" onChange="UpdateSelect()">
									<form:options items="${incDims2}" />
								</form:select></td>
						</tr>

						<tr id="three" style="display:none">
							<td><p title="Specify one or more dimension filters">Dimension 3</p></td>
							<td><form:select path="incDims3" onChange="UpdateSelect()">
									<form:options items="${incDims3}" />
								</form:select></td>
						</tr>

						<tr>
							<td>Number of Records To Return:</td>
							<td><form:select path="numRecords">
									<form:options items="${numRecords}" />
								</form:select></td>
						</tr>
						<tr>
							<td>Breakdown Results By:</td>
							<td><form:select path="breakdownBy"
									title="generate separate reports for each">
									<form:options items="${breakdownBy}" />
								</form:select></td>
						</tr>
						<tr>
							<td>Take order of searches into account?</td>
							<td><form:select path="searchOrder"
									title="Enable or disable sequencing">
									<form:options items="${searchOrder}" />
								</form:select></td>
						</tr>
						<tr>
							<td><br /> <input class="button" type="submit"
								value="View Report" onclick="formsubmit()" /><br /> <br /></td>
							<td><br /> <input class="button" type="button"
								value="Reset" onclick="location.href='topsearches?Reset'" /></td>
						</tr>
					</table>
				</form:form>


			</div>

			<div id="sidebar">
				<jsp:include page="../includes/sidebar.jsp" />

			</div>

		</div>
		<!-- content-wrap ends here -->

		<!--footer starts here-->
		<div id="footer">
			<jsp:include page="../includes/footer.jsp" />
		</div>


	</div>
	<!-- wrap ends here -->

	<a id="popup" href="#dialog" name="modal"></a>
	<!--Popup starts here-->
	<div id="boxes">
		<div id="dialog" class="window">
			<br /> <br />
			<div style="font-size: 20px; text-align: center; color: white">Generating
				report, this may take several minutes...</div>
		</div>
		<!-- Mask to cover the whole screen -->
		<div id="mask"></div>
	</div>
	<!--Popup ends here-->


</body>
</html>