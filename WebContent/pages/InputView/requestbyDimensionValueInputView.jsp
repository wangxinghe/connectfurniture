<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>requestbyhourInputView</title>
<link type="text/css" href="css/datepicker.css" rel="stylesheet" />
<link type="text/css" href="css/Style.css" rel="stylesheet" />
<link rel="shortcut icon" href="http://connectfurniture.com.au/favicon.ico" />
<script type="text/javascript" src="js/datepicker.js"></script>
<script type="text/javascript" src="js/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="js/jquery.tabify.js"></script>
<script type='text/javascript' src="/ConnectFurniture/dwr/engine.js"></script>
<script type='text/javascript' src="/ConnectFurniture/dwr/interface/service.js"></script>
<script type='text/javascript' src='/ConnectFurniture/dwr/util.js'></script>
</head>
<body>
<script type="text/javascript">
$(document).ready(function() {

	//select all the a tag with name equal to modal
	$('a[name=modal]').click(function(e) {
		//Cancel the link behavior
		e.preventDefault();

		//Get the A tag
		var id = $(this).attr('href');

		//Get the screen height and width
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();

		//Set heigth and width to mask to fill up the whole screen
		$('#mask').css({
			'width' : maskWidth,
			'height' : maskHeight
		});

		//transition effect		
		//$('#mask').fadeIn(200);
		$('#mask').fadeTo("slow", 0.2);

		//Get the window height and width
		var winH = $(window).height();
		var winW = $(window).width();

		//Set the popup window to center
		$(id).css('top', (winH - 20) / 2 - $(id).height() / 2);
		$(id).css('left', (winW - 20) / 2 - $(id).width() / 2);

		//transition effect
		//$(id).fadeIn(2000);
		$(id).fadeTo("slow", 0.8);

	});

	// devide into tabs
	$('#menu1').tabify();
	
	//refresh zip file name text box default value
	changeTextBoxValue();
	
	//hide or show sub options
	subOptions();

});
//populate dropdown lists
function changeSelect(value) {
	//alert("changeSelect()...");
	service.getDimensionValues(value, fillSelect);
	function fillSelect(data) {
		if (typeof window['DWRUtil'] == 'undefined')
			window.DWRUtil = dwr.util;

		var q_status = document.getElementById("q_status");
		DWRUtil.removeAllOptions(q_status);
		DWRUtil.addOptions(q_status, [ "All" ]);
		DWRUtil.addOptions(q_status, data);

	}
	changeTextBoxValue();
}

function changeTextBoxValue()
{
	$('#zipName').val($('#dims').val());	
}

function subOptions()
{
	if($('#q_status').val()=='All')
	{
		$('#panel2').show();
		$('#panel1').hide();
	}
	else
	{
		$('#panel2').hide();
		$('#panel1').show();
	}
}

function formsubmit() {
	var popup = document.getElementById("popup");
	clickLink(popup);
}
function clickLink(link) {
	var cancelled = false;
	if (document.createEvent) {
		var event = document.createEvent("MouseEvents");
		event.initMouseEvent("click", true, true, window, 0, 0, 0, 0,
				0, false, false, false, false, 0, null);
		cancelled = !link.dispatchEvent(event);
	} else if (link.fireEvent) {
		cancelled = !link.fireEvent("onclick");
	}
	if (!cancelled) {
		window.location = link.href;
	}
}
</script>
	<!-- wrap starts here -->
	<div id="wrap">

		<!--header -->
		<div id="header"></div>

		<!-- menu -->
		<div id="menu">
			<p class="align-right">
				<a class="login" href=<c:out value="${pageName}" />><c:out
						value="${value}" /> </a>
			</p>
			<p class="breadcrumb">
				<a href="home">Home</a><a href="requestbydimensionvalue">Request By Dimension Value</a>
			</p>
		</div>
		<br />
		<!-- content-wrap starts here -->
		<div id="content-wrap">

			<div id="main">
				<h1>Requests By Dimension Value</h1>
				<br />
				<h2>Shows the number of requests over a period of time of a specific Dimension Value</h2>
				<br/>
				<form:form method="POST" commandName="RequestByDimensionValueForm">
					<table id="hour">
						<tr>
							<td>From <form:input path="fromDate"
									class="w16em dateformat-d-sl-m-sl-Y show-weeks statusformat-l-cc-sp-d-sp-F-sp-Y  disable-drag"
									id="dp-8" name="dp-8" size="13" />
							</td>
							<td>To <form:input path="toDate"
									class="w16em dateformat-d-sl-m-sl-Y show-weeks statusformat-l-cc-sp-d-sp-F-sp-Y  disable-drag"
									id="dp-9" name="dp-9" size="13" />
							</td>
						</tr>
						<tr>
							<td><form:errors path="fromDate" class="error" />
							</td>
							<td><form:errors path="toDate" class="error" />
							</td>
						</tr>
						<tr>
							<td>Choose Dimension & Value
							</td>
						</tr>
						<tr>
							<td colspan="2"><p>
										Dimension-Value:<br />
										<form:select path="incDims" items="${dims}" id="dims"
											onchange="changeSelect(this.value)" multiple="false" />
										<form:select path="incVals" items="${vals}" id="q_status"
											onchange="subOptions()" multiple="false"/><br/>
										<span>*select All in the value list to export graph of every value</span>
									</p>
							</td>
						</tr>
						<tr>
							<td id="panel1">
								Rank By:
								<form:select path="breakdownby" items="${breakdownby}" ></form:select>
							</td>
						</tr>
						<tr>
							<td id="panel2">
								Enter the name for the exported zip file:<br/>
								<form:input type="text" path="zipFileName" id="zipName"/>
							</td>
						</tr>	
						<tr>
							<td><input class="button" type="submit" value="View Report"
								onclick="formsubmit()" /></td>
							<td>
								<input class="button" type="button" value="Reset" onclick="location.href='requestbydimensionvalue?Reset'" />
							</td>
						</tr>
					</table>
				</form:form>

			</div>

			<div id="sidebar">
				<jsp:include page="../includes/sidebar.jsp" />

			</div>

		</div>
		<!-- content-wrap ends here -->

		<!--footer starts here-->
		<div id="footer">
			<jsp:include page="../includes/footer.jsp" />
		</div>


	</div>
	<!-- wrap ends here -->

	<a id="popup" href="#dialog" name="modal"></a>
	<!--Popup starts here-->
	<div id="boxes">

		<div id="dialog" class="window">
			<br /> <br />
			<div style="font-size: 20px; text-align: center; color: white">Generating report, this may take several minutes...</div>
		</div>
		<!-- Mask to cover the whole screen -->
		<div id="mask"></div>
	</div>
	<!--Popup ends here-->
</body>
</html>