<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ConnectFurniture - Top Dimension Value Searches</title>
<link type="text/css" href="css/datepicker.css" rel="stylesheet" />
<link type="text/css" href="css/Style.css" rel="stylesheet" />
<link rel="shortcut icon" href="http://connectfurniture.com.au/favicon.ico" />
<script type="text/javascript" src="js/datepicker.js"></script>
<script type="text/javascript" src="js/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="js/jquery.tabify.js"></script>
<script type='text/javascript' src="/ConnectFurniture/dwr/engine.js"></script>
<script type='text/javascript' src="/ConnectFurniture/dwr/interface/service.js"></script>
<script type='text/javascript' src='/ConnectFurniture/dwr/util.js'></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#numRecords option").each( function() {
		$(this).text("Top "+$(this).val());
	});
 });
</script>
</head>
<body>
	<script type="text/javascript">
		$(document).ready(function() {
			//select all the a tag with name equal to modal
			$('a[name=modal]').click(function(e) {
				//Cancel the link behavior
				e.preventDefault();

				//Get the A tag
				var id = $(this).attr('href');

				//Get the screen height and width
				var maskHeight = $(document).height();
				var maskWidth = $(window).width();

				//Set heigth and width to mask to fill up the whole screen
				$('#mask').css({
					'width' : maskWidth,
					'height' : maskHeight
				});

				//transition effect		
				//$('#mask').fadeIn(200);
				$('#mask').fadeTo("slow", 0.2);

				//Get the window height and width
				var winH = $(window).height();
				var winW = $(window).width();

				//Set the popup window to center
				$(id).css('top', (winH - 20) / 2 - $(id).height() / 2);
				$(id).css('left', (winW - 20) / 2 - $(id).width() / 2);

				//transition effect
				//$(id).fadeIn(2000);
				$(id).fadeTo("slow", 0.8);

			});

			// devide into tabs
			$('#menu1').tabify();
			
		});

		function formsubmit() {
			var popup = document.getElementById("popup");
			clickLink(popup);
		}
		function clickLink(link) {
			var cancelled = false;
			if (document.createEvent) {
				var event = document.createEvent("MouseEvents");
				event.initMouseEvent("click", true, true, window, 0, 0, 0, 0,
						0, false, false, false, false, 0, null);
				cancelled = !link.dispatchEvent(event);
			} else if (link.fireEvent) {
				cancelled = !link.fireEvent("onclick");
			}
			if (!cancelled) {
				window.location = link.href;
			}
		}
		//populate dropdown lists
		function changeSelect(value) {
			//alert("changeSelect()...");
			service.getDimensionValues(value, fillSelect);
			function fillSelect(data) {
				if (typeof window['DWRUtil'] == 'undefined')
					window.DWRUtil = dwr.util;

				var q_status = document.getElementById("q_status");
				DWRUtil.removeAllOptions(q_status);
				DWRUtil.addOptions(q_status, [ "All" ]);
				DWRUtil.addOptions(q_status, data);

			}
		}

		function changeSelect1(value) {
			service.getDimensionValues(value, fillSelect);
			function fillSelect(data) {
				if (typeof window['DWRUtil'] == 'undefined')
					window.DWRUtil = dwr.util;

				var q_status = document.getElementById("q_status1");
				DWRUtil.removeAllOptions(q_status);
				DWRUtil.addOptions(q_status, [ "All" ]);
				DWRUtil.addOptions(q_status, data);

			}
		}

		function changeSelect3(value) {
			service.getDimensionValues(value, fillSelect);
			function fillSelect(data) {
				if (typeof window['DWRUtil'] == 'undefined')
					window.DWRUtil = dwr.util;

				var q_status = document.getElementById("q_status3");
				DWRUtil.removeAllOptions(q_status);
				DWRUtil.addOptions(q_status, [ "--Select--" ]);
				DWRUtil.addOptions(q_status, data);

			}
		}

		function changeSelect4(value) {
			service.getDimensionValues(value, fillSelect);
			function fillSelect(data) {
				if (typeof window['DWRUtil'] == 'undefined')
					window.DWRUtil = dwr.util;

				var q_status = document.getElementById("q_status4");
				DWRUtil.removeAllOptions(q_status);
				DWRUtil.addOptions(q_status, [ "--Select--" ]);
				DWRUtil.addOptions(q_status, data);

			}
		}
		
		function notifyUsers()
		{
			if($('#depth option:selected').text()=="Two")
			{
			}
			if($('#depth option:selected').text()=="Three")
			{
				alert("Number of Search Dimension: Three is chosen.\nThis report may run for an extremly long time.\nOne day approx 7 mins.");	
			}
		}
		
	</script>
	<!-- wrap starts here -->
	<div id="wrap">

		<!--header -->
		<div id="header"></div>

		<!-- menu -->
		<div id="menu">
			<p class="align-right">
				<a class="login" id='login' href=<c:out value="${pageName}" />><c:out
						value="${value}" /> </a>
			</p>
			<p class="breadcrumb">
				<a href="home">Home</a><a href="topdimensionvalues">Top Search Values</a>
			</p>
		</div>
		<br />
		<!-- content-wrap starts here -->
		<div id="content-wrap">

			<div id="main">
				<h1>Top Search Values</h1>
				<br />
				<h2>Shows the top search values including trends</h2>
				<br />
				<ul id="menu1" class="menu1">
					<li class="active"><a href="#report">Report</a></li>
					<li><a href="#trend">Trend</a></li>
				</ul>
				<div id="report" class="content2">
					<form:form method="POST" commandName="topDimensionValuesForm">
						<table id="dimvalssearch">
							<tr>
								<td>From <form:input path="fromDate"
										class="w16em dateformat-d-sl-m-sl-Y show-weeks statusformat-l-cc-sp-d-sp-F-sp-Y  disable-drag"
										id="dp-8" name="dp-8" size="13" /></td>
								<td>To <form:input path="toDate"
										class="w16em dateformat-d-sl-m-sl-Y show-weeks statusformat-l-cc-sp-d-sp-F-sp-Y  disable-drag"
										id="dp-9" name="dp-9" size="13" /></td>
							</tr>
							<tr>
								<td><form:errors path="fromDate" class="error" /></td>
								<td><form:errors path="toDate" class="error" /></td>
							</tr>
							<tr>
								<td>Number Of Search Dimensions:</td>
								<td><form:select path="depth" id="depth" onchange="notifyUsers()">
										<form:options items="${depth}" />
									</form:select></td>
							</tr>
							<tr>
								<td><h3 >
										Choose Dimension-Values To Include:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h3>
								</td>
							</tr>
							<tr>
								<td colspan="2"><p>
										Dimension-Value Pair #1:<br />
										<form:select path="incDims" items="${incDims}"
											onchange="changeSelect(this.value)" multiple="false" />
										<form:select path="incVals" items="${incVals}" id="q_status"
											multiple="false"/>
									</p>
								</td>
							</tr>
							<tr>
								<td colspan="2"><p>
										Dimension-Value Pair #2:<br />
										<form:select path="incDims1" items="${incDims1}"
											onchange="changeSelect1(this.value)" multiple="false" />
										<form:select path="incVals1" items="${incVals1}"
											id="q_status1" multiple="false" />
									</p>
								</td>
							</tr>

							<tr>
								<td>Number Of Records to Return:</td>
								<td><form:select path="numRecords">
										<form:options items="${numRecords}" />
									</form:select></td>
							</tr>
							<tr>
								<td>Breakdown Results By:</td>
								<td><form:select path="breakdownBy">
										<form:options items="${breakdownBy}" />
									</form:select></td>
							</tr>
							<tr>
								<td><br /> <form:input class="button"
										type="submit" path="submitButton" value="View Report"
										onclick="formsubmit()" /><br /> <br />
								</td>
								<td><br />
									<input class="button" type="button" value="Reset" onclick="location.href='topdimensionvalues?Reset'" />
								</td>
							</tr>
						</table>
					</form:form>
				</div>

				<div id="trend" class="content2">
					<form:form method="POST" commandName="topDimensionValuesForm">
						<table id="searchTrend">
							<tr>
								<td>From <form:input path="fromDateOfTrend"
										class="w16em dateformat-d-sl-m-sl-Y show-weeks statusformat-l-cc-sp-d-sp-F-sp-Y  disable-drag"
										id="dp-10" name="dp-10" size="13" /></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To
									<form:input path="toDateOfTrend"
										class="w16em dateformat-d-sl-m-sl-Y show-weeks statusformat-l-cc-sp-d-sp-F-sp-Y  disable-drag"
										id="dp-11" name="dp-11" size="13" /></td>
							</tr>
							<tr>
								<td><form:errors path="fromDateOfTrend" class="error" /></td>
								<td><form:errors path="toDateOfTrend" class="error" /></td>
							</tr>
							<tr>
								<td><h3 >
										Choose Dimension-Values To Include:</h3>
								</td>
							</tr>
							<tr>
								<td colspan="2">Dimension-Value Pair
									#1:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
								<form:select path="incDimsOfTrend" items="${incDimsOfTrend}"
										onchange="changeSelect3(this.value)" multiple="false" />
									<form:select path="incValsOfTrend" items="${incValsOfTrend}"
										id="q_status3" multiple="false" />
								</td>

							</tr>
							<tr>
								<td><form:errors path="incDimsOfTrend" class="error" /></td>
								<td><form:errors path="incValsOfTrend" class="error" /></td>
							</tr>
							<tr>
								<td colspan="2">Dimension-Value Pair #2:<br />
								<form:select path="incDims1OfTrend" items="${incDims1OfTrend}"
										onchange="changeSelect4(this.value)" multiple="false" />
									<form:select path="incVals1OfTrend" items="${incVals1OfTrend}"
										id="q_status4" multiple="false" />
								</td>
							</tr>
							<tr>
								<td><form:errors path="incDims1OfTrend" class="error" /></td>
								<td><form:errors path="incVals1OfTrend" class="error" /></td>
							</tr>
							<tr>
								<td><br /> <form:input class="button"
										type="submit" path="submitButton" value="View Trend"
										onclick="formsubmit()" /><br /> <br />
								</td>
								<td><br />
									<input class="button" type="button" value="Reset" onclick="location.href='topdimensionvalues?Reset'" />
								</td>
							</tr>
							<tr>
								<td><form:errors path="submitButton" class="error" /></td>
							</tr>
						</table>
					</form:form>
				</div>

			</div>

			<div id="sidebar">
				<jsp:include page="../includes/sidebar.jsp" />

			</div>

		</div>
		<!-- content-wrap ends here -->

		<!--footer starts here-->
		<div id="footer">
			<jsp:include page="../includes/footer.jsp" />
		</div>


	</div>
	<!-- wrap ends here -->

	<a id="popup" href="#dialog" name="modal"></a>
	<!--Popup starts here-->
	<div id="boxes">
		<div id="dialog" class="window">
			<br /> <br />
			<div style="font-size: 20px; text-align: center; color: white">Generating
				report, this may take several minutes...</div>
		</div>
		<!-- Mask to cover the whole screen -->
		<div id="mask"></div>
	</div>
	<!--Popup ends here-->
</body>
</html>