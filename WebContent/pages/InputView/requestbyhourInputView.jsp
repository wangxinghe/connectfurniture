<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>requestbyhourInputView</title>
<link type="text/css" href="css/datepicker.css" rel="stylesheet" />
<link type="text/css" href="css/Style.css" rel="stylesheet" />
<link rel="shortcut icon" href="http://connectfurniture.com.au/favicon.ico" />
<script type="text/javascript" src="js/datepicker.js"></script>
<script type="text/javascript" src="js/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="js/InputCommon.js"></script>
</head>
<body>
	<!-- wrap starts here -->
	<div id="wrap">

		<!--header -->
		<div id="header"></div>

		<!-- menu -->
		<div id="menu">
			<p class="align-right">
				<a class="login" href=<c:out value="${pageName}" />><c:out
						value="${value}" /> </a>
			</p>
			<p class="breadcrumb">
				<a href="home">Home</a><a href="requestbyhour">Request By Hour</a>
			</p>
		</div>
		<br />
		<!-- content-wrap starts here -->
		<div id="content-wrap">

			<div id="main">
				<h1>Request By Hour</h1>
				<br />
				<form:form method="POST" commandName="requestbyhourForm">
					<table id="hour">
						<tr>
							<td>From <form:input path="fromDate"
									class="w16em dateformat-d-sl-m-sl-Y show-weeks statusformat-l-cc-sp-d-sp-F-sp-Y  disable-drag"
									id="dp-8" name="dp-8" size="13" />
							</td>
							<td>To <form:input path="toDate"
									class="w16em dateformat-d-sl-m-sl-Y show-weeks statusformat-l-cc-sp-d-sp-F-sp-Y  disable-drag"
									id="dp-9" name="dp-9" size="13" />
							</td>
						</tr>
						<tr>
							<td><form:errors path="fromDate" class="error" />
							</td>
							<td><form:errors path="toDate" class="error" />
							</td>
						</tr>
						<tr>
							<td><p class="flip">Show/Hide More Options:</p></td>
						</tr>
						<tr>
							<td><p class="panel">Compare With:</p>
							</td>
						</tr>
						<tr>
							<td><form:errors path="compareFromDate" class="error" />
							</td>
							<td><form:errors path="compareToDate" class="error" /></td>
						</tr>
						<tr>
							<td><p class="panel">
									From
									<form:input path="compareFromDate"
										class="w16em dateformat-d-sl-m-sl-Y show-weeks statusformat-l-cc-sp-d-sp-F-sp-Y  disable-drag"
										id="dp-10" name="dp-10" size="13" />
								</p>
							</td>
							<td><p class="panel">
									To
									<form:input path="compareToDate"
										class="w16em dateformat-d-sl-m-sl-Y show-weeks statusformat-l-cc-sp-d-sp-F-sp-Y  disable-drag"
										id="dp-11" name="dp-11" size="13" />
								</p>
							</td>
						</tr>
						<tr>
							<td><input class="button" type="submit" value="View Report"
								onclick="formsubmit()" /></td>
							<td>
								<input class="button" type="button" value="Reset" onclick="location.href='requestbyhour?Reset'" />
							</td>
						</tr>
					</table>
				</form:form>

			</div>

			<div id="sidebar">
				<jsp:include page="../includes/sidebar.jsp" />

			</div>

		</div>
		<!-- content-wrap ends here -->

		<!--footer starts here-->
		<div id="footer">
			<jsp:include page="../includes/footer.jsp" />
		</div>


	</div>
	<!-- wrap ends here -->

	<a id="popup" href="#dialog" name="modal"></a>
	<!--Popup starts here-->
	<div id="boxes">

		<div id="dialog" class="window">
			<br /> <br />
			<div style="font-size: 20px; text-align: center; color: white">Generating report, this may take several minutes...</div>
		</div>
		<!-- Mask to cover the whole screen -->
		<div id="mask"></div>
	</div>
	<!--Popup ends here-->
</body>
</html>