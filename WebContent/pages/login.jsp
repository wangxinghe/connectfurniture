<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
<link type="text/css" href="css/datepicker.css" rel="stylesheet" />
<link type="text/css" href="css/Style.css" rel="stylesheet" />
<link rel="shortcut icon" href="http://connectfurniture.com.au/favicon.ico" />
<script type="text/javascript" src="js/datepicker.js"></script>
<script type="text/javascript" src="js/jquery-1.6.4.min.js"></script>
</head>
<body>
	<!-- wrap starts here -->
	<div id="wrap">

		<!--header -->
		<div id="header"></div>

		<!-- menu -->
		<div id="menu">
			<p class="align-right">
				<a class="login" href=<c:out value="${pageName}" />><c:out
						value="${value}" />
				</a>
			</p>

			<p class="breadcrumb">
				<a href="home">Home</a><a href="login">Login</a>
			</p>
		</div>
		<br/>
		<!-- content-wrap starts here -->
		<div id="content-wrap">

			<div id="main">
			<h1>Administrator Login</h1>
				<br />
				<p>Please enter your username and password:</p>
				<form:form method="POST" commandName="loginForm">
					<table id="login">
						<tr>
							<td><label>Username</label></td>
						</tr>	
						<tr>
							<td><form:input path="username" size="40" /></td>
							<td><form:errors path="username" class="error" /></td>
						</tr>
						<tr>
							<td><label>Password</label></td>
						</tr>
						<tr>
							<td><form:input path="password" size="40" type="password" />
							</td>
							<td><form:errors path="password" class="error" /></td>
						</tr>
						<tr>
							<td><input class="button" type="submit" value="Login" /><br />
								<br />
							</td>
						</tr>
					</table>
				</form:form>
			</div>
			
			<div id="sidebar">
				<jsp:include page="includes/sidebar.jsp" />
				
			</div>

		</div>
		<!-- content-wrap ends here -->
		<!--footer starts here-->
		<div id="footer">
			<jsp:include page="includes/footer.jsp" />
		</div>


	</div>
	<!-- wrap ends here -->
</body>
</html>