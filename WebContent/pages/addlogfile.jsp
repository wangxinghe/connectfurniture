<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin - Add Log File</title>
<link type="text/css" href="css/datepicker.css" rel="stylesheet" />
<link type="text/css" href="css/Style.css" rel="stylesheet" />
<link rel="shortcut icon" href="http://connectfurniture.com.au/favicon.ico" />
<script type="text/javascript" src="js/datepicker.js"></script>
<script type="text/javascript" src="js/jquery-1.6.4.min.js"></script>
</head>
<body>
	<script type="text/javascript">
		$(document).ready(function() {

			//select all the a tag with name equal to modal
			$('a[name=modal]').click(function(e) {
				//Cancel the link behavior
				e.preventDefault();

				//Get the A tag
				var id = $(this).attr('href');

				//Get the screen height and width
				var maskHeight = $(document).height();
				var maskWidth = $(window).width();

				//Set heigth and width to mask to fill up the whole screen
				$('#mask').css({
					'width' : maskWidth,
					'height' : maskHeight
				});

				//transition effect		
				//$('#mask').fadeIn(200);
				$('#mask').fadeTo("slow", 0.2);

				//Get the window height and width
				var winH = $(window).height();
				var winW = $(window).width();

				//Set the popup window to center
				$(id).css('top', (winH-20) / 2 - $(id).height() / 2);
				$(id).css('left', (winW-20) / 2 - $(id).width() / 2);

				//transition effect
				//$(id).fadeIn(2000);
				$(id).fadeTo("slow", 0.8);

			});
		});
		function formsubmit() {
			if(document.getElementById("uploadButton").getAttribute("value")=="Upload"){
				var popup = document.getElementById("popup");
				clickLink(popup);
			}
		}
		function clickLink(link) {
			var cancelled = false;
			if (document.createEvent) {
				var event = document.createEvent("MouseEvents");
				event.initMouseEvent("click", true, true, window, 0, 0, 0, 0,
						0, false, false, false, false, 0, null);
				cancelled = !link.dispatchEvent(event);
			} else if (link.fireEvent) {
				cancelled = !link.fireEvent("onclick");
			}
			if (!cancelled) {
				window.location = link.href;
			}
		}
	</script>
	<!-- wrap starts here -->
	<div id="wrap">

		<!--header -->
		<div id="header"></div>

		<!-- menu -->
		<div id="menu">
			<p class="align-right">
				<a class="login" href=<c:out value="${pageName}" />><c:out
						value="${value}" /> </a>
			</p>

			<p class="breadcrumb">
				<a href="home">Home</a><a href="addlogfile">Add Log File</a>
			</p>
		</div>
		<br/>
		<!-- content-wrap starts here -->
		<div id="content-wrap">

			<div id="main">
			<h1>Add Log Files</h1>
				<br />
				<p>Please specify the folder containing the log files:</p>
				<form:form method="POST" commandName="uploadForm">
					<table id="add">
						<tr>
							<td><form:input path="folderPath" size="40" />
							<form:input id="uploadButton" class="button" path="buttonName" type="submit" onclick="formsubmit()" />
							</td>
						</tr>
					</table>
				</form:form>
			</div>

			<div id="sidebar">
				<jsp:include page="includes/sidebar.jsp" />

			</div>



		</div>
		<!-- content-wrap ends here -->
		<!--footer starts here-->
		<div id="footer">
			<jsp:include page="includes/footer.jsp" />
		</div>


	</div>
	<!-- wrap ends here -->
	
	<a id="popup" href="#dialog" name="modal"></a>
	<!--Popup starts here-->
	<div id="boxes">

		<div id="dialog" class="window">
			<br />
			<br />
			<div style="font-size: 20px; text-align: center; color: white">Uploading,
				please wait...
				</div>
		</div>
		<!-- Mask to cover the whole screen -->
		<div id="mask"></div>
	</div>
	<!--Popup ends here-->
</body>
</html>