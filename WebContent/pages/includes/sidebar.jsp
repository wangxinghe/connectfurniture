<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
	<h1>Reports</h1>
	<div class="left-box">
		<ul class="sidemenu">
			<li>
				<a href="topsearches" title="Depicting common usage paths users tend to follow when using the website">Top Searches </a>
			</li>
			<li>
				<a href="topdimensionvalues" title="Analysing popular values selected by users for different options and viewing weekly trends for specific values or pairs of values">Top Search Values</a>
			</li>
			<li>
				<a href="topkeywordssearches" title="Exposing the popular and non-popular words being searches on the website">Top Keywords Searches</a>
			</li>
			<li>
				<a href="keywordsearch" title="Displaying all related results for keyword searching">Keyword Searches</a>
			</li>
			<li>
				<a href="requestbytype" title="Displaying how many users prefer using the navigation toolbar vs searching for keywords">Requests By Type</a>
			</li>
			<li>
				<a href="requestbyuser" title="Display different types of request information with their number of users">Requests By User</a>
			</li>
			<li>
				<a href="requestby" title="The new RequestBy search function">Requests By</a>
			</li>
			<li>
				<a href="sessionbyminutes" title="Display All Sessions Group By Their Duration Time">Sessions By Minutes</a>
			</li>
			<li>
				<a href="sessionby" title="Showing how many search ephisodes the user has had based on the time of the day">Sessions By</a>
			</li>
			<li>
				<a href="correctterms" title="Displaying how many requests with  terms have been corrected">Auto-Corrected Terms</a>
			</li>
			<li>
				<a href="requestbydimensionvalue" title="Request by Dimension Value">Request by Dimension Value</a>
			</li>
			<li>
				<a href="simpleusertrends" title="Simple User Trends">Simple User Trends</a>
			</li>
		</ul>
	</div>
	<br />

	<c:if test="${common.title!=null}">
		<h1>
			<c:out value="${common.title}" />
		</h1>
		<div class="left-box">
			<ul class="sidemenu">
				<li><a href=<c:out value="${common.addPageName}" />><c:out
							value="${common.addValue}" /> </a>
				</li>
				<li><a href=<c:out value="${common.removePageName}" />><c:out
							value="${common.removeValue}" /> </a>
				</li>
			</ul>
		</div>
		<br />
	</c:if>

	<c:if test="${reportName!=null && reportName!='Quick Guide' && csvReport && pdfReport && xlsReport}">
		<h1>Save Reports</h1>
		<a id="savereport" class="hidden"></a>
		<div class="left-box">
			<div class="thumbnails">
		<!-- start entry-->
		<c:if test="${pdfReport}">
			<div class="thumbnailimage">
				<div class="thumb_container">
					<div class="large_thumb">
						<img id="pages/reports/${savedReportName}.pdf" src="images/pdf.jpg" class="large_thumb_image" alt="pdf" />
						<div class="large_thumb_border"></div>
						<div class="large_thumb_shine"></div>
					</div>
				</div>
			</div>
		</c:if>
		
		<% %>
		<!--end entry-->

		<!-- start entry-->
		<c:if test="${csvReport}">
			<div class="thumbnailimage">
				<div class="thumb_container">
					<div class="large_thumb">
						<img id="pages/reports/${savedReportName}.csv" src="images/csv.jpg" class="large_thumb_image" alt="csv" />
						<div class="large_thumb_border"></div>
						<div class="large_thumb_shine"></div>
					</div>
				</div>
			</div>
		</c:if>
		<!--end entry-->

		<!-- start entry-->
		<c:if test="${xlsReport}">
			<div class="thumbnailimage">
				<div class="thumb_container">
					<div class="large_thumb">
						<img id="pages/reports/${savedReportName}.xls" src="images/excel.jpg" class="large_thumb_image" alt="xls" />
						<div class="large_thumb_border"></div>
						<div class="large_thumb_shine"></div>
					</div>
				</div>
		</div>
		</c:if>
		<!--end entry-->
	</div>
		</div>
		<br />
	</c:if>


	

</body>
</html>