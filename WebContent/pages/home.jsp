<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>
<link type="text/css" href="css/datepicker.css" rel="stylesheet" />
<link type="text/css" href="css/Style.css" rel="stylesheet" />
<link rel="shortcut icon"
	href="http://connectfurniture.com.au/favicon.ico" />
<script type="text/javascript" src="js/datepicker.js"></script>
<script type="text/javascript" src="js/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="js/jquery.tabify.js"></script>
<script type="text/javascript" src="js/OutputCommon.js"></script> 
</head>
<body>
	<!-- wrap starts here -->
	<div id="wrap">

		<!--header -->
		<div id="header"></div>

		<!-- menu -->
		<div id="menu">
			<p class="align-right">
				<a class="login" href=<c:out value="${pageName}" />><c:out
						value="${value}" /> </a>
			</p>
			<p class="breadcrumb">
				<a href="home">Home</a>
			</p>
		</div>
		<br />
		<!-- content-wrap starts here -->
		<div id="content-wrap">

			<div id="main">
				<h1>
					Quick Guide
				</h1>
				<h3>
					Welcome to the ConnectFurniture user trends application
				</h3>
				<p>
					Please select a report type from the panel on the left. Below is a brief description of the available reports.
				</p>
				<c:if test="${hr!=null}">
				<br>
				</c:if>

				<h2>
					Description Of Reports
				</h2>
				<c:if test="${hr!=null}">
				<hr>
				</c:if>
				<h4>
					Top Searches 
				</h4>
				<p>
					Shows the top searches conducted by users
				</p>
				<h4>
					Top Search Values
				</h4>
				<p>
					Shows the top search values including trends
				</p>
				<h4>
					Top Keyword Searches
				</h4>
				<p>
					Identifies the popular and non-popular words being searches on the website
				</p>

				<h4>
					Keyword Searches
				</h4>
				<p>
					Shows details of all keyword searches, including searches returning 0 records
				</p>
				<h4>
					Requests By Type
				</h4>
				<p>
					Shows how many users prefer using the navigation toolbar vs searching for keywords
				</p>
				<h4>
					Request By User
				</h4>
				<p>
					Shows the numbers of the requests conducted by the user
				</p>
				<h4>
					Request By
				</h4>
				<p>
					Shows the numbers  of search requests broken down by hour of day, month of year and day of year
				</p>
				<h4>
					Session By Minutes
				</h4>
				<p>
					Shows how long users are spending conducting searches
				</p>
				<h4>
					Session By
				</h4>
				<p>
					Shows the numbers of user sessions, broken down by hour of day, month of year and day of year
				</p>
				<h4>
					Auto-Corrected Terms
				</h4>
				<p>
					Shows which keywords have been auto-corrected
				</p>
				<h5>
					Requests By Dimension Value
				</h4>
				<p>
					Shows the number of requests over a period of time of a specific Dimension Value
				</p>
				<h4>
					Simple User Trends
				</h4>
				<p>
					Ranks values under the specified dimension according to their number of requests
				</p>
				<br/>
			</div>

			<div id="sidebar">
				<jsp:include page="includes/sidebar.jsp" />

			</div>
		</div>
		<!-- content-wrap ends here -->

		<!--footer starts here-->
		<div id="footer">
			<jsp:include page="includes/footer.jsp" />
		</div>


	</div>
	<!-- wrap ends here -->
</body>
</html>